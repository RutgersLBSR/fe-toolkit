# -*- coding: utf-8 -*-
import os
import subprocess
import sys


ROOT_DIR = os.path.join(os.path.dirname(__file__), "bin")

def _program(name, args):
    return subprocess.call([os.path.join(ROOT_DIR, name)] + args, close_fds=False)


def fetkutils_tischedule():
    raise SystemExit(_program('fetkutils-tischedule.py', sys.argv[1:]))


def fetkutils_nma_kie():
    raise SystemExit(_program('fetkutils-nma-kie.py', sys.argv[1:]))


def fetkutils_nma_MergeHessianNetCDFs():
    raise SystemExit(_program('fetkutils-nma-MergeHessianNetCDFs.py', sys.argv[1:]))


def fetkutils_nma_thermochem():
    raise SystemExit(_program('fetkutils-nma-thermochem.py', sys.argv[1:]))


def fetkutils_nma_approxfreqratio():
    raise SystemExit(_program('fetkutils-nma-approxfreqratio.py', sys.argv[1:]))

def fetkutils_ipi_setupfromamber():
    raise SystemExit(_program('fetkutils-ipi-setupfromamber.py', sys.argv[1:]))


def fetkutils_ipi_tdfepkie():
    raise SystemExit(_program('fetkutils-ipi-tdfepkie.py', sys.argv[1:]))


def fetkutils_ipi_xcrd2dumpave():
    raise SystemExit(_program('fetkutils-ipi-xcrd2dumpave.py', sys.argv[1:]))

def fetkutils_amberti2img():
    raise SystemExit(_program('fetkutils-amberti2img.py', sys.argv[1:]))


