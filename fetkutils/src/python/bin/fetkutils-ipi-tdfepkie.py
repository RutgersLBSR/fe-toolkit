#!/usr/bin/env python3

#import numpy as np
#import scipy.integrate as integrate


k_au = 3.16681534222374e-06;
au_per_kcal = 1.59360145069066e-03;
k_kcal = (k_au / au_per_kcal);
#beta = 1. / ( 298. * k_kcal )


def write_Tcv_plot(fname,entry):
    fh = open(fname,"w")
    ms = entry.get_masses()
    for i in range(len(ms)):
        m = ms[i]
        t = entry.Tcvs[i]
        d = 1.96 * entry.dTcvs[i]
        fh.write("%20.10e %20.10e %20.10e\n"%(m,t,d))


def StatIneff(arr):
    """Computes the statistical inefficiency of a timeseries"""
    
    import numpy as np    
    A = np.copy( arr )
    N = A.shape[0]
    g=1.0
    if N > 1:
        mu = np.mean(A)
        dA = A.astype(np.float64) - mu
        s2 = np.mean(dA**2)
        if s2 > 0:
            mintime=3
            t = 1
            inc = 1
            while(t<N-1):
                C = np.sum( dA[0:(N - t)] * dA[t:N] ) / (float(N - t) * s2)
                if (C <= 0.0) and (t > mintime):
                    break
                g += 2.0 * C * (1.0 - float(t) / float(N)) * float(inc)
                t += inc

        g = int(round(max(g,1.0)))
        #g = max(int(g),1)
    return g


def MaxStatIneff(mat):
    nprop = mat.shape[1]
    return max( [ StatIneff(mat[:,i]) for i in range(nprop) ] )


def get_block_bootstrap_idxs( n, g ):
    from random import randrange
    idxs = []
    while len(idxs) < n:
        k = randrange(n)
        for j in range(k,k+g+1):
            idxs.append( j%n )
    return idxs[:n]



def get_estimate(cols):
    """This is the best estimate of the scaled-coordinates free energy 
    perturbation scaled mass KE estimator from M. Ceriotti, T. Markland,
    J. Chem. Phys. 138, 014112 (2013). See the description of the
    "isotope_tdfep" property in the i-pi documentation 
    https://ipi-code.org/i-pi/output-tags.html#list-of-available-properties

    When i-Pi is told to output the isotope_tdfep property, it provides a
    a list of 7 numbers (per time step).  The estimate is made from the
    last 3 columns.

    The log sum of the weights,
       LW = ln(sum(exp(-h)))

    The sum of the re-weighted kinetic energies, stored as a log modulus 
    and sign, 
       LTW=ln(abs(sum(T_CV e**(-h)))) 
       STW=sign(sum(T_CV e**(-h)))

    In practice, the best estimate of the estimator can be computed as 
       [sum_i exp(LTW_i)*STW_i]/[sum_i exp(LW_i)].

    Parameters
    ----------
    cols : numpy.ndarray, shape=(nframe,7)
        The time-series of the isotope_tdfep property, as output by i-pi

    Returns
    -------
    KE : float
        The thermodynamic free energy perturbation scaled mass KE estimator in kcal/mol
    """
    import numpy as np
    LW  = cols[:,4]
    LTW = cols[:,5]
    STW = cols[:,6]
    return sum( np.exp(LTW)*STW ) / sum(np.exp(LW)) / au_per_kcal





class tdfep_entry(object):
    """A class that stores basic information about a isotop_tdfep property

    Attributes
    ----------
    idx : int
        The atom index

    mass : float
        The light isotope mass (the simulated mass)

    ratios : list of float
        The heavy mass at which the ipi property is tabulated

    icols : list of int
        The output column number where the isotope_tdfep property begins

    data : list of numpy.ndarray
        The length of the list is the same as the length of ratios.
        The ndarray is a matrix of size (nframe,7). In other words,
        data is a collection of isotope_tdfep timeseries values.
        There is a separate timeseries for each of the masses.

    Tcvs : list of float
        The atom kinetic energy centroid virial estimator, calculated
        from TD-FEP


    dTcvs : list of float
        The standard error of the Tcv values

    
    Methods
    -------
    """
    def __init__(self,tag,icol):
        ratio,idx = tag.replace("isotope_scfep(","").replace("isotope_tdfep(","").replace(")","").split(";")
        self.idx = int(idx)
        self.mass = 0.
        self.ratios = [ float(ratio) ]
        self.icols = [ icol ]
        self.data = None
        self.Tcvs = None
        self.dTcvs = None

    def set_mass(self,mass):
        self.mass = mass
        
    def append(self,ratio,icol):
        self.ratios.append(ratio)
        self.icols.append(icol)

    def get_masses(self):
        import numpy as np
        return np.array([ self.mass * ratio for ratio in self.ratios ])
    
        
class ipiinfo(object):
    """A lightweight class that stores basic information from a ipi xml input file
    
    Attributes
    ----------
    directory : string, default = "."
        The directory where the input file resides

    output : string
        The name of the ipi output file

    timestep_fs : float
        The simulation timestep in fs

    output_stride : int
        The frequency at which the output is written

    Methods
    -------
    """

    def __init__(self):
        self.directory = "."
        self.output = ""
        self.timestep_fs = 0.
        self.output_stride = 0
        
    def get_output_filename(self):
        """Return the directory+filename of the output file

        Returns
        -------
        ofile : str
            The name of the output file
        """
        import os.path
        return os.path.join(self.directory,self.output)

    def get_numlines_of_equil(self,equil_time_ps):
        """Calculate the number of output lines to skip to treat the first
        X ps as equilibration

        Parameters
        ----------
        equil_time_ps : float
            The equilibration time in ps

        Returns
        -------
        equil_in_lines : int
            The number of output lines to skip
        """
        fs_per_step = self.timestep_fs
        #print("timestep",fs_per_step)
        steps_per_line = self.output_stride
        steps_per_ps = 1000/fs_per_step
        line_per_ps = steps_per_ps / steps_per_line
        equil_in_lines = int( line_per_ps * equil_time_ps )
        #print(steps_per_line,steps_per_ps,line_per_ps,equil_time_ps, equil_in_lines)
        return equil_in_lines
    

    
def read_xml(fname,scfep):
    """Reads an ipi xml input file and returns the output and isotope_tdfep information

    Parameters
    ----------
    fname : str
        The ipi xml input filename

    scfep : bool
        If true, read the isotope_scfep columns rather than isotope_tdfep

    Returns
    -------
    info : ipiinfo
        Describes the location of the output file, and basic time-related information

    tdfeps : dict of tdfep_entry
        The keys are the (0-based) atom indexes where isotopic substitutions occur.
        The values are lightweight objects that store information about how to locate
        the isotope_tdfep data within the output file
    
    idxorder : list of int
        The list of atom indexes (the keys of tdfeps) in the order that they appear
        in the xml file.  The ordering is significant; when one calculates EIEs or
        KIEs, we assume that the isotopic substitutions are listed in the same order.
    """
    import os
    import xml.etree.ElementTree as ET
    from collections import defaultdict as ddict

    info = ipiinfo()
    
    tree = ET.parse(fname)
    root = tree.getroot()
    
    output = root.find("output")
    idxorder = []

    if scfep:
        prop = "isotope_scfep"
    else:
        prop = "isotope_tdfep"
        
    properties=None
    for node in output.findall("properties"):
        if prop in node.text:
            properties = node
            break

    if properties is None:
        print("Could not find <properties> node in %s that prints isotope_tdfep\n"%(fname))
        raise Exception("Failed to read xml input")
        
    info.output = output.attrib["prefix"] + "." + properties.attrib["filename"]
    info.output_stride = int(properties.attrib["stride"])
    info.directory = os.path.dirname(fname)
    
    props = [ f.replace(",","").strip() for f in properties.text.strip().replace("[","").replace("]","").split(",") ]
    tdfeps = ddict(int)
    icol=0
    for itag,tag in enumerate(props):
        if prop in tag:
            entry = tdfep_entry(tag,icol)
            if entry.idx in tdfeps:
                tdfeps[entry.idx].append(entry.ratios[0],entry.icols[0])
            else:
                tdfeps[entry.idx] = entry
                idxorder.append( entry.idx )
            icol += 7
        else:
            icol += 1

    timestep = root.find("system").find("motion").find("dynamics").find("timestep")
    info.timestep_fs = float( timestep.text )
    #print("timestep",info.timestep)
    if timestep.attrib["units"] != "femtosecond":
        raise Exception("Expected timestep units to be femtosecond in %s"%(fname))
            
    initialize = root.find("system").find("initialize")
    masses = ddict(int)
    for child in initialize:
        if child.tag == "masses":
            idx = int(child.attrib["index"])
            mass = float(child.text)
            masses[idx] = mass
            
    for idx in tdfeps:
        entry = tdfeps[idx]
        entry.set_mass( masses[idx] )
        #entry.mass = masses[idx]
        #entry.masses = [ entry.mass * ratio for ratio in entry.ratios ]
    
    return info,tdfeps,idxorder
        

def read_output(outfile,tdfeps):
    """Reads an ipi output file and populates the isotope_tdfep timeseries
    data for each mass of each substituted atom.

    Parameters
    ----------
    outfile : str
        The name of the ipi output file

    tdfeps : dict of tdfep_entry
        This .data field of each entry is modified with the appropriate
        isotope_tdfep timeseries. There is no return value; this is a
        side-effect.
    """

    import numpy as np
    
    table = np.loadtxt(outfile)

    # Only select the first occurence of unique rows
    rows = []
    seen = {}
    for irow,step in enumerate(table[:,0]):
        if step not in seen:
            seen[step] = 1
            rows.append(irow)
            
    #print(len(rows),table.shape[0])
    
    for idx in tdfeps:
        entry = tdfeps[idx]
        entry.data = []
        for icol in entry.icols:
            col = table[rows,icol:icol+7]
            entry.data.append( col )


def get_numentries(tdfeps):
    """Returns the number of stored time steps

    Parameters
    ----------
    tdfeps : dict of tdfep_entry
        The stored data

    Returns
    -------
    nstep : int
        The number of rows
    """
    keys = [ idx for idx in tdfeps ]
    return len(tdfeps[keys[0]].data[0])



def calc_dA(masses,kestimate_vs_mass,beta):
    """Given the masses and kinetic energy estimator, calculate the
    free energy of isotopic substitution.

    This is the integral of eq 3 in M. Ceriotti, T. Markland, J. Chem. Phys. 138, 
    014112 (2013); that is to say, this function returns the second term in
    eq 4.  The first term is a contribution from a fictious reservoir of
    non-interacting atoms. The first term cancels when comparing free energy
    differences, such as when computing a KIE or EIE.

    Parameters
    ----------
    masses : list of float
        The list of masses at which isotope_tdfep data was collected.

    kestimate_vs_mass : list of float
        The kinetic energy estimator for each of the masses in kcal/mol.
        These are calculated from get_estimate.

    beta : float
        1/kT in kcal/mol

    Returns
    -------
    dA : float
        Free energy in kcal/mol
    """
    import numpy as np
    from scipy.interpolate import Akima1DInterpolator

    xs = masses
    ys = kestimate_vs_mass

    # for x,y in zip(xs,ys):
    #     print("%20.10e %20.10e"%(x,y))
    # print("")

    
    spl = Akima1DInterpolator(xs, ys, axis=1)
    ms = np.linspace( xs[0], xs[-1], 1000 )
    # The integrand
    ts = - spl(ms) / ms
    # The integral, approximated from trapezoidal rule
    igrl = np.trapz(ts,ms)

    # This is the contribution from a reservior of
    # non-interacting atoms, see Eq. 1 in
    # 
    pref = 3*np.log( xs[-1]/xs[0] ) / (2. * beta)
    #
    # We will ignore the non-interacting atoms
    #
    pref = 0
    dA = pref + igrl
    
    
    return dA



def calc_freeenergies(tdfeps,beta,nskip=0,nmax=-1,nboot=0):
    """ Calculate the free energy for each of the single isotopic substitutions
    using the data stored in tdfeps

    Parameters
    ----------
    tdfeps : list of tdfep_entry
        Each key is an atom index. The values store the isotope_tdfep
        timeseries of values for each mass.

    beta : float
        1/kT in kcal/mol

    nskip : int
        The number of data points excluded as equilibration

    nmax : int
        The last data point in the timeseries to consider

    nboot : int
        The number of bootstrap trials used to estimate standard errors

    Results
    -------
    dAs : dict of float
        The keys are the atom indexes (the same keys as tdfeps)
        The values are the free energy in kcal/mol

    stderr : dict of float
        The keys are the atom indexes (the same keys as tdfeps)
        The values are standard errors in the free energy (kcal/mol)
        estimated from the standard deviation of bootstrap trials.
    """
    from collections import defaultdict as ddict
    import numpy as np
    from random import randrange
    
    dAs    = ddict(int)
    stderr = ddict(int)
    
    for idx in tdfeps:
        entry = tdfeps[idx]
        xs = entry.get_masses()
        nmass = len(xs)
        ys = np.zeros( (nmass,) )
        bootys = np.zeros( (nboot,nmass) )
        
        for i in range(len(xs)):
            n = entry.data[i].shape[0]
            if nmax > 0:
                n = min(nmax,n)
            if n < nskip+1:
                n = nskip+1
            #print(nmax,n)
            data = entry.data[i][nskip:n,:]
            gmax = MaxStatIneff(data)
            #print(idx,i,gmax)
            ys[i] = get_estimate( data )
            for iboot in range(nboot):
                #bidxs = [ randrange(data.shape[0]) for k in range(data.shape[0]) ]
                bidxs = get_block_bootstrap_idxs( data.shape[0], gmax )
                bootys[iboot,i] = get_estimate( data[bidxs,:] )

        
        # spl = Akima1DInterpolator(xs, ys, axis=1)
        # ms = np.linspace( xs[0], xs[-1], 1000 )
        # ts = - spl(ms) / ms

        # igrl = np.trapz(ts,ms)
        # pref = np.log10( xs[-1]/xs[0] ) / (2. * beta)
        # dA = pref + igrl

        tdfeps[idx].Tcvs = ys
        if nboot > 0:
            tdfeps[idx].dTcvs = np.std(bootys,axis=0)
        else:
            tdfeps[idx].dTcvs = np.zeros( ys.shape )


        dAs[idx] = calc_dA(xs,ys,beta)

        if nboot > 1:
            bootdA = np.array( [calc_dA(xs,bootys[i,:],beta) for i in range(nboot)] )
            stderr[idx] = np.std(bootdA,ddof=1)
        else:
            stderr[idx] = 0
        
    return dAs,stderr


def calc_kies(mins,se_mins,tss,se_tss,minorder,tsorder,beta):
    """Calculates each of the single isotope substitution KIEs (or EIEs)

    Parameters
    ----------
    mins : dict of float
        The keys are reactant state atom indexes
        The values are isotopic substitution free energies (kcal/mol)

    se_mins : dict of float
        The keys are reactant state atom indexes
        The values are standard errors (kcal/mol)

    tss : dict of float
        The keys are transition state (or product state) atom indexes
        The values are isotopic substitution free energies (kcal/mol)

    se_tss : dict of float
        The keys are transition state (or product state) atom indexes
        The values are standard errors (kcal/mol)

    minorder : list of int
        This is a list of keys in mins in the order that they appeared
        in the ipi input file. The ordering is assumed to be consistent
        with tsorder, such that the KIE (or EIE) involving the first 
        index in minorder corresponds to a substitution of the the atom
        in the first index of tsorder.

    tsorder : list of int
        This is a list of keys in tss in the order that they appeared
        in the ipi input file. The ordering is assumed to be consistent
        with minorder.

    beta : float
        1/kT in kcal/mol

    Returns
    -------
    kies : list of float
        The KIE or EIE values. The length of the list is the same as minorder.

    se_kies : list of float
        The KIE (or EIE) error propagated from the standard error in the free energies.
    """
    import numpy as np
    
    kies = []
    se_kies = []
    norder = len(minorder)
    if len(tsorder) != norder:
        raise Exception("Number of isotopic substitutions in min structure does not match those in ts structure")
    for isub in range(norder):
        minidx = minorder[isub]
        tsidx = tsorder[isub]
        dG_light2heavy_min = mins[minidx]
        dG_light2heavy_ts  = tss[tsidx]
        se_min = se_mins[minidx]
        se_ts  = se_tss[tsidx]
        kie = np.exp( beta * (dG_light2heavy_ts - dG_light2heavy_min) )
        kies.append( kie )
        se = np.sqrt( (kie*beta*se_min)**2 + (kie*beta*se_ts)**2 )
        se_kies.append(se)
        
    return kies,se_kies


if __name__ == "__main__":

    import argparse
    import os.path
    import numpy as np

    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""
Processes i-pi output to calculate isotope free energy changes and KIEs.

To calculate the PIMD contribution to the KIE (excluding the contribution
of the frozen degree of freedom at the transition state), run:

fetkutils-ipi-tdfepkie.py -m min/ipi.xml -t ts/ipi.xml

where min/ipi.xml and ts/ipi.xml are the i-pi xml input files
that include isotope_tdfep analysis on select atom(s).

For more information on how to write the xml files with this
analysis, see the --tdfep option in fetkutils-ipi-setupfromamber.py.
""" )
    
    parser.add_argument \
    ("-m","--minxml",
     help="i-pi xml file for the minimum/reactant state",
     type=str,
     required=True )

    parser.add_argument \
    ("-t","--tsxml",
     help="i-pi xml file for the transition state",
     type=str,
     required=False )

    
    parser.add_argument \
    ("-e","--equil",
     help="number of ps of equilibration",
     type=float,
     default=0.,
     required=False )

    parser.add_argument \
    ("-l","--length",
     help="pretend the simulations were no longer than this number of ps",
     type=float,
     default=-1,
     required=False )

    
    parser.add_argument \
    ("--temp",
     help="Temperature, K (default: 298)",
     type=float,
     default=298,
     required=False )

    
    parser.add_argument \
        ("--nboot",
         help="Number of bootstrap trials (default: 25)",
         type=int,
         default=50,
        required=False )
    
    parser.add_argument \
        ("--plot",
         help="If present, then write Tcv vs mass plots",
         action='store_true')

    parser.add_argument \
        ("--scfep",
         help="If present, then read the isotope_scfep columns rather than isotop_tdfep",
         action='store_true')

    

    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))

         
    args = parser.parse_args()

    equil_in_ps = args.equil
    
    simlen_in_ps = args.length

    beta = 1. / ( args.temp * k_kcal )

    #
    # Read the reactant state ipi xml input file
    # 
    #
    
    mininfo,mininp,minorder = read_xml(args.minxml,args.scfep)
    
    read_output( mininfo.get_output_filename(), mininp )

    nlines = get_numentries( mininp )
    
    tottime = nlines*mininfo.output_stride*mininfo.timestep_fs/1000.
    
    print("There are %5i lines %10.5f ps in %s"%(nlines,tottime,mininfo.get_output_filename()))

    line0 = mininfo.get_numlines_of_equil(equil_in_ps)
    lineN = mininfo.get_numlines_of_equil(simlen_in_ps)
    lineM = lineN
    if lineN < 0:
        lineM = nlines
    print("Analyzing lines %5i to %5i"%(line0,lineM))
    
    mins,stderr_mins = calc_freeenergies(mininp,beta,nskip=line0,nmax=lineN,nboot=args.nboot)

    if args.plot:
        for i,idx in enumerate(minorder):
            write_Tcv_plot( f"min_Tcv_vs_mass_{i+1}.dat", mininp[idx] )
        

    if args.tsxml:
    
        tsinfo,tsinp,tsorder = read_xml(args.tsxml,args.scfep)
        read_output( tsinfo.get_output_filename(), tsinp )


        nlines = get_numentries( tsinp )
        tottime = nlines*tsinfo.output_stride*tsinfo.timestep_fs/1000.

        print("There are %5i lines %10.5f ps in %s"%(nlines,tottime,tsinfo.get_output_filename()))
    
        line0 = tsinfo.get_numlines_of_equil(equil_in_ps)
        lineN = tsinfo.get_numlines_of_equil(simlen_in_ps)
        lineM = lineN
        if lineN < 0:
            lineM = nlines
        print("Analyzing lines %5i to %5i"%(line0,lineM))

        print("")
        
        tss,stderr_tss = calc_freeenergies(tsinp,beta,nskip=line0,nmax=lineN,nboot=args.nboot)

        if args.plot:
            for i,idx in enumerate(tsorder):
                write_Tcv_plot( f"ts_Tcv_vs_mass_{i+1}.dat", tsinp[idx] )
        

        kies,stderr_kies = calc_kies\
            (mins,stderr_mins,  tss,stderr_tss, minorder,tsorder, beta)

        
        print("%9s %9s  %11s %21s %20s"%("Atom(min)","Atom(ts)","dA(min)","dA(ts)","KIE"))
        for isub in range(len(minorder)):
            minidx = minorder[isub]
            tsidx = tsorder[isub]
            print("%4i      %4i     %9.5f +- %7.5f   %9.5f +- %7.5f   %9.4f +- %6.4f"%\
                  (minidx,tsidx,
                   mins[minidx], stderr_mins[minidx],
                   tss[tsidx], stderr_tss[tsidx],
                   kies[isub], stderr_kies[isub]))

    else:
        print("")
        
        print("Atom     dA(min)")
        for idx in mins:
            print("%4i %9.5f +- %7.5f"%(idx,mins[idx],stderr_mins[idx]))

        

        
#print("%.3f"%(dA))
