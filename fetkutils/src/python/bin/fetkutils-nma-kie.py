#!/usr/bin/env python3

def GetFilenameAndIsosub(name):
    isosub = []
    if "=" in name:
        fname,extra = name.rsplit("=")
        isosub = [int(x) for x in extra.split(",")]
        isosub.sort()
    else:
        fname=name
    return fname,isosub



if __name__ == "__main__":

    from ndfes.normalmode import Vibrator
    from ndfes.normalmode import BigeleisenMayerKIE
    from ndfes.normalmode import KIETunnelingFactor
    from ndfes.constants import BOLTZMANN_CONSTANT_AU
    import sys
    import argparse
    import numpy as np
    from collections import defaultdict as ddict
    
    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
          Calculates kinetic isotopope effect (KIE) or equilibrium isotope
          effect (EIE) values in the harmonic approximation.
          The input can be Gaussian output files or a Hessian NetCDF4 files.

          If an input is a Gaussian file, then the coordinates, energy, and 
          Hessian are extracted from the first archive section containing a
          Hessian.

          If the input is a Hessian NetCDF4 file, then the file is first 
          searched to see if a normal mode analysis has been performed. If the
          normal modes are not found in the NetCDF4 file, then an analysis is 
          performed and the results are written to the NetCDF4; otherwise the
          results are directly read from the file.

          To calculate a KIE (or EIE), one-or-more atoms must be
          isotopically substituted. To denote the isotopomers, one
          postfixes the filename with '=I', where I is a 1-based integer.
          If multiple substitutions are needed, use a comma separated list;
          e.g., '=I,J'.

          The following example calculates a KIE, where the heavy atom in 
          the reactant state appears as atom 5 and the heavy atom in the
          transition state appears as atom 6.

          fetkutils-nma-kie.py --min min.nc=5 --ts ts.nc=6

          The following example calculates a KIE, where the reactant
          state consists of 2 molecules which were independently optimized
          with Gaussian.  The heavy atom is the 2nd atom in the first
          reactant state molecule, and it is the 7th atom in the transition
          state.

          fetkutils-nma-kie.py --min minA.log=2 --min minB.log --ts tsAB.log=7

          """ )

    parser.add_argument \
        ("--parmidx",
         help="Interpret integers as the 1-based atom indexes "+
         "appearing in the original Amber parameter file. This is only "+
         "meaningful when reading a Hessian NetCDF4 file.",
         action='store_true')
    
    # parser.add_argument \
    #     ("--miniso",
    #      help="Defines the 'heavy' isotopologue of the reactant state by "+
    #      "substituting the specified atom with the 2nd most abundant mass. "+
    #      "This is a 1-based integer. This option can be used more than once.",
    #      type=int,
    #      action='append',
    #      required=True)

    # parser.add_argument \
    #     ("--tsiso",
    #      help="Defines the 'heavy' isotopologue of the transition state by "+
    #      "substituting the specified atom with the 2nd most abundant mass. "+
    #      "This is a 1-based integer. This option can be used more than once.",
    #      type=int,
    #      action='append',
    #      required=True)

    parser.add_argument \
        ("--temp","-T",
         help="Temperature (K). Default: 298.15",
         type=float,
         default=298.15)

    
    parser.add_argument \
        ("--save",
         help="Save the isotopomer eigensolution to the netcdf file",
         action="store_true")
    
    parser.add_argument \
        ("--min",
         help="The Gaussian output or Hessian NetCDF file for a reactant "+
         "state molecule. This option can technically be used more than once "+
         "if the reactant state consists of multiple gas phase molecules.",
         type=str,
         action='append',
         required=True)
    
    parser.add_argument \
        ("--ts",
         help="The Gaussian output or Hessian NetCDF file for the transition "+
         "state molecule or product state molecule. If it is a transition "+
         "state, then a KIE is evaluated. If it is a product state, then a "+
         "EIE is evaluated. This option can technically be used more than "+
         "once if the reactant state consists of multiple gas phase molecules.",
         type=str,
         action='append',
         required=True)

    

    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))


    
    args = parser.parse_args()


    rs_names = []
    rs_isosubs = []
    rs_numiso = 0
    for x in args.min:
        name,isosub = GetFilenameAndIsosub(x)
        rs_names.append(name)
        rs_isosubs.append(isosub)
        rs_numiso += len(isosub)

    ts_names = []
    ts_isosubs = []
    ts_numiso = 0
    for x in args.ts:
        name,isosub = GetFilenameAndIsosub(x)
        ts_names.append(name)
        ts_isosubs.append(isosub)
        ts_numiso += len(isosub)

    if rs_numiso == 0:
        raise Exception("The --min option(s) failed to specify an isotopic"+
                        " substution")
    
    if ts_numiso == 0:
        raise Exception("The --ts option(s) failed to specify an isotopic"+
                        " substution")

    if ts_numiso != rs_numiso:
        raise Exception("The --ts and --min option(s) specified a different"+
                        f" number of substitutions: {ts_numiso} vs {rs_numiso}")

    
    
    rslight = [ Vibrator.from_file( fname, [], args.parmidx, save=args.save )
                for fname in rs_names ]
    
    rsheavy = [ Vibrator.from_file( fname, isosub, args.parmidx, save=args.save )
                for fname,isosub in zip(rs_names,rs_isosubs) ]

    
    tslight = [ Vibrator.from_file( fname, [], args.parmidx, save=args.save )
                for fname in ts_names ]
    
    tsheavy = [ Vibrator.from_file( fname, isosub, args.parmidx, save=args.save )
                for fname,isosub in zip(ts_names,ts_isosubs) ]

    print("\nInput summary:\n")

    print("Temperature (K): %.2f\n"%(args.temp))

    elecnt_rs = ddict(int)
    numimag_rs = 0
    for i in range(len(rslight)):
        fname = rs_names[i]
        isosub = rs_isosubs[i]
        freqs = rslight[i].GetFreqs_InvCM()
        eles = rsheavy[i].GetIsosubElements()
        for ele in eles:
            elecnt_rs[ele] += 1
        print(f"MIN filename:  {fname}")
        print( "    isosubs: ",isosub)
        print( "    elements:",eles)
        print( "    freqs:   "," ".join(["%9.1f"%(x) for x in freqs[:3]]))
        
        print("")
        
        if freqs[0] < 0:
            numimag_rs += 1


    
    elecnt_ts = ddict(int)
    numimag_ts = 0
    for i in range(len(tslight)):
        fname = ts_names[i]
        isosub = ts_isosubs[i]
        freqs = tslight[i].GetFreqs_InvCM()
        eles = tsheavy[i].GetIsosubElements()
        for ele in eles:
            elecnt_ts[ele] += 1
        print(f" TS filename: {fname}")
        print( "    isosubs:",isosub)
        print( "    elements:",eles)
        print( "    freqs:  "," ".join(["%9.1f"%(x) for x in freqs[:3]]))
        print("")
        if freqs[0] < 0:
            numimag_ts += 1

            
    if numimag_ts == 1:
        is_kie = True
        label = "transition"
    else:
        is_kie = False
        label = "product"


    print("The reactant state molecule(s) contain "+
          f"{numimag_rs} imaginary frequencies.\n")


    print(f"The {label} state molecule(s) contain "+
          f"{numimag_ts} imaginary frequencies.\n")
    
        
    if numimag_ts > 1:
        print(f"Error: The TS contains {numimag_ts} imaginary frequencies")

        
    if numimag_rs > 1:
        print(f"Error: The reactant contains {numimag_rs} imaginary "+
              "frequencies")


    allele = list(set([ ele for ele in elecnt_rs ] + [ ele for ele in elecnt_ts ]))
    has_error = False
    for ele in allele:
        rs = elecnt_rs[ele]
        ts = elecnt_ts[ele]
        if rs != ts:
            print(f"Error: Reactant state substitutes {rs} {ele}'s,"+
                  f" but TS substitutes {ts}")
            has_error = True
    if has_error:
        raise Exception("There was an isotopic substitution mismatch"+
                        " between the reactant and transition states")
        
    
    big_no_tunnel = BigeleisenMayerKIE( rslight, rsheavy,
                                        tslight, tsheavy,
                                        temperature = args.temp )

    tunnel = 1
    FRC = 1
    if is_kie:
        tunnel = KIETunnelingFactor( tslight, tsheavy,
                                     temperature = args.temp )
        Lfreqs = tslight[0].GetFreqs_InvCM()
        Hfreqs = tsheavy[0].GetFreqs_InvCM()
        FRC = Lfreqs[0] / Hfreqs[0]

    big_w_tunnel = tunnel * big_no_tunnel


    G_rs_light = sum( [x.GetThermalCorrectionToGibbsFreeEnergy_AU(args.temp)
                       for x in rslight] )
    
    G_rs_heavy = sum( [x.GetThermalCorrectionToGibbsFreeEnergy_AU(args.temp)
                       for x in rsheavy] )

    
    G_ts_light = sum( [x.GetThermalCorrectionToGibbsFreeEnergy_AU(args.temp)
                       for x in tslight] )
    
    G_ts_heavy = sum( [x.GetThermalCorrectionToGibbsFreeEnergy_AU(args.temp)
                       for x in tsheavy] )

    
    beta = 1. / ( BOLTZMANN_CONSTANT_AU() * args.temp )
    
    #dGlight = np.exp(- beta * (G_ts_light-G_rs_light))
    #dGheavy = np.exp(- beta * (G_ts_heavy-G_rs_heavy))
    #conv_no_tunnel = dGlight / dGheavy

    dGts = G_ts_light - G_ts_heavy
    dGrs = G_rs_light - G_rs_heavy
    conv_no_tunnel = np.exp(-beta * (dGts-dGrs))
    conv_w_tunnel = tunnel * conv_no_tunnel

    label = "EIE"
    tslabel = "ps"
    if is_kie:
        label = "KIE"
        tslabel = "ts"

    print("="*48)
    print("%23s %10s %12s"%("Quantity","Tunneling","Value"))
    print("%23s %10s %12s"%("",      "Correction",""))
    print("- "*(48//2))
    print("%23s %10s %12.5f"%(f"Bigeleisen-Mayer {label}","No",big_no_tunnel))
    if is_kie:
        print("%23s %10s %12.5f"%(f"Bigeleisen-Mayer {label}","Yes",big_w_tunnel))
    print("%23s %10s %12.5f"%(f"Conventional {label}","No",conv_no_tunnel))
    if is_kie:
        print("%23s %10s %12.5f"%(f"Conventional {label}","Yes",conv_w_tunnel))
        print("%23s %10s %12.5f"%("Tunneling correction","",tunnel))
    print("%23s  %22.12e"%("dG_{"+tslabel+"} (E_h)",dGts))
    print("%23s  %22.12e"%("dG_{rs} (E_h)",dGrs))
    print("%23s  %22.12f"%("beta (1/E_h)",beta))
    if is_kie:
        print("%23s %10s %12.5f"%("w_{TS,L}/w_{TS,H}","",FRC))

    print("="*48)

    print("")
    print("Bigeleisen-Mayer EIE without tunneling correction is given")
    print("by eq. 15 in https://doi.org/10.1016/j.bbapap.2015.04.021")
    
    print("")
    print("Bigeleisen-Mayer KIE without tunneling correction is given")
    print("by eq. 16 in https://doi.org/10.1016/j.bbapap.2015.04.021")

    print("")
    print("Bigeleisen-Mayer KIE with tunneling correction is given by ")
    print("eq. 22 in https://doi.org/10.1016/j.bbapap.2015.04.021")

    print("")
    print("The tunneling correction approximates the barrier by an")
    print("infinite concave parabola; it is the first line of ")
    print("eq. 22 in https://doi.org/10.1016/j.bbapap.2015.04.021")

    print("")
    print("The conventional KIE (or EIE) is given by: ")
    print("")
    print("        exp(-beta * ( G_{%s,light} - G_{rs,light} ))"%(tslabel))
    print("IE   = -----------------------------------------------")
    print("        exp(-beta * ( G_{%s,heavy} - G_{rs,heavy} ))"%(tslabel))
    print("")
    print("")
    print("        exp(-beta * ( G_{%s,light} - G_{%s,heavy} ))"%(tslabel,tslabel))
    print("IE   = -----------------------------------------------")
    print("        exp(-beta * ( G_{rs,light} - G_{rs,heavy} ))")
    print("")
    print("")
    print("        exp(-beta * dG_{%s})"%(tslabel))
    print("IE   = -------------------------")
    print("        exp(-beta * dG_{rs})")
    print("")
    print("where ts either refers to the transition state (KIE) or")
    print("product state (EIE), beta=1/kT,  and the free energies ")
    print("are calculated within the riged rotor and harmonic ")
    print("osciallator approximations, as described in the Gaussian ")
    print("thermochemistry paper: https://gaussian.com/thermo/")
    print("Formally, the conventional KIE should also contain a ratio")
    print("of transmission coefficients. The results reported by this")
    print("program do not include this ratio.")
    print("")
    
        

    
    
