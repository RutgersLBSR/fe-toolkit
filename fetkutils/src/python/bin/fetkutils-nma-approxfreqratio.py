#!/usr/bin/env python3


def ReadSegAvgs(tfile,aidxs,masses,nseg,ref=None,equil=0):
    import numpy as np
    from ndfes.amber.Geometry import PerformRmsOverlay
    from ndfes.amber.Geometry import RemoveCoM
    from ndfes.amber import ReadCrds
    from pathlib import Path
    
    wts = np.array(masses)
    allcrds = []
    if ref is not None:
        if ref.shape[0] != len(aidxs) or ref.shape[1] != 3:
            raise Exception(f"ref has shape {ref.shape}, but expected "
                            "({len(aidxs),3})")

    segs = []

    pf = Path(tfile)
    if not pf.is_file():
        raise Exception("File not found %s"%(pf))

    crds = ReadCrds(tfile)[:,aidxs,:]
    neq = int(crds.shape[0] * equil)
    if neq > 0:
        crds = crds[neq:,:,:]

    chunks = np.array_split(range(crds.shape[0]),nseg)
    for frames in chunks:
        if ref is None:
            myref = crds[frames[0],:,:]
        else:
            myref = ref
        allcrds = []
        for frame in frames:
            rms,rmscrd = PerformRmsOverlay(crds[frame,:,:],myref,wts)
            allcrds.append(rmscrd)
        avgcrds = np.mean(np.array(allcrds),axis=0)
        avg = RemoveCoM( avgcrds, wts )
        segs.append(avg)

    return segs



if __name__ == "__main__":

    import argparse
    import numpy as np
    import parmed
    from parmed.amber.mask import AmberMask
    from ndfes.constants import GetStdIsotopeMass
    from ndfes.constants import GetSecondIsotopeMass
    from ndfes.amber.Geometry import PerformRmsOverlay

    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
Approximates the light-to-heavy ratio of imaginary frequencies at the 
transition state from harmonic analysis of the umbrella sampling performed
along the minimum free energy path. The frequency ratio, FR, is:

FR = w^{\ddagger}_{L} / w^{\ddagger}_{H}

          { \sum_a m_{H,a} \sum_k (dR_{ak}(p^{\ddagger}/dp)^2 }
   = sqrt { ------------------------------------------------- }
          { \sum_a m_{L,a} \sum_k (dR_{ak}(p^{\ddagger}/dp)^2 }
          
where p is a progress variable that denotes the two ends of the minimum 
free energy path (p=0 and p=1). One approximates the FR from umbrella
sampling by performing simulations on either side of the transition state
and calculating the derivatives from finite difference. This is the method
described by T. J. Giese and D. M. York, "Estimation of frequency factors 
for the  calculation of kinetic isotope effects from classical and path 
integral free  energy simulations", J. Chem. Phys. (2023) 158, 174105.""" )


    parser.add_argument \
        ("--parm","-p",
         help="amber parm7 file",
         type=str,
         required=True)

    parser.add_argument \
        ("--nclo",
         help="amber netcdf trajectory for a simulation performed at "
         +"p = p^{ts} - \delta p",
         type=str,
         required=True)
    
    parser.add_argument \
        ("--nchi",
         help="amber netcdf trajectory for a simulation performed at "
         +"p = p^{ts} + \delta p",
         type=str,
         required=True)

    parser.add_argument \
        ("--mask",
         help="amber mask defining the selected atoms",
         type=str,
         required=True)

    parser.add_argument \
        ("--iso",
         help="isotopically substitute the atom specified by the 1-based index",
         type=int,
         action='append',
         required=True)

    parser.add_argument \
        ("--nseg",
         help="split the trajectories into nseg segments, and make "
         +"nseg**2 estimates of the frequency ratio. (default: 10)",
         type=int,
         default=10)

    parser.add_argument \
        ("--equil",
         help="Exclude a fraction of the simulation as equilibration."
         +"If 0, means no samples are excluded. A value of 0.5 excludes "
         +"the first half of the simulaiton. (default: 0.)",
         type=float,
         default=0)


    
    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))

    
    args = parser.parse_args()

    isosub = [ item for item in args.iso ]
    isosub.sort()

    p = parmed.load_file( args.parm )
    
    aidxs = [ p.atoms[i].idx for i in AmberMask( p, args.mask ).Selected() ]
    
    atnums = [ p.atoms[i].atomic_number for i in aidxs ]
    
    lmass = [ GetStdIsotopeMass(z) for z in atnums ]

    nat = len(aidxs)
    n3 = 3*nat


    xlos = ReadSegAvgs(args.nclo,aidxs,lmass,args.nseg,equil=args.equil)
    xhis = ReadSegAvgs(args.nchi,aidxs,lmass,args.nseg,equil=args.equil)


    
    print("%7s %8s %6s %12s %12s %12s"%("Atom","Block 0","N",
                                        "mean","Std","StdErr"))
    
    for iso in isosub:
        
        if iso-1 not in aidxs:
            raise Exception(f"Isotopic substitution {iso} is not in the "
                            +f"atom mask: {[a+1 for a in aidxs]}")
        ii = aidxs.index(iso-1)
        hmass = [ m for m in lmass ]
        hmass[ii] = GetSecondIsotopeMass( atnums[ii] )

        Mlight = np.array( [ [m,m,m] for m in lmass ] ).reshape( (n3,) )
        Mheavy = np.array( [ [m,m,m] for m in hmass ] ).reshape( (n3,) )

        nhi = len(xhis)
        nlo = len(xlos)
        nmin = min(nhi,nlo)
        
        for block0 in range(max(1,nmin)):
            frc = []
            for hseg in range(block0,nhi):
                for lseg in range(block0,nlo):
                    xlo = np.array(xlos[lseg],copy=True)
                    rms,xhi = PerformRmsOverlay(xhis[hseg],xlo,lmass)

                    Xl = (xhi-xlo).reshape( (n3,) )
                    mul = np.dot( Xl, Mlight*Xl )
                    muh = np.dot( Xl, Mheavy*Xl )
                    frc.append( np.sqrt(muh/mul) )
        
            nfrc = len(frc)
            if nfrc < 1:
                continue
            frc = np.array(frc)
            stdfrc = np.std(frc)
            
            print("%7i %8i %6i %12.5f %12.5f %12.5f"\
                  %(iso,block0,nfrc,np.mean(frc),stdfrc,
                    stdfrc/np.sqrt(nfrc)))
            
    
        
