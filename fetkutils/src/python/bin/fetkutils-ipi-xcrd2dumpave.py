#!/usr/bin/env python3


def read_xcrd(filename):
    """Reads coordinates of selected atoms from an i-Pi output file

    Usage:
        read_xcrd("filename")

    Returns:
        aidxs,crds,units

        aidxs: list of int, len=nat, 0-based atom indexes
        crds: numpy.array, shape=(nframe,nat,3) atomic coordinates
        units: list of str, len=nat, units of coordinates for each atom
    """
    
    import re
    import numpy as np

    # headers with atom indexes
    header_pattern = re.compile(
        r"#\s*(column|cols\.)\s+(\d+)(?:-(\d+))?\s*-->\s*([^\s\{]+)(?:\{([^\}]+)\})(\([\d]+\))?\s*:\s*(.*)"
    )

    # Reading the file
    data = np.loadtxt(filename)
    with open(filename, "r") as file:
        lines = file.readlines()

    header_lines = [line for line in lines if line.startswith("#")]
    #data_lines = [line for line in lines if not line.startswith("#") and line.strip()]

    aidxs = []
    acols = []
    cunits = []
    
    # Interprets properties
    for line in header_lines:
        #print(line)
        match = header_pattern.match(line)
        if match:
            # Extracting matched groups
            col_type, start_col, end_col, property_name, units, aidx, description = (
                match.groups()
            )
            #print(col_type,start_col,end_col,property_name,units,aidx,description)
            col_info = f"{start_col}-{end_col}" if end_col else start_col
            if aidx is not None:
                aidxs.append( int(aidx.replace("(","").replace(")","")) )
                acols.append( ( int(start_col)-1, int(end_col) ) )
                cunits.append( units )
            

    nat = len(aidxs)
    crds = np.zeros( (data.shape[0],nat,3) )
    for a in range(nat):
        start,stop = acols[a]
        crds[:,a,:] = data[:,start:stop]
    #for i in range(crds.shape[0]):
    #    print(crds[i,:,:])

    return aidxs,crds,cunits
    

if __name__ == "__main__":

    import argparse
    import numpy as np
    from ndfes.amber import Disang
    from ndfes.constants import AU_PER_KCAL_PER_MOL

    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""Writes an amber dumpave file from a i-Pi xcrd file.

Umbrella sampling performed with i-Pi should apply the restraint to the 
centroid positions rather than the individual beads. If a restraint was applied
to the individual beads, then one could artificially increase the mass of the 
atoms. Therefore, the &cntrl section of the sander input should contain 
nmropt=0, and the i-pi input file should be configured to apply restraints via 
plumed.  The fetkutils-ipi-setupfromamber.py will create a plumed file based on
a provided disang file, and it will modify the ipi input appropriately.  i-Pi 
does not produce a dumpave file that one would normally use to analyze the 
umbrella sampling. Instead, fetkutils-ipi-setupfromamber.py will setup the ipi
input so ipi will create a "xcrd" file. The xcrd file contains 3N+1 columns, 
where N is the number of atoms needed to define the reaction coordinates.  The
columns of the xcrd file are the cartesian positions of the petite set of 
atoms, and the rows represent time.  The present script will read a xcrd file,
reconstruct the reaction coordinates, and output a dumpave file.

""" )

    parser.add_argument \
        ("-d","--disang",
         help="amber disang restraint file",
         type=str,
         required=True )

    parser.add_argument \
        ("-y","--xcrd",
         help="i-Pi xcrd file",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--dumpave",
         help="amber dumpave to write",
         type=str,
         required=True )


    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))

    
    args = parser.parse_args()

    aidxs,crds,units = read_xcrd(args.xcrd)

    #print(aidxs)
    
    for u in units:
        if u != "angstrom":
            raise Exception(f"Expected units 'angstrom', found {u}")

    
    disang = Disang(args.disang)
    validcols = []
    fillcols = []
    fillvals = []
    for ires,res in enumerate(disang.restraints):
        if (res.rk2 == res.rk3) and \
           (res.r2 == res.r3):
            validcols.append(ires)
        else:
            fillcols.append(ires)
            fillvals.append( 0.5*(res.r2+res.r3) )

    sdisang = disang.Subset([ i+1 for i in validcols ])
    #sdisang.Save("crap.disang")
    
    
    
    qs = []
    #es = []
    for i in range(crds.shape[0]):
        q = sdisang.CptCrds(crds[i,:,:],aidxs=aidxs)
        #e = disang.CptBiasEnergy(q)
        qs.append(q)
        #es.append(e)
    qs = np.array(qs)

    fullqs = np.zeros( (qs.shape[0],len(disang.restraints)) )
    for ic,idx in enumerate(validcols):
        fullqs[:,idx] = qs[:,ic]
    for ic,idx in enumerate(fillcols):
        fullqs[:,idx] = fillvals[ic]
    
    # es = np.array(es)*AU_PER_KCAL_PER_MOL()
    # for e in es:
    #     print("%16.7e"%(e))

        
    print("Saving %s"%(args.dumpave))
    fh = open(args.dumpave,"w")
    for i in range(fullqs.shape[0]):
        fh.write("%6i %s\n"%(i+1, " ".join(["%12.5f"%(x) for x in fullqs[i,:]])))
    fh.close()

    
