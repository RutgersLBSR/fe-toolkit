#!/usr/bin/env python3


def show_atom_number(mol, label, offset):
    for atom in mol.GetAtoms():
        atom.SetProp(label, str(atom.GetIdx()+1+offset))
    return mol


def GetSelectedAtomIndices(param,maskstr):
    import parmed
    sele = []
    if len(maskstr) > 0:
        newmaskstr = maskstr.replace("@0","!@*")
        sele = [ param.atoms[i].idx for i in parmed.amber.mask.AmberMask( param, newmaskstr ).Selected() ]
    return sele


class Ligand(object):
    def __init__(self, mdin, parmfile, rstfile, image_loc, edge_name, showidxs, size):
        """ This is a class to generate an image of a ligand with softcore atoms highlighted

        Parameters
        ----------
        mdin : str
            Path to the mdin mdin file [mdin]
        parmfile : str
            Path to the parameter file [parm7]
        rstfile : str
            Path to the restart file [rst7]
        image_loc : str
            Path to the directory where the images will be saved
        edge_name : str
            Name of the edge (e.g. 0~1)
        showidxs : bool
            If True, show the atom indexes in the image
        size : tuple of int
            The image size in pixels.

        Returns
        -------
        None

        Raises
        ------
        FileNotFoundError
            If the mdin, parmfile, or rstfile does not exist

        
        """
        from pathlib import Path

        self.mdin = Path(mdin)
        self.parmfile = Path(parmfile)
        self.rstfile = Path(rstfile)    
        self.softcore = []
        self.timask = []
        self.image_loc = Path(image_loc)
        self.edge_name = edge_name
        self.showidxs = showidxs
        self.size = size
        if not self.mdin.exists():
            raise FileNotFoundError(f"{self.mdin} does not exist")
        if not self.parmfile.exists():
            raise FileNotFoundError(f"{self.parmfile} does not exist")
        if not self.rstfile.exists():
            raise FileNotFoundError(f"{self.rstfile} does not exist")
        if "~" not in self.edge_name:
            raise Exception(f"Edge name {self.edge_name} does not contain '~'")
        else:
            ligs = self.edge_name.split("~")
            if len(ligs) != 2:
                raise Exception(f"Could interpret edge name {self.edge_name} as 2 ligands: {ligs}")
        

    def run(self):
        """ This function runs the class and generates the images """
        self.parm = self._read_parm()
        self._find_softcore()
        ti_only, sc_only, ti_all = [], [], []
        for i in range(len(self.softcore)):
            ti_only.append(self._find_difference(self.softcore[i], self.timask[i]))
            sc_only.append(self._parse_range(self.softcore[i]))
            ti_all.append(self._parse_range(self.timask[i]))

        self.ti_only = ti_only
        self.sc_only = sc_only
        self.ti_all = ti_all
        self._write_2d_structure()
        return

    def _read_parm(self):
        """ This function reads the parameter and restart files and returns a parameter file object"""
        import parmed
        parm = None
        try:
            parm = parmed.load_file(str(self.parmfile))
            parm.load_rst7(str(self.rstfile))
        except Exception as e:
            raise RuntimeError(f"Failed to read parameter or restart file: {e}")
        return parm
    
    def _write_2d_structure(self):
        """ This function writes the 2D structure of the ligand with the softcore atoms highlighted 
        
        Parameters
        ----------
        parm : parmed.Structure
            A parmed structure object
            
        Returns
        -------
        None
        
        TODO
        ----
        This function should be modified to check charges on the ligands. 
        
        """
        import rdkit
        from rdkit import Chem
        from rdkit.Chem import Draw, rdDetermineBonds
        from pathlib import Path

        ligand_resnames = [res.name for res in self.parm.residues[0:2]]
        fake_pdb = FakeFile()
        self.parm.write_pdb(fake_pdb)
        for i, mol in enumerate(ligand_resnames):
            mol_content = fake_pdb.getvalue(mask=[mol])
            rdmol = Chem.MolFromPDBBlock(mol_content, removeHs=False)

            q1 = round(sum([atom.charge for atom in self.parm.residues[i].atoms]))

            rdDetermineBonds.DetermineBonds(rdmol,charge=q1)
            
            Chem.rdDepictor.SetPreferCoordGen(True)

            Chem.rdDepictor.Compute2DCoords(rdmol)
            sc = [idx - min(self.ti_all[i]) for idx in self.sc_only[i]]

            img = Draw.rdMolDraw2D.MolDraw2DCairo(*self.size)
            opts = img.drawOptions()
            opts.baseFontSize = 0.46
            opts.useMolBlockWedging = True
            opts.singleColourWedgeBonds = True
            opts.highlightBondWidthMultiplier = 12

            img.DrawMolecule(rdmol,highlightAtoms=sc)
            img.FinishDrawing()
            node_names = self.edge_name.split('~')

            Path(self.image_loc).mkdir(parents=True, exist_ok=True)
            
            with open(f"{self.image_loc}/{self.edge_name}_{node_names[i]}.png",'wb') as f:
                f.write(img.GetDrawingText())

              
            if self.showidxs:

                
                offset = self.parm.residues[i].atoms[0].idx
                #show_atom_number(rdmol,'molAtomMapNumber')
                show_atom_number(rdmol,'atomLabel',offset)
                img = Draw.rdMolDraw2D.MolDraw2DCairo(*self.size)
                opts = img.drawOptions()
                opts.baseFontSize = 0.42
                opts.useMolBlockWedging = True
                opts.singleColourWedgeBonds = True
                opts.highlightBondWidthMultiplier = 12

                img.DrawMolecule(rdmol,highlightAtoms=sc)
                img.FinishDrawing()
                node_names = self.edge_name.split('~')
                with open(f"{self.image_loc}/{self.edge_name}_{node_names[i]}_idxs.png",'wb') as f:
                    f.write(img.GetDrawingText())
            
        return 

    def _find_softcore(self):
        """ This function finds the softcore atoms from the mdin file """
        with self.mdin.open() as f:
            for line in f:
                if "scmask" in line:
                    line = line.split('!')[0].strip()
                    key,value = line.split('=')
                    value=value.strip()
                    self.softcore.append(value)
                if "timask" in line:
                    line = line.split('!')[0].strip()
                    key,value = line.split('=')
                    value=value.strip()
                    self.timask.append(value)

    def _parse_range(self, mask):
        value=mask.strip()
        if value[0] == "\"" and value[1] == "\"":
            value = value[1:-1]
        if value[0] == "'":
            value = value[1:]
            if value[-1] == "'":
                value = value[:-1]
        atomids = [idx+1 for idx in GetSelectedAtomIndices(self.parm,value)]
        return atomids
        
                    
    def _find_difference(self, list1, list2):
        """ This function finds the difference between two sets """
        set1 = set()
        set2 = set()
        set1.update(self._parse_range(list1))
        set2.update(self._parse_range(list2))
        return sorted(set2-set1)
    
class FakeFile(object):
    def __init__(self):
        """ This is a fake file class to capture the output of parmed.write_pdb """
        self.content = ""

    def write(self, text):
        """ This function writes the text to the content 
        
        Parameters
        ----------
        text : str
            The text to write to the content

        """
        self.content += text

    def getvalue(self, mask=None):
        """ This function returns the content of the file 
        
        Parameters
        ----------
        mask : list
            A list of strings to filter the content
            
        Returns
        -------
        str
            The content of the file

        """
        content = []
        if mask == None:
            return self.content
        for line in self.content.split('\n'):
            for elem in mask:
                if elem in line:
                    content.append(line)
        content = '\n'.join(content)
        return content
    
        
if __name__ == "__main__":
    
    import argparse
    
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""Reads a pmemd mdin file, rst7 coordinate file, amber parameter (parm7) file, and writes chemical structures of the first 2 residues. The atoms within the softcore regions are highlighted.
""")
    
    parser.add_argument \
        ("-i","--mdin",
         type=str,
         required=True,
         help="A pmemd mdin file containing scmask and timask setting within the cntrl namelist")
    
    parser.add_argument \
        ("-p","--parm",
         type=str,
         required=True,
         help="An Amber parameter (parm7) file")
    
    parser.add_argument \
        ("-c","--crd",
         type=str,
         required=True,
         help="A formatted Amber restart coordinate (rst7) file")
    
    parser.add_argument \
        ("-e","--edge",
         type=str,
         required=True,
         help="Name of the edge. It should be formatted like 'lig1~lig2'")

    parser.add_argument \
        ("--imgdir",
         type=str,
         default=".",
         help="The directory where the image is written to. Default: .")
    
    parser.add_argument \
        ("--showidxs",
         action='store_true',
         help="If present, write a second image showing the atom indexes rather than element symbols")
    
    parser.add_argument \
        ("--width",
         type=int,
         default=400,
         help="Width of the image in pixels. Default: 400")
    
    parser.add_argument \
        ("--height",
         type=int,
         default=-1,
         help="Height of the image in pixels. Default: -1, which assumes the same value as --width")
    
    args = parser.parse_args()

    size = (args.width,args.height)

    ligand = Ligand(args.mdin,
                    args.parm,
                    args.crd,
                    args.imgdir,
                    args.edge,
                    args.showidxs,
                    size)

    ligand.run()

    
