#!/usr/bin/env python3

# def h2abc(h):
#     """Returns a description of the cell in terms of the length of the
#        lattice vectors and the angles between them in radians.

#     Takes the representation of the system box in terms of an upper triangular
#     matrix of column vectors, and returns the representation in terms of the
#     lattice vector lengths and the angles between them in radians.

#     Args:
#        h: Cell matrix in upper triangular column vector form.

#     Returns:
#        A list containing the lattice vector lengths and the angles between them.
#     """

#     a = float(h[0, 0])
#     b = math.sqrt(h[0, 1] ** 2 + h[1, 1] ** 2)
#     c = math.sqrt(h[0, 2] ** 2 + h[1, 2] ** 2 + h[2, 2] ** 2)
#     gamma = math.acos(h[0, 1] / b)
#     beta = math.acos(h[0, 2] / c)
#     alpha = math.acos(np.dot(h[:, 1], h[:, 2]) / (b * c))

#     return a, b, c, alpha, beta, gamma


# def h2abc_deg(h):
#     """Returns a description of the cell in terms of the length of the
#        lattice vectors and the angles between them in degrees.

#     Takes the representation of the system box in terms of an upper triangular
#     matrix of column vectors, and returns the representation in terms of the
#     lattice vector lengths and the angles between them in degrees.

#     Args:
#        h: Cell matrix in upper triangular column vector form.

#     Returns:
#        A list containing the lattice vector lengths and the angles between them
#        in degrees.
#     """
#     import math
    
#     (a, b, c, alpha, beta, gamma) = h2abc(h)
#     return a, b, c, alpha * 180 / math.pi, beta * 180 / math.pi, gamma * 180 / math.pi


# def abc2h(a, b, c, alpha, beta, gamma):
#     """Returns a lattice vector matrix given a description in terms of the
#     lattice vector lengths and the angles in between.

#     Args:
#        a: First cell vector length.
#        b: Second cell vector length.
#        c: Third cell vector length.
#        alpha: Angle between sides b and c in radians.
#        beta: Angle between sides a and c in radians.
#        gamma: Angle between sides b and a in radians.

#     Returns:
#        An array giving the lattice vector matrix in upper triangular form.
#     """
#     import numpy as np
#     import math
    
#     h = np.zeros((3, 3), float)
#     h[0, 0] = a
#     h[0, 1] = b * math.cos(gamma)
#     h[0, 2] = c * math.cos(beta)
#     h[1, 1] = b * math.sin(gamma)
#     h[1, 2] = (b * c * math.cos(alpha) - h[0, 1] * h[0, 2]) / h[1, 1]
#     h[2, 2] = math.sqrt(c**2 - h[0, 2] ** 2 - h[1, 2] ** 2)
#     return h


# def abc2h_deg(a, b, c, alpha, beta, gamma):
#     """Returns a lattice vector matrix given a description in terms of the
#     lattice vector lengths and the angles in between.

#     Args:
#        a: First cell vector length.
#        b: Second cell vector length.
#        c: Third cell vector length.
#        alpha: Angle between sides b and c in degrees.
#        beta: Angle between sides a and c in degrees.
#        gamma: Angle between sides b and a in degrees.

#     Returns:
#        An array giving the lattice vector matrix in upper triangular form.
#     """
#     import math
#     return abc2h(a,b,c,alpha*math.pi/180,beta*math.pi/180,gamma*math.pi/180)
    
#     h = np.zeros((3, 3), float)
#     h[0, 0] = a
#     h[0, 1] = b * math.cos(gamma)
#     h[0, 2] = c * math.cos(beta)
#     h[1, 1] = b * math.sin(gamma)
#     h[1, 2] = (b * c * math.cos(alpha) - h[0, 1] * h[0, 2]) / h[1, 1]
#     h[2, 2] = math.sqrt(c**2 - h[0, 2] ** 2 - h[1, 2] ** 2)
#     return h


# def write_ipi_bin_frame(fh,parm,box,crd=None):
#     from ndfes.constants import AU_PER_ANGSTROM
#     from ndfes.constants import GetAtomicSymbol
#     import numpy as np
    
#     nat = len(parm.atoms)
#     xyz = np.zeros( (nat,3) )
#     if crd is not None:
#         xyz[:,:] = crd[:,:] * AU_PER_ANGSTROM()
#     else:
#         for i in range(nat):
#             xyz[i,0] = parm.atoms[i].xx * AU_PER_ANGSTROM()
#             xyz[i,1] = parm.atoms[i].xy * AU_PER_ANGSTROM()
#             xyz[i,2] = parm.atoms[i].xz * AU_PER_ANGSTROM()
#     h = abc2h_deg( box[0] * AU_PER_ANGSTROM(),
#                    box[1] * AU_PER_ANGSTROM(),
#                    box[2] * AU_PER_ANGSTROM(),
#                    box[3],box[4],box[5] )
#     buff = fh
#     h.flatten().tofile(buff)
#     natv = np.asarray([nat])
#     natv.tofile(buff)
#     xyz.tofile(buff)
#     names = "|".join([ GetAtomicSymbol(a.atomic_number) for a in parm.atoms ])
#     natv[0] = len(names)
#     natv.tofile(buff)
#     np.asarray([names]).tofile(buff)
#     np.asarray(["title"]).tofile(buff)

    
# def write_ipi_bin_restart(fname,parm):
#     fh = open(fname,"wb")
#     write_ipi_bin_frame(fh,parm,parm.box)
#     fh.close()
    
    
# def read_ipi_bin_frame(filedesc):
#     from ndfes.constants import AU_PER_ANGSTROM
#     import numpy as np
#     try:
#         cell = np.fromfile(filedesc, dtype=float, count=9)
#         cell.shape = (3, 3)
#         nat = np.fromfile(filedesc, dtype=int, count=1)[0]
#         qatoms = np.fromfile(filedesc, dtype=float, count=3 * nat)
#         nat = np.fromfile(filedesc, dtype=int, count=1)[0]
#         names = np.fromfile(filedesc, dtype="|U1", count=nat)
#         names = "".join(names)
#         names = names.split("|")
#         title = "".join(np.fromfile(filedesc, dtype="|U1", count=-1))
#         qatoms /= AU_PER_ANGSTROM()
#         cell = np.array([ x for x in h2abc_deg(cell) ])
#         cell[0:3] /= AU_PER_ANGSTROM()
#     except (StopIteration, ValueError):
#         raise EOFError
#     return title, cell, qatoms, names


# def read_ipi_bin_traj(fname):
#     import numpy as np
#     #titles=[]
#     cells=[]
#     crds=[]
#     #names=[]
#     fh = open(fname,"rb")
#     try:
#         while True:
#             title,cell,crd,names = read_ipi_bin_frame(fh)
#             #titles.append(title)
#             cells.append(cell)
#             crds.append(crd)
#             #names.append(name)
#     except EOFError as e:
#         fh.close()
#         cells = np.array(cells)
#         crds = np.array(crds)
#     return crds,cells


##################################################################################
##################################################################################


def GetHarmonicRestrainedAtomIdxs(disang):
    """Returns the atom indexes involved in harmonic restraints

    Parameters
    ----------
    disang : ndfes.amber.Disang
        The disang object

    Results
    -------
    seen : list of int
        The 0-based atom indexes
    """
    seen = []
    for ires,res in enumerate(disang.restraints):
        if (res.r2 == res.r3) and \
           (res.rk2 == res.rk3):
            seen.extend( [i-1 for i in res.iat ] )
    seen = list(set(seen))
    return seen


##################################################################################
##################################################################################


def write_ipi_xyz(xyzcrd,p):
    """Writes an i-Pi compatible xyz file
    
    Parameters
    ----------
    xyzcrd : str
        The filename to write

    p : parmed.amber.AmberFormat
        The amber parm7 file object
    """
    fh = open( xyzcrd, "w" )
    fh.write("%i\n"%(len(p.atoms)))
    fh.write("# positions{angstrom} CELL{abcABC}: %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f cell{angstrom}\n"%(p.box[0],p.box[1],p.box[2],p.box[3],p.box[4],p.box[5]))
    for a in p.atoms:
        if a.element < 1:
            raise Exception("Can't run i-pi with rigid waters nor dummy particles. Found particle with atomic number %i\n"%(a.element))
        fh.write("%2s  %12.7f %12.7f %12.7f\n"%(GetAtomicSymbol(a.element),a.xx,a.xy,a.xz))
    fh.close()

    
##################################################################################
##################################################################################



if __name__ == "__main__":

    import argparse
    import parmed
    import numpy as np
    from pathlib import Path
    from ndfes.amber import Disang
    from ndfes.constants import GetAtomicSymbol
    from ndfes.constants import GetStdIsotopeMass
    from ndfes.constants import GetSecondIsotopeMass


    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""Writes input xml and xyz coordinate files for i-Pi

This script assumes you want to perform PIMD with the PIGLET thermostat using 6
beads. If not, you will have to modify the XML file. The system cannot contain 
dummy particles, and atoms cannot be shaked. A flexible water model must be 
used.  To change water models, you will likely need to recreate parm7 and rst7 
files using tleap. A description of how to change water models is provided 
below.  When you run i-pi and use sander (or sander.MPI) as a driver program, 
the &cntrl section must set the following options:

imin=7   ! turn on ipi interface
ntc=1    ! no shake
ntf=1    ! calculate all forces (no shake)
jfastw=4 ! flexible water model
nmropt=0 ! no restraints

If you want to apply restraints, it must be through ipi (not sander) because 
the restraints should be applied to the centroid positions rather than the 
individual beads.  When you supply this script with the --disang option, it 
will output all harmonic restraints as a plumed file, and the ipi input is 
configured to apply the restraints to the centroid positions.  Rather than 
producing a "dumpave" file, the ipi input is instructed to print the centroid 
positions for those atoms involved in the restraints.  These coordinates are 
written to a file called {prefix}.xcrd.  You can then use the script 
fetkutils-ipi-xcrd2dumpave.py to reconstruct an amber-like dumpave file from 
the xcrd file.

=========================================================================
Given an equilibrated system, you can replace the water model in the following 
manner.

1. Use cpptraj to recenter the system.
--------------------------------------
cat <<EOF | cpptraj
parm orig.parm7
trajin orig.rst7
center QMMASK origin mass
image origin center familiar
trajout imgcrd.rst7
run
quit
EOF

   Where QMMASK is the QM active site

2. Use ambpdb to output the coordinates in PDB format.
------------------------------------------------------

ambpdb -p orig.parm7 -c imgcrd.rst7 -ext > imgcrd.pdb

3. Use tleap to read the PDB and output new parm7 and rst7 files.
-----------------------------------------------------------------

cat <<EOF > tleap.inp
ource leaprc.constph
source leaprc.RNA.OL3
source leaprc.protein.ff19SB

# Replace SPC/E with q-SPC/Fw and set the flag to indicate 
# a flexible water model (otherwise the angle term will be
# missing
source leaprc.water.spce
loadAmberParams frcmod.qspcfw
HOH = SPG
WAT = SPG
set default FlexibleWater on

mol = loadpdb imgcrd.pdb
setbox mol centers
saveamberparm mol tmp.parm7 tmp.rst7
quit
EOF

4. Reset the box dimensions
---------------------------

c=($(tail -n 1 imgcrd.rst7))
ChBox -X ${c[0]} -Y ${c[1]} -Z ${c[2]} \\
      -al ${c[3]} -bt ${c[4]} -gm ${c[5]} \\
      -c tmp.rst7 -o new.rst7
rm tmp.rst7

5. Reset the ifbox pointer if one was using a truncated octahedron.
-------------------------------------------------------------------

c=($(tail -n 1 imgcrd.rst7))
O="109.4712190"
if [ "${c[3]}" == "${O}" \
     -a "${c[4]}" == "${O}" \
     -a "${c[5]}" == "${O}" ]; then

     # line 9 (1-based), column 63 (0-based)
     perl -pe 'substr($_,63,1)="2" if $.==9' tmp.parm7 > new.parm7
     rm tmp.parm7
else
     mv tmp.parm7 new.parm7
fi

=========================================================================


EXAMPLES


Example 1: Perform PIMD in the NVT ensemble with the PIGLET thermostat without
restraints, nor mass thermodynamic integration.

fetkutils-ipi-setupfromamber.py -p new.parm7 -c new.rst7



Example 2: Perform PIMD umbrella sampling in the NVT ensemble with the PIGLET 
thermostat.

fetkutils-ipi-setupfromamber.py -p my.parm7 -c my.rst7 -d my.disang


Example 3: Perform PIMD umbrella sampling in the NVT ensemble with the PIGLET
thermostat. Furthermore, output TDFEP (thermodynamic free energy perturbation)
analysis for isotopically substituting select atom(s). In this example, atom 
522 (1-based index) is selected.

fetkutils-ipi-setupfromamber.py -p my.parm7 -c my.rst7 -d my.disang --tdfep 522

If you perform simulation of two states; e.g., a reactant and a  transition 
state (or product states), then you can estimate a KIE or EIE value using the 
fetkutils-ipi-tdfepkie.py script.  Note that the resulting KIE value will need
to be corrected to account for the constraint applied to the transition state.
See: https://doi.org/10.1063/5.0147218

""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="amber parameter file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--crd",
         help="amber restart file",
         type=str,
        required=True )

    parser.add_argument \
        ("-d","--disang",
         help="amber disang restraint file",
         type=str,
         required=False )

    parser.add_argument \
        ("--port",
         help="Internet port (default: 31415)",
         type=int,
         default=31415,
         required=False )

    parser.add_argument \
        ("--seed",
         help="Random number seed (default: 123456)",
         type=int,
         default=123456,
         required=False )

    parser.add_argument \
        ("--prefix",
         help="Filename prefix (default: ipi)",
         type=str,
         default="ipi",
         required=False)

    parser.add_argument \
        ("--dt",
         help="timestep in fs (default: 0.25)",
         type=float,
         default=0.25,
         required=False)
    
    parser.add_argument \
        ("--nstlim",
         help="Number of time steps (default: 40000 -- 10 ps @ 0.25 ps/step)",
         type=int,
         default=40000,
         required=False )

    parser.add_argument \
        ("--ntwr",
         help="i-Pi checkpoint file write frequency (default: 0)",
         type=int,
         default=0,
         required=False)

    parser.add_argument \
        ("--ntpr",
         help="i-Pi output file print frequency (default: 20)",
         type=int,
         default=20,
         required=False)

    parser.add_argument \
        ("--ntwx",
         help="i-Pi trajectory file write frequency (default: 0)",
         type=int,
         default=0,
         required=False)

    parser.add_argument \
        ("--dump-freq",
         help="Frequency for writing centroids of those atoms involved in restraints (default: 20)",
         type=int,
         default=20,
         required=False)
    

    parser.add_argument \
        ("--temp",
         help="Temerature, K (default: 298.)",
         type=float,
         default=298.,
         required=False)

    parser.add_argument \
        ("--tdfep",
         help="TD-FEP + PIGLET isotopic substitution of selected atom (1-based index)",
         action='append',
         type=int,
         nargs='+',
         required=False )

    parser.add_argument \
        ("--nlam",
         help="Number of lambdas used to integrate the free energy (default: 11)",
         type=int,
         default=11,
         required=False )



    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))

    
    #
    # I hardcoded PIGLET parameters for nbeads=6
    #
    nbeads = 6
    
    #
    # Read the input options
    #
    
    args = parser.parse_args()

    #
    # Create a flat list of tdfep atom indexes
    #
    
    if args.tdfep is not None:
        args.tdfep = [item for sublist in args.tdfep for item in sublist]

    #
    # Read the input parameter/coordinate files
    #
        
    p = parmed.load_file( args.parm, xyz=args.crd )

    for a in p.atoms:
        if a.mass < 0.5 or a.atomic_number < 1:
            raise Exception\
                ("Parm7 file contains dummy atoms. "+
                 "Rebuild your system with tleap "+
                 "without dummy atoms")
    

    #
    # Convert Amber disang restraint file to plumed
    #
    
    plumed = None
    if args.disang is not None:
        disang = Disang(args.disang)
        plumed = Path(args.disang).stem + ".plumed"
        #
        # stride=0 means plumed will not write to an
        # output file, because this behavior appear to
        # be broken when used with i-pi
        #
        disang.SavePlumed(plumed,stride=0)

    #
    # Convert amber restart to i-pi xyz file
    #
        
    xyzcrd = Path(args.crd).stem + ".xyz"
    write_ipi_xyz( xyzcrd, p )

        
    
    xmlinp = args.prefix + ".xml"
    fh = open( xmlinp, "w" )
    
    fh.write(f"""
<simulation verbosity='low'>
  <output prefix='{args.prefix}'>
""")

    #
    # Should we write a checkpoint file?
    #
    
    precomment = "<!-- "
    postcomment = " -->"
    if args.ntwr > 0:
        precomment = ""
        postcomment = ""
    fh.write(f"""
{precomment}
    <checkpoint stride='{args.ntwr}'/>
{postcomment}
""")
    
    #
    # Should we write a trajectory file?
    #
    
    precomment = "<!-- "
    postcomment = " -->"
    if args.ntwx > 0:
        precomment = ""
        postcomment = ""
    
    fh.write(f"""
{precomment}
    <trajectory stride="{args.ntwx}" filename="xc" format="xyz">x_centroid{{angstrom}}</trajectory>
{postcomment}
""")

    
    #
    # If we have restraints, should we output the relevant atom positions?
    #
    if plumed:
        aidxs = GetHarmonicRestrainedAtomIdxs(disang)
        aidxs.sort()
        astr = ",".join(["atom_x{angstrom}(%i)"%(iat) for iat in aidxs])
        precomment = "<!-- "
        postcomment = " -->"
        if args.dump_freq > 0:
            precomment = ""
            postcomment = ""
        fh.write(f"""
{precomment}
    <properties stride="{args.dump_freq}" filename="xcrd">
      [ time{{picosecond}}, {astr} ]
    </properties>
{postcomment}
""")
            

    #
    # Standard properties
    #
    
    fh.write(f"""
    <properties stride='{args.ntpr}' filename='out'> 
         [ step, time{{picosecond}}, ensemble_bias{{atomic_unit}}, conserved{{atomic_unit}}, temperature{{kelvin}}, kinetic_md{{atomic_unit}}, potential{{atomic_unit}}, kinetic_cv{{atomic_unit}}""")


    #
    # Also print the tdfep properties if we are calculating the
    # free energy of isotopic substitution
    #
    
    if args.tdfep:
        for a1idx in args.tdfep:
            idx = a1idx-1
            m0 = GetStdIsotopeMass( p.atoms[idx].atomic_number )
            m1 = GetSecondIsotopeMass( p.atoms[idx].atomic_number )
            ms = np.linspace(m0,m1,num=args.nlam)
            for m in ms:
                alpha = m/m0
                fh.write(", isotope_tdfep(%.10f;%i)"%(alpha,idx))

    #
    # end of properties
    #
    
    fh.write(" ]\n    </properties>\n")
    fh.write("  </output>\n")

    

    fh.write(f"""
  <total_steps>{args.nstlim}</total_steps>

  <prng>
    <seed>{args.seed}</seed>
  </prng>
  
  <ffsocket mode='inet' name='sander'>
    <address>127.0.0.1</address>
    <port>{args.port}</port>
  </ffsocket>
""")

    
    if plumed is not None:
        fh.write(f"""
  <ffplumed name="plumed" pbc="False">
    <file mode="xyz"> {xyzcrd} </file>
    <plumeddat> {plumed} </plumeddat>
  </ffplumed>
  
""")

    fh.write(f"""
  <system>

    <initialize nbeads='{nbeads}'>
      <file mode='xyz'> {xyzcrd} </file>""")

    if args.tdfep:
        for a1idx in args.tdfep:
            idx = a1idx-1
            m0 = GetStdIsotopeMass( p.atoms[idx].atomic_number )
            fh.write("\n      "+
                     "<masses mode='manual' units='dalton' "+
                     "index='%i'>%.8f</masses>"%(idx,m0))
    
    fh.write(f"""
      <velocities mode='thermal' units='kelvin'> {args.temp} </velocities>
    </initialize>
    
    <forces>
      <force forcefield='sander'/>
    </forces>
    """)


    
    fh.write(f"""
    <ensemble>
      <temperature units='kelvin'> {args.temp} </temperature>
""")

    if plumed is not None:
        fh.write("""
      <bias>
        <force forcefield="plumed" nbeads="1"></force>
      </bias>
      <bias_weights shape="(1)"> [ 1.0 ] </bias_weights>
""")

    fh.write("    </ensemble>\n")

    
    
    fh.write(f"""

    <motion mode='dynamics'>
      <dynamics mode='nvt'>
        <timestep units='femtosecond'>{args.dt:.4f}</timestep>

""")
    
    fh.write("""
	<!--
            <thermostat mode='pile_l'>
            <tau units='femtosecond'>100</tau>
            </thermostat>
	-->

	<!--
	    # Generated at http://cosmo-epfl.github.io/gle4md
	    # Please cite:
	    # M. Ceriotti, D.E. Manolopoulos, Phys. Rev. Lett. 109, 100604 (2012) 
	    # M. Ceriotti, G. Bussi and M. Parrinello, Phys. Rev. Lett. 103, 030603 (2009)
	    # PIGLET parameters. Accelerate convergence of path integral dynamics, including quantum kinetic energy.
	    # Use for simulations with 6 beads, using a RPMD (physical masses) representation
	    # of the path.
	    # The parameters were picked from
	    # library/piglet/pigle-6_20_8_t.[ac], 
	    # and shifted so that correspond to quantum fluctuations at 
	    # T=298 K
	    # and that they span a range up to 
	    # omega max=4142.4121164 cm^-1
	-->
	<thermostat mode='nm_gle'>
	  <A shape='(6,9,9)'>
	    [
	    1.300513766690e-2,    9.078220950722e-6,    8.180522706851e-6,    1.196620464216e-5,    1.108609196233e-4,   -8.941338246404e-4,    7.817382329484e-3,   -1.206049888192e-2,   -5.215913547478e-2, 
	    -9.756343549369e-6,    2.131200614277e-7,    2.972243541454e-6,   -4.459298032276e-6,    2.177011229810e-7,    4.960251269751e-7,   -2.083064995647e-6,   -7.004617074013e-6,    2.299410255689e-5, 
	    -1.851243089560e-6,   -2.972243541454e-6,    1.956991859501e-6,    1.742357040415e-6,   -2.082265548357e-6,   -1.760771137012e-6,   -3.733162998255e-6,   -3.711884630223e-5,   -3.625483838477e-5, 
	    1.492481502899e-5,    4.459298032276e-6,   -1.742357040415e-6,    5.092476869103e-6,    2.033910859306e-6,    5.856365217540e-7,   -3.020170664006e-6,    1.868034354962e-5,   -5.049113665348e-6, 
	    1.059383195368e-4,   -2.177011229810e-7,    2.082265548357e-6,   -2.033910859306e-6,    5.467813757620e-5,   -6.684243951800e-6,   -9.770331146786e-7,   -2.159991642805e-4,    4.667176340213e-4, 
	    -7.611448585233e-4,   -4.960251269751e-7,    1.760771137012e-6,   -5.856365217540e-7,    6.684243951800e-6,    6.616597356640e-4,   -1.637891086976e-6,   -2.056652206438e-4,    2.960975881160e-4, 
	    7.659946833472e-3,    2.083064995647e-6,    3.733162998255e-6,    3.020170664006e-6,    9.770331146786e-7,    1.637891086976e-6,    6.390977118535e-3,   -6.246090363901e-5,    5.054994461623e-4, 
	    -1.078245092236e-2,    7.004617074013e-6,    3.711884630223e-5,   -1.868034354962e-5,    2.159991642805e-4,    2.056652206438e-4,    6.246090363901e-5,    1.730397061203e-1,    1.004651317366e-4, 
	    -5.467410217589e-2,   -2.299410255689e-5,    3.625483838477e-5,    5.049113665348e-6,   -4.667176340213e-4,   -2.960975881160e-4,   -5.054994461623e-4,   -1.004651317366e-4,    1.795223909984e+0, 
	    5.117136183963e-6,    5.932911372963e-3,    0.000000000000e+0,    1.067909281228e-2,    0.000000000000e+0,    4.379206927614e-3,    0.000000000000e+0,    8.105095013679e-5,    0.000000000000e+0, 
	    -5.932911372963e-3,    6.234672441897e-2,    3.224579408508e-2,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,   -3.224579408508e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -1.067909281228e-2,    0.000000000000e+0,    0.000000000000e+0,    1.530700540905e-2,    5.268453339500e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -5.268453339500e-3,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -4.379206927614e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    4.613575757408e-2,    1.054432354473e-2,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.054432354473e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0, 
	    -8.105095013679e-5,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    3.354861319292e-3,    1.064306224633e-3, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.064306224633e-3,   1.892775386091e-14, 
	    5.117136183963e-6,    5.932911372963e-3,    0.000000000000e+0,    1.067909281228e-2,    0.000000000000e+0,    4.379206927614e-3,    0.000000000000e+0,    8.105095013679e-5,    0.000000000000e+0, 
	    -5.932911372963e-3,    6.234672441897e-2,    3.224579408508e-2,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,   -3.224579408508e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -1.067909281228e-2,    0.000000000000e+0,    0.000000000000e+0,    1.530700540905e-2,    5.268453339500e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -5.268453339500e-3,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -4.379206927614e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    4.613575757408e-2,    1.054432354473e-2,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.054432354473e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0, 
	    -8.105095013679e-5,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    3.354861319292e-3,    1.064306224633e-3, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.064306224633e-3,   1.892775386091e-14, 
	    5.117136183963e-6,    5.932911372963e-3,    0.000000000000e+0,    1.067909281228e-2,    0.000000000000e+0,    4.379206927614e-3,    0.000000000000e+0,    8.105095013679e-5,    0.000000000000e+0, 
	    -5.932911372963e-3,    6.234672441897e-2,    3.224579408508e-2,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,   -3.224579408508e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -1.067909281228e-2,    0.000000000000e+0,    0.000000000000e+0,    1.530700540905e-2,    5.268453339500e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -5.268453339500e-3,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -4.379206927614e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    4.613575757408e-2,    1.054432354473e-2,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.054432354473e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0, 
	    -8.105095013679e-5,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    3.354861319292e-3,    1.064306224633e-3, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.064306224633e-3,   1.892775386091e-14, 
	    5.117136183963e-6,    5.932911372963e-3,    0.000000000000e+0,    1.067909281228e-2,    0.000000000000e+0,    4.379206927614e-3,    0.000000000000e+0,    8.105095013679e-5,    0.000000000000e+0, 
	    -5.932911372963e-3,    6.234672441897e-2,    3.224579408508e-2,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,   -3.224579408508e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -1.067909281228e-2,    0.000000000000e+0,    0.000000000000e+0,    1.530700540905e-2,    5.268453339500e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -5.268453339500e-3,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -4.379206927614e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    4.613575757408e-2,    1.054432354473e-2,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.054432354473e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0, 
	    -8.105095013679e-5,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    3.354861319292e-3,    1.064306224633e-3, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.064306224633e-3,   1.892775386091e-14, 
	    5.117136183963e-6,    5.932911372963e-3,    0.000000000000e+0,    1.067909281228e-2,    0.000000000000e+0,    4.379206927614e-3,    0.000000000000e+0,    8.105095013679e-5,    0.000000000000e+0, 
	    -5.932911372963e-3,    6.234672441897e-2,    3.224579408508e-2,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,   -3.224579408508e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -1.067909281228e-2,    0.000000000000e+0,    0.000000000000e+0,    1.530700540905e-2,    5.268453339500e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -5.268453339500e-3,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    -4.379206927614e-3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    4.613575757408e-2,    1.054432354473e-2,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.054432354473e-2,   1.892775386091e-14,    0.000000000000e+0,    0.000000000000e+0, 
	    -8.105095013679e-5,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    3.354861319292e-3,    1.064306224633e-3, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,   -1.064306224633e-3,   1.892775386091e-14
	    ]
	  </A>
	  <C shape='(6,9,9)' units='kelvin'>
	    [
	    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3,    0.000000000000e+0, 
	    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    0.000000000000e+0,    1.788000000000e+3, 
	    2.227801036988e+3,   -1.299779789508e+2,   -1.107796656610e+3,    6.357571103560e+2,    5.923767941180e+2,   -1.366431488370e+3,   -3.255829741680e+3,   -4.508453243540e+2,   -1.150492747266e+3, 
	    -1.299779789508e+2,    9.335305054940e+3,    5.189385097100e-9,    1.866215755146e+2,    6.303195967360e+1,   -7.629714100100e+1,    4.533065728080e+0,   -1.073825264828e+1,    2.028728422702e+0, 
	    -1.107796656610e+3,    5.173495778820e-9,    9.131481282720e+3,   -3.857898061700e+2,    1.761128482058e+1,   -1.386265353382e+1,   -6.327545472860e+2,   -6.146535412480e+1,   -2.159563770420e+2, 
	    6.357571103560e+2,    1.866215755146e+2,   -3.857898061700e+2,    9.302421648940e+2,    6.589129852000e-9,   -1.835524424332e+1,   -1.349899849268e+3,   -3.180285111620e+2,   -4.684809542220e+1, 
	    5.923767941180e+2,    6.303195967360e+1,    1.761128482058e+1,    6.589298376960e-9,    2.130982943550e+3,    6.744751656360e+2,   -2.714255285474e+3,    2.319041259104e+2,   -2.260168921464e+3, 
	    -1.366431488370e+3,   -7.629714100100e+1,   -1.386265353382e+1,   -1.835524424332e+1,    6.744751656360e+2,    2.461322627988e+4,    4.122354597620e-8,   -3.530454377320e+1,    3.267403501440e+0, 
	    -3.255829741680e+3,    4.533065728080e+0,   -6.327545472860e+2,   -1.349899849268e+3,   -2.714255285474e+3,    4.122370424400e-8,    2.326103407038e+4,   -3.237090875880e+1,   -4.956756844540e+2, 
	    -4.508453243540e+2,   -1.073825264828e+1,   -6.146535412480e+1,   -3.180285111620e+2,    2.319041259104e+2,   -3.530454377320e+1,   -3.237090875880e+1,    8.062143059320e+4,    1.426957197656e-6, 
	    -1.150492747266e+3,    2.028728422702e+0,   -2.159563770420e+2,   -4.684809542220e+1,   -2.260168921464e+3,    3.267403501440e+0,   -4.956756844540e+2,    1.426952962778e-6,    8.053381620920e+4, 
	    2.227801036988e+3,   -1.299779789508e+2,   -1.107796656610e+3,    6.357571103560e+2,    5.923767941180e+2,   -1.366431488370e+3,   -3.255829741680e+3,   -4.508453243540e+2,   -1.150492747266e+3, 
	    -1.299779789508e+2,    9.335305054940e+3,    5.189385097100e-9,    1.866215755146e+2,    6.303195967360e+1,   -7.629714100100e+1,    4.533065728080e+0,   -1.073825264828e+1,    2.028728422702e+0, 
	    -1.107796656610e+3,    5.173495778820e-9,    9.131481282720e+3,   -3.857898061700e+2,    1.761128482058e+1,   -1.386265353382e+1,   -6.327545472860e+2,   -6.146535412480e+1,   -2.159563770420e+2, 
	    6.357571103560e+2,    1.866215755146e+2,   -3.857898061700e+2,    9.302421648940e+2,    6.589129852000e-9,   -1.835524424332e+1,   -1.349899849268e+3,   -3.180285111620e+2,   -4.684809542220e+1, 
	    5.923767941180e+2,    6.303195967360e+1,    1.761128482058e+1,    6.589298376960e-9,    2.130982943550e+3,    6.744751656360e+2,   -2.714255285474e+3,    2.319041259104e+2,   -2.260168921464e+3, 
	    -1.366431488370e+3,   -7.629714100100e+1,   -1.386265353382e+1,   -1.835524424332e+1,    6.744751656360e+2,    2.461322627988e+4,    4.122354597620e-8,   -3.530454377320e+1,    3.267403501440e+0, 
	    -3.255829741680e+3,    4.533065728080e+0,   -6.327545472860e+2,   -1.349899849268e+3,   -2.714255285474e+3,    4.122370424400e-8,    2.326103407038e+4,   -3.237090875880e+1,   -4.956756844540e+2, 
	    -4.508453243540e+2,   -1.073825264828e+1,   -6.146535412480e+1,   -3.180285111620e+2,    2.319041259104e+2,   -3.530454377320e+1,   -3.237090875880e+1,    8.062143059320e+4,    1.426957197656e-6, 
	    -1.150492747266e+3,    2.028728422702e+0,   -2.159563770420e+2,   -4.684809542220e+1,   -2.260168921464e+3,    3.267403501440e+0,   -4.956756844540e+2,    1.426952962778e-6,    8.053381620920e+4, 
	    2.227801036988e+3,   -1.299779789508e+2,   -1.107796656610e+3,    6.357571103560e+2,    5.923767941180e+2,   -1.366431488370e+3,   -3.255829741680e+3,   -4.508453243540e+2,   -1.150492747266e+3, 
	    -1.299779789508e+2,    9.335305054940e+3,    5.189385097100e-9,    1.866215755146e+2,    6.303195967360e+1,   -7.629714100100e+1,    4.533065728080e+0,   -1.073825264828e+1,    2.028728422702e+0, 
	    -1.107796656610e+3,    5.173495778820e-9,    9.131481282720e+3,   -3.857898061700e+2,    1.761128482058e+1,   -1.386265353382e+1,   -6.327545472860e+2,   -6.146535412480e+1,   -2.159563770420e+2, 
	    6.357571103560e+2,    1.866215755146e+2,   -3.857898061700e+2,    9.302421648940e+2,    6.589129852000e-9,   -1.835524424332e+1,   -1.349899849268e+3,   -3.180285111620e+2,   -4.684809542220e+1, 
	    5.923767941180e+2,    6.303195967360e+1,    1.761128482058e+1,    6.589298376960e-9,    2.130982943550e+3,    6.744751656360e+2,   -2.714255285474e+3,    2.319041259104e+2,   -2.260168921464e+3, 
	    -1.366431488370e+3,   -7.629714100100e+1,   -1.386265353382e+1,   -1.835524424332e+1,    6.744751656360e+2,    2.461322627988e+4,    4.122354597620e-8,   -3.530454377320e+1,    3.267403501440e+0, 
	    -3.255829741680e+3,    4.533065728080e+0,   -6.327545472860e+2,   -1.349899849268e+3,   -2.714255285474e+3,    4.122370424400e-8,    2.326103407038e+4,   -3.237090875880e+1,   -4.956756844540e+2, 
	    -4.508453243540e+2,   -1.073825264828e+1,   -6.146535412480e+1,   -3.180285111620e+2,    2.319041259104e+2,   -3.530454377320e+1,   -3.237090875880e+1,    8.062143059320e+4,    1.426957197656e-6, 
	    -1.150492747266e+3,    2.028728422702e+0,   -2.159563770420e+2,   -4.684809542220e+1,   -2.260168921464e+3,    3.267403501440e+0,   -4.956756844540e+2,    1.426952962778e-6,    8.053381620920e+4, 
	    2.227801036988e+3,   -1.299779789508e+2,   -1.107796656610e+3,    6.357571103560e+2,    5.923767941180e+2,   -1.366431488370e+3,   -3.255829741680e+3,   -4.508453243540e+2,   -1.150492747266e+3, 
	    -1.299779789508e+2,    9.335305054940e+3,    5.189385097100e-9,    1.866215755146e+2,    6.303195967360e+1,   -7.629714100100e+1,    4.533065728080e+0,   -1.073825264828e+1,    2.028728422702e+0, 
	    -1.107796656610e+3,    5.173495778820e-9,    9.131481282720e+3,   -3.857898061700e+2,    1.761128482058e+1,   -1.386265353382e+1,   -6.327545472860e+2,   -6.146535412480e+1,   -2.159563770420e+2, 
	    6.357571103560e+2,    1.866215755146e+2,   -3.857898061700e+2,    9.302421648940e+2,    6.589129852000e-9,   -1.835524424332e+1,   -1.349899849268e+3,   -3.180285111620e+2,   -4.684809542220e+1, 
	    5.923767941180e+2,    6.303195967360e+1,    1.761128482058e+1,    6.589298376960e-9,    2.130982943550e+3,    6.744751656360e+2,   -2.714255285474e+3,    2.319041259104e+2,   -2.260168921464e+3, 
	    -1.366431488370e+3,   -7.629714100100e+1,   -1.386265353382e+1,   -1.835524424332e+1,    6.744751656360e+2,    2.461322627988e+4,    4.122354597620e-8,   -3.530454377320e+1,    3.267403501440e+0, 
	    -3.255829741680e+3,    4.533065728080e+0,   -6.327545472860e+2,   -1.349899849268e+3,   -2.714255285474e+3,    4.122370424400e-8,    2.326103407038e+4,   -3.237090875880e+1,   -4.956756844540e+2, 
	    -4.508453243540e+2,   -1.073825264828e+1,   -6.146535412480e+1,   -3.180285111620e+2,    2.319041259104e+2,   -3.530454377320e+1,   -3.237090875880e+1,    8.062143059320e+4,    1.426957197656e-6, 
	    -1.150492747266e+3,    2.028728422702e+0,   -2.159563770420e+2,   -4.684809542220e+1,   -2.260168921464e+3,    3.267403501440e+0,   -4.956756844540e+2,    1.426952962778e-6,    8.053381620920e+4, 
	    2.227801036988e+3,   -1.299779789508e+2,   -1.107796656610e+3,    6.357571103560e+2,    5.923767941180e+2,   -1.366431488370e+3,   -3.255829741680e+3,   -4.508453243540e+2,   -1.150492747266e+3, 
	    -1.299779789508e+2,    9.335305054940e+3,    5.189385097100e-9,    1.866215755146e+2,    6.303195967360e+1,   -7.629714100100e+1,    4.533065728080e+0,   -1.073825264828e+1,    2.028728422702e+0, 
	    -1.107796656610e+3,    5.173495778820e-9,    9.131481282720e+3,   -3.857898061700e+2,    1.761128482058e+1,   -1.386265353382e+1,   -6.327545472860e+2,   -6.146535412480e+1,   -2.159563770420e+2, 
	    6.357571103560e+2,    1.866215755146e+2,   -3.857898061700e+2,    9.302421648940e+2,    6.589129852000e-9,   -1.835524424332e+1,   -1.349899849268e+3,   -3.180285111620e+2,   -4.684809542220e+1, 
	    5.923767941180e+2,    6.303195967360e+1,    1.761128482058e+1,    6.589298376960e-9,    2.130982943550e+3,    6.744751656360e+2,   -2.714255285474e+3,    2.319041259104e+2,   -2.260168921464e+3, 
	    -1.366431488370e+3,   -7.629714100100e+1,   -1.386265353382e+1,   -1.835524424332e+1,    6.744751656360e+2,    2.461322627988e+4,    4.122354597620e-8,   -3.530454377320e+1,    3.267403501440e+0, 
	    -3.255829741680e+3,    4.533065728080e+0,   -6.327545472860e+2,   -1.349899849268e+3,   -2.714255285474e+3,    4.122370424400e-8,    2.326103407038e+4,   -3.237090875880e+1,   -4.956756844540e+2, 
	    -4.508453243540e+2,   -1.073825264828e+1,   -6.146535412480e+1,   -3.180285111620e+2,    2.319041259104e+2,   -3.530454377320e+1,   -3.237090875880e+1,    8.062143059320e+4,    1.426957197656e-6, 
	    -1.150492747266e+3,    2.028728422702e+0,   -2.159563770420e+2,   -4.684809542220e+1,   -2.260168921464e+3,    3.267403501440e+0,   -4.956756844540e+2,    1.426952962778e-6,    8.053381620920e+4
	    ]
	  </C>
	</thermostat>

      </dynamics>

    </motion>

  </system>

</simulation>
""")
    fh.close()


    
