#!/usr/bin/env python3


def GetFilenameAndIsosub(name):
    isosub = []
    if "=" in name:
        fname,extra = name.rsplit("=")
        isosub = [int(x) for x in extra.split(",")]
        isosub.sort()
    else:
        fname=name
    return fname,isosub


if __name__ == "__main__":

    from ndfes.normalmode import Vibrator
    import sys
    import argparse

    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
          Performs normal mode analysis and outputs a thermochemistry report.
          The input can be a Gaussian output file or a Hessian NetCDF4 file.

          If the input is a Gaussian file, then the coordinates, energy, and 
          Hessian are extracted from the first archive section containing a
          Hessian.

          If the input is a Hessian NetCDF4 file, then the file is first 
          searched to see if a normal mode analysis has been performed. If the
          normal modes are not found in the NetCDF4 file, then an analysis is 
          performed and the results are written to the NetCDF4; otherwise the
          results are directly read from the file.

          The following example prints a thermochemistry report and
          saves all vibrations to a XYZ+vib file.

          fetkutils-nma-thermochem.py --stop 10000 --xyz mol.vib.xyz mol.nc

          The following example prints a thermochemistry report upon
          isotopically subtituting atom 5 with the 2nd most abundant mass.

          fetkutils-nma-thermochem.py mol.nc=5

          """ )

    # parser.add_argument \
    #     ("--iso",
    #      help="Isotopically substitute the specified atoms with the 2nd most"+
    #      " abundant mass. All other atoms use the most abundant isotope mass."+
    #      " This is a 1-based integer. This option can be used more than once.",
    #      type=int,
    #      action='append')
    
    parser.add_argument \
        ("--parmidx",
         help="Interpret the isotopic substitution atom integers as the "+
         "1-based atom indexes appearing in the original Amber parameter "+
         "file. This is only meaningful when reading a Hessian NetCDF4 file.",
         action='store_true')
    
    parser.add_argument \
        ("--temp","-T",
         help="Temperature (K). Default: 298.15",
         type=float,
         default=298.15)

    parser.add_argument \
        ("--start",
         help="First vibration to print to xyz. Default: 1",
         type=int,
         default=1)

    parser.add_argument \
        ("--stop",
         help="Last vibration to print to xyz. Default: 1",
         type=int,
         default=1)

    parser.add_argument \
        ("--xyz",
         help="Output filename containing vibrations in XYZ+vib format."+
         " Animations of the vibrations can be viewed with jmol: "+
         "https://jmol.sourceforge.net",
         type=str,
         required=False )

    
    parser.add_argument \
        ("--save",
         help="Save the isotopomer eigensolution to the netcdf file",
         action="store_true")
    
    parser.add_argument \
        ('input',
         help="The Hessian NetCDF4 or Gaussian output filename. "+
         "One can make isotopically substitute an atom with the "+
         "second most abundant mass by postfixing the filename with: "+
         "=I, where I is a 1-based integer. To substitute multiple "+
         "atoms, use commas; e.g., filename=I,J",
         type=str)

    
    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))

    
    args = parser.parse_args()

    fname,isosub = GetFilenameAndIsosub(args.input)
    
    #isosub = []
    #if args.iso is not None:
    #    isosub = [ item for item in args.iso ]
    #isosub.sort()
    
    vib = Vibrator.from_file(fname,isosub,args.parmidx,save=args.save)

    vib.PrintFullReport(sys.stdout,args.temp)
    
    if args.xyz is not None:
        nmode = vib.GetNumModes()
        stop = min(args.stop+1,nmode+1)
        vibs = [ i-1 for i in range(args.start,stop) ]
        fh = open(args.xyz,"w")
        vib.PrintVibrations(fh,vibs)
        fh.close()
        
