#!/usr/bin/env python3

def CreateHessianNetCDF(fname,natoms,has_box,conmat):
    #dst = Dataset(fname,"w",format="NETCDF4")
    dst = Dataset(fname,"w",format="NETCDF3_64BIT_OFFSET")

    hessize = 3*natoms
    
    dst.Conventions = "HESSIAN"
    dst.ConventionVersion = "1.0"
    
    nisoDID = dst.createDimension("niso", None)
    cntDID = dst.createDimension("cnt", 1)
    #hesscolsDID = dst.createDimension("hesscols", hesssize)
    spatialDID = dst.createDimension("spatial", 3)
    natomsDID = dst.createDimension("natoms", natoms)
    natoms3DID = dst.createDimension("natoms3", natoms3)

    if conmat is not None:
        if conmat.shape[0] > 0:
            nconDID = dst.createDimension("ncon", conmat.shape[0])
            con5DID = dst.createDimension("con5", 5)

    # i4 f8 c
    
    spatialVID = dst.createVariable\
        ("spatial",'c',dimensions=(spatialDID,))
    
    coordVID = dst.createVariable\
        ("coord",'f8',dimensions=(natomsDID,spatialDID))
    coordVID.units = "bohr"
    
    atidxsVID = dst.createVariable\
        ("atidxs",'i4',dimensions=(natomsDID,))
    atidxsVID.units="unitless"
    
    atnumsVID = dst.createVariable\
        ("atnums",'i4',dimensions=(natomsDID,))
    atnumsVID.units="unitless"
    
    nsavedcolsVID = dst.createVariable\
        ("nsavedcols",'i4',dimensions=(cntDID,))
    colidxVID.units = "unitless"

    colidxVID = dst.createVariable\
        ("colidx",'i4',dimensions=(natoms3DID,))
    colidxVID.units = "1-based column index"

    colvalsVID = dst.createVariable\
        ("colvals",'f8',dimensions=(natoms3DID,natoms3DID))
    colvalsVID.units = "hartree/bohr/bohr"
    
    nvibVID = dst.createVariable\
        ("nbvib",'i4',dimensions=(nisoDID,))
    nvibVID.units = "unitless"

    isomaskVID = dst.createVariable\
        ("isomask",'i4',dimensions=(nisoDID,natomsDID))
    isomaskVID.units = "unitless"

    atmassVID = dst.createVariable\
        ("atmass",'f8',dimensions=(nisoDID,natomsDID))
    atmassVID.units = "au mass"

    redmassVID = dst.createVariable\
        ("redmass",'f8',dimensions=(nisoDID,natoms3DID))
    redmassVID.units = "au mass"

    xmatVID = dst.createVariable\
        ("xmat",'f8',dimensions=(nisoDID,natoms3DID,natoms3DID))
    xmatVID.units = "au"

    freqsVID = dst.createVariable\
        ("freqs",'f8',dimensions=(nisoDID,natoms3DID))
    freqsVID.units = "au"

    dispmatVID = dst.createVariable\
        ("dispmat",'f8',dimensions=(nisoDID,natoms3DID,natoms3DID))
    dispmatVID.units = "au"

    if has_box:
        labelDID = dst.createDimension("label", 5)
        cell_spatialDID = dst.createDimension("cell_spatial", 3)
        cell_angularDID = dst.createDimension("cell_angular", 3)
        
        cell_spatialVID = dst.createVariable\
            ("cell_spatial",'c',dimensions=(cell_spatialDID,))
        
        cell_angularVID = dst.createVariable\
            ("cell_angular",'c',dimensions=(cell_spatialDID,labelDID))
        
        cell_lengthsVID = dst.createVariable\
            ("cell_lengths",'f8',dimensions=(cell_spatialDID,))
        cell_lengthsVID.units="bohr"
        
        cell_anglesVID = dst.createVariable\
            ("cell_angles",'f8',dimensions=(cell_angularDID,labelDID))
        cell_anglesVID.units = "degree"
        
    if conmat is not None:
        if conmat.shape[0] > 0:
            conmatVID = dst.createVariable\
                ("conmat",'i4',dimensions=(nconDID,con5DID))
        
    dst.set_fill_off()

    if has_box:
        cell_spatialVID[:] = ["x","y","z"]
        cell_angularVID[:,:] = ["alpha","beta ","gamma"]

    if conmat is not None:
        if conmat.shape[0] > 0:
            conmatVID[:,:] = conmat[:,:]

    nsavedcolsVID[:] = 0
        
    return dst


if __name__ == "__main__":

    import glob
    import argparse
    import pathlib
    import numpy as np
    from netCDF4 import Dataset
    #import netCDF4

    
    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
          Combine NetCDF3 Hessian files. Each input NetCDF file is expected
          to contain only a portion of the full Hessian. The columns are
          combined and a new NetCDF3 file is written that contains the
          full Hessian. An error is thrown if there are missing columns.""" )


    parser.add_argument \
        ("--delete",
         help="Delete the isotopologues",
         action='store_true')

          
    parser.add_argument \
        ("-o","--out",
         help="The output NetCDF4 Hessian file",
         type=str,
         required=True)
          
    
    parser.add_argument \
         ('netcdf',
          help="One-or-more input NetCDF Hessian files",
          nargs='+',
          action='append',
          type=str)

    

    try:
        import pkg_resources
        version = pkg_resources.require("fetkutils")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))


    args = parser.parse_args()

    
    inphess = [pathlib.Path(fname)
               for sublist in args.netcdf
               for item in sublist
               for fname in glob.glob(item) ]
    
    for hessfile in inphess:
        if not hessfile.is_file():
            raise Exception("NetCDF3 file not found: %s"%(hessfile))

    if len(inphess) < 1:
        raise Exception("No input NetCDF3 files were provided")

    #for hessfile in inphess:
    #    rootgrp = Dataset(hessfile,"r",format="NETCDF4")
    #    print(rootgrp)

    src = Dataset(inphess[0],"r",format="NETCDF3_64BIT_OFFSET")
    
    natoms = src.dimensions["natoms"].size
    natoms3 = 3*natoms
    has_box = False
    if "cell_spatial" in src.dimensions:
        has_box = True
        #lengths = np.zeros( (3,) )
        #angles = np.zeros( (3,) )
    #atidxs = np.zeros( (natoms,), dtype=int )
    #atnums = np.zeros( (natoms,), dtype=int )
    #atcrds = np.zeros( (natoms,3) )
    colidx = np.array( [i+1 for i in range(natoms3)], dtype=int )
    colseen = [ False ]*natoms3
    H = np.zeros( (natoms3,natoms3) )

    has_ncon = ( "ncon" in src.dimensions )
    if has_ncon:
        ncon = src.dimensions["ncon"].size

    nsavedcols = src.variables["nsavedcols"][0]
        
    for i,idx in enumerate(src.variables["colidx"][:nsavedcols]):
        cidx = idx-1
        if colseen[cidx]:
            print(f"Warning: repeated Hessian column ({idx}) in {inphess[0]}")
        colseen[cidx] = True
        H[:,cidx] = src.variables["colvals"][i,:]

    coords_error = []
    atnums_error = []
    atidxs_error = []
    ncon_error = []
    conmat_error = []
    for ihess in inphess[1:]:
        tmp = Dataset(ihess,"r",format="NETCDF3_64BIT_OFFSET")
        
        nsavedcols = tmp.variables["nsavedcols"][0]
        for i,idx in enumerate(tmp.variables["colidx"][:nsavedcols]):
            cidx=idx-1
            if colseen[cidx]:
                print(f"Warning: repeated Hessian column ({idx}) in {ihess}")
            colseen[cidx] = True
            H[:,cidx] = tmp.variables["colvals"][i,:]
            
        if not np.allclose( src.variables["coordinates"][:,:],
                            tmp.variables["coordinates"][:,:] ):
            coords_error.append(ihess)
        if not np.allclose( src.variables["atnums"][:],
                            tmp.variables["atnums"][:] ):
            atnums_error.append(ihess)
        if not np.allclose( src.variables["atidxs"][:],
                            tmp.variables["atidxs"][:] ):
            atidxs_error.append(ihess)
        this_has_ncon = ("ncon" in tmp.dimensions)
        if has_ncon + this_has_ncon == 1:
            ncon_error.append(ihess)
        elif has_ncon:
            if not np.allclose( src.variables["conmat"][:,:],
                                tmp.variables["conmat"][:,:] ):
                conmat_error.append(ihess)

    has_error = False
    
    missing = []
    for i,x in enumerate(colseen):
        if not x:
            missing.append(i+1)
    if len(missing) > 0:
        has_error = True
        print(f"Missing Hessian columns: {missing}")

    if len(coords_error):
        print(f"coords mismatch between {inphess[0]} and {coords_error}")
        has_error = True
        
    if len(atnums_error):
        print(f"atnums mismatch between {inphess[0]} and {atnums_error}")
        has_error = True
        
    if len(atidxs_error):
        print(f"atidxs mismatch between {inphess[0]} and {atidxs_error}")
        has_error = True
        
    if len(ncon_error):
        print(f"ncon mismatch between {inphess[0]} and {atidxs_error}")
        has_error = True

    if len(conmat_error):
        print(f"conmat mismatch between {inphess[0]} and {atidxs_error}")
        has_error = True
        
    if has_error:
        raise Exception("Encountered an error")


    #
    # Symmetrize the Hessian
    #
    Hdiff = np.abs(H-H.T).flatten()
    avgdiff = np.mean(Hdiff)
    maxdiff = np.amax(Hdiff)
    H = 0.5*(H + H.T)

    print(f"Avg and max asymmetry in the Hessian (au): {avgdiff}, {maxdiff}")
    print(f"Hessian has been symmetrized")

    #
    # Copy netcdf from src to a new variable dst
    # Replace the "hesscols" dimension to be 3*natoms (rather than unlimited)
    # Replace the "colidx" variable to be 1 .. 3*natoms
    # Replace the "colvals variable to be the full Hessian
    #

    print(f"Writing full Hessian to: {args.out}")
    dst = Dataset(args.out,"w",format="NETCDF3_64BIT_OFFSET")

    exclusionlist = ["colidx","colvals","nsavedcols"]

    if args.delete:
        exclusionlist.extend( ["atmass","dispmat","freqs","isomask","nvib","redmass","xmat"] )
    
    # copy global attributes all at once via dictionary
    dst.setncatts(src.__dict__)
        
    # copy dimensions
    for name, dimension in src.dimensions.items():
        #if name == "hesscols":
        #    # Replace with the fixed, full size of the Hessian
        #    dst.createDimension( name, natoms3 )
        #else:
        dst.createDimension(
            name, (len(dimension)
                   if not dimension.isunlimited() else None))

    # copy all file data except for the excluded
    for name, variable in src.variables.items():
        x = dst.createVariable(name, variable.datatype, variable.dimensions)

    dst.set_fill_off()

    for name, variable in src.variables.items():
        # Do not copy the colidx and colvals variables
        if name not in exclusionlist:
            dst[name][:] = src[name][:]
        # copy variable attributes all at once via dictionary
        dst[name].setncatts(src[name].__dict__)

    # Manually set the colidx and colvals variables
    dst.variables["colidx"][:] = colidx[:]
    dst.variables["colvals"][:,:] = H[:,:]
    dst.variables["nsavedcols"][:] = sum( [ colseen[x] for x in colseen ] )

    dst.close()
        

    
