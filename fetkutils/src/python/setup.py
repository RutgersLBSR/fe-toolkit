#!/usr/bin/env python3

from setuptools import find_packages, setup
from glob import glob

install_requires = [
    #'matplotlib>=3.5.1',
    'matplotlib',
    #'numpy',
    'numpy<2',
    'scipy',
    'netCDF4',
    'rdkit'
    ]

# can use 'joblib', but not required

package_data = {}

#scripts = []
scripts = glob("bin/*.py")

setup( name="fetkutils",
       version="3.3",
       description="FE-ToolKit utility library and scripts",
       author="Timothy J. Giese",
       author_email="TimothyJGiese@gmail.com",
       platforms=["any"],
       license="MIT",
       url=None,
       python_requires='>3.5',
       install_requires=install_requires,
       include_package_data=True,
       package_data=package_data,
       scripts=scripts,
       packages=["fetkutils"],
       package_dir={"fetkutils": "./lib/fetkutils"} )

