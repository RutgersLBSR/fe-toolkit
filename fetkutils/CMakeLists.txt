# check if we are building as part of AmberTools
# PROJECT_NAME will be overriden by `project` command,
# so we need to check it before calling `project`
if (NOT DEFINED IS_AMBERTOOLS)
  if (PROJECT_NAME STREQUAL "Amber")
    set(IS_AMBERTOOLS TRUE)
  else()
    set(IS_AMBERTOOLS FALSE)
  endif()
endif()

#
# Let's use 3.10, if possible, because that's the highest
# requirement of any part of AmberTools
#
if (IS_AMBERTOOLS)
  cmake_minimum_required(VERSION 3.10)
else()
  cmake_minimum_required(VERSION 3.12)
endif()

#
# Fortran is needed so we get -lgfortran when linking to some
# blas/lapack implementations
#
if(NOT DEFINED FETOOLKITVERSION)
  set(FETOOLKITVERSION 0.0)
endif()

project(fetkutils VERSION "${FETOOLKITVERSION}" LANGUAGES NONE)


if(NOT IS_AMBERTOOLS)
  
  if (SKBUILD)

    install(FILES src/python/bin/fetkutils-tischedule.py
      DESTINATION bin
      PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ GROUP_EXECUTE GROUP_READ WORLD_EXECUTE WORLD_READ)
    #
    # We can't install scripts.py to the installation prefix
    # That would be, like, installing to /usr/scripts.py
    #
    install(FILES src/python/scripts.py DESTINATION .)
  endif()


  if(BUILD_PYTHON)
    add_subdirectory(src/python)
  endif()

else()
  
  if(NOT ${nlopt_DISABLED})
    
    if(BUILD_PYTHON)
      add_subdirectory(src/python)
    endif()

  endif()

endif()
