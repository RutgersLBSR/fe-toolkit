#!/bin/bash
set -e
set -u

git ls-files | grep -vE 'public|examples|gitlab|gitignore|build|README.txt|install-python|mkrelease|pyproject.toml|python/extra|AMBER|filelist|scripts.py|diffchk' > filelist.txt


DEST=${AMBERHOME}/AmberTools/src/fe-toolkit
if [ ! -d "${DEST}" ]; then
    echo "Directory not found: ${DEST}"
fi

if [ -d rsync_tmp ]; then
    rm -fr rsync_tmp
fi

rsync -au --files-from=filelist.txt ./ rsync_tmp/

cd rsync_tmp
rsync -avc --ignore-times --delete ./ ${DEST}/
cd ../

if [ -d rsync_tmp ]; then
    rm -fr rsync_tmp
fi

if [ -e filelist.txt ]; then
   rm filelist.txt
fi      

