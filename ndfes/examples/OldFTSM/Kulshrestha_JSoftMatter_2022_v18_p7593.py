#!/usr/bin/env python3

def relative_symlink(src, dst):
    import os
    d   = os.path.dirname(dst)
    Src = os.path.relpath(src, d)
    Dst = os.path.join(d, os.path.basename(dst))
    return os.symlink(Src, Dst)


def GetDirName(it):
    name = None
    if it == 0:
        name = "init"
    elif it > 0:
        name = "it%03i"%(it)
    return name


def ReadPCVBias(plumed):
    import numpy as np
    import re
    rc=np.zeros( (2,) )
    fc=np.zeros( (2,) )
    fh = open(plumed,"r")
    for line in fh:
        if "epcv1:" in line:
            m = re.match(r".*AT=([0-9\.\-\+Ee]+).*",line)
            if m:
                rc[0] = float(m.groups()[0])
            else:
                raise Exception(f"Failed to find AT= on line {line}")
            m = re.match(r".*KAPPA=([0-9\.\-\+Ee]+).*",line)
            if m:
                fc[0] = float(m.groups()[0])
            else:
                raise Exception(f"Failed to find KAPPA= on line {line}")
        if "epcv2:" in line:
            m = re.match(r".*AT=([0-9\.\-\+Ee]+).*",line)
            if m:
                rc[1] = float(m.groups()[0])
            else:
                raise Exception(f"Failed to find AT= on line {line}")
            m = re.match(r".*KAPPA=([0-9\.\-\+Ee]+).*",line)
            if m:
                fc[1] = float(m.groups()[0])
            else:
                raise Exception(f"Failed to find KAPPA= on line {line}")
    return rc,fc


def FillMdin(odir_path,isim,tmdin):
    img = "img%03i"%(isim+1)
    fname = str(odir_path / (img+".mdin"))
    fh = open(fname,"w")
        
    found_disang=False
    found_dumpave=False
    found_plumedfile=False
    for line in tmdin:
        if "DISANG" in line:
            m = re.match(r"DISANG *=.*",line)
            if m:
                line = re.sub(r"DISANG *=.*",
                              "DISANG=img%03i.disang"%(isim+1),
                              line)
                found_disang=True
        if "DUMPAVE" in line:
            m = re.match(r"DUMPAVE *=.*",line)
            if m:
                line = re.sub(r"DUMPAVE *=.*",
                              "DUMPAVE=img%03i.dumpave"%(isim+1),
                              line)
                found_dumpave=True
                
        if "plumedfile" in line:
            line = re.sub(r"plumedfile *= *([^, ]+)",
                          f"plumedfile = \"{img}.plumed\"",
                          line)
            found_plumedfile=True
                
        fh.write(line+"\n")
        
    if not found_disang:
        raise Exception("DISANG field was not found in "
                            +"template mdin")
    if not found_dumpave:
        raise Exception("DUMPAVE field was not found in "
                            +"template mdin")
    if not found_plumedfile:
        raise Exception("plumedfile field was not found in "
                            +"template mdin")
    fh.close()



def SimpleBspline(grid,bins,order,nrc,pt):
    from ndfes.Bspline import CptBsplineValues
    from ndfes import LinearWtsToMeshWts
    from ndfes import LinearPtsToMeshPts
    def INTWRAP(i,n):
        return (i%n+n)%n

    lidxs = []
    lwts = []
    for dim in range(grid.ndim):
        mydim = grid.dims[dim]
        lbidx,lwt = CptBsplineValues(pt[dim],mydim.xmin,mydim.width,order)
        if mydim.isper:
            for idx in range(len(lbidx)):
                lbidx[idx] = INTWRAP(lbidx[idx],mydim.size)
        else:
            for idx in range(len(lbidx)):
                lbidx[idx] = min(max(0,lbidx[idx]),mydim.size-1)
        lidxs.append(lbidx)
        lwts.append(lwt)
    lwts=np.array(lwts)
    cwts = LinearWtsToMeshWts(lwts)
    midxs = LinearPtsToMeshPts(lidxs)
    gidxs = [ grid.CptGlbIdxFromBinIdx(midx)
              for midx in midxs ]

    val = np.zeros((nrc,))
    wt = 0
    for ii,gidx in enumerate(gidxs):
        if gidx in bins:
            w = cwts[ii]
            wt += w
            val += w*bins[gidx].avgs
    if wt > 0:
        val /= wt
    else:
        val = [None]*nrc
    return val


class PCVBin(object):
    def __init__(self,gidx,bidxs,nrc):
        import copy
        import numpy as np
        self.gidx = gidx
        self.bidxs = copy.deepcopy(bidxs)
        self.nsamples = 0
        self.incavgs = np.zeros( (nrc,2) )
        self.avgs = np.zeros( (nrc,) )

    def append(self,pt,isper):
        import numpy as np
        from ndfes.GridUtils import BasicWrap
        ndim = self.avgs.shape[0]
        n = self.nsamples + 1
        for dim in range(ndim):
            if isper[dim]:
                ss = np.sin(np.deg2rad(pt[dim]))
                cs = np.cos(np.deg2rad(pt[dim]))
                self.incavgs[dim,0] += (ss-self.incavgs[dim,0])/n
                self.incavgs[dim,1] += (cs-self.incavgs[dim,1])/n
                m = np.rad2deg(np.atan2(self.incavgs[dim,0],
                                        self.incavgs[dim,1]))
                self.avgs[dim] = BasicWrap(m,360)
            else:
                self.incavgs[dim,0] += (pt[dim]-self.incavgs[dim,0])/n
                self.avgs[dim] = self.incavgs[dim,0]


class Binner(object):
    def __init__(self,isper,widths):
        import copy
        import numpy as np
        from collections import defaultdict as ddict
        self.isper = copy.deepcopy(isper)
        self.widths = np.array(widths,copy=True)
        self.ndim = len(self.isper)
        self.xmins = np.array([1.e+30]*self.ndim)
        self.xmaxs = np.array([-1.e+30]*self.ndim)
        self.bins = ddict(int)
        self.grid = None
        for dim in range(self.ndim):
            if self.isper[dim]:
                self.xmaxs[dim] = 360
                self.xmins[dim] = 0

                
    def Enlarge(self,pts):
        mins = np.amin(pts,axis=0)
        maxs = np.amax(pts,axis=0)
        for dim in range(self.ndim):
            if not self.isper[dim]:
                self.xmins[dim] = min(self.xmins[dim],mins[dim])
                self.xmaxs[dim] = max(self.xmaxs[dim],maxs[dim])

                
    def FinalizeGrid(self):
        from ndfes import SpatialDim
        from ndfes import VirtualGrid

        dims = []
        for dim in range(self.ndim):
            if self.isper[dim]:
                xmin = 0
                xmax = 360
                width = xmax-xmin
                size = int( 0.5 + width / self.widths[dim] )
                self.widths[dim] = width / size
                self.xmins[dim] = xmin
                self.xmaxs[dim] = xmax
            else:
                w = self.widths[dim]

                clo = int(np.floor(self.xmins[dim]/w))
                chi = int(np.ceil(self.xmaxs[dim]/w))
                self.xmins[dim] = clo * w
                self.xmaxs[dim] = chi * w
                size = int(chi-clo)
                #if center[dim]:
                #    self.xmins[dim] -= 0.5*w
                #    self.xmaxs[dim] += 0.5*w
                #    size += 1
            dims.append( SpatialDim( self.xmins[dim], self.xmaxs[dim],
                                     size, self.isper[dim] ) )
        self.grid = VirtualGrid( dims )

        
    def append(self,pcv,pts):
        sz = pcv(pts)
        nrc = pts.shape[1]
        npt = pts.shape[0]
        
        for i in range(npt):
            bidxs = self.grid.GetBinIdx(sz[i,:])
            gidx = self.grid.CptGlbIdxFromBinIdx(bidxs)
            if gidx in self.bins:
                mybin = self.bins[gidx]
            else:
                mybin = PCVBin(gidx,bidxs,nrc)
            mybin.append( pts[i,:], pcv.isper )
            self.bins[gidx] = mybin

    
    def GetBin(self,pt):
        mybin = None
        bidxs = self.grid.GetBinIdx(pt)
        gidx = self.grid.CptGlbIdxFromBinIdx(bidx)
        if gidx in self.bins:
            mybin = self.bins[gidx]
        return mybin
    

    def GetRegGridCenters(self):
        grids = []
        ndim=self.grid.ndim
        for d in range(ndim):
            dim = self.grid.dims[d]
            grids.append( [ (i+0.5)*dim.width + dim.xmin
                            for i in range(0,dim.size) ] )
            #print(len(grids[-1]))
        mgrid = np.array( np.meshgrid(*grids,indexing='ij') )
        return mgrid

    
    def GetBinValues(self,dim,pts=None):
        vs = []
        occbins = self.bins
        if pts is None:
            vs = [ sbin.avgs[dim] for gidx,sbin in sorted(occbins.items()) ]
        else:
            for pt in pts:
                gidx = self.grid.GetGlbBinIdx(pt)
                if gidx in occbins:
                    vs.append( occbins[gidx].avgs[dim] )
                else:
                    vs.append( None )
        return vs

    
    def GetOccMask(self,pts):
        mask=[]
        occbins = self.bins
        for gidx in [ self.grid.GetGlbBinIdx(pt) for pt in pts ]:
            if gidx in occbins:
                mask.append(True)
            else:
                mask.append(False)
        return mask

    
    def GetRegGridExtent(self):
        ndim = self.grid.ndim
        extents = []
        for d in range(ndim):
            dim = self.grid.dims[d]
            extents.append( dim.xmin )
            extents.append( dim.xmax )
        return extents

    
    def GetInterpolationGrid(self,sizes):
        import numpy as np
        
        ndim = self.grid.ndim
        extents = np.array(self.GetRegGridExtent())
        extents.resize(ndim,2)
        grids=[ np.linspace(extents[i,0],extents[i,1],sizes[i])
                for i in range(ndim) ]
        return np.array( np.meshgrid(*grids,indexing='ij') )


    def GetValues(self,pts,order):
        ndim = 0
        for gidx in self.bins:
            ndim = self.bins[gidx].avgs.shape[0]
            break
        
        vals = []
        for pt in pts:
            v = SimpleBspline(self.grid,self.bins,order,ndim,pt)
            vals.append(v)
        return vals

    
            

if __name__ == "__main__":

    import ndfes
    import numpy as np
    import argparse
    import sys
    from pathlib import Path
    import glob
    import re
    import os
    import shutil
    
    import ndfes.amber
    from ndfes.amber import Disang
    from ndfes import WrapAngleRelativeToRef
    from ndfes import MeanAngleRelativeToRef
    from ndfes.PathData import ReadPaths
    from ndfes.PathData import PathOpt
    from ndfes.PathData import PathIter
    from ndfes.PathData import PathSims
    from ndfes.PathData import PathSpl
    #from ndfes.FTSM import CopyFiles

    
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="Perform the FTSM described in Kulshrestha https://doi.org/10.1039/D2SM00888B")

    parser.add_argument \
        ("-d","--disang",
         help="template disang file",
         type=str,
        required=True )
    
    parser.add_argument \
        ("-i","--mdin",
         help="template mdin file",
         type=str,
         required=True )

    parser.add_argument \
        ("--curit",
         type=int,
         required=True,
         help="Index of the current iteration. The current directory is "
         +"assumed to be itXXX, where XXX is a zero-padded integer, "
         +"specified by --curit.  If --curit=0, then the directory name "
         +"is assumed to be init")

    parser.add_argument \
        ("--odir",
         type=str,
         required=False,
         default=None,
         help="Output directory. If unspecified, it is itXXX, where XXX "
         +"is XXX + 1")

    parser.add_argument \
        ("--start",
         type=float,
         required=False,
         default=0.,
         help="Skip the first fraction of samples as equilibration. Default: 0.  To exclude the first half of the simulation as equilibration, set --start=0.5")

    parser.add_argument \
        ("--linear",
         action='store_true',
         help="Use piecewise linear spline [Default if neither --akima nor --linear are specified]")
    
    parser.add_argument \
        ("--akima",
         action='store_true',
         help="Use akima spline")
    
    parser.add_argument \
        ("--no-smooth",
         action='store_true',
         help="Do not apply smoothing to the data when generating a new path")
    
    parser.add_argument \
        ("--varpcv",
         action='store_true',
         help="Use a variable PCV reference. Default is a fixed reference.")
    
    parser.add_argument \
        ("--img",
         action='store_true',
         help="Write images of the reaction coordinates in the pcv-space")
    
    
    args = parser.parse_args()


    #
    # Check if the template mdin file exists,
    # if so, store its contents in tmdin
    #
    
    mdin_path = Path(args.mdin)
    if not mdin_path.is_file():
        raise Exception("mdin %s does not exist"%(mdin_path))

    fh = open(mdin_path,"r")
    tmdin = fh.read().split("\n")
    fh.close()

    istep1 = 1
    found_plumed = False
    found_plumedfile = False
    founr_istep1 = False
    for line in tmdin:
        m = re.match(r".*[, ]istep1 *= *([0-9]+)",line)
        if m:
            istep1 = int(m.groups()[0])
            found_istep1 = True

        m = re.match(r".*[, ]plumed *= *([0-9]+)",line)
        if m:
            myflag = int(m.groups()[0])
            if myflag != 1:
                raise Exception("plumed=1 must be in mdin, but found: "
                                +f"{line}\n")
            found_plumed = True

        m = re.match(r".*[, ]plumedfile *= *([^, ]+)",line)
        if m:
            myflag = m.groups()[0].replace('"',"").replace("'","")
            if len(myflag) < 1:
                raise Exception("plumedfile= must be in mdin, but found: "
                                +f"{line}\n")
            found_plumedfile = True

    if not found_plumed:
        raise Exception(f"Did not find plumed keyword in {args.mdin}")
             
    if not found_plumedfile:
        raise Exception(f"Did not find plumedfile keyword in {args.mdin}")
    

    if args.curit < 0:

        odir_path = Path("init")
        
        disang_path = Path(args.disang)
        if not disang_path.is_file():
            raise Exception("Template disang %s does not exist"%(disang_path))

        print("Reading template disang: %s\n"%(args.disang))
    
        tdisang = ndfes.amber.Disang( args.disang )
        tdisang.ValidateTemplate()
        idxs = tdisang.GetTemplateIdxs()
        idxsp1 = [ idx+1 for idx in idxs ]

        idisfiles = glob.glob( "init/img*.disang" )
        idisfiles.sort()
        idisangs = [ Disang(idis) for idis in idisfiles]
        allok = True
        for idis,ifile in zip(idisangs,idisfiles):
            ilen = len(idis.restraints)
            tlen = len(tdisang.restraints)
            if ilen != tlen:
                allok=False
                print(f"Inconsistent num. restraints {ilen} vs {tlen} in {ifile}")
        if not allok:
            print("Exiting without action; assuming disang->pcv has already been performed")
            exit(0)
            
        ndim = len(idxs)
        nsim = len(idisangs)

        pdisangs = [ idis.Subset(idxsp1) for idis in idisangs ]
        pcv = ndfes.PCV.from_disangs( pdisangs )

        nsimm1 = nsim-1
        pcvks = 2*2000.
        pcvkz = 2*250.

        for isim in range(nsim):
            idis = idisangs[isim]
            ifile = idisfiles[isim]
            plumed = ifile.replace(".disang",".plumed")
            bak = ifile + ".bak"
            ks = pcvks
            kz = pcvkz
            if isim == 0 or isim == nsimm1:
                ks = kz
            xdis = idis.SavePlumed(plumed,stride=istep1,pcv=pcv,
                                   pcvs0=(isim/nsimm1),
                                   pcvks=ks, pcvz0=0, pcvkz=kz)
            if not os.path.exists(bak):
                shutil.copyfile(ifile,bak)
            xdis.Save(ifile)
            FillMdin(odir_path,isim,tmdin)

        exit(0)
    
    
    prevdir = GetDirName( args.curit - 1 )
    curdir  = GetDirName( args.curit )
    nextdir = GetDirName( args.curit + 1 )
    if args.odir is not None:
        nextdir = args.odir
    
    cdir = Path( curdir )
    if not cdir.is_dir():
        raise Exception("Current directory does not exist %s"%(cdir))


    #
    # We need a reference path, which we assume is fixed and located
    # in the init/img001.plumed.pcv.pdb
    # We implicitly read it when we instantiate ndfes.PCV from
    # init/img001.plumed
    #

    
    refpath = "init/img001.plumed"
    if args.varpcv:
        refpath =  str(cdir / "img001.plumed")
    if not os.path.exists(refpath):
        raise Exception("File not found: {refpath}")
    pcv = ndfes.PCV.from_plumed(refpath)
    
    
    irsts = []
    idumps = []
    iplumeds = []
    for irst in sorted(glob.glob( str(cdir / "img*.rst7") )):

        try:
            iplumed = Path(irst).with_suffix(".plumed")
            if not iplumed.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding plumed %s"%(iplumed))

            p = Path(irst)
            
            ctraj = p.with_suffix(".plumed.dumpave")
            
            if not ctraj.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding dumpave %s"%(ctraj))
        
            irsts.append(irst)
            iplumeds.append(iplumed)
            idumps.append(ctraj)
            
            print("IMG: ")
            print("     restart: ",irsts[-1])
            print("     dumpave: ",idumps[-1])
            print("     plumed:  ",iplumeds[-1])
        except Exception as e:
            print(e)
        
    print("")


    #
    # Check if the output directory exists
    #
    
    odir_path = Path(nextdir)
    if not odir_path.is_dir():
        raise Exception("Directory %s does not exist"%(odir_path))


    #
    # Check if the template disang file exists
    #
    
    disang_path = Path(args.disang)
    if not disang_path.is_file():
        raise Exception("Template disang %s does not exist"%(disang_path))

    print("Reading template disang: %s\n"%(args.disang))
    
    tdisang = ndfes.amber.Disang( args.disang )
    tdisang.ValidateTemplate()
    idxs = tdisang.GetTemplateIdxs()
    idxsp1 = [ idx+1 for idx in idxs ]
    
    ndim = len(idxs)
    nsim = len(idumps)
    isper = [ tdisang.restraints[i].dihed for i in idxs ]
    
    
    rcs = []
    fcs = []
    means = []
    pmeans = []
    allsamples = []
    for i in range(nsim):

        #
        # These are the umbrella potential parameters for the
        # path collective variables
        #
        rc,fc = ReadPCVBias(iplumeds[i])
        rcs.append(rc)
        fcs.append(fc)

        #
        # The time series of the ndim primitive reaction coordinates
        # 
        data = np.loadtxt( idumps[i] )[ :, 1:ndim+1 ]

        #
        # Exclude some amount as equilibration
        #
        n = data.shape[0]
        istart = int(n*args.start)
        data = data[istart:,:]
        
        allsamples.append(np.array(data,copy=True))
        
        #
        # The mean of each primitive reaction coordinate, with
        # consideration of periodic angles
        #
        primmeans = []
        for dim in range(data.shape[1]):
            if isper[dim]:
                primmeans.append( MeanAngleRelativeToRef(data[:,dim],0) )
            else:
                primmeans.append( np.mean(data[:,dim]) )
        pmeans.append( primmeans )

        #
        # The mean of each path collective variable
        #
        means.append( np.mean(pcv(data),axis=0) )

        
    rcs = np.array(rcs)
    fcs = np.array(fcs)
    pts = np.array(means)
    pmeans = np.array(pmeans)

    print("PRE-SMOOTHING, PRE-DISCRETIZATION")
    print("obs: The observed centroid position")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pts[i,:]])
        p = "".join(["%14.5e"%(x) for x in pmeans[i,:]])
        print("isim: %3i rcs: %s obs: %s prim: %s"%(i,s,t,p))
        
    
    stype = 'linear'
    if args.akima:
        stype = 'akima'
    smooth = True
    if args.no_smooth:
        smooth=False

    #
    # A parametric curve of path collective variables
    #
    pathspl = PathSpl(stype,smooth,pts)

    #
    # Uniform discretization of the PCV spline
    #
    ts = np.linspace(0,1,nsim)
    pathuni = np.array( [ pathspl.GetValue(t) for t in ts ] )
    
    outsims = PathSims(pathuni,fcs)
    outiter = PathIter(pathspl,outsims)

    # # cerrs = np.zeros((nsim,))
    # # eps = 100.
    # # isper = np.array( [False]*ndim, dtype=bool )
    # # omeans = []
    # # for dim in range(ndim):
    # #     rbf = ndfes.RBF(pts,pmeans[:,dim],cerrs,eps,isper)
    # #     omeans.append( rbf.GetValues(pathuni).values )
    # # omeans = np.array(omeans).T

    if True:
        #
        # "Unwrap" the primitive reaction coordinates in a manner
        # to create a continuous curve without periodic wrapping
        #
        tvals = np.array(pmeans,copy=True)
        for dim in range(ndim):
            if isper[dim]:
                refang = 0
                for isim in range(nsim):
                    tvals[isim,dim] = WrapAngleRelativeToRef\
                        ( tvals[isim,dim], refang )
                    refang = tvals[isim,dim]
            
        #
        # If varpcv, then uniformly discretize the parametric
        # curve of primitive reaction coordinates; otherwise
        # lookup the values of the reaction coordinates at
        # the uniformly discretized SZ-curve.
        #
        splt = pathspl.spl.t
        spltype = 'linear'
        if args.varpcv:
            splt = None
            spltype = stype
        tspl = PathSpl(spltype,smooth,tvals) #,ts=splt)
        omeans = np.array([ tspl.GetValue(t) for t in ts ])

    else:

        pcvgrid = Binner([False,False],[0.05,0.005])
        for data in allsamples:
            pcvgrid.Enlarge(pcv(data))
        pcvgrid.FinalizeGrid()
        for data in allsamples:
            pcvgrid.append(pcv,data)

        if args.img:
            from matplotlib import pyplot as plt
            for dim in range(2):
                fig = plt.figure()
                fig.set_size_inches( 3.33, 3.0, forward=True)
                plt.rcParams.update({'font.size': 8})
                ax = plt.subplot(1,1,1)
                
                mgrid   = np.array( pcvgrid.GetRegGridCenters() )
                mgrid = pcvgrid.GetInterpolationGrid([101,101])
                nx = mgrid.shape[1]
                ny = mgrid.shape[2]
                gridpts = ndfes.GetPtsFromRegGrid(mgrid)
                mask    = pcvgrid.GetOccMask(gridpts)
                
                #vals    = np.array(pcvgrid.GetBinValues(dim,gridpts))
                vals    = [ avgs[dim] for avgs in pcvgrid.GetValues(gridpts,6) ]
                mvals = np.array(np.where(mask,vals,1.e+5),dtype=float).reshape(nx,ny)
                minval = min([ v for v in vals if v is not None ])
                maxval = max([ v for v in vals if v is not None ])
                colordata = ndfes.ColorAndContours(minval,maxval)
                extent = pcvgrid.GetRegGridExtent()
                dx = extent[1]-extent[0]  # xmax - xmin
                dy = extent[3]-extent[2]  # ymax - ymin
                aspect = dx/dy
                contour_kwargs = {
                    'extent': extent,  # range of plot
                    'aspect': aspect,  # shape of plot
                    'origin': 'lower', 
                    'vmin': colordata.Zmin,
                    'vmax': colordata.Zmax,
                    'cmap': colordata.cmap, # the colormap
                    'interpolation': None # heatmap smoothing method
                }
                ax.imshow(mvals.T,**contour_kwargs)
                cax = None
                cb,cticker = colordata.DrawColorbar(fig,pad=0.015,labelpad=0,cax=cax)
                cb.ax.tick_params(labelsize=8,rotation=90)
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(8)

                ax.plot(pathuni[:,0],pathuni[:,1],c='k',lw=2)
                
                fig.subplots_adjust(left=0.12,right=0.98,bottom=0.1,top=0.93,wspace=0,hspace=0)
                plt.savefig(f"foo.{dim}.png", dpi=300)

        
      
        primpts = []
        for t in np.linspace(0,1,max(101,pathuni.shape[0])):
            sz = pathspl.GetValue(t)
            xs = pcvgrid.GetValues([sz],6)[0]
            if xs[0] is not None:
                primpts.append(xs)
    
        tspl = PathSpl('linear',False,primpts)
        omeans = np.array([ tspl.GetValue(t) for t in ts ])

    
    
    print("POST-SMOOTHING, POST-DISCRETIZATION")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pathuni[i,:]])
        p = "".join(["%14.5e"%(x) for x in omeans[i,:]])
        print("isim: %3i rcs: %s obs: %s prim: %s"%(i,s,t,p))
        

    

        
    if args.curit == 0:
        inpsims = PathSims(rcs,fcs)
        inpspl = PathSpl(stype,smooth,rcs)
        inpiter = PathIter(inpspl,inpsims)
        opt = PathOpt( [inpiter,outiter] )
    else:
        oldopt = "%s/path.sims.xml"%(prevdir)
        if not os.path.exists(oldopt):
            sys.stderr.write(f"File not found: {oldopt}\n")
            opt = PathOpt([outiter])
        else:
            opt = PathOpt.from_file(oldopt)
            opt.iters.append(outiter)
    opt.SaveXml( "%s/path.sims.xml"%(curdir) )
        
    
    print("\nPreparing simulations in: %s\n"%(odir_path))

    if args.varpcv:
        newpcv = ndfes.PCV(omeans,pcv.wts,pcv.isangle,pcv.isper)
        for isim in range(nsim):
            base = "img%03i"%(isim+1)
            ofile = odir_path / (base + ".plumed")
            xdis = tdisang.SavePlumed\
                (str(ofile), stride=istep1,
                 pcv=newpcv,
                 pcvs0=(isim/(nsim-1)),
                 pcvz0=0,
                 pcvks=fcs[isim,0],
                 pcvkz=fcs[isim,1])
            xdis.Save( str(odir_path / (base+".disang")) )
    else:
        for isim in range(nsim):
            base = "img%03i"%(isim+1)
            ofile = odir_path / (base + ".plumed")
            xdis = tdisang.SavePlumed\
                (str(ofile), stride=istep1,
                 pcv=pcv,
                 pcvs0=pathuni[isim,0],
                 pcvz0=pathuni[isim,1],
                 pcvks=fcs[isim,0],
                 pcvkz=fcs[isim,1])
            xdis.Save( str(odir_path / (base+".disang")) )


    cdir_path = Path(curdir)
    for isim in range(nsim):
        img = "img%03i"%(isim+1)
        init = "init%03i"%(isim+1)
        oldrst = cdir_path / (img+".rst7")
        newrst = odir_path / (init+".rst7")
        if os.path.exists(newrst):
            os.remove(newrst)
        if os.path.islink(newrst):
            os.unlink(newrst)
        relative_symlink(oldrst,newrst)

        FillMdin(odir_path,isim,tmdin)
        
        
        
