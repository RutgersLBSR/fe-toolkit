#!/usr/bin/env python3

def CptFTM( parm, disang, trajfile, ntwx=1, dt=0.001, gamma=1500., gamma_ratio=None, start=0. ):
    # dt (ps)
    # gamma (inv ps)
    
    import ndfes
    import ndfes.amber
    from ndfes.constants import AU_PER_ATOMIC_MASS_UNIT
    from ndfes.constants import AU_PER_ANGSTROM
    from ndfes.constants import AU_PER_KCAL_PER_MOL
    from ndfes.constants import AU_TIME_SI
    import numpy as np
    
    # au time per second
    AU_PER_S = 1. / AU_TIME_SI()

    # au time per picosecond
    S_PER_PS = 1.e-12
    AU_PER_PS = AU_PER_S * S_PER_PS
    
    aidxs = disang.GetUniqueAtomIdxs()
    
    invms = np.zeros( (3*len(aidxs),) )

    if parm is not None:
        #
        # Read the masses from the parm file
        #
        for i,a in enumerate(aidxs):
            m = parm.atoms[a].mass * AU_PER_ATOMIC_MASS_UNIT()
            if m > 0:
                invms[0+i*3] = 1./m
                invms[1+i*3] = 1./m
                invms[2+i*3] = 1./m
    else:
        #
        # Assume all atoms are carbons
        #
        invms[:] =  1. / (12.*AU_PER_ATOMIC_MASS_UNIT())
        
    invms = np.diag( invms )
    
    n = len(disang.restraints)

    rcs = np.zeros( (n,) )
    for i in range(n):
        rcs[i] = disang.restraints[i].r2
    

    M = np.zeros( (n,n) )
    dG = np.zeros( (n,) )
            
    traj = ndfes.amber.ReadCrds(trajfile)
    nframes = traj.shape[0]

    istart = int(nframes*start)
    traj = traj[istart:,:,:]
    nframes = traj.shape[0]

    DT = dt * ntwx * nframes

    DT_fs = DT * 1000

    if gamma_ratio is not None:
        if gamma_ratio <= 0:
            gamma_ratio = 1500./20.
        gamma = gamma_ratio * DT_fs
        print("Overriding --gamma with --gamma-ratio: %12.3f (1/(ps*fs))"%(gamma_ratio))

    print("frames: %12i"%(nframes))
    print("dt:     %12.3f ps"%(DT))
    print("gamma:  %12.3f inv ps"%(gamma))


    fcs = np.array( [ res.rk2 * AU_PER_KCAL_PER_MOL()
                      for res in disang.restraints ] )
    q0s = np.array( [ res.r2
                      for res in disang.restraints ] )

    
    qs = []
    for i in range(nframes):
        crds = traj[i,aidxs,:]
        q,B = disang.GetWilsonB_FromCrds(crds,aidxs)
        B /= AU_PER_ANGSTROM()
        #print(B)
        qs.append(q)
        M += np.dot( B, np.dot( invms, B.T ) ) / nframes
    
    qs = np.array(qs)
    avgq = np.mean( qs, axis=0 )
    Delta =  avgq - q0s
    dG = - 2*fcs*Delta
    
    fact = (DT/gamma) * AU_PER_PS**2
    dQ = - fact * np.dot( M, dG )

    #print("Q0   ",q0s)
    #print("AvgQ ",avgq)
    #print("Delta",Delta)
    #print("dG   ",dG)
    #print("M    ",M)
    #print("dQ   ",dQ)
    #print("")
    #print( np.dot(dQ,Delta) / ( np.linalg.norm(dQ) * np.linalg.norm(Delta) ) )

    #dQ = dQ * np.linalg.norm(Delta)/np.linalg.norm(dQ)
    #print("".join(["%14.8f"%(x) for x in rcs + Delta]))
    
    return rcs + dQ,avgq
    #return rcs + Delta


def GetDirName(it):
    name = None
    if it == 0:
        name = "init"
    elif it > 0:
        name = "it%03i"%(it)
    return name

# def GetBeta(T):
#     import scipy.constants
#     Jperkcal = scipy.constants.calorie * 1000 / scipy.constants.Avogadro
#     boltz = scipy.constants.Boltzmann / Jperkcal
#     return 1./(boltz*T)



if __name__ == "__main__":

    import ndfes
    import numpy as np
    import argparse
    import sys
    from pathlib import Path
    import glob
    import re
    import os
    
    #from ndfes.FTSM import FTSMSim
    #from ndfes.FTSM import FTSMString
    #from ndfes.FTSM import FTSMOpt
    #from ndfes.FTSM import CheckConvAndSavePathPickles
    #from ndfes.FTSM import CopyFiles

    from ndfes.PathData import ReadPaths
    from ndfes.PathData import PathOpt
    from ndfes.PathData import PathIter
    from ndfes.PathData import PathSims
    from ndfes.PathData import PathSpl
    from ndfes.FTSM import CopyFiles

    
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="Perform the FTSM described in Ovichinnikov et al https://doi.org/10.1063/1.3544209")

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file. If not present, then all atoms are assumed to have a mass of 12 amu",
         type=str,
         default=None,
         required=False )
    
    parser.add_argument \
        ("-d","--disang",
         help="template disang file",
         type=str,
        required=True )
    
    parser.add_argument \
        ("-i","--mdin",
         help="template mdin file",
         type=str,
         required=True )

    parser.add_argument \
        ("--curit",
         type=int,
         required=True,
         help="Index of the current iteration. The current directory is "
         +"assumed to be itXX, where XX is a zero-padded integer, "
         +"specified by --curit.  If --curit=0, then the directory name "
         +"is assumed to be init")

    parser.add_argument \
        ("--odir",
         type=str,
         required=False,
         default=None,
         help="Output directory. If unspecified, it is itXX, where XX "
         +"is XX + 1")

    parser.add_argument \
        ("--gamma",
         type=float,
         required=False,
         default=1500.,
         help="Friction coefficient, gamma (inverse ps). Default: 1500.")

    parser.add_argument \
        ("--gamma-ratio",
         type=float,
         required=False,
         default=None,
         help="The value of gamma should follow the ratio: gamma =~ ((1500 inv ps)/(20 fs)) * (simtime fs). If you set --gamma-ratio=-1, then this ratio will be used. This is the same as specifying --gamma-ratio=75. (with units of (fs*ps)^{-1}). If --gamma-ratio > 0, then the specified ratio is used. If --gamma-ratio is not used, then the value of gamma is taken directly from --gamma")

    parser.add_argument \
        ("--start",
         type=float,
         required=False,
         default=0.,
         help="Skip the first fraction of samples as equilibration. Default: 0.  To exclude the first half of the simulation as equilibration, set --start=0.5")

    parser.add_argument \
        ("--linear",
         action='store_true',
         help="Use piecewise linear spline [Default if neither --akima nor --linear are specified]")
    
    parser.add_argument \
        ("--akima",
         action='store_true',
         help="Use akima spline")
    
    parser.add_argument \
        ("--no-smooth",
         action='store_true',
         help="Do not apply smoothing to the data when generating a new path")
    
    
    args = parser.parse_args()



    #
    # Check if the template mdin file exists,
    # if so, store its contents in tmdin
    #
    
    mdin_path = Path(args.mdin)
    if not mdin_path.is_file():
        raise Exception("mdin %s does not exist"%(mdin_path))

    fh = open(mdin_path,"r")
    tmdin = fh.read().split("\n")
    fh.close()

    dt = 0.001
    ntwx = 1
    for line in tmdin:
        if "dt" in line:
            m = re.match(r",* *dt *= *([0-9\.]+)",line)
            if m:
                dt = float(m.groups()[0])
        if "ntwx" in line:
            m = re.match(r",* *ntwx *= *([0-9-]+)",line)
            if m:
                #print(m,m.groups())
                ntwx = int(m.groups()[0])
                if ntwx < 1:
                    raise Exception(f"Invalid ntwx: {ntwx}")
                

    
    prevdir = GetDirName( args.curit - 1 )
    curdir = GetDirName( args.curit )
    nextdir = GetDirName( args.curit + 1 )
    if args.odir is not None:
        nextdir = args.odir
    
    cdir = Path( curdir )
    if not cdir.is_dir():
        raise Exception("Current directory does not exist %s"%(cdir))
    
    irsts = []
    itrajs = []
    idisangs = []
    for irst in sorted(glob.glob( str(cdir / "img*.rst7") )):

        try:
            idisang = Path(irst).with_suffix(".disang")
            if not idisang.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding disang %s"%(idisang))
        
            ctraj = Path(irst).with_suffix(".nc")
            if not ctraj.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding trajectory %s"%(ctraj))
        
            irsts.append(irst)
            idisangs.append(idisang)
            itrajs.append(ctraj)
            
            print("IMG: ")
            print("     restart: ",irsts[-1])
            print("     traj:    ",itrajs[-1])
            print("     disang:  ",idisangs[-1])
        except Exception as e:
            print(e)
        
    print("")


    parm = None
    if args.parm is not None:
        if not Path(args.parm).is_file():
            raise Exception("parm7 file %s does not exist"%(args.parm))
        import parmed
        parm = parmed.load_file( args.parm )
    else:
        print("Assuming all masses are 12 amu")

    #
    # Check if the output directory exists
    #
    
    odir_path = Path(nextdir)
    if not odir_path.is_dir():
        raise Exception("Directory %s does not exist"%(odir_path))


    #
    # Check if the template disang file exists
    #
    
    disang_path = Path(args.disang)
    if not disang_path.is_file():
        raise Exception("Template disang %s does not exist"%(disang_path))

    print("Reading template disang: %s\n"%(args.disang))
    
    tdisang = ndfes.amber.Disang( args.disang )
    tdisang.ValidateTemplate()
    idxs = tdisang.GetTemplateIdxs()

    ndim = len(idxs)
    nsim = len(itrajs)


    isangle = []
    for idx in idxs:
        isangle.append( tdisang.restraints[idx].angle )

    
    rcs = []
    fcs = []
    means = []
    obsmeans = []
    for i in range(nsim):
        disang = ndfes.amber.Disang( idisangs[i] )
        disang = disang.Subset([idx+1 for idx in idxs])

        rcs.append( [ res.r2 for res in disang.restraints ] )
        fcs.append( [ res.rk2 for res in disang.restraints ] )
    
        out,obs = CptFTM( parm, disang, itrajs[i],
                          ntwx = ntwx,
                          dt = dt,
                          gamma = args.gamma,
                          gamma_ratio = args.gamma_ratio,
                          start = args.start )
        means.append( out )
        obsmeans.append( obs )

    rcs = np.array(rcs)
    fcs = np.array(fcs)
    pts = np.array(means)
    obs = np.array(obsmeans)

    print("PRE-SMOOTHING, PRE-DISCRETIZATION")
    print("rcs: The umberella window centers")
    print("fts: The FTS predicted placement of the path")
    print("obs: The observed centroid position")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pts[i,:]])
        m = "".join(["%14.5e"%(x) for x in obs[i,:]])
        print("isim: %3i rcs: %s fts: %s obs: %s"%(i,s,t,m))
        
    
    stype = 'linear'
    if args.akima:
        stype = 'akima'
    smooth = True
    if args.no_smooth:
        smooth=False

    pathspl = PathSpl(stype,smooth,pts)
    obsspl  = PathSpl(stype,smooth,obs)

    ts = np.linspace(0,1,nsim)
    obsuni = np.array( [ obsspl.GetValue(t) for t in ts ] )
    pathuni = np.array( [ pathspl.GetValue(t) for t in ts ] )
    
    outsims = PathSims(pathuni,fcs)
    outiter = PathIter(pathspl,outsims)


    print("POST-SMOOTHING, POST-DISCRETIZATION")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pathuni[i,:]])
        m = "".join(["%14.5e"%(x) for x in obsuni[i,:]])
        print("isim: %3i rcs: %s fts: %s obs: %s"%(i,s,t,m))
        

    if args.curit == 0:
        inpsims = PathSims(rcs,fcs)
        inpspl = PathSpl(stype,smooth,rcs)
        inpiter = PathIter(inpspl,inpsims)
        opt = PathOpt( [inpiter,outiter] )
    else:
        oldopt = "%s/path.sims.xml"%(prevdir)
        if not os.path.exists(oldopt):
            sys.stderr.write(f"File not found: {oldopt}\n")
            opt = PathOpt([outiter])
        else:
            opt = PathOpt.from_file(oldopt)
            opt.iters.append(outiter)
    opt.SaveXml( "%s/path.sims.xml"%(curdir) )
        
    
    print("\nPreparing simulations in: %s\n"%(odir_path))

    CopyFiles\
        ( args.disang, args.mdin, args.curit, odir_path, None, None,
          False, False, args.curit, pathuni, fcs )
