#!/usr/bin/env python3


def GetDirName(it):
    name = None
    if it == 0:
        name = "init"
    elif it > 0:
        name = "it%03i"%(it)
    return name

if __name__ == "__main__":

    import ndfes
    import numpy as np
    import argparse
    import sys
    from pathlib import Path
    import glob
    import re
    import os
    
    from ndfes.PathData import ReadPaths
    from ndfes.PathData import PathOpt
    from ndfes.PathData import PathIter
    from ndfes.PathData import PathSims
    from ndfes.PathData import PathSpl
    from ndfes.FTSM import CopyFiles

    
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="Perform the FTSM described in Rosta et al. https://doi.org/10.1021/ja200173a")

    parser.add_argument \
        ("-d","--disang",
         help="template disang file",
         type=str,
        required=True )
    
    parser.add_argument \
        ("-i","--mdin",
         help="template mdin file",
         type=str,
         required=True )

    parser.add_argument \
        ("--curit",
         type=int,
         required=True,
         help="Index of the current iteration. The current directory is "
         +"assumed to be itXX, where XX is a zero-padded integer, "
         +"specified by --curit.  If --curit=0, then the directory name "
         +"is assumed to be init")

    parser.add_argument \
        ("--odir",
         type=str,
         required=False,
         default=None,
         help="Output directory. If unspecified, it is itXX, where XX "
         +"is XX + 1")

    parser.add_argument \
        ("--start",
         type=float,
         required=False,
         default=0.,
         help="Skip the first fraction of samples as equilibration. Default: 0.  To exclude the first half of the simulation as equilibration, set --start=0.5")

    parser.add_argument \
        ("--linear",
         action='store_true',
         help="Use piecewise linear spline [Default if neither --akima nor --linear are specified]")
    
    parser.add_argument \
        ("--akima",
         action='store_true',
         help="Use akima spline")
    
    parser.add_argument \
        ("--no-smooth",
         action='store_true',
         help="Do not apply smoothing to the data when generating a new path")
    
    
    args = parser.parse_args()



    #
    # Check if the template mdin file exists,
    # if so, store its contents in tmdin
    #
    
    mdin_path = Path(args.mdin)
    if not mdin_path.is_file():
        raise Exception("mdin %s does not exist"%(mdin_path))

    fh = open(mdin_path,"r")
    tmdin = fh.read().split("\n")
    fh.close()

    dt = 0.001
    for line in tmdin:
        if "dt" in line:
            m = re.match(r",* *dt *= *([0-9\.]+)",line)
            if m:
                dt = float(m.groups()[0])
                

    
    prevdir = GetDirName( args.curit - 1 )
    curdir = GetDirName( args.curit )
    nextdir = GetDirName( args.curit + 1 )
    if args.odir is not None:
        nextdir = args.odir
    
    cdir = Path( curdir )
    if not cdir.is_dir():
        raise Exception("Current directory does not exist %s"%(cdir))

    irsts = []
    idumps = []
    idisangs = []
    for irst in sorted(glob.glob( str(cdir / "img*.rst7") )):

        try:
            idisang = Path(irst).with_suffix(".disang")
            if not idisang.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding disang %s"%(idisang))

            p = Path(irst)
            # name = p.name
            # p = p.parent / "analysis" / "dumpaves" / cdir / p.name
            ctraj = p.with_suffix(".dumpave")
            if not ctraj.is_file():
                raise Exception("Restart %s does not have a "%(irst)+
                                "corresponding dumpave %s"%(ctraj))
        
            irsts.append(irst)
            idisangs.append(idisang)
            idumps.append(ctraj)
            
            print("IMG: ")
            print("     restart: ",irsts[-1])
            print("     dumpave: ",idumps[-1])
            print("     disang:  ",idisangs[-1])
        except Exception as e:
            print(e)
        
    print("")


    #
    # Check if the output directory exists
    #
    
    odir_path = Path(nextdir)
    if not odir_path.is_dir():
        raise Exception("Directory %s does not exist"%(odir_path))


    #
    # Check if the template disang file exists
    #
    
    disang_path = Path(args.disang)
    if not disang_path.is_file():
        raise Exception("Template disang %s does not exist"%(disang_path))

    print("Reading template disang: %s\n"%(args.disang))
    
    tdisang = ndfes.amber.Disang( args.disang )
    tdisang.ValidateTemplate()
    idxs = tdisang.GetTemplateIdxs()

    ndim = len(idxs)
    nsim = len(idumps)


    isangle = []
    for idx in idxs:
        isangle.append( tdisang.restraints[idx].angle )

    
    rcs = []
    fcs = []
    means = []
    for i in range(nsim):
        disang = ndfes.amber.Disang( idisangs[i] )
        disang = disang.Subset([idx+1 for idx in idxs])

        rcs.append( [ res.r2 for res in disang.restraints ] )
        fcs.append( [ res.rk2 for res in disang.restraints ] )
    
        data = np.loadtxt( idumps[i] )[ :, [ idx+1 for idx in idxs ] ]
        n = data.shape[0]
        istart = int(n*args.start)
        data = data[istart:,:]
        
        means.append( np.mean(data,axis=0) )

    rcs = np.array(rcs)
    fcs = np.array(fcs)
    pts = np.array(means)

    print("PRE-SMOOTHING, PRE-DISCRETIZATION")
    print("obs: The observed centroid position")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pts[i,:]])
        print("isim: %3i rcs: %s obs: %s"%(i,s,t))
        
    
    stype = 'linear'
    if args.akima:
        stype = 'akima'
    smooth = True
    if args.no_smooth:
        smooth=False

    pathspl = PathSpl(stype,smooth,pts)

    ts = np.linspace(0,1,nsim)
    pathuni = np.array( [ pathspl.GetValue(t) for t in ts ] )
    
    outsims = PathSims(pathuni,fcs)
    outiter = PathIter(pathspl,outsims)


    print("POST-SMOOTHING, POST-DISCRETIZATION")
    for i in range(nsim):
        s = "".join(["%14.5e"%(x) for x in rcs[i,:]])
        t = "".join(["%14.5e"%(x) for x in pathuni[i,:]])
        print("isim: %3i rcs: %s obs: %s"%(i,s,t))
        

    if args.curit == 0:
        inpsims = PathSims(rcs,fcs)
        inpspl = PathSpl(stype,smooth,rcs)
        inpiter = PathIter(inpspl,inpsims)
        opt = PathOpt( [inpiter,outiter] )
    else:
        oldopt = "%s/path.sims.xml"%(prevdir)
        if not os.path.exists(oldopt):
            sys.stderr.write(f"File not found: {oldopt}\n")
            opt = PathOpt([outiter])
        else:
            opt = PathOpt.from_file(oldopt)
            opt.iters.append(outiter)
    opt.SaveXml( "%s/path.sims.xml"%(curdir) )
        
    
    print("\nPreparing simulations in: %s\n"%(odir_path))

    CopyFiles\
        ( args.disang, args.mdin, args.curit, odir_path, None, None,
          False, False, args.curit, pathuni, fcs )
