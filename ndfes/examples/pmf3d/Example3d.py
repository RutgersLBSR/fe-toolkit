#!/usr/bin/env python3

if __name__ == "__main__":

    import numpy as np
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import matplotlib.ticker as plticker
    import matplotlib.cm as cm
    import ndfes
    import os
    import argparse
    from pathlib import Path

    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="Example 3D String FES Plotter")
    
    
    ndfes.AddStdOptsToCLI(parser)

    parser.add_argument \
        ("--zerobyhist",
         help="Choose the zero of energy from the lowest histogram value rather than the interpolated free energies along the input path",
         action='store_true')

    
    parser.add_argument \
        ("--ipath",
         help="path xml, txt, or metafile. If xml, then the last path is used",
         type=str,
         default=None,
         required=True )

    parser.add_argument \
        ("--range-180",
         help="Plot periodic coordinates from -180 to 180 rather than 0 to 360",
         action='store_true')

    parser.add_argument \
        ("--font-size",
         help="font size in pts (default: 8)",
         type=int,
         default=8,
         required=False )

    parser.add_argument \
        ("--title",
         help="Title appearing in the generated plots. Default: ''",
         type=str,
         default="",
         required=False)
    
    parser.add_argument \
        ("--xlabel",
         help="X-axis label Default: 'RC1'. "
         +r"A useful example is (note the single-quotes): '${\xi}_{\mathregular{1}}$'",
         type=str,
         default="RC1",
         required=False)
    
    parser.add_argument \
        ("--ylabel",
         help="Y-axis label Default: 'RC2'. "
         +r"A useful example is (note the single-quotes): '${\xi}_{\mathregular{2}}$'",
         type=str,
         default="RC2",
         required=False)
    
    parser.add_argument \
        ("--zlabel",
         help="Z-axis label Default: 'RC3'. "
         +r"A useful example is (note the single-quotes): '${\xi}_{\mathregular{3}}$'",
         type=str,
         default="RC3",
         required=False)
    


    
    args = parser.parse_args()


    model, oprefix, interp = ndfes.SetupModelFromCLI(args)
    
    ofile_projplot = oprefix + ".proj.png"
    ofile_cubeplot = oprefix + ".cube.png"
    ofile_pathfile = oprefix + ".path.dat"
    oobk           = 100.

    plot_full_grid = False
    if args.grid is not None:
        plot_full_grid = True

    
    #
    # Utility lambda functions for creating common axes labels
    #

    # Angstrom symbol in round brackets
    sAng    = r"($\mathregular{\AA}$)"
    # Add a prime to a symbol
    sPrimed = lambda x: r"%s^{\bf\prime}"%(x)
    # Add a subscript to R
    sR      = lambda x: r"R${}_{\mathregular{%s}}$"%(x)
    # Difference between two subscripted R's
    sR12    = lambda x,y: sR(x) + "-" + sR(y)
    # Subscript a greek xi
    sXi     = lambda x: r"${\xi}_{\mathregular{%s}}$"%(x)

    #
    # The axes labels
    #
    
    axlabels = ndfes.AxesLabels(3)
    # axlabels.axes[0].label = sXi("1") + " " + sAng
    # axlabels.axes[1].label = sXi("2") + " " + sAng
    # axlabels.axes[2].label = sXi("3") + " " + sAng
    labels = [ args.xlabel, args.ylabel, args.zlabel ]
    dimisangle = []
    for dim in range(3):
        axlabels.axes[dim].tickpad = 1
        axlabels.axes[dim].label = labels[dim]
        dimrange = model.grid.dims[dim].xmax - model.grid.dims[dim].xmin
        if model.grid.dims[dim].isper or dimrange >= 15:
            axlabels.axes[dim].label += r" (Deg.)"
            if dimrange >= 170:
                axlabels.axes[dim].SetTicks(60)
            elif dimrange >= 80:
                axlabels.axes[dim].SetTicks(30)
            elif dimrange >= 30:
                axlabels.axes[dim].SetTicks(15)
            else:
                axlabels.axes[dim].SetTicks(5)
            dimisangle.append(True)
        else:
            axlabels.axes[dim].label += r" ($\mathregular{\AA}$)"
            axlabels.axes[dim].SetTicks(1)
            dimisangle.append(False)
    
            


    # ---------------------------------------------------------
    # Write path projections to file
    # ---------------------------------------------------------
    
    uts = np.linspace(0,1,401)

    path = ndfes.ReadPaths(args.ipath,model.grid.ndim,onlylast=True)
    path_pts = path[-1].GetControlPts()
    if not args.zerobyhist:
        if interp == 'none':
            vals = [x for x in model.GetBinValues(pts=path_pts)
                    if x is not None]
            if len(vals) > 0:
                minval = min(vals)
            else:
                minval = 0
        else:
            minval = min(model.CptInterp(path_pts,k=oobk).values)
        model.ShiftInterpolatedValues( minval )
        model.ShiftBinValues( minval )
    else:
        vals = [x for x in model.GetBinValues()
                if x is not None]
        if len(vals) > 0:
            minval = min(vals)
        else:
            minval = 0
        model.ShiftBinValues( minval )
        if interp != 'none':
            model.ShiftInterpolatedValues( minval )

    pts = np.array([path[-1].GetValue(t) for t in uts])
    ipathcrds = pts
    ndfes.WritePathProjection(uts,pts,model,interp,ofile_pathfile)

    
    # ------------------------------------------
    #  Make a 3-d plot of the path
    # ------------------------------------------

    #
    # The range of free energy values
    #
    
    Zmin = 0
    Zmax = max(model.GetBinValues())

    #
    # The object that transforms free energy values
    # to colors, and sets the countour levels
    #
    
    colordata = ndfes.ColorAndContours(Zmin,Zmax)

    #
    # The spatial ranges of the plot
    #
    
    extent = model.GetRegGridExtent()
    for i in range(3):
        extent[0+i*2] -= 0.05
        extent[1+i*2] += 0.05

        
    #
    # The meshgrid of the 3d volume
    #
        
    sizes = [ min(max(dim.size+1,50),100)
              for dim in model.grid.dims ]
        
    mgrid = model.GetInterpolationGrid(sizes)

    #
    # The object that plots 3d data
    #

    pcube = ndfes.PathCube(extent,path[-1].path.spl,
                           mgrid,model,colordata)
    
    pcube.SetSurfaceStyle(show=True,
                          alpha_occupied=1,
                          alpha_unoccupied=0.7,
                          percent_white=0,
                          percent_height=0)
    
    pcube.SetContourStyle(show=True,
                          linewidth=0.6,
                          linestyle='-',
                          color='k',
                          alpha=0.5)
    
    pcube.SetLineStyle(show=True,
                       linewidth=1.2,
                       linestyle='-',
                       color='k',
                       alpha=1,
                       tmax=1,
                       interp=True)

    pcube.linestyle.linewidth=2.4
    pcube.linestyle.color = None
    pcube.linestyle.interp = True

    
    #
    # Create the 3d image
    #
    
    fig = plt.figure()
    plt.rcParams.update({'font.size': args.font_size})
    ax = fig.add_subplot(111, projection='3d')

    #
    # Draw the planes, contours, and lines
    #
    
    pcube.imshow(ax)

    #
    # Set the tick labels
    #

    axlabels.axes[0].labelpad = -5
    axlabels.axes[1].labelpad = -5
    axlabels.axes[2].labelpad = -4.5
    axlabels.axes[0].tickpad  = -3
    axlabels.axes[1].tickpad  = -3
    axlabels.axes[2].tickpad  = -0.2

    axlabels.ApplyToPlot( ax )

    #
    # This is a rather complicated way to draw the colorbar
    #
    # The cax object is a special space where the colorbar
    # should be drawn.  The +0.05 causes the colorbar to be
    # drawn further to the right than it normally would to
    # create extra space between the figure and colorbar.
    # the "0.02" value controls the width of the colorbar,
    # which is being made skinnier than it normally would.
    #
    # The cticker object controls which free energy values
    # are labeled on the colorbar
    #

    
    
    cax = fig.add_axes([ax.get_position().x1+0.06,
                        ax.get_position().y0,0.02,
                        ax.get_position().height])
    cbarmap = cm.ScalarMappable( cmap=colordata.colorbar_cmap )
    cbarmap.set_array( [colordata.Zmin,colordata.colormax] )
    cticker = plticker.MultipleLocator(base=colordata.contour_spacing)
    cbar = fig.colorbar(cbarmap,
                        cax=cax,
                        orientation='vertical',
                        ticks=cticker, pad=0)
    cbar.set_label("kcal/mol",labelpad=0)

    #
    # Save the figure
    #
    
    fig.set_size_inches( 3.6, 2.85, forward=True)

    
    # Shift the plot positioning in the figure area with
    # left,right,top,bottom
    if len(args.title) > 0:
        plt.rcParams.update({'font.size': args.font_size+2})
        ax.text2D(0.55, 0.9, args.title, transform=ax.transAxes)
        plt.rcParams.update({'font.size': args.font_size})
    fig.subplots_adjust(left=-0.17,top=1.05,bottom=0.04)
    plt.savefig(ofile_cubeplot, dpi=300)

    #
    # Delete the figure; we will work on a new figure below
    #
    
    ax.remove()
    plt.close(fig)

    for i in range(3):
        axlabels.axes[i].labelpad = 0
        axlabels.axes[i].tickpad = 5

    # --------------------------------------------------
    # Create a figure that shows the 2d projections of
    # the free energy planes, and how those planes warp
    # to the free energy pathway. There are a total of
    # 12 plots in the figure. 4 rows and 3 columns.
    # The 3 columns correspond to the xy,xz,yz planes.
    # The first row are 2d heatmaps.
    # The 2nd row are the 3d representation of the planes
    # The 3rd row are partially warped planes
    # The 4th row fully warps the planes, which are the
    # surfaces on which the free energy values are
    # interpolated
    # --------------------------------------------------
    
    fig = plt.figure()
    axs = [None]*12

    #
    # The first row of figures are 2d plots
    #
    
    axs[0] = plt.subplot(4,3,1)
    axs[1] = plt.subplot(4,3,2)
    axs[2] = plt.subplot(4,3,3)

    #
    # The remaining rows are 3d plots
    #
    
    for i in range(3,12):
        axs[i] = plt.subplot(4,3,i+1,projection='3d')

    pcube.linestyle.color = 'k'
    
    #
    # Draw the 2d heatmap of the xy plane
    #

    pcube.xy.surfacestyle.alpha_unoccupied = 0
    pcube.xy.plot2d(axs[0])
    axlabels.ApplyToPlot(axs[0],[0,1])

    #
    # Draw the 2d heatmap of the xz plane
    #

    pcube.xz.surfacestyle.alpha_unoccupied = 0
    pcube.xz.plot2d(axs[1])
    axlabels.ApplyToPlot(axs[1],[0,2])

    #
    # Draw the 2d heatmap of the yz plane
    #

    pcube.yz.surfacestyle.alpha_unoccupied = 0
    pcube.yz.plot2d(axs[2])
    axlabels.ApplyToPlot(axs[2],[1,2])


    
    #
    # The row of 3d plots corresponding to
    # flat planes.  Start by turning off all planes,
    # contours, and lines.
    #
    
    pcube.SetSurfaceStyle(show=False,
                          alpha_unoccupied=0.3,
                          shade=True)
    pcube.SetContourStyle(show=False)
    pcube.SetLineStyle(show=False)

    #
    # Turn on the 3d path though
    #
    pcube.linestyle.show=True

    #
    # Only show the xy plane
    #
    pcube.xy.surfacestyle.show = True
    pcube.imshow( axs[3] )
    pcube.xy.surfacestyle.show = False
    #
    # Only show the xz plane
    #
    pcube.xz.surfacestyle.show = True
    pcube.imshow( axs[4] )
    pcube.xz.surfacestyle.show = False
    #
    # Only show the yz plane
    #
    pcube.yz.surfacestyle.show = True
    pcube.imshow( axs[5] )

    #
    # The row of 3d plots corresponding to
    # partially warped planes
    #
    
    pcube.SetSurfaceStyle(show=False,
                          percent_height=0.5)

    #
    # Only show the xy plane
    #
    pcube.xy.surfacestyle.show = True
    pcube.imshow( axs[6] )
    pcube.xy.surfacestyle.show = False
    #
    # Only show the xz plane
    #
    pcube.xz.surfacestyle.show = True
    pcube.imshow( axs[7] )
    pcube.xz.surfacestyle.show = False
    #
    # Only show the yz plane
    #
    pcube.yz.surfacestyle.show = True
    pcube.imshow( axs[8] )


    #
    # The row of 3d plots corresponding to
    # fully warped planes
    #
    
    pcube.SetSurfaceStyle(show=False,
                          percent_height=1)
    #
    # Only show the xy plane
    #
    pcube.xy.surfacestyle.show = True
    pcube.imshow( axs[9] )
    pcube.xy.surfacestyle.show = False
    #
    # Only show the xz plane
    #
    pcube.xz.surfacestyle.show = True
    pcube.imshow( axs[10] )
    pcube.xz.surfacestyle.show = False
    #
    # Only show the yz plane
    #
    pcube.yz.surfacestyle.show = True
    pcube.imshow( axs[11] )

    #
    # Remove the axes and tick labels from the
    # x, y, and z axes in the 3d plots
    #
    
    for i in range(3,12):
        axs[i].tick_params(axis='both',
                           left=False,
                           top=False,
                           right=False,
                           bottom=False,
                           labelleft=False,
                           labeltop=False,
                           labelright=False,
                           labelbottom=False)

    #
    # Save the figure
    #
    
    fig.set_size_inches( 6.25, 7.75, forward=True)
    fig.tight_layout(pad=0.75)
    fig.subplots_adjust(wspace=0.4,hspace=0)
    plt.savefig(ofile_projplot, dpi=300)
