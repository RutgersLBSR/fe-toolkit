#!/bin/bash

if [ ! -e metafile.mbar ]; then
    ndfes_omp --mbar -w 0.1 --nboot 20 -c metafile.mbar metafile
fi

python3 Example3d.py --ipath metafile metafile.mbar
