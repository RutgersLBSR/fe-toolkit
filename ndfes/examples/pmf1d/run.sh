#!/bin/bash

if [ ! -e metafile.mbar ]; then
    ndfes_omp --mbar -w 0.1 --nboot 20 -c metafile.mbar metafile
fi

#if [ ! -e metafile.vfep ]; then
#    ndfes_omp --vfep -w 0.1 --nboot 20 -c metafile.vfep metafile
#fi


python3 ../../src/python/bin/ndfes-PrintFES.py metafile.mbar > metafile.mbar.dat
#python3 ../../src/python/bin/ndfes-PrintFES.py metafile.vfep > metafile.vfep.dat



