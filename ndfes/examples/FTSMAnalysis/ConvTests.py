#!/usr/bin/env python3


def GetPathBins(pspl,ws):
    import numpy as np
    from collections import defaultdict as ddict
    ndim = pspl.x.shape[1]
    if len(ws) != ndim:
        if len(ws) == 1:
            ws = [ ws[0] ]*ndim
        else:
            raise Exception(f"Expected {pspl.x.shape[1]} widths")
    N = 500
    xs = np.array([ pspl.GetValue(t) for t in np.linspace(0,1,N) ])
    seen = ddict(int)
    for x in xs:
        gidxs = [0]*ndim
        for dim in range(ndim):
            f = x[dim] / ws[dim]
            if f < 0:
                gidxs[dim] = int(f) - 1
            else:
                gidxs[dim] = int(f)
        seen[tuple(gidxs)] += 1
    return [ gidxs for gidxs in seen ]


def GetNumExtraPathBins(pspl,ws,refidxs):
    ref = set( refidx )
    idxs = set( GetPathBins(pspl,ws) )
    extra = idxs - ref
    return len(extra)


def CountAggregatePathBins(pspls,ws):
    import copy
    idxs = [ set(GetPathBins(pspl,ws)) for pspl in pspls ]
    
    agg_from_back = []
    agg = []
    for i,s in enumerate(reversed(idxs)):
        if i == 0:
            agg = copy.deepcopy(s)
        else:
            agg = agg.union(s)
        agg_from_back.append( len(agg) )
    agg_from_back.reverse()

    agg_from_front = []
    agg = []
    for i,s in enumerate(idxs):
        if i == 0:
            agg = copy.deepcopy(s)
        else:
            agg = agg.union(s)
        agg_from_front.append( len(agg) )

    cnts = [ len(idx) for idx in idxs ]
        
    return cnts,agg_from_front,agg_from_back



def TestBinCounts(pspls,ws,mlen,fh=None):
    if fh is None:
        import sys
        fh = sys.stdout
        cnts, pfront, pback = CountAggregatePathBins(pspls,widths)
        
        fh.write("\n"+"="*80+"\n\n")
        fh.write("Unique bins touched by the collection of aggregate paths\n\n")
        fh.write("Col 1: iteration\n")
        fh.write("Col 2: num bins touched by path\n")
        fh.write("Col 3: num bins touched by all paths from first iteration\n")
        fh.write("Col 4: num bins touched by all paths from last iteration\n\n")
        
        for it,c in enumerate(zip(cnts,pfront,pback)):
            fh.write("%3i %6i %6i %6i\n"%(it,c[0],c[1],c[2]))
        fh.write("\n")

        fh.write("Testing bin count convergence\n\n")
        fh.write("Given the current path and the mlen-1 previous paths,\n"+
              "one can compute an aggregate bin count of all paths\n"+
              "from the last iteration (the 4th column in the\n"+
              "table shown above). A plot of bin count versus\n"+
              "iteration is made, and a regression is performed\n"+
              "to find a slope.  The slope is shown below as the\n"+
              "second column.  Furthermore, each path has its own\n"+
              "bin count. We can measure the standard deviation\n"+
              "of the path bin counts using the last mlen paths.\n"+
              "The standard deviations are the 3rd column below.\n"+
              "We ask the question: is the magnitude of the slope\n"+
              "less than or equal to half that standard deviation?\n"+
              "The answer is the 4th column.  The first column\n"+
              "is the iteration index. That is, the table below\n"+
              "performs the test for each iteration. If the test\n"+
              "is satisifed for 3 iterations in a row, then the\n"+
              "string is likely converged.\n\n")

        passes = []
        for N in range(1,len(opt)+1):
            its = [ i for i in range(max(0,N-args.mlen),N) ]
            if len(its) < 2:
                continue
            pvals = [ cnts[i] for i in its ]
            pstd = np.std(pvals)
            ics = [ pback[i] for i in its ]
            fcn = scipy.stats.linregress( its, ics )
            fm = fcn.slope
            passes.append( abs(fm) <= 1.e-8 + pstd/2 )
            fh.write("%2i %7.2f %7.2f %6s\n"\
                  %(N-1,fm,pstd,passes[-1]))
        fh.write("\n")
        passes.reverse()
        npass = 0
        for p in passes:
            if p:
                npass += 1
            else:
                break
        return npass



def ReadPathFromDumpaves(idxs,idumps):
    import numpy as np

    vals = []
    errs = []
    nobs = []
    for i in range(len(idumps)):
        # the first column is a time step... strip it out
        data = np.loadtxt( idumps[i] )[:,1:]
        # the idxs array is zero-based
        qs = np.mean(data[:,idxs],axis=0)
        es = np.std(data[:,idxs],axis=0)
        vals.append( qs )
        errs.append( es )
        nobs.append( data.shape[0] )
    return np.array(nobs),np.array(vals),np.array(errs)



def OneDOverlap(ma,sa,mb,sb):
    import numpy as np
    if sa == 0:
        za = 0
    else:
        za = 1. / ( 2 * sa*sa )
    if sb == 0:
        zb = 0
    else:
        zb = 1. / ( 2 * sb*sb )
    
    zab = za*zb/(za+zb)
    return np.sqrt(zab/np.pi) * np.exp( -zab * (ma-mb)**2 )


def CptOverlap(mas,sas,mbs,sbs):
    import numpy as np
    ndim = len(mas)
    stot = 1
    for d in range(ndim):
        stot *= OneDOverlap(mas[d],sas[d],mbs[d],sbs[d])
    return stot


def OverlapIdx(mas,sas,mbs,sbs):
    saa = CptOverlap(mas,sas,mas,sas)
    sbb = CptOverlap(mbs,sbs,mbs,sbs)
    sab = CptOverlap(mas,sas,mbs,sbs)
    return sab / max( saa,sbb )




if __name__ == "__main__":
    import sys
    import argparse
    import pathlib
    import ndfes
    import ndfes.amber
    #from ndfes.FTSM import FTSMSimLimits
    from ndfes.FTSM import FTSMCheckSameMeans
    from ndfes.FTSM import FTSMCheckSlopes
    from ndfes.FTSM import FTSMOpt
    import numpy as np
    import scipy
    
    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
          Check convergence of path
          """ )
    
    parser.add_argument \
        ("disang",
         help="template disang file",
         type=str )

    parser.add_argument \
        ("pathpkl",
         help="Pickled FTSMOpt object",
         type=str )

    
    parser.add_argument \
        ("-M","--meta",
         help="Metafile of the last iteration (not all iterations)",
         type=str,
         default=None,
         required=False )

    
    parser.add_argument \
        ("--ptol",
         help="FES-FTSM centroid termination critical p-value (default: 0.5).",
         type=float,
         default=0.3,
         required=False )

    parser.add_argument \
        ("--distol",
         help="FES-FTSM slope termination tolerance for distance coordinates (default: 1.e-2).",
         type=float,
         default=1.e-2,
         required=False )

    parser.add_argument \
        ("--angtol",
         help="FES-FTSM slope termination tolerance for angle coordinates (default: 1.).",
         type=float,
         default=1.,
         required=False )
    
    parser.add_argument \
        ("--mlen",
         help="The number of iterations to consider when calculating "
         +"the slope of the path with respect to iteration (default: 5). ",
         type=int,
         default=5,
         required=False )

    parser.add_argument \
        ("--closest",
         help="If present, then the slopes are determined from a linear "
         +"regression involving a uniformly discretized final path, and "
         +"the closest points from the previous paths. If not present, "
         +"then the previous paths are also uniformly discretized.",
         action='store_true' )

    
    parser.add_argument \
        ("-w","--width",
         help="The bin width. Use mutliple times for multiple dimensions. "
         +"If it is used only once, then every dimension will have the same "
         +"bin width.",
         nargs='+',
         action='append',
         required = False,
         type=float)


    args = parser.parse_args()

    widths = None
    if args.width is not None:
        widths  = [item for sublist in args.width for item in sublist]

    disang_path = pathlib.Path(args.disang)
    if not disang_path.is_file():
        raise Exception("Template disang %s does not exist"%(disang_path))

    print("Reading template disang: %s\n"%(args.disang))
    tdisang = ndfes.amber.Disang( args.disang )
    tdisang.ValidateTemplate()

    #
    # These are the 0-based indexes of the restraints
    # used to define the path
    #
    
    idxs = tdisang.GetTemplateIdxs()

    isangle = []
    for idx in idxs:
        isangle.append( tdisang.restraints[idx].angle )


    if not pathlib.Path( args.pathpkl ).is_file():
        raise Exception(f"pathpkl {args.pathpkl} not found")

    print(f"Reading optimization pickle {args.pathpkl}")
    opt = FTSMOpt.load(args.pathpkl)

    ##############################################################
    ##############################################################

    if len(opt) > 1:
        
        print("\n"+"="*80+"\n")
        print("Checking slopes\n")
        print("For each dimension, d, of each window, i, we make a\n"
              +"plot of position, R_{di}, with respect to iteration,\n"
              +"I. That is, a plot of R_{di}(I) vs I.  A linear\n"
              +"regression is performed to measure a slope, m_{di}.\n"
              +"The number of previous iterations included in the\n"
              +"regression is controlled by the --mlen parameter.\n"
              +"In the current run, --mlen=%i.\n"%(args.mlen))
    
        conv,dismax,angmax = FTSMCheckSlopes\
            (opt,args.mlen,args.distol,args.angtol,isangle,
             args.closest,fh=sys.stdout)

        print("Convergence would be met if "
              +"distol >= %.2e and angtol >= %.2e"%(dismax,angmax))
        if conv:
            print("String slopes appear to be small")
        else:
            print("String slopes are large")
    else:
        print("\n"+"="*80+"\n")
        print("Not checking slopes because there's only 1 iteration")

        


    ##############################################################
    ##############################################################

    if widths is not None:
        pspls = [ opt[i].pspl for i in range(len(opt)) ]
        npass = TestBinCounts(pspls,widths,args.mlen)
        print(f"Iteration {len(opt)-1} passed the slope test"+
              f" {npass} times in a row")


    ##############################################################
    ##############################################################

    if len(opt) > 1:
        
        print("\n"+"="*80+"\n")
        print("Checking centroid means\n")
        print("Each dimension, d, of each window, i, has a\n"
              +"centroid position and standard deviation,\n"
              +"R_{di} and dR_{di}, corresponding to N_{di}\n"
              +"observations drawn from simulation. The\n"
              +"distribution is compared between the current\n"
              +"iteration and the previous iteration by\n"
              +"performing a Welch's t-test. The result of\n"
              +"the t-test is a p-value, which is close to 0\n"
              +"if the centroid positions are different, and\n"
              +"it is close to 1 if one can confidently say\n"
              +"that they are the same.\n"
              +"Furthermore, the final path is a parametric,\n"
              +"and one can check if the final centroids lie\n"
              +"along the parametric curve or if they have\n"
              +"been displaced perpendicular to the curve.\n"
              +"The cos values are the cosines of the angles\n"
              +"formed by the parametric curve and the\n"
              +"centroid displacement vectors. It is 1 if the\n"
              +"the centroid is parallel to the path and\n"
              +"displaced along the +t direction. It is -1 if\n"
              +"it is parallel and displaced in the -t\n"
              +"direction. It is 0 if the displacement is\n"
              +"small (formally the angle is undefined), or\n"
              +"the displacement is perpendicular to the path.\n")
          
        conv,pmin = FTSMCheckSameMeans\
            (opt.paths[-2],opt.paths[-1],args.ptol,fh=sys.stdout)

        print("Convergence would be met if "
              +"ptol < %.2e"%(pmin))
        if conv:
            print("String centroid means appear to be the same")
        else:
            print("String centroid means are different")

    else:
        
        print("\n"+"="*80+"\n")
        print("Not checking centroid means because there's only 1 iteration")


    ##############################################################
    ##############################################################
    
    print("\n"+"="*80+"\n")
    print("Approximating centroid overlaps between adjacent, uniformly")
    print("discretized window centers from the last iteration\n")


    path = opt[-1]
    
    cs = path.GetMeans()
    ss = path.GetStdDevs()
    for i in range( len(path) - 1 ):
        o = OverlapIdx(cs[i],ss[i],cs[i+1],ss[i+1])
        print("%2i <-> %2i : %8.3f"%(i+1,i+2,o))


    ##############################################################
    ##############################################################

    if args.meta is not None:
    
        print("\n"+"="*80+"\n")
        print("Approximating centroid overlaps from actual simulations\n")

        meta = ndfes.Metafile(args.meta)
        meta = meta.RelativeTo(".")
        dumpaves = [ t.dumpave for t in meta.trajs ]
        nobs,cs,ss = ReadPathFromDumpaves(idxs,dumpaves)
        for i in range( len(path) - 1 ):
            o = OverlapIdx(cs[i],ss[i],cs[i+1],ss[i+1])
            print("%2i <-> %2i : %8.3f"%(i+1,i+2,o))

        

        
