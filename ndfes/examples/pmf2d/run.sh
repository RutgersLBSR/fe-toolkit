#!/bin/bash

if [ ! -e metafile.mbar ]; then
    ndfes_omp --mbar -w 0.15 --nboot 20 -c metafile.mbar metafile
fi

#if [ ! -e metafile.vfep ]; then
#    ndfes_omp --vfep -w 0.15 --nboot 20 -c metafile.vfep metafile
#fi

python3 Example2d.py --rbf metafile.mbar

