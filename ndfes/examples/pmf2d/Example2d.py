#!/usr/bin/env python3

def DrawInset(ax,inset,cc,cbar,ipathcrds,xpathcrds,uts):
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes

    insax = None
    if inset is not None and ipathcrds is not None:
        insax = inset_axes(ax,width="48%",height="48%",
                           loc=inset,borderpad=0)
        #print(cc.Zmin,cc.Zmax,cc.colormax,cc.clipcolormax)
        insax.set_ylim(cc.Zmin,cc.colormax)
        levs = [ t for t in cbar.ax.get_yticks()
                 if t > cc.Zmin+1.e-7 and t < cc.colormax-1.e-7 ]
        intlevs = [ int(round(t)) for t in levs ]
        levlabels = []
        for t in levs:
            ival = int(round(t))
            if abs(t-ival) > 0.5:
                levlabels.append("%.1f"%(t))
            else:
                levlabels.append("%i"%(ival))
        
        insax.yaxis.set_label_position("right")
        insax.yaxis.tick_right()
        insax.axis["right"].major_ticklabels.set_ha("right")
        insax.set_xlim([0,1.25])
        insax.set_xticks([])
        insax.set_yticks(levs)
        insax.set_yticklabels(levlabels)
        insax.tick_params(axis='both', which='major', direction="in",
                          labelsize=7,length=2.,pad=-3)

        for lev in levs:
            insax.plot([0,1],[lev,lev],color='k',
                       lw=0.8, ls="", dashes=(1,3))
            
        if interp=='none':
            vals = model.GetBinValues(pts=ipathcrds)
        else:
            vals = model.CptInterp(ipathcrds,k=100).values
        insax.plot(uts,vals,color='k',lw=1.5)

        
        if xpathcrds is not None:
            cs = ['r','g','b','y','c','m']
            for im in range(len(xpathcrds)):
                pts = xpathcrds[im]
                if interp=='none':
                    vals = model.GetBinValues(pts=pts)
                else:
                    vals = model.CptInterp(pts,k=100).values
                insax.plot(uts,vals,color=cs[im],lw=0.8)
    return insax


    


if __name__ == "__main__":

    from matplotlib import pyplot as plt
    import numpy as np
    import ndfes
    import argparse
    import os
    import os.path
    import pickle
    import io
    import subprocess as subp
    import glob
    import copy
    from pathlib import Path

    
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="Example 2D FES Plotter and MFEP Optimizer")
    
    ndfes.AddStdOptsToCLI(parser)
    
    parser.add_argument \
        ("--ipath",
         help="path xml, txt, or metafile. If xml, then the last path is used",
         type=str,
         default=None,
         required=False )

    
    parser.add_argument \
        ("--imovie",
         help="read multiple paths from a path xml file to create a movie. The movie frames are written to --omovie.",
         type=str,
         default=None,
         required=False )

    
    parser.add_argument \
        ("--omovie",
         help="directory name to write movie images",
         type=str,
         default=None,
         required=False )

    
    parser.add_argument \
        ("--nmovie",
         help="desired number of movie images; used to calculate a stride, default: 20",
         type=int,
         default=20,
         required=False )

    
    parser.add_argument \
        ("--range-180",
         help="Plot periodic coordinates from -180 to 180 rather than 0 to 360",
         action='store_true')

    
    parser.add_argument \
        ("--font-size",
         help="font size in pts (default: 8)",
         type=int,
         default=8,
         required=False )

    parser.add_argument \
        ("--title",
         help="Title appearing in the generated plots. Default: '--title'",
         type=str,
         default="--title",
         required=False)
    
    parser.add_argument \
        ("--xlabel",
         help="X-axis label Default: 'RC1'. "
         +r"A useful example is (note the single-quotes): '${\xi}_{\mathregular{1}}$'",
         type=str,
         default="RC1",
         required=False)
    
    parser.add_argument \
        ("--ylabel",
         help="Y-axis label Default: 'RC2'. "
         +r"A useful example is (note the single-quotes): '${\xi}_{\mathregular{2}}$'",
         type=str,
         default="RC2",
         required=False)

    parser.add_argument \
        ("--xpath",
         help="plot an extra path. Can be used more than once",
         type=str,
         action='append',
         nargs='+',
         required=False)

    parser.add_argument \
        ("--minene",
         help="Defines the lower bound of the color scale. Default: 0",
         type=float,
         default=0.0,
         required=False)
    
    parser.add_argument \
        ("--maxene",
         help="Defines the upper bound of the color scale. If None, then it is "
         +"determined from the largest energy on the free energy surface. Default: None",
         type=float,
         default=None,
         required=False)
    

    parser.add_argument \
        ("--zerobyhist",
         help="Choose the zero of energy from the lowest histogram value rather than the interpolated free energies along the input path",
         action='store_true')

    parser.add_argument \
        ("--no-zero",
         help="Do not shift the free energy values (usually only used for debugging)",
         action='store_true')

    
    parser.add_argument \
        ("--zerobypath0",
         help="Choose the zero of energy from the first point on the path",
         action='store_true')
    
    
    parser.add_argument \
        ("--zerobypath1",
         help="Choose the zero of energy from the last point on the path",
         action='store_true')

    parser.add_argument \
        ("--inset-tleft",
         help="Include 1d path projection in top-left corner",
         action='store_true')
    
    parser.add_argument \
        ("--inset-tright",
         help="Include 1d path projection in top-right corner",
         action='store_true')
    
    parser.add_argument \
        ("--inset-bleft",
         help="Include 1d path projection in bottom-left corner",
         action='store_true')
    
    parser.add_argument \
        ("--inset-bright",
         help="Include 1d path projection in bottom-right corner",
         action='store_true')
    

    

    args = parser.parse_args()
    args.nmovie = max(1,args.nmovie)
    
    model, oprefix, interp = ndfes.SetupModelFromCLI(args)
    
    ofile_histplot = oprefix + ".hist.png"
    ofile_pathplot = oprefix + ".path.png"
    ofile_pathfile = oprefix + ".path.dat"

    plot_full_grid = False
    if args.grid is not None:
        plot_full_grid = True

    if args.xpath is not None:
        args.xpath = [item for sublist in args.xpath
                      for item in sublist]
 
    if int(args.zerobyhist) + int(args.zerobypath0) + \
       int(args.zerobypath1) > 1:
        raise Exception("--zerobyhist --zerobypath0 and --zerobypath1"
                        +" are mutually exclusive options")

    if int(args.inset_tleft) + int(args.inset_tright) + \
       int(args.inset_bleft) + int(args.inset_bright) > 1:
        raise Exception("--inset-tleft --inset-tright --inset-bleft "
                        +"--inset-bright are mutually exclusive options")

    inset=None
    if args.inset_tleft:
        inset='upper left'
    elif args.inset_tright:
        inset='upper right'
    elif args.inset_bleft:
        inset='lower left'
    elif args.inset_bright:
        inset='lower right'
        
    
    #
    # Utility lambda functions for creating common axes labels
    #

    # Angstrom symbol in round brackets
    sAng    = r"($\mathregular{\AA}$)"
    # Add a prime to a symbol
    sPrimed = lambda x: r"%s^{\bf\prime}"%(x)
    # Add a subscript to R
    sR      = lambda x: r"R${}_{\mathregular{%s}}$"%(x)
    # Difference between two subscripted R's
    sR12    = lambda x,y: sR(x) + "-" + sR(y)
    # Subscript a greek xi
    sXi     = lambda x: r"${\xi}_{\mathregular{%s}}$"%(x)

    #
    # The axes labels
    #
    
    axlabels = ndfes.AxesLabels(2)
    dimisangle = []
    for dim in range(2):
        axlabels.axes[dim].tickpad = 1
        #axlabels.axes[dim].label = "RC%i"%(dim+1)
        if dim == 0:
            axlabels.axes[dim].label = args.xlabel
        else:
            axlabels.axes[dim].label = args.ylabel
        dimrange = model.grid.dims[dim].xmax - model.grid.dims[dim].xmin
        if model.grid.dims[dim].isper or dimrange >= 15:
            axlabels.axes[dim].label += r" (Deg.)"
            if dimrange >= 170:
                axlabels.axes[dim].SetTicks(60)
            elif dimrange >= 80:
                axlabels.axes[dim].SetTicks(30)
            elif dimrange >= 30:
                axlabels.axes[dim].SetTicks(15)
            else:
                axlabels.axes[dim].SetTicks(5)
            dimisangle.append(True)
        else:
            axlabels.axes[dim].label += r" ($\mathregular{\AA}$)"
            axlabels.axes[dim].SetTicks(1)
            dimisangle.append(False)


    #
    #axlabels.axes[0].label = sR12("P",sPrimed("O2")) + " " + sAng
    #axlabels.axes[1].label = sR12("P",sPrimed("O5")) + " " + sAng
    #
    #axlabels.axes[0].SetTicks(60)
    #axlabels.axes[1].SetTicks(60)

    #
    # The style of the contour lines
    #
    
    csty = ndfes.LineStyle()
    csty.color='k'
    csty.linewidth = 0.5
    csty.linestyle = "-"
    csty.alpha = 1

    #
    # The style of the path line
    #
    
    lsty = ndfes.LineStyle()
    lsty.color='k'
    lsty.linewidth = 1.25
    lsty.linestyle = "-"
    lsty.alpha = 1


    #
    # extents / bounds
    #
    # If one (or both) of the coordinates are periodic,
    # AND the user specified --range-180 as an argument,
    # then explicitly set the extent of the plot ... otherwise
    # it would go from 0 to 360
    #
    bounds = None
    if (model.grid.dims[0].isper or model.grid.dims[1].isper) \
       and args.range_180:
        bounds = model.GetRegGridExtent(full=plot_full_grid)
        bounds = np.array(bounds).reshape((2,2))
        for d in range(2):
            if model.grid.dims[d].isper:
                bounds[d,0] = -180.
                bounds[d,1] =  180.

        
    # ---------------------------------------------------------
    # Create a histogram heatmap
    # ---------------------------------------------------------

    if args.imovie is None:
        
        print("Plotting 2d histogram")

        fig = plt.figure()
        fig.set_size_inches( 3.33, 3.0, forward=True)
        plt.rcParams.update({'font.size': args.font_size})

        ax = plt.subplot(1,1,1)

        #
        # Draw the histogram heatmap to the figure
        #

        
        ndfes.Plot2dHistogram(fig,ax,model,axlabels,csty,
                              shift_fes=(not args.no_zero),
                              full=plot_full_grid,
                              bounds=bounds)

        #
        # Save the figure
        #
    
        ax.set_title(args.title)
        #fig.tight_layout(pad=0.4)
        #fig.subplots_adjust(wspace=0)
        fig.subplots_adjust(left=0.12,right=0.98,bottom=0.1,top=0.93,wspace=0,hspace=0)
        plt.savefig(ofile_histplot, dpi=300)
        
        #
        # Delete the figure; we will work on a new figure below
        #
    
        ax.remove()
        plt.close(fig)

        
    # ---------------------------------------------------------
    # Write path projections to file
    # ---------------------------------------------------------
    
    uts = np.linspace(0,1,401)

    ipathcrds = None

    if args.ipath is not None:
        ipath = args.ipath
    elif args.imovie is not None:
        ipath = args.imovie
    else:
        ipath = None

    if ipath is not None:
        path = ndfes.ReadPaths(ipath,model.grid.ndim,onlylast=True)
        path_pts = path[-1].GetControlPts()

        if not args.no_zero:
            model.ShiftFES(path_pts,
                           zerobyhist=args.zerobyhist,
                           zerobypath0=args.zerobypath0,
                           zerobypath1=args.zerobypath1,
                           interp=interp)

        pts = np.array([path[-1].GetValue(t) for t in uts])
        ipathcrds = pts
        ndfes.WritePathProjection(uts,pts,model,interp,ofile_pathfile)

        
    xpathcrds = None    
    if args.xpath is not None:
        xpathcrds = []
        for ipath,fname in enumerate(args.xpath):
            x = ndfes.ReadPaths(fname,model.grid.ndim,onlylast=True)
            pts = np.array([x[-1].GetValue(t) for t in uts])
            xpathcrds.append(pts)
            pfile = ofile_pathfile.replace("path.dat",f"path.{ipath+1}.dat")
            ndfes.WritePathProjection(uts,pts,model,interp,pfile)

     
    # ---------------------------------------------------------
    # Create an interpolated heatmap with a path
    # ---------------------------------------------------------
    

    #
    # Start the figure
    #
    print("Plotting interpolated heatmap with path")
        
    fig = plt.figure()
    fig.set_size_inches( 3.33, 3.0, forward=True)
    plt.rcParams.update({'font.size': args.font_size})
    ax = plt.subplot(1,1,1)

    #
    # Draw the heatmap and path to the figure
    #
    # sizes : list of int
    #     Create the heatmap and contours using a meshgrid of the
    #     specified sizes. If this is not used, then the bin centers
    #     are used
    #
    # mgrid : numpy,array, shape(2,sizes[0],sizes[1])
    #     The meshgrid us
    #

    cbar = None
    cticker = None
    if interp=='none':
        mgrid = model.GetRegGridCenters\
            (full=plot_full_grid,
             bounds=bounds)
        cc,cbar,cticker = ndfes.Plot2dPath\
            (fig,ax,ipathcrds,mgrid,model,
             axlabels,csty,lsty,
             minene=args.minene, maxene=args.maxene,
             full=plot_full_grid,
             bounds=bounds,range180=args.range_180)
    else:
        sizes = [101,101]
        mgrid = model.GetInterpolationGrid\
            (sizes,
             full=plot_full_grid, 
             bounds=bounds)
        cc,cbar,cticker = ndfes.Plot2dPath\
            (fig,ax,ipathcrds,mgrid,model,
             axlabels,csty,lsty,
             minene=args.minene, maxene=args.maxene,
             full=plot_full_grid,
             bounds=bounds,range180=args.range_180)

    if xpathcrds is not None:
        cs = ['r','g','b','y','c','m']
        for im in range(len(xpathcrds)):
            pts = xpathcrds[im]
            ax.plot( pts[:,0], pts[:,1],
                     linestyle=lsty.linestyle,
                     linewidth=lsty.linewidth*0.4,
                     color=cs[im],
                     alpha=lsty.alpha )

    DrawInset(ax,inset,cc,cbar,ipathcrds,xpathcrds,uts)
            


    #
    # Save the figure
    #
    
    ax.set_title(args.title)
    #fig.tight_layout(pad=0.4)
    #fig.subplots_adjust(wspace=0)
    fig.subplots_adjust(left=0.12,right=0.98,bottom=0.1,top=0.93,wspace=0,hspace=0)

    plt.savefig(ofile_pathplot, dpi=300)

    ax.remove()
    plt.close(fig)

    

    # ---------------------------------------------------------
    # Make a movie
    # ---------------------------------------------------------
    

    if args.imovie is not None and args.omovie is not None:

        if len(args.omovie) > 0:
            if not os.path.isdir(args.omovie):
                raise Exception("Directory not found:",args.omovie)

        last=False
        if args.nmovie == 1:
            last=True
        mpaths = ndfes.ReadPaths(args.imovie,model.grid.ndim,
                                 onlylast=last)

        
        psty = copy.deepcopy(lsty)
        psty.color="m"
                

        pathimgs = os.path.join(args.omovie,
                                os.path.basename(oprefix)
                                + ".*.path.png")
        for pathimg in glob.glob(pathimgs):
            os.remove(pathimg)

        simimgs = os.path.join(args.omovie,
                               os.path.basename(oprefix)
                               + ".*.sims.png")
                
        for simimg in glob.glob(simimgs):
            os.remove(simimg)
                    

        #
        # Which iterations are we going to plot
        #

        nframes = len(mpaths)
        stride = max(1,int(nframes/args.nmovie))
        its=[]
        for it in range(nframes):
            if it > 0 and it < nframes - 1 and it % stride != 0:
                continue
            its.append(it)
            
        if args.nmovie == 1:
            its = [ nframes - 1 ]

        #
        # Choose the zero of energy based on the last iteration
        #
        
        #ts = np.linspace(0,1,201)
        pts = np.array([mpaths[its[-1]].GetValue(t) for t in uts])
        
        model.ShiftFES(pts,
                       zerobyhist=args.zerobyhist,
                       zerobypath0=args.zerobypath0,
                       zerobypath1=args.zerobypath1,
                       interp=interp)

        vals = model.CptInterp(pts).values
        #print(vals[0],vals[-1])
        #print(min(vals),max(vals))
        #
        # Create a template image. Each frame will draw a different
        # line on this template
        #
        
        fig = plt.figure()
        fig.set_size_inches( 3.33, 3.0, forward=True)
        plt.rcParams.update({'font.size': args.font_size})
        ax = plt.subplot(1,1,1)

        if interp=='none':
            mgrid = model.GetRegGridCenters\
                (full=plot_full_grid,bounds=bounds)
            cc,cbar,ctick = ndfes.Plot2dPath\
                (fig,ax,None,mgrid,model,
                 axlabels,csty,lsty,
                 minene=args.minene,maxene=args.maxene,
                 full=plot_full_grid, 
                 bounds=bounds)
        else:
            sizes = [101,101]
            mgrid = model.GetInterpolationGrid \
                (sizes,full=plot_full_grid,bounds=bounds)
            cc,cbar,ctick = ndfes.Plot2dPath\
                (fig,ax,None,mgrid,model,
                 axlabels,csty,lsty,
                 minene=args.minene,maxene=args.maxene,
                 full=plot_full_grid,
                 bounds=bounds)

        #
        # Save this template figure in memory
        # We will reuse it to draw each frame
        #
        buf = io.BytesIO()
        pickle.dump(fig,buf)

                
        #modes = ["path","sims"]
        modes = ["sims"]

        doXs = True
        
        sims = None
        if mpaths[-1].sims is not None:
            if mpaths[-1].sims.rcs is not None:
                sims = mpaths[-1].sims.rcs
        if sims is not None:
            if sims.shape[0] >= 100:
                doXs = False

        
        frameidx = 0
        for it in its:
            pts = np.array([mpaths[it].GetValue(t) for t in uts])

            centers = mpaths[it].GetControlPts()

            sims = None
            
            if False:
                from pathlib import Path
                #print(args.imovie)
                ipath = Path(args.imovie)
                pathparts = list(ipath.parts[:-1])
                #print(pathparts)
                try:
                    if pathparts[-1] == "init":
                        cit = 0
                    else:
                        cit = int(pathparts[-1].replace("it",""))
                    pathparts[-1] = "it%03i"%(cit+1)
                    pathparts.append("sims.txt")
                    simpath = Path(*pathparts)
                    #print(simpath)
                    if os.path.exists(simpath):
                        sims = np.loadtxt(str(simpath))[:,2:]
                    #print(sims)
                except:
                    pass
                    
            if sims is None and mpaths[it].sims is not None:
                if mpaths[it].sims.rcs is not None:
                    sims = mpaths[it].sims.rcs

                
            
                    

            for imode,mode in enumerate(modes):

                buf.seek(0)
                fig = pickle.load(buf)
                ax = fig.axes[0]

                segs = model.grid.WrapPathSegments(pts,range180=args.range_180)
                for seg in segs:
                    ax.plot( seg[:,0], seg[:,1],
                             linestyle=lsty.linestyle,
                             linewidth=lsty.linewidth,
                             color=lsty.color,
                             alpha=lsty.alpha )

                
                if mode == "sims" and sims is not None:

                    nsim = sims.shape[0]
                    if doXs:
                        segs = model.grid.WrapPathSegments(sims,range180=args.range_180)
                        for seg in segs:
                            ax.scatter( seg[:,0], seg[:,1],
                                        marker='x',
                                        s=5, linewidth=0.8,
                                        color=lsty.color,
                                        alpha=lsty.alpha )

                        upts = np.array([mpaths[it].GetValue(t) for t in np.linspace(0,1,nsim)])
                        segs = model.grid.WrapPathSegments(upts,range180=args.range_180)
                        for seg in segs:
                            ax.scatter( seg[:,0], seg[:,1],
                                        marker='o',
                                        s=0.6,
                                        color=lsty.color,
                                        alpha=lsty.alpha )

                elif mode == "path":

                    segs = model.grid.WrapPathSegments(centers,range180=args.range_180)
                    for seg in segs:
                        ax.scatter( seg[:,0], seg[:,1],
                                    marker='o',
                                    s=0.6,
                                    color=lsty.color,
                                    alpha=lsty.alpha )


                cs = ['r','g','b','y','c','m']
                if xpathcrds is not None:
                    for im in range(len(xpathcrds)):
                        pts = xpathcrds[im]
                        segs = model.grid.WrapPathSegments(pts,range180=args.range_180)
                        for seg in segs:
                            ax.plot( seg[:,0], seg[:,1],
                                     linestyle=lsty.linestyle,
                                     linewidth=lsty.linewidth*0.4,
                                     color=cs[im],
                                     alpha=lsty.alpha )
                    

                DrawInset(ax,inset,cc,cbar,pts,xpathcrds,uts)

                    
                fname = os.path.join\
                    (args.omovie,
                     os.path.basename(oprefix)
                     + ".%04i"%(frameidx+1) + ".%s.png"%(mode))

                print("Writing",fname)
  
                ax.set_title(args.title)
                #fig.tight_layout(pad=0.4)
                #fig.subplots_adjust(wspace=0)
                fig.subplots_adjust(left=0.12,right=0.98,bottom=0.1,top=0.93,wspace=0,hspace=0)
                plt.savefig(fname, dpi=300)
                        
            frameidx += 1
            
        if len(its) > 1:
            if "path" in modes:
                    
                fname = os.path.join(args.omovie,
                                     os.path.basename(oprefix)
                                     + ".%04d.path.png")

                cmd = "ffmpeg -y -framerate 4/1 -i %s -c:v libx264 -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" -r 30 -pix_fmt yuv420p %s/movie.path.mp4"%(fname,args.omovie)
                cargs = [ arg.replace("\"","") for arg in cmd.split(" ") ]
                print("Running:",cmd)
                subp.run(cargs)
                    
            if "sims" in modes:
                fname = os.path.join(args.omovie,
                                     os.path.basename(oprefix)
                                     + ".%04d.sims.png")

                cmd = "ffmpeg -y -framerate 4/1 -i %s -c:v libx264 -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" -r 30 -pix_fmt yuv420p %s/movie.sims.mp4"%(fname,args.omovie)
                cargs = [ arg.replace("\"","") for arg in cmd.split(" ") ]
                print("Running:",cmd)
                subp.run(cargs)
    
            
