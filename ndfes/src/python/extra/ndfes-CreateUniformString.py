#!/usr/bin/env python3

import numpy as np
import ndfes

if __name__ == "__main__":

    import sys
    import argparse


    def printpts(pts):
        for pt in pts:
            print(" ".join(["%25.16e"%(x) for x in pt]))

    def writepts(fname,pts,dx=None):
        fh=open(fname,"w")
        if dx is not None:
            for pt,er in zip(pts,dx):
                pstr = " ".join(["%25.16e"%(x) for x in pt])
                estr = " ".join(["%25.16e"%(x) for x in er])
                fh.write("%s %s\n"%(pstr,estr))
        else:
            for pt in pts:
                fh.write("%s\n"%(" ".join(["%25.16e"%(x) for x in pt])))
        fh.close()



    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""Generates a parametric path from a set of points""" )
    

    parser.add_argument \
        ("-m", "--meta",
         type=str,
         help='ndfes metafile',
         required=True)

    parser.add_argument \
        ("-o", "--out",
         type=str,
         help='output file',
         required=True)
    
    parser.add_argument \
        ("-r","--rxn",
         help="a list of 1-or-more integers that specify which coordinates are included in the parametric curve; e.g., -r 1 3 would creates a 2d parametric spline from the first and third restraints",
         type=int,
         nargs='+',
         action='append',
         required=True )
    
    parser.add_argument \
        ("-n","--npts",
         help="number of output points. If npts < 2, then it outputs the same number of points as was read in. Default: 0",
         type=int,
         default=0,
         required=False )

    parser.add_argument \
        ("-p","--pspl",
         help="if present then use parametric Akima spline rather "+
         "than Gaussian Process Regression",
         action='store_true',
         required=False )

    parser.add_argument \
        ("-c","--selfc",
         help="if present then self-consistently iterate the parametric "+
         "spline so the result is a spline that interpolates to itself. "+
         "This option is only valid if --pspl is used. If this option "+
         "is used, then the resulting spline will not pass through the "+
         "input data, however.",
         action='store_true',
         required=False )

    
    parser.add_argument \
        ("-e","--extraerr",
         help="adds an extra standard deviation to each point to "+
         "increase the amount of GPR smoothing. Default: 0.025",
         type=float,
         default=0.025,
         required=False )
    
    parser.add_argument \
        ("-s","--sigmatol",
         help="refit GPR until the interpolated free energies are within "+
         "sigmatol standard deviations of the training values. Only used "+
         "if --gpr is specified. Default: 1.96 (this corresponds to a "+
         "95%% confidence interval)",
         type=float,
         default=1.96,
         required=False )
    

    
    args = parser.parse_args()

    
    if not args.pspl and args.selfc:
        raise Exception("Invalid combination of options: --selfc is "+
                        "only valid if --pspl is used")
    

    rxncols  = [item for sublist in args.rxn for item in sublist]
    ndim = len(rxncols)

    meta = ndfes.Metafile( args.meta )
    dumpaves = meta.GetDumpavesFromHamidx(0)

    
    xs=[]
    es=[]
    for dumpave in dumpaves:
        avg,err = ndfes.GetCrdMeanAndErrorFromFile(dumpave,rxncols)
        xs.append(avg)
        es.append(err)
    xs = np.array(xs)
    es = np.array(es)

    npts = args.npts
    if npts < 2:
        npts = xs.shape[0]
    
    if args.pspl:
        pts = ndfes.AkimaStringMethodUpdate( npts, xs, scf=args.selfc )
    else:
        pts = ndfes.PGPRStringMethodUpdate( npts, xs, dx=es,
                                            extra_error=args.extraerr,
                                            sigma_fit_tol=args.sigmatol )
        


        
    writepts(args.out + ".inp",xs,es)
    writepts(args.out,pts)


    bxys = []
    for i in range(ndim-1):
        bxys.append( "-bxy 1:%i:%i:%i"%( 2+i, ndim+1, ndim+2+i ) )
    options = " ".join(bxys)
    
    fh = open(args.out + ".sh","w")
    fh.write("""#!/bin/bash
set -e
set -u

xmgrace -hardcopy -noprint -settype xydxdy -block %s %s -settype xy -nxy %s -saveall %s.agr

echo "To view a parametric plot, run: xmgrace %s.agr"
"""%(args.out+".inp",options,args.out,args.out,args.out))

    print("To make a parametric plot, run: bash %s.sh"%(args.out))
