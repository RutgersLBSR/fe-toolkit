#!/usr/bin/env python3

if __name__ == "__main__":

    import argparse
    import ndfes
    import pathlib
    import numpy as np
    from matplotlib import pyplot as plt

    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawDescriptionHelpFormatter,
          description="""
          Plot the energy and reaction coordinates from a pickled
          FTSM path object.

          Examples
          --------

          1. Read the string from path.pkl and write the coordinates to p.dat

          ndfes-PlotMultistringPath.py -P path.pkl -o p.dat

          p.dat will have Ndim+1 columns. The first column is the progress 
          variable and the remaining columns are the reaction coordinate values



          2. Read the string from path.pkl, the free energy surface from
          fes.chk, and then write the coordinates and energy to p.dat

          ndfes-PlotMultistringPath.py -P path.pkl -c fes.chk -o p.dat

          p.dat will have Ndim+3 columns. The first column is the progress
          variable, the next Ndim columns are the reaction coordinates,
          and the last two columns are the free energy and standard error.
          If one wanted the 95%% confidence interval rather than the
          standard error, then use the --ci flag; e.g.,

          ndfes-PlotMultistringPath.py --ci -P path.pkl -c fes.chk -o p.dat



          3. Read the string from path.pkl, the free energy surface from
          fes.chk, and then save a plot of the reaction coordinates and
          and energy profile to path.png

          ndfes-PlotMultistringPath.py -P path.pkl -c fes.chk --img img.png



          4. Read strings from path.pkl and other.pkl and save a plot that
          compares the reaction coordinates and energy profiles

          ndfes-PlotMultistringPath.py -P path.pkl -P other.pkl \
                     -c fes.chk --img img.png

          The path.pkl is drawn in black and other.pkl is drawn in red.
          One can compare any number of paths, and the ordering of the
          colors are: black, red, green, blue, purple, black, red, green, ...



          5. Read two halves of a multistring path from first.pkl and 
          second.pkl and write the coordinates to p.dat

          ndfes-PlotMultistringPath.py -P first.pkl second.pkl -o p.dat

          In this case, the progress variable goes from 0 to 2 rather
          than 0 to 1.  The first range (0-to-1) corresponds to first.pkl
          and the second range (1-to-2) correspond to second.pkl.
          One can stack an arbitrary number of paths and include several
          multistrings in any combination; e.g.,

          ndfes-PlotMultistringPath.py --img img.png \
             -P path1.part1.pkl path1.part2.pkl path1.part3.pkl \
             -P path2.part1.pkl path2.part2.pkl path2.part3.pkl \
             -P path3.pkl

          """ )

    parser.add_argument \
        ("-o","--out",
         help="output text file of path (and possibly energies)",
         type=str,
         required=False)
    
    parser.add_argument \
        ("-n","--nuni",
         help="number of uniform points to output (default: 301)",
         type=int,
         default=301,
         required=False )


    parser.add_argument \
        ("-I","--img",
         help="output image file (optional)",
         type=str,
         required=False )
    
    parser.add_argument \
        ("-c","--chkpt",
         help="ndfes checkpoint file (energies are returned if used)",
         type=str,
         required=False )

    parser.add_argument \
        ("-m","--model",
         help="model index to read within the checkpoint file",
         type=int,
         default=0,
         required=False )

    parser.add_argument \
        ("-P","--readpath",
         help="Read a path (this can be a ndfes.DensityOpt pickle)."+
         "If multiple arguments are provided to the same option,"+
         "then the paths are stacked end-to-end.",
         type=str,
         action='append',
         nargs='+',
         required=True )
    
    parser.add_argument \
        ("--ci",
         help="if present, write the confidence intervals rather than "+
         "standard errors",
         action='store_true',
         required=False )


    parser.add_argument \
        ("-r","--rbf",
         help="Radial Basis Function interpolation. "+
         "Valid for both vFEP and MBAR",
         action='store_true',
         required=False)
    
    parser.add_argument \
        ("-b","--bspl",
         help="Cardinal B-spline interpolation. "+
         "Valid only for vFEP",
         action='store_true',
         required=False)


    parser.add_argument \
        ("-H","--hist",
         help="No interpolation; only use histogram values",
         action='store_true',
         required=False)

    args = parser.parse_args()

    if args.img is not None and args.chkpt is None:
        raise Exception("If --img is used, then --chkpt is required")

    
    if args.chkpt is not None:
        
        model = ndfes.GetModelsFromFile(args.chkpt)[args.model]
    
        if isinstance(model,ndfes.MBAR):
            interp = 'rbf'
        elif isinstance(model,ndfes.vFEP):
            interp = 'bspl'
        
        if args.rbf:
            interp = 'rbf'
        elif args.bspl:
            interp = 'bspl'
        elif args.hist:
            interp = 'none'
        
        if interp == "bspl" and isinstance(model,ndfes.MBAR):
            raise Exception("bspl interpolation is only available for vFEP")

        if interp == 'rbf':
            print("Creating RBF interpolator")
            model.UseRBFInterp() # filename=iofile_interp, epsilon=rbf_shape )
        elif interp == 'none':
            print("Not using interpolator")
        else:
            print("Using B-spline basis")



    uts = np.linspace(0,1,args.nuni)

    its = []
    for itfiles in args.readpath:
        it = []
        for pfile in itfiles:
            p = ndfes.DensityStringOpt.load(pfile)
            it.append(p)
        its.append(it)

    ndim = p[0].inp_pts.shape[1]

    
    if args.img is not None:
        
        if ndim == 2:
            #idim=0
            #jdim=1
            #k=str((jdim)+idim*ndim)
            #print(k)
            mosaic = [ ["1","ene"] ]
            fig,axs = plt.subplot_mosaic(mosaic,
                                         layout="constrained")
            fig.set_size_inches( (3.33-2.85)+2*(ndim-1)*2.85,
                                 (ndim-1)*2.85,
                                 forward=True)
        else:
            w = (ndim-1)//2
            print(w)
            mosaic = []
            for idim in range(ndim-1):
                row=[]
                for jdim in range(ndim-1):
                    if jdim >= idim:
                        row.append(str((jdim+1)+idim*ndim))
                    else:
                        row.append(".")
                if idim > ndim-2-w:
                    for jdim in range(w):
                        row[jdim] = "ene"
                print(row)
                mosaic.append(row)
        
            fig,axs = plt.subplot_mosaic(mosaic,
                                         layout="constrained")

            fig.set_size_inches( (3.33-2.85)+(ndim-1)*2.85,
                                 (ndim-1)*2.85,
                                 forward=True)
        
    #ndim = model.grid.ndim

        
        

    colors=['k','r','g','b','p']

    if args.out is not None:
        fh=open(args.out,"w")

    for ival,it in enumerate(its):
        if ival > 0 and args.out is not None:
            fh.write("\n")
            
        c = colors[ival % len(colors)]
        tlo = 0
        mpts = []
        mprcs = []
        # for p in it:
        #     mprcs.extend( [ p[0].inp_pc.GetValue(t)
        #                     for t in uts[:-1] ] )
        #     mpts.extend( [ tlo + t for t in uts[:-1] ] )
        #     tlo += 1
        # mprcs.append( p[-1].inp_pc.GetValue(uts[-1]) )
        # mpts.append( tlo )
        for p in it:
            mprcs.extend( [ p[0].inp_pc.GetValue(t)
                            for t in uts ] )
            mpts.extend( [ tlo + t for t in uts ] )
            tlo += 1

        
        mprcs = np.array(mprcs)
        mpts = np.array(mpts)

        if args.chkpt is not None:
            if interp == 'none':
                vals = np.array(model.GetBinValues(pts=mprcs))
                errs = np.array(model.GetBinErrors(pts=mprcs))
                ders = np.zeros( (vals.shape[0],model.grid.ndim) )
                res = ndfes.EvalT(vals,ders,errs)
            else:
                res = model.CptInterp(mprcs,return_std=True)
            minval = np.min(res.values)
            res.values -= minval
            if args.out is not None:
                for i in range(mpts.shape[0]):
                    r = " ".join(["%14.8f"%(x) for x in mprcs[i,:]])
                    v = res.values[i]
                    e = res.errors[i]
                    if args.ci:
                        e *= 1.96
                    fh.write("%14.8f %s "%(mpts[i],r)+
                             "%15.6e %12.3e\n"%(v,e))
        elif args.out is not None:
            for i in range(mpts.shape[0]):
                r = " ".join(["%14.8f"%(x) for x in mprcs[i,:]])
                fh.write("%14.8f %s\n"%(mpts[i],r))
            

        if args.img is not None:
            for idim in range(ndim):
                for jdim in range(idim+1,ndim):
                    k = jdim+idim*ndim
                    axs[str(k)].plot(mprcs[:,idim],mprcs[:,jdim],lw=0.8,c=c)
            axs["ene"].plot(mpts,res.values,lw=0.8,c=c)

            
    if args.img is not None:
        for idim in range(ndim-1):
            for jdim in range(idim+1,ndim):
                k = jdim+idim*ndim
                axs[str(k)].set_xlabel(r"${\xi}_{\mathregular{%i}}$"%(idim+1))
                axs[str(k)].set_ylabel(r"${\xi}_{\mathregular{%i}}$"%(jdim+1))
            
        axs["ene"].set_xlabel(r"Progress")
        axs["ene"].set_ylabel(r"${\Delta}$A (kcal/mol)")

        plt.savefig( args.img, dpi=300 )
        for k in axs:
            axs[k].remove()
        plt.close(fig)
