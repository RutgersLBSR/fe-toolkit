#!/bin/bash
set -e

#
# PWD    is /path/to/edgembar/build
# TDIR   is /path/to/edgembar
# PREFIX is /path/to/edgembar/local
#

TDIR=$(dirname ${PWD})
export PREFIX=${TDIR}/local

#
# Some clusters are a heterogeneous mix of
# new and old nodes; perhaps much older than
# the login node. If CROSSCOMPILE=1, then
# it builds everything for nehalem processors
# circa 2008.
#
# BLASTARGET is only used if we cannot find
# BLAS/LAPACK and need to download openblas
# from github
#

CROSSCOMPILE=0

if [ "${CROSSCOMPILE}" -ge 1 ]; then
    export ARCH="-march=nehalem -mtune=nehalem"
    export BLASTARGET="-DTARGET=NEHALEM"
else
    export ARCH="-march=native -mtune=native"
    unset BLASTARGET
fi

DEBUG=1

if [ "${DEBUG}" -ge 1 ]; then
    export FLAGS="-O2 -g"
else
    export FLAGS="-O3 -DNDEBUG"
fi

cmake .. \
      -DCMAKE_INSTALL_PREFIX=${PREFIX} \
      -DBUILD_PYTHON=TRUE \
      -DPython3_EXECUTABLE=`which python3` \
      -DCMAKE_CXX_COMPILER=`which g++` \
      -DCMAKE_Fortran_COMPILER=`which gfortran` \
      -DCMAKE_CXX_FLAGS="${FLAGS} -Wall -Wextra -Wunused ${ARCH}" \
      ${BLASTARGET}

echo ""
echo "If the configuration looks OK, then run:"
echo "make install VERBOSE=1 -j4"
echo ""



