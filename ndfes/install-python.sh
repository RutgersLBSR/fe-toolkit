#!/bin/bash
set -e
set -u

PREFIX=${PWD}/local
PYTHON=$(which python3)
echo ${PYTHON}
PYTHONUSERBASE=${PREFIX} ${PYTHON} -m pip install --prefix=${PREFIX} src/python

