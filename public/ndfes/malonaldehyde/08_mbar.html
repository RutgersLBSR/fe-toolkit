<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
  </head>
  <body>

    <a href="07_qmmm_nvt_us_prod.html">Previous</a>
    <a href="index.html">Top</a>
    <a href="09_wtp.html">Next</a>

    
    <h1>
      08_mbar : Calculate the DFTB3 QM/MM free energy surface from the DFTB3 QM/MM sampling
    </h1>
    
    <p>
      The purpose of the directory is to use the MBAR method implemented
      in the ndfes program to remove the effect of the biasing potentials
      to estimate the unbiased DFTB3 QM/MM free energy surface from the
      biased DFTB3 QM/MM sampling.
      Rather than analyzing the raw output, we use the
      ndfes-CheckEquil.py script to identify "equilibration" and
      "production" regions of each simulation and output a new set
      of dumpave files that contain only the samples within the
      production region.
      Analysis is performed on each independent trial and
      on the aggregate production sampling from all trials.
      The bootstrap error estimates of the individual trials
      and the variation between the trials 
      are then combined to make an overall error estimate.
    </p>
    <p>
      In practice, free energy surfaces often do not possess symmetry
      because most systems of interest are relatively complex.
      There are a few small molecule reactions,
      such as Cl-CH<sub>3</sub>-Cl<sup>-</sup> and
      the malonaldehyde system used in the present example
      that do, however.
      The present example begins by demonstrating how you would
      analyze the simulation
      results without considering symmetry in the &xi;<sub>1</sub>
      coordinate, which is the most common situation one would normally
      encounter.
      The files used to perform this analysis are labeled "nosym".
      The example then reanalyzes the simulations with consideration
      of the symmetry in the &xi;<sub>1</sub> coordinate,
      and these files are labeled "sym".
    </p>



    
    <h3>
      Overview of the operations
    </h3>

    <p>
      <ul>
        
        <li>
          <a href="#01_checkequil">01_checkequil.sh</a>
	  <br>
	  This script reads each pair of disang and dumpave files
	  and outputs a new set of dumpave files that contain
	  only those samples identified as being part of the
	  production region.
	  The production region is identified by constructing a
	  time-series of the biasing potential and excluding
	  some percentage from the beginning of the simulation
	  as equilibration
	  such that the proposed production region yields
	  statistically equivalent biasing potential distributions
	  when it is split into two halves (as measured from Welsch's T-test).
	  The details of how the proposed production region is chosen
	  is a bit more complicated, and it is likely to change in the future.
	  The output of the script is a corresponding set of
	  t%02i/prod_%.2f.dumpave files that contain only those
	  samples within the identified production regions.
        </li>
	
        <li>
          <a href="#02_writemetafile_nosym">02_writemetafile_nosym.sh</a>
	  <br>
	  This script reads the disang and t%02i/prod_%.2f.dumpave
	  files, and it outputs a series of t%02i/win_%.2f.dumpave files
	  containing only the reaction coordinate(s) of interest.
	  In other words, it strips the dumpave files of unwanted information.
	  Furthermore, it writes the ndfes input files t%02i.nosym.metafile
	  and all.nosym.metafile, corresponding to the analysis of
	  each trial and the aggregate sampling, respectively.
        </li>
	
        <li>
          <a href="#03_runmbar_nosym">03_runmbar_nosym.sh</a>
	  <br>
	  This script runs the ndfes program to perform MBAR
	  analysis of the sampling from each trial and the
	  aggregate sampling from all trials.
	  The results of the analysis are stored in checkpoint
	  files.
	  The ndfes-AvgFESs.py script is used to read
	  the checkpoint files to make an overall
	  error estimate from the
	  deviations between the trial estimates
	  and the bootstrap errors of the individual
	  estimates.
	  It outputs a new checkpoint file containing
	  the aggregate sampling with the overall error
	  estimates and the individual trials.
	  The ndfes-PrintFES.py script is used to
	  read the resulting checkpoint file
	  and output the free energy surface values
	  and errors to a text file suitable for
	  plotting.
        </li>
	
        <li>
          <a href="#04_writemetafile_sym">04_writemetafile_sym.sh</a>
	  <br>
	  This script is analogous to 02_writemetafile_nosym.sh;
	  however, it enforces symmetry in the free energy
	  surface such that the free energy at +&xi;<sub>1</sub>
	  is the same as -&xi;<sub>1</sub>.
	  It does this by reading the
	  t%02i/prod_%.2f.dumpave files
	  and outputting two sets of t%02i/win_%.2f.dumpave.pos
	  and t%02i/win_%.2f.dumpave.neg dumpave files.
	  The "neg" dumpave files are the same samples as those
	  within the "pos" files, <i>except</i>:
	  the &xi;<sub>1</sub> values have been negated,
	  as if one obtained a corresponding set of samples
	  upon simulating a biasing potential centered about
	  -&xi;<sub>1,ref</sub>.
        </li>
	
        <li>
          <a href="#05_runmbar_sym">05_runmbar_sym.sh</a>
	  <br>
	  This script is the exact same as 03_runmbar_nosym.sh;
	  however, it performs MBAR analysis on the symmetrized
	  sampling such that the resulting free energy surfaces
	  are symmetric about the origin.
	  The ndfes-AvgFESs.py script is again used to
	  make an overall error estimate, and the ndfes-PrintFES.py
	  script outputs the surfaces to file for plotting.
        </li>
	
        <li>
          <a href="#06_makeplots">06_makeplots.sh</a>
	  <br>
	  This script uses the xmgrace program to plot the
	  free energy surfaces.  Two plots are created:
	  DFTB3.SymVsNoSym.agr compares the DFTB3 QM/MM
	  free energy surfaces with and without symmetrization
	  of the sampling, and DFTB3.TrialsVsAvg.agr
	  compares the free energy surfaces from the aggregate
	  sampling to the individual trial estimates.

	  The following plots are created:
	  <p>
	    <table class="center">
              <tr>
		<td width="792">
		  <img src="imgs/DFTB3.TrialsVsAvg.png">
		</td>
              </tr>
              <tr>
		<td>

		</td>
              </tr>
	    </table>
	  </p>
	  <p>
	    <table class="center">
              <tr>
		<td width="792">
		  <img src="imgs/DFTB3.SymVsNoSym.png">
		</td>
              </tr>
              <tr>
		<td>

		</td>
              </tr>
	    </table>
	  </p>

	  
	  
	  
        </li>
	
      </ul>
    </p>



    <h3>
      Required input files:
    </h3>
    <p>
      <ul>
	
        <li>
          ../07_qmmm_nvt_us_prod/t%02i/win_%.2f.disang
          <br>
          The restraint definition files used to perform production
	  sampling.
        </li>
	
        <li>
          ../07_qmmm_nvt_us_prod/t%02i/win_%.2f.dumpave
          <br>
          The reaction coordinate time series produced from production
	  sampling.
        </li>
	
      </ul>
    </p>



    

    <h3>
      The output files:
    </h3>
    <p>
      <ul>
        <li>
          checkequil.out
          <br>
          The output of the  ndfes-CheckEquil.py which summarizes
	  the number of samples identified as being equilibration
	  and production regions, and the statistical inefficiency
	  of the production samples.
	  This file is generated within the 01_checkequil.sh script.
        </li>

	<li>
	  t%02i/prod_%.2f.dumpave
	  <br>
	  The samples identified by ndfes-CheckEquil.py as
	  being part of the "production region".
	  This file is generated within the 01_checkequil.sh script.
	</li>

	
	<li>
	  t%02i/win_%.2f.dumpave
	  <br>
	  These are the same as t%02i/prod_%.2f.dumpave, except
	  all reaction coordinates are stripped from the file except the
	  &xi;<sub>1</sub> coordinate.
	  These files are produced from running ndfes-PrepareAmberData.py
	  within 02_writemetafile_nosym.sh.
	</li>

	
	<li>
	  t%02i.nosym.metafile
	  <br>
	  These are the ndfes input files that list the dumpave files
	  to read and the definition of the umbrella potential used
	  during sampling.
	  The metafiles with this naming convention are used to
	  perform MBAR analysis of the individual trials, whose
	  samples are stored in  t%02i/win_%.2f.dumpave.
	  These files are produced from running ndfes-PrepareAmberData.py
	  within 02_writemetafile_nosym.sh.
	</li>
	
	<li>
	  all.nosym.metafile
	  <br>
	  This is a ndfes input file listing all dumpave files and
	  umbrella potentials from all trials.
	  This file is produced by capturing the standard output of
	  ndfes-PrepareAmberData.py
	  within 02_writemetafile_nosym.sh.
	  This file is literally a concatenation of the
	  t%02i.nosym.metafile input files.
	</li>

	<li>
	  t%02i.nosym.chk and all.nosym.chk
	  <br>
	  These are the output checkpoint files produced by running
	  the ndfes program within 03_runmbar_nosym.sh.
	  They correspond to the analysis of the sampling listed
	  in the t%02i.nosym.metafile and all.nosym.metafile
	  input files.
	</li>

	<li>
	  t%02i.nosym.dat and all.nosym.dat
	  <br>
	  These are the free energy surfaces of the individual
	  trials and aggregate sampling produced by running
	  ndfes-PrintFES.py within 03_runmbar_nosym.sh.
	  The error estimates of each surface are from
	  a cyclic block bootstrap analysis.
	</li>

	<li>
	  avg.nosym.dat
	  <br>
	  This is the free energy surface of the aggregate
	  sampling and an overall error estimate by
	  combining the bootstrap error estimates and
	  trial deviations.
	  This is generated in 03_runmbar_nosym.sh
	  by running ndfes-PrintFES.py on the checkpoint
	  file generated by ndfes-AvgFESs.py.
	</li>



	<li>
	  t%02i/win_%.2f.dumpave.pos and t%02i/win_%.2f.dumpave.neg
	  <br>
	  These files are time series of the &xi;<sub>1</sub>
	  reaction coordinate produced by ndfes-PrepareAmberData.py
	  within 04_writemetafile_sym.sh.
	  The time series are taken from t%02i/prod_%.2f.dumpave,
	  described above, and  ndfes-PrepareAmberData.py
	  strips out all but the desired reaction coordinate.
	  The pos files should be identical to the t%02i/win_%.2f.dumpave
	  files described above, whereas the neg files
	  reverse the sign of the reaction coordinate
	  to enforce symmetry of the sampling about the origin.
	</li>

	
	<li>
	  t%02i.sym.metafile
	  <br>
	  These are the ndfes input files for analyzing the
	  individual trials of symmetrized sampling.
	  The t%02i.sym.metafile files will contain twice
	  as many lines as the t%02i.nosym.metafile files
	  because it instructs ndfes to read both the pos
	  and neg dumpave files described above.
	  The metafiles are constructed in 04_writemetafile_sym.sh
	  by capturing the standard output of ndfes-PrepareAmberData.py.
	</li>
	
	<li>
	  all.sym.metafile
	  <br>
	  This is the ndfes input file for analyzing
	  the aggregate symmetrized sampling from all
	  trials.
	  It is the concatenation of the t%02i.sym.metafile
	  input files generated within 04_writemetafile_sym.sh.
	</li>

	<li>
	  t%02i.sym.chk and all.sym.chk
	  <br>
	  These are the checkpoint files produced by
	  ndfes within 05_runmbar_sym.sh.
	  The checkpoint files contain the analysis of
	  the individual trials and aggregate sampling.
	</li>

	<li>
	  t%02i.sym.dat and all.sym.dat
	  <br>
	  These are the free energy surfaces of the
	  symmetrized sampling from each and all trials
	  produced with ndfes-PrintFES.py within 05_runmbar_sym.sh.
	</li>

	<li>
	  avg.sym.dat
	  <br>
	  This is the free energy surface of the
	  symmetrized aggregate sampling with an
	  overall error estimate produced by running
	  ndfes-PrintFES.py on the checkpoint file
	  generated from ndfes-AvgFESs.py within
	  05_runmbar_sym.sh.
	</li>
		
	<li>
	  DFTB3.SymVsNoSym.agr
	  <br>
	  An xmgrace input file that compares the
	  avg.sym.dat and avg.nosym.dat free energy
	  surfaces.
	  This file is generated by xmgrace
	  within 06_makeplots.sh.
	</li>
	
	<li>
	  DFTB3.TrialsVsAvg.agr
	  <br>
	  An xmgrace input file that compares the
	  avg.sym.dat free energy surface to the
	  individual trial estimates t%02i.sym.dat.
	  This file is generated by xmgrace
	  within  06_makeplots.sh.
	</li>


	
      </ul>
    </p>




    <h3>
      Saved outputs in data.tar.gz
    </h3>
    <p>
      <ul>
        <li>
	  checkequil.out
        </li>
	<li>
	  t%02i.nosym.dat and t%02i.sym.dat
	</li>
	<li>
	  all.nosym.dat and all.sym.dat
	</li>
	<li>
	  avg.nosym.dat and avg.sym.dat
	</li>
	<li>
	  avg.nosym.dat and avg.sym.dat
	</li>
	<li>
	  DFTB3.SymVsNoSym.agr and DFTB3.SymVsNoSym.png
	</li>
	<li>
	  DFTB3.TrialsVsAvg.agr and DFTB3.TrialsVsAvg.png
	</li>
	
      </ul>
    </p>


    
    <h3 id="01_checkequil">01_checkequil</h3>
    <p>
      <div class="caption">Contents of 08_mbar/01_checkequil.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

# Note: This script will produce 4.3 MB of output to disk
# and it will take 2-4 minutes of wallclock time
# to complete

simdir=../07_qmmm_nvt_us_prod

for trial in t01 t02 t03 t04 t05 t06; do
    
    if [ ! -d "${trial}" ]; then
	mkdir "${trial}"
    fi
        
    for RC1 in $(seq -1.3 0.1 1.3); do
	orig=$(printf "${simdir}/${trial}/win_%.2f" "${RC1}")
	prod=$(printf "${trial}/prod_%.2f" "${RC1}")

	#
	# Identify the unequilibrated portion of the
	# simulation and write a new dumpave containing
	# only those frames in the "production region"
	#
	# The ${orig}.disang and ${orig}.dumpave files
	# are the restraint definitions and time series
	# values obtained from the production sampling
	# in the previous step.
	#

	if [ ! -e "${orig}.disang" ]; then
	    echo "File not found: ${orig}.disang"
	    exit 1
	fi
	
	if [ ! -e "${orig}.dumpave" ]; then
	    echo "File not found: ${orig}.dumpave"
	    exit 1
	fi

	#
	# The default behavior of ndfes-CheckEquil.py is to
	# write a new dumpave file containing only the
	# statistically independent samples from the detected
	# "production region" of the biasing potential time
	# series. The statistically independent samples are
	# a subset of the data taken with a stride equal to
	# the "statistical inefficiency".
	# The statistical inefficiency is twice the auto-
	# correlation time plus 1, and rounded to an integer.
	# 
	# The --skipg option means that the output dumpave
	# should contain *all samples* from the production
	# region, not just the statistically independent
	# samples.  The correlation will instead be accounted
	# for within ndfes by using a cyclic block bootstrap
	# analysis, whose block size is the statistical
	# ineffiency.
	# 
	# The -r 1 option means that only the first reaction
	# coordinate defined in the disang file (the 2nd
	# column in the input dumpave file) should be used
	# to calculate the biasing energy.
	#
	# Run ndfes-CheckEquil.py --help for a complete
	# list of options and example usages.
	#
	
	ndfes-CheckEquil.py --skipg \
	 		    -d "${orig}.disang" \
	 		    -i "${orig}.dumpave" \
	 		    -o "${prod}.dumpave" \
	 		    -r 1 

    done

    #
    # The standard output of ndfes-CheckEquil.py contains
    # a brief 1-line summary of the time-series.
    # It will look something like:
    #
    # filename Ninp=%i Nout=%i Teq=%i i0=%i s=%i fst=%.2f +- %.2f lst=%.2f +- %.2f
    #
    # filename : The input dumpave file
    # Ninp : The number of samples in the input dumpave
    # Nout : The number of samples written to the output dumpave
    # Teq  : The percentage of the simulation to be discarded as equilibration
    #        This is an integer from 0 to 100.
    # i0   : The index of the first sample corresponding to the production
    #        region.
    # s    : The stride through the input data used to write the output data.
    #        If --skipg was set on the command line, then s will be 1 rather
    #        than the statistical inefficiency.
    # fst  : The average and standard error of the biasing potential
    #        analyzed from the first half of the samples within the
    #        production region.
    # lst  : The average and standard error of the biasing potential
    #        analyzed from the second half of the samples within the
    #        production region.
    #

    #
    # The file redirection, below, captures the standard output of
    # each call to ndfes-CheckEquil.py and stores the results in
    # checkequil.out
    #
    
done &> checkequil.out
</code></pre>
    </p>


    
    <h3 id="02_writemetafile_nosym">02_writemetafile_nosym</h3>
    <p>
      <div class="caption">Contents of 08_mbar/02_writemetafile_nosym.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

# Note: This script will produce 3 MB of output to disk
# and it will take 1 minute of wallclock time
# to complete

simdir=../07_qmmm_nvt_us_prod

for trial in t01 t02 t03 t04 t05 t06; do
    
    if [ ! -d "${trial}" ]; then
	echo "Directory not found: ${trial}"
	exit 1
    fi

    meta="${trial}.nosym.metafile"
    truncate -s0 ${meta}
    
    for RC1 in $(seq -1.3 0.1 1.3); do
	orig=$(printf "${simdir}/${trial}/win_%.2f" "${RC1}")
	prod=$(printf "${trial}/prod_%.2f" "${RC1}")
	dump=$(printf "${trial}/win_%.2f" "${RC1}")

	if [ ! -e "${prod}.dumpave" ]; then
	    echo "File not found: ${prod}.dumpave"
	    exit 1
	fi

	#
	# This performs two actions:
	# (1) It writes a new dumpave files that removes
	#     all reaction coordinates except the first
	#     coordinate defined in the disang file.
	#     (The -r option lists the desired reaction
	#      coordinates.)
	# (2) It writes a line to standard output that
	#     lists the new dumpave filename, the
	#     temperature, Hamilonian index, and
	#     restraint definition.  That is, the
	#     standard output is one line of the input
	#     file read by ndfes.  By redirecting the
	#     output to ${meta}, we build the input
	#     file one-line-at-a-time.
	#
	
	ndfes-PrepareAmberData.py -T 298. \
				  -d "${orig}.disang" \
				  -i "${prod}.dumpave" \
				  -o "${dump}.dumpave" \
				  -r 1 >> ${meta}

    done

done

#
# The input file used to analyze the aggregate sampling
# from all trials is the concatenation of each trial's
# input file.
#

allmeta="all.nosym.metafile"
truncate -s0 "${allmeta}"
for trial in t01 t02 t03 t04 t05 t06; do
    meta="${trial}.nosym.metafile"
    cat ${meta} >> ${allmeta}
done</code></pre>
<!-- ' -->
    </p>

    
    <h3 id="03_runmbar_nosym">03_runmbar_nosym</h3>
    <p>
      <div class="caption">Contents of 08_mbar/03_runmbar_nosym.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

# Note: This script will produce 1 MB of output to disk
# and it will take 1 minute of wallclock time
# to complete
    
for trial in t01 t02 t03 t04 t05 t06 all; do
    base="${trial}.nosym"
    echo "Analyzing ${base}.metafile"

    #
    # This runs the ndfes program to perform MBAR analysis.
    # When built as a standalone application, the OpenMP
    # version of ndfes is called ndfes_omp. If you are
    # using ndfes built within AmberTools, it is called
    # ndfes.OMP. The AmberTools OpenMP version is compiled
    # only if you configured cmake with -DOPENMP. The
    # serial version is called "ndfes".
    #
    # The -w 0.05 option specifies the bin width to
    # discretize the first reaction coordinate. Because
    # our reaction coordinate is measured in Angstroms,
    # the units of the width is also Angstroms. If
    # this was a multidimensional free energy surface,
    # the value of -w would be applied to all reaction
    # coordinates, but only can manually specify the
    # bin width of each dimension by using -w multiple
    # times.
    #
    # The --nboot option specifies the number of bootstrap
    # trials to perform to estimate the errors. The total
    # number of MBAR solutions is nboot + 1. First the
    # MBAR/UWHAM equations are solved with the input data,
    # and the bootstrap estimates reperform the calculation
    # with different subsets of resampled data. The
    # standard deviation of the bootstrap results is the
    # standard error in the free energy estimate.
    # By default, ndfes will perform a block bootstrap
    # procedure to account for correlation in the
    # data, based on the auto-correlation in the biasing
    # potential. Other options are present to instead
    # extract the statistically independent samples.
    # See ndfes --help for a full set of options.
    #
    # The -c option specifies the name of the checkpoint
    # file to create. The checkpoint file contains the
    # necessary information to plot and/or manipulate
    # the binned free energies.
    #
    # The standard output gives basic information about
    # the progress of the calculation, the properties
    # of the histogram, and the free energies of the
    # biased simulations.  The standard output is not
    # used in any manner to create plots of the free
    # energy surface; only the checkpoint file contains
    # the required information.
    #
    
    ndfes_omp --temp 298. --mbar -w 0.05 --nboot=50 \
	      -c "${base}.chk" \
	      "${base}.metafile" &> "${base}.out"
done

#
# The ndfes-AvgFES.py script will take the checkpoint
# files from the analysis of the individual trials
# and the checkpoint file from the aggregate sampling
# to produce an output checkpoint file.
# The output checkpoint file will contain 7 free
# energy surfaces, referred to as "models".
#
# The first model is the free energy surface generated
# from the analysis of the aggregate sampling; however,
# the uncertainty in the free energy values are replaced
# by a combined error estimate based on the bootstrap
# errors and deviations between the individual trials.
# The bootstrap errors are an uncertainty from the
# observed sampling, and the trial-deviations account
# for the finite sampling. The combined error estimate
# accounts for both sources of uncertainty.
#
# The remaining 6 models coorrespond to the 6
# individual trials.  The only difference between
# these models and their individual checkpoint files
# is that the free energy values have been shifted
# by a constant to reduce the squared differences
# with respect to the aggregate sampling.
# Formally, the "shifts" are chosen to minimize
# a weighted sum of squared differences, where the
# weights are related to the bootstrap errors and
# "reweighting entropies" (so that the shift is
# not dominated by large differences between bins
# with unreliable sampling).
#
# The name of the output checkpoint file is avg.nosym.chk.
# The --ene option means that the free energy values
# remain unchanged (only the error estimate is updated).
# If one wanted a true average of the trials, one could
# instead use the --ref option. In practice, these
# tend to produce nearly identical free energies, except
# in cases where some trials insufficiently sample
# a few bins (in which case, the --ene option tends to
# produce a more accurate result).
#

echo "Estimating errors from the 4 indpendent trials.."

ndfes-AvgFESs.py --fes "t01.nosym.chk" \
		 --fes "t02.nosym.chk" \
		 --fes "t03.nosym.chk" \
		 --fes "t04.nosym.chk" \
		 --fes "t05.nosym.chk" \
		 --fes "t06.nosym.chk" \
		 --ene "all.nosym.chk" \
		 -o "avg.nosym.chk"

echo "Done"


#
# As previously stated, the avg.nosym.chk will
# contain 7 models. The commands below extract
# each model and writes the free energy surface
# to a file suitable for plotting.
#
# The --model option selects the model from the
# checkpoint file.
#
# By default, the reported uncertainties are
# returned as a standard error.  The --ci option
# reports the uncertainties as a 95% confidence
# interval (1.96 * standard error).
#
# The output will contain 4 columns:
# X FE DFE RE
# where X is the center of a bin (with the same units
# as listed in the dumpave file),
# FE is the free energy value of the bin (kcal/mol),
# DFE is the uncertainty of the free energy (kcal/mol),
# RE is the "reweighting entropy", which is more
# useful when performing wTP or gwTP analysis to
# predict the free energy of a target potential
# from the sampling performed with a reference
# potential.

ndfes-PrintFES.py --model=0 --ci avg.nosym.chk > avg.nosym.dat
ndfes-PrintFES.py --model=1 --ci avg.nosym.chk > t01.nosym.dat
ndfes-PrintFES.py --model=2 --ci avg.nosym.chk > t02.nosym.dat
ndfes-PrintFES.py --model=3 --ci avg.nosym.chk > t03.nosym.dat
ndfes-PrintFES.py --model=4 --ci avg.nosym.chk > t04.nosym.dat
ndfes-PrintFES.py --model=5 --ci avg.nosym.chk > t05.nosym.dat
ndfes-PrintFES.py --model=6 --ci avg.nosym.chk > t06.nosym.dat
</code></pre>
    </p>

    
    <h3 id="04_writemetafile_sym">04_writemetafile_sym</h3>
    <p>
      <div class="caption">Contents of 08_mbar/04_writemetafile_sym.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

# Note: This script will produce 7 MB of output to disk
# and it will take 1-to-3 minutes of wallclock time
# to complete

simdir=../07_qmmm_nvt_us_prod
width=0.05


for trial in t01 t02 t03 t04 t05 t06; do
    
    if [ ! -d "${trial}" ]; then
	echo "Directory not found: ${trial}"
	exit 1
    fi

    meta="${trial}.sym.metafile"
    truncate -s0 ${meta}
    
    for RC1 in $(seq -1.3 0.1 1.3); do
	orig=$(printf "${simdir}/${trial}/win_%.2f" "${RC1}")
	prod=$(printf "${trial}/prod_%.2f" "${RC1}")
	dump=$(printf "${trial}/win_%.2f" "${RC1}")

	if [ ! -e "${prod}.dumpave" ]; then
	    echo "File not found: ${prod}.dumpave"
	    exit 1
	fi

	#
	# Write a new dumpave file that extracts the first
	# reaction coordinate (the -r 1 selects the first
	# reaction coordinate). The standard output will
	# be one line of the ndfes input file, which
	# we capture and append to ${trial}.sym.metafile.
	#
	
	ndfes-PrepareAmberData.py -T 298. \
				  -d "${orig}.disang" \
				  -i "${prod}.dumpave" \
				  -o "${dump}.dumpave.pos" \
				  -r 1 -w ${width} >> ${meta}

	#
	# This writes a symmetrized dumpave file whose
	# reaction coordinate values are negated.
	# The -s 1 option states that a symmetry operation
	# should be applied to the first reaction coordinate.
	# The standard output of ndfes-PrepareAmberData.py
	# is a line that tells ndfes to read a dumpave
	# file and informs it of the umbrella potential
	# definition. When the -s option is used, the sign
	# of the umbrella potential center will also be
	# negated.  The standard output is redirected to
	# the same metafile, such that the analysis
	# produces a symmetric free energy surface about
	# the origin.
	#
	
	ndfes-PrepareAmberData.py -T 298. \
				  -d "${orig}.disang" \
				  -i "${prod}.dumpave" \
				  -o "${dump}.dumpave.neg" \
				  -r 1 -s 1 -w ${width} >> ${meta}

	#
	# Notice that the -w option is used to specify the width of the
	# bins we will eventually use to perform the analysis.  The bin
	# width is included in this example to account for artifacts in
	# the rounding when analysis is performed. When ndfes is later
	# used to perform MBAR analysis, it will assign each sample to a
	# bin. The assignment takes the reaction coordinate value and
	# converts it to an integer.  The process "rounds down" (it
	# counts the number of whole bins "from the left" that must be
	# traversed to reach the sample of interest).  If a sample
	# happens to lie exactly on the boundary of two bins, the sample
	# is assigned to the left-most bin; however, the symmetrized
	# samples would need to change the rounding behavior by
	# selecting the right-most bin to preserve symmetry in the free
	# energy surface.  By setting the value of the bin width,
	# ndfes-PrepareAmberData.py will determine if a sample lies
	# directly on a boundary and perturbs the sample by 0.000001 to
	# avoid the inconsistency in rounding behavior between true and
	# symmetrized samples.  This is largely only an issue because of
	# the limited number of digits in the format written by sander.
	# If the dumpave files were instead created by measuring the
	# distances in the trajectory file, one would only rarely
	# encounter situations where a sample lied directly on a bin
	# boundary. Setting the bin width is only meaningful when
	# attempting to enforce symmetry, which -- again -- is the
	# exception rather than the rule.
	#
    done

done


#
# The input file for the aggregate symmetrized sampling
# is the concatenation of the inputs for the individual trials
#

allmeta="all.sym.metafile"
truncate -s0 "${allmeta}"
for trial in t01 t02 t03 t04 t05 t06; do
    meta="${trial}.sym.metafile"
    cat ${meta} >> ${allmeta}
done
</code></pre>
    </p>


    
    <h3 id="05_runmbar_sym">05_runmbar_sym</h3>
    <p>
      <div class="caption">Contents of 08_mbar/05_runmbar_sym.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

# Note: This script will produce 1 MB of output to disk
# and it will take 1 minutes of wallclock time
# to complete

#
# This is the exact same as 03_runmbar_nosym.sh, except
# it performs the MBAR analysis on the symmetrized
# sampling. Please see 03_runmbar_nosym.sh for further
# details
#

for trial in t01 t02 t03 t04 t05 t06 all; do
    base="${trial}.sym"
    echo "Analyzing ${base}.metafile"
    ndfes_omp --temp 298. --mbar -w 0.05 --nboot=50 \
	      -c "${base}.chk" \
	      "${base}.metafile" &> "${base}.out"
done

echo "Estimating errors from the 4 indpendent trials.."

ndfes-AvgFESs.py --fes "t01.sym.chk" \
		 --fes "t02.sym.chk" \
		 --fes "t03.sym.chk" \
		 --fes "t04.sym.chk" \
		 --fes "t05.sym.chk" \
		 --fes "t06.sym.chk" \
		 --ene "all.sym.chk" \
		 -o "avg.sym.chk"

echo "Done"

ndfes-PrintFES.py --model=0 --ci avg.sym.chk > avg.sym.dat
ndfes-PrintFES.py --model=1 --ci avg.sym.chk > t01.sym.dat
ndfes-PrintFES.py --model=2 --ci avg.sym.chk > t02.sym.dat
ndfes-PrintFES.py --model=3 --ci avg.sym.chk > t03.sym.dat
ndfes-PrintFES.py --model=4 --ci avg.sym.chk > t04.sym.dat
ndfes-PrintFES.py --model=5 --ci avg.sym.chk > t05.sym.dat
ndfes-PrintFES.py --model=6 --ci avg.sym.chk > t06.sym.dat
</code></pre>
    </p>

    
    <h3 id="06_makeplots">06_makeplots</h3>
    <p>
      <div class="caption">Contents of 08_mbar/06_makeplots.sh</div>
      <pre class="file"><code>#!/bin/bash
set -e
set -u

for f in avg.sym.dat avg.nosym.dat t01.sym.dat t02.sym.dat \
	 t03.sym.dat t04.sym.dat t05.sym.dat t06.sym.dat \
	 DFTB3.SymVsNoSym.par DFTB3.TrialsVsAvg.par; do
    if [ ! -e "${f}" ]; then
	echo "File not found: ${f}"
	exit 1
    fi
done

#
# This is a shift chosen to make the minimum of the avg.sym.dat free
# energy surface be zero. The value was manually chosen.
#

DY=1.61157

#
# This uses the first 3 columns of avg.sym.dat and avg.nosym.dat to make
# a comparison of the free energy surfaces with and without
# symmetrization of the samples. The first column defines the x-values,
# the second column is the y-values, and the third column used to draw
# vertical error bars.
# The provided DFTB3.SymVsNoSym.par file defines the general look of the
# graph by defining the axis labels and set appearance.
# The -hardcopy option tells xmgrace to print the generated plot.
# The -hdevice option tells xmgrace that the plot should be printed as
# a PNG image.
# The -printfile option tells xmgrace to print the PNG image to a file
# called DFTB3.SymVsNoSym.png.
# The -saveall option tells xmgrace to save the plot to DFTB3.SymVsNoSym.agr
# such that one can open the plot by typing "xmgrace DFTB3.SymVsNoSym.agr"
#


xmgrace -settype xydy avg.sym.dat \
	-settype xydy avg.nosym.dat \
	-pexec "S0.y = S0.y - ${DY}" \
	-pexec "S1.y = S1.y - 1.61557" \
	-para DFTB3.SymVsNoSym.par \
	-hardcopy -hdevice PNG \
	-printfile DFTB3.SymVsNoSym.png \
	-saveall DFTB3.SymVsNoSym.agr

#
# This plots the free energies of the 6 individual trials and compares
# them to the surface estimated from the aggregate sampling.  The -pexec
# options shift the surfaces to a common zero of energy, and the
# interpolate command constructs an 8th data set by generating a cubic
# spline of the 7th data set. The x-values of the spline consists of 501
# points ranging from -1.5 to 1.5.
#

xmgrace -settype xy t01.sym.dat \
	-settype xy t02.sym.dat \
	-settype xy t03.sym.dat \
	-settype xy t04.sym.dat \
	-settype xy t05.sym.dat \
	-settype xy t06.sym.dat \
	-settype xydy avg.sym.dat \
	-pexec "S0.y = S0.y - ${DY}" \
	-pexec "S1.y = S1.y - ${DY}" \
	-pexec "S2.y = S2.y - ${DY}" \
	-pexec "S3.y = S3.y - ${DY}" \
	-pexec "S4.y = S4.y - ${DY}" \
	-pexec "S5.y = S5.y - ${DY}" \
	-pexec "S6.y = S6.y - ${DY}" \
	-pexec "INTERPOLATE(S6,MESH(-1.5,1.5,501),SPLINE,OFF)" \
	-para DFTB3.TrialsVsAvg.par \
	-hardcopy -hdevice PNG \
	-printfile DFTB3.TrialsVsAvg.png \
	-saveall DFTB3.TrialsVsAvg.agr
</code></pre>
    </p>


    
  </body>
</html>
