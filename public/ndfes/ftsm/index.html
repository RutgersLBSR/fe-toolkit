<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
  </head>
  <body>

    <h1>
      Umbrella sampling finite temperature string method
    </h1>

    <p>This tutorial performs the grid-based string method
      to obtain the minimum free energy pathway of the MTR1
      methyltransferase reaction using 2 reaction coordinates.
      The reaction coordinates are:
      <ol>
	<li>RC1: An R12 coordinate describing a proton transfer
	between C10:N3 and <i>O</i><sup>6</sup>mG:N1</li>
	<li>RC2: An R12 coordinate describing a methyl transfer
	between <i>O</i><sup>6</sup>mG:O6 and A63:N1</li>
      </ol>
      The umbrella sampling is performed with DFTB3 QM/MM,
      and the QM region of the approximate reactant
      and product states are shown below.
    </p>

    <img src="imgs/MTR1.reactant.png"/>
    <img src="imgs/MTR1.product.png"/>


    
    <p>
      <ul>
        <li>
          <a href="#Overview">Overview</a>
        </li>
	<li>
	  <a href="#Dir">Example directory and files</a>
	</li>
	<li>
	  <a href="#Cmds">Commands to run the example</a>
	</li>
        <li>
          <a href="#InitialGuess">Make an initial guess</a>
        </li>
        <li>
          <a href="#Sample">Run the simulations</a>
        </li>
        <li>
          <a href="#Analysis">Analyze the results and make a new guess</a>
        </li>
      </ul>
    </p>


    <h3 id="Overview">Overview</h3>
    <p>
      <ol>
	<li>
	  Make a guess for the parametric curve describing the
	  free energy pathway.
	  If you performed umbrella sampling from a previous guess,
	  then the sampling can be used to
	  <a href="#Analysis">generate a new guess</a>,
	  otherwise you need to prepare an
	  <a href="#InitialGuess">initial guess</a>.
	</li>
	<li>
	  Decide where along the curve to place the umbrella
	  potentials and what force constants should be used.
	</li>
	<li>
	  Perform umbrella sampling.
	</li>
	<li>
	  Analyze the sampling to update the path and generate
	  a new series of simulations.
	</li>
      </ol>
    </p>

    <p>
      <p>What you absolutely need to get started is a directory that contains
      the mdin, restraint, and restart files for the first guess of the string.
      It should look like:</p>
      <pre class="file"><code>[user@computer]$ ls init/*.mdin init/*.disang init/init*.rst7
init/img001.disang  init/img009.disang  init/img017.disang  init/img025.disang  init/init001.rst7  init/init017.rst7
init/img001.mdin    init/img009.mdin    init/img017.mdin    init/img025.mdin    init/init002.rst7  init/init018.rst7
init/img002.disang  init/img010.disang  init/img018.disang  init/img026.disang  init/init003.rst7  init/init019.rst7
init/img002.mdin    init/img010.mdin    init/img018.mdin    init/img026.mdin    init/init004.rst7  init/init020.rst7
init/img003.disang  init/img011.disang  init/img019.disang  init/img027.disang  init/init005.rst7  init/init021.rst7
init/img003.mdin    init/img011.mdin    init/img019.mdin    init/img027.mdin    init/init006.rst7  init/init022.rst7
init/img004.disang  init/img012.disang  init/img020.disang  init/img028.disang  init/init007.rst7  init/init023.rst7
init/img004.mdin    init/img012.mdin    init/img020.mdin    init/img028.mdin    init/init008.rst7  init/init024.rst7
init/img005.disang  init/img013.disang  init/img021.disang  init/img029.disang  init/init009.rst7  init/init025.rst7
init/img005.mdin    init/img013.mdin    init/img021.mdin    init/img029.mdin    init/init010.rst7  init/init026.rst7
init/img006.disang  init/img014.disang  init/img022.disang  init/img030.disang  init/init011.rst7  init/init027.rst7
init/img006.mdin    init/img014.mdin    init/img022.mdin    init/img030.mdin    init/init012.rst7  init/init028.rst7
init/img007.disang  init/img015.disang  init/img023.disang  init/img031.disang  init/init013.rst7  init/init029.rst7
init/img007.mdin    init/img015.mdin    init/img023.mdin    init/img031.mdin    init/init014.rst7  init/init030.rst7
init/img008.disang  init/img016.disang  init/img024.disang  init/img032.disang  init/init015.rst7  init/init031.rst7
init/img008.mdin    init/img016.mdin    init/img024.mdin    init/img032.mdin    init/init016.rst7  init/init032.rst7</code></pre>
      <p>You will also need an Amber parameter file and template mdin and restraint files.
      In the scripts I have prepared, they are called template/qmmm.parm7 template/long.mdin and template/img.disang.
      Much of the discussion below focuses the preparation of the init/ directory, but feel free to
      prepare the init directory in any manner that you like.</p>
    </p>

    <p>
      When you compile ndfes from the gitlab distribution, you will have installed
      the ndfes, ndfes-path, ndfes_omp, and ndfes-path_omp programs
      in addition to a python library and several python scripts.
      The "_omp" versions of the programs support OpenMP parallelization.
      You are encouraged to use the parallelized versions, and you
      can set the number of threads by "export OMP_NUM_THREADS=16", for example.
    </p>
    
    <p>
      If you are using the ndfes installed as a part of AmberTools,
      you will not have the OpenMP parallel versions of these codes unless
      you configured Amber with the "-DOPENMP=TRUE" option included
      in the cmake command.
      If you set this option, the parallelized versions will be compiled,
      but they will be called ndfes.OMP and ndfes-path.OMP (based on the
      amber filename conventions).
      In this case, you will need to edit the provided examples to replace
      "_omp" with ".OMP". Alternatively, you could create a symbolic links
      from ndfes.OMP and ndfes-path.OMP to ndfes_omp and ndfes-path_omp,
      respectively.
    </p>
    

    <h3 id="Dir">Example directory and files</h3>

    <p>
      <a href="ftsm_example.tar.gz">DOWNLOAD: ftsm_example.tar.gz</a>
      (2.9 MB) Unpacking this file will create the directory ./example<br>
      
      <div class="caption">Example directory contents</div>
      <pre class="file"><code>[user@computer]$ ls -R
.:
scripts  start  template

./scripts:
Make2dMovie.sh  submit_ftsm_pipeline.sh

./start:
md.mdin  mol.parm7  product.disang  reactant.disang  reactant.rst7

./template:
ana.slurm  img.disang  simandana.slurm  sim.slurm

      </code></pre>

      <ul>
	<li>start/md.mdin
	  <br>
	  A sander input file for performing biased QM/MM simulations of MTR1.
	</li>
	<li>start/mol.parm7
	  <br>
	  The Amber parameter file for the MTR1 system.
	</li>
	<li>start/product.disang
	  <br>
	  <i>This file is only used to prepare an initial guess for the first string.</i><br>
	  The umbrella window positions defining the approximate positions of the reaction product.
	</li>
	<li>start/reactant.disang
	  <br>
	  <i>This file is only used to prepare an initial guess for the first string.</i><br>
	  The umbrella window positions defining the approximate positions of the reactant state
	</li>
	<li>start/reactant.rst7
	  <br>
	  <i>This file is only used to prepare an initial guess for the first string.</i><br>
	  The Amber restart file of an equilibrated reactant state.
	</li>
	<li>template/img.disang
	  <br>
	  A template restraint file used to build new inputs during the string calculations.
	  This file replace the r2 and r3 values (the positions of umbrella centers)
	  with special  placeholders.
	</li>
	<li>template/simandana.slurm
	  <br>
	  <p>An example slurm script that performs the string calculations.
	  It assumes that you have prepared a directory named ./init which contains
	  32 mdin files (init/img%03i.mdin), 32 restraint files (init/img%03i.disang),
	  and 32 restart files (init/init%03i.rst7).
	  You can choose to use more (or fewer) images per string, but you would need
	  to edit the slurm script appropriately.</p>
	</li>
	<li>template/sim.slurm
	  <br>
	  <i>This script is not needed if you use simandana.slurm.</i><br>
	  <p>The simandana.slurm script performs all of the simulations at the same time,
	  but you may not have the resources available to do that. In principle, the
	  simulations can be performed independently of each other, and the analysis
	  is made after all the simulations have completed.  The sim.slurm script
	  performs a simulation of a single image within a single string iteration.
	  It uses <a href="https://slurm.schedmd.com/job_array.html">slurm job arrays</a>
	  to launch the simulations.  One needs to modify the slurm script by
	  replacing "CIT" with an integer to tell it which string iteration you are
	  performing. For example, to perform the 32 simulations in the ./init directory
	  (string iteration 0), you would type:</p>
	  <pre><code>[user@cluster] sed -e "s/CIT/0/g" template/sim.slurm > sim.0.slurm
[user@cluster] sbatch --array=1-32 sim.0.slurm
sbatch: Submitted batch job 65541</code></pre>
	</li>
	<li>template/ana.slurm
	  <br>
	  <i>This script is not needed if you use simandana.slurm.</i><br>
	  <p>This analyzes the results of a particular iteration and creates a new
	  directory and input files for the next iteration. One would only use
	  this script if sim.slurm is used in place of simandana.slurm.
	  Like the sim.slurm script, you need to replace CIT with the current string
	  iteration. If your simulations are still running, then you will want to
	  include a slurm dependency.</p>
	  <pre><code>[user@cluster] sed -e "s/CIT/0/g" template/ana.slurm > ana.0.slurm
[user@cluster] sbatch --dependency=afterok:65541 ana.0.slurm
sbatch: Submitted batch job 65542</code></pre>
	</li>
	
	<li>scripts/submit_ftsm_pipline.sh
	  <br>
	  <i>This script is not needed if you use simandana.slurm.</i><br>
	  <p>This launches a series of jobs based in sim.slurm and ana.slurm. You will need
	  to edit te NSIM, START, and STOP variables to tell it the number of images
	  and the first and last string iterations to calculate. All it really does is
	  perform the sed and sbatch commands shown above.</p>
	</li>
	
	<li>scripts/Make2dMovie.sh
	  <br>
	  Generates a movie showing the evolution of the string and free energy surface.
	</li>

	
      </ul>

      
      <div class="caption">Contents of start/md.mdin</div>
      <pre class="file"><code>title
&cntrl
! IO =======================================
      irest = 1       ! 0 = start, 1 = restart
        ntx = 5       ! 1 = start, 5 = restart
       ntxo = 1       ! read/write rst as formatted file
      iwrap = 1       ! wrap crds to unit cell
     ioutfm = 1       ! write mdcrd as netcdf
       imin = 0
      ntmin = 1
       ntpr = 50
       ntwr = 50
       ntwx = 0
       ntwf = 0
! DYNAMICS =================================
     nstlim = 625    ! number of time steps
         dt = 0.001   ! ps/step
        ntb = 1       ! 1=NVT periodic, 2=NPT periodic, 0=no box
! TEMPERATURE ==============================
      temp0 = 298     ! target temp
   gamma_ln = 5.0     ! Langevin collision freq
        ntt = 3       ! thermostat (3=Langevin)
! PRESSURE  ================================
        ntp = 0       ! 0=no scaling, 1=isotropic, 2=anisotropic
! SHAKE ====================================
        ntc = 2       ! 1=no shake, 2=HX constrained, 3=all constrained
!noshakemask = ":69|@272-282,290-291,1975-1988"    ! do not shake these
        ntf = 1       ! 1=cpt all bond E, 2=ignore HX bond E, 3=ignore all bond E
! MISC =====================================
        cut = 10.0
      ifqnt = 1
         ig = -1
     nmropt = 1
/


&ewald
  dsum_tol = 1.e-6
/

&qmmm
    qm_theory   = 'DFTB3'
        qmmask  = ':69|@272-282,290-291,1975-1988'
      qmcharge  = 1
          spin  = 1
       qmshake  = 0
      qm_ewald  = 1
   qmmm_switch  = 1
       scfconv  = 1.e-10
     verbosity  = 0
  tight_p_conv  = 1
  diag_routine  = 0
   pseudo_diag  = 1
  dftb_maxiter  = 100
/


&wt type = 'DUMPFREQ', istep1 = 5, /
&wt type='END' /
DISANG=img01.disang
DUMPAVE=img01.dumpave
LISTOUT=POUT
LISTIN=POUT

</code></pre>

      <div class="caption" id="imgdisang">Contents of template/img.disang</div>
      <pre class="file"><code>#protonation: |C9-H3|-|GN-H3| 
&rst iat= 291, 290, 291, 2193,
r1=-999, r2=RC1, r3=RC1, r4=999, rk2=100, rk3=100, rstwt=1.0,-1.0 /

#methylation: |GN-C5|-|A62-C5|
&rst iat= 2200, 2189, 2200, 1984,
r1=-999, r2=RC2, r3=RC2, r4=999, rk2=100, rk3=100, rstwt=1.0,-1.0 /

#Max distances for H-bonds

## 1 ##
&rst iat= 1403, 2207,
r1=-999, r2=-999, r3=3.00, r4=999,rk2=0, rk3=100 /

## 2 ##
&rst iat= 1405, 2192,
r1=-999, r2=-999, r3=2.20, r4=999,rk2=0, rk3=100 /

## 3 ##
&rst iat= 1407, 2206,
r1=-999, r2=-999, r3=2.00, r4=999,rk2=0, rk3=100 /

## 4 ##
&rst iat= 2205, 282,
r1=-999, r2=-999, r3=2.20, r4=999,rk2=0, rk3=100 /

## 5 ##
&rst iat= 280, 2189,
r1=-999, r2=-999, r3=2.20, r4=999,rk2=0, rk3=100 /

## 6 ##
&rst iat= 2190, 1982,
r1=-999, r2=-999, r3=3.00, r4=999,rk2=0, rk3=100 /

</code></pre>

      
      <div class="caption" id="simslurm">Contents of template/simandana.slurm</div>
      <pre class="file"><code>#!/bin/bash
#SBATCH --job-name="JOBNAME"
#SBATCH --output="JOBNAME.slurmout"
#SBATCH --error="JOBNAME.slurmerr"
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=128
#SBATCH --mem=243G
#SBATCH --account=rut149
#SBATCH --no-requeue
#SBATCH --export=ALL
#SBATCH -t 1-06:00:00

#set -e
set -u

#
# Setup the environment to use sander and ndfes
#

module load york/amber
module load fetoolkit/01dec23

export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1

#
# The parm7 file and the number of simulations per string
#

PARM=template/qmmm.parm7
NSIM=32

#
# cit is an integer: "current (string) iteration"
#

for cit in $(seq 0 100); do

    #
    # The command to generate a new directory of simulations
    #
    
    MAKESIMS="ndfes-path_omp --curit=${cit} -d template/img.disang -i template/long.mdin --sasm --minsize=10 --wavg=4 --wavg-niter=1 --neqit=4"

    #
    # The command to post-process a directory of simulations
    #
    
    MAKEANA="ndfes-path-analyzesims.py --curit=${cit} -d template/img.disang --maxeq=0.25 --skipg --neqit=4"
    
    #
    # Iteration 0 refers to the initial guess
    # curdir: "current directory" (to be simulated and analyzed)
    # newdir: "new directory" (write the inputs for the next iteration here)
    #
    
    if [ "${cit}" -eq 0 ]; then
        curdir=init
    else
        curdir=$(printf "it%03i" ${cit})
    fi
    newdir=$(printf "it%03i" $(( ${cit} + 1 )))

    #
    # Do we already have newdir/img01.disang ?
    # If so, then skip this iteration because it has already been completed
    #

    if [ -e ${newdir}/img001.disang ]; then
        continue
    fi
    
    #
    # Run simulations. First check if they have already been run
    #
    
    cd ${curdir}

    NEEDSIM=0
    for simidx in $(seq ${NSIM}); do
        base=$(printf "img%03i" ${simidx})
        if [ ! -e "${base}.mdout" ]; then
            NEEDSIM=1
            break
        else
	    if grep -q "Convergence could not be achieved" ${base}.mdout; then
		NEEDSIM=1
	    elif grep -q "   5.  TIMINGS" ${base}.mdout; then
                continue
            else
                NEEDSIM=1
                break
            fi
        fi
    done

    #
    # Run simulations if necessary
    #
    
    if [ "${NEEDSIM}" == "1" ]; then
        
        for simidx in $(seq ${NSIM}); do
            if [ -e "failed_${simidx}.stat" ]; then
                rm "failed_${simidx}.stat"
            fi
            base=$(printf "img%03i" ${simidx})
            crd=$(printf "init%03i.rst7" ${simidx})
            srun --exclusive --mpi=pmi2 -n 4 sander.MPI -O -p ../${PARM} -i ${base}.mdin -c ${crd} -o ${base}.mdout -r ${base}.rst7 -x ${base}.nc -inf ${base}.mdinfo &
        done
    
        wait
    fi

    #
    # Check for failures
    #
    
    FAILED=0
    for simidx in $(seq ${NSIM}); do
        base=$(printf "img%03i" ${simidx})
	if grep -q "Convergence could not be achieved" ${base}.mdout; then
             FAILED=1
        elif grep -q "   5.  TIMINGS" ${base}.mdout; then
            continue
        else
            FAILED=1
        fi
    done

    if [ "${FAILED}" == 1 ]; then

	#
	# Try rerunning the simulations if something failed
	#

        for simidx in $(seq ${NSIM}); do
            if [ -e "failed_${simidx}.stat" ]; then
                rm "failed_${simidx}.stat"
            fi
            base=$(printf "img%03i" ${simidx})
            crd=$(printf "init%03i.rst7" ${simidx})
	    if grep -q "Convergence could not be achieved" ${base}.mdout; then
               srun --exclusive --mpi=pmi2 -n 4 sander.MPI -O -p ../${PARM} -i ${base}.mdin -c ${crd} -o ${base}.mdout -r ${base}.rst7 -x ${base}.nc -inf ${base}.mdinfo &
            fi
        done

        wait

	#
	# If it STILL failed, then stop
	#

        FAILED=0
        for simidx in $(seq ${NSIM}); do
            base=$(printf "img%03i" ${simidx})
            if grep -q "Convergence could not be achieved" ${base}.mdout; then
                FAILED=1
                echo 1 > failed_${simidx}.stat
            elif grep -q "   5.  TIMINGS" ${base}.mdout; then
                continue
            else
                FAILED=1
                echo 1 > failed_${simidx}.stat
            fi
        done

    fi

    if [ "${FAILED}" == 1 ]; then
	echo "Detected a failed simulation"
        exit
    fi

    cd ../
    
    #
    # Prepare the dumpaves and metafile
    #
    
    echo "RUNNING: ${MAKEANA}" >> ${curdir}/analysis.slurmout 2>&1
    echo "$(date)" >> ${curdir}/analysis.slurmout 2>&1
    
    ${MAKEANA} >>  ${curdir}/analysis.slurmout 2>&1

    echo "FINISHED MAKEANA" >> ${curdir}/analysis.slurmout 2>&1
    echo "$(date)" >> ${curdir}/analysis.slurmout 2>&1

    #
    # Run ndfes to get the FES
    #

    topdir=${PWD}
    cd ${curdir}/analysis

    export OMP_NUM_THREADS=64
	
    srun --exclusive -n 1 -c 64 ndfes_omp --mbar -w 0.15 --nboot 0 -c metafile.all.chk metafile.all >> ../analysis.slurmout 2>&1
	
    export OMP_NUM_THREADS=1

    echo "FINISHED NDFES" >> ${topdir}/${curdir}/analysis.slurmout 2>&1
    echo "$(date)" >> ${topdir}/${curdir}/analysis.slurmout 2>&1

    cd ${topdir}

    #
    # Prepare a new directory for the next iteration
    #
    
    if [ ! -d ${newdir} ]; then
        mkdir ${newdir}
    fi
    
    echo "RUNNING: ${MAKESIMS}" >> ${curdir}/analysis.slurmout 2>&1
    echo "$(date)" >> ${curdir}/analysis.slurmout 2>&1

    export OPENBLAS_NUM_THREADS=64
    export OMP_NUM_THREADS=64

    srun --exclusive -n 1 -c 64 ${MAKESIMS} >> ${curdir}/analysis.slurmout 2>&1
    
    echo "FINISHED MAKESIMS" >> ${curdir}/analysis.slurmout 2>&1
    echo "$(date)" >> ${curdir}/analysis.slurmout 2>&1

    export OPENBLAS_NUM_THREADS=1
    export OMP_NUM_THREADS=1

done
      </code></pre>



      


    
    <h3 id="Cmds">Commands to run the example</h3>
    <p>
      <ol>
	<li>
	  <code class="cmd">cp start/mol.parm7 template/qmmm.parm7</code><br>
	  <p>The template/simandana.slurm submission script assumes the
	  parameter file is called template/qmmm.parm7.</p>
	</li>
	<li>
	  <pre class="cmd"><code>cp start/md.mdin template/short.mdin
sed -i \
    -e 's/nstlim.*/nstlim = 312/' \
    -e 's/ntpr.*/ntpr = 100/' \
    -e 's/ntwr.*/ntwr = 10000/' template/short.mdin</code></pre>
	  
	  <p>The short.mdin file will be used to scan the path
	  for the sole purpose of generating initial structures
	  by nudging the umbrella windows along the initial guess
	  pathway.</p>
	</li>

	<li>
	  <pre class="cmd"><code>cp start/md.mdin template/long.mdin
sed -i \
    -e 's/nstlim.*/nstlim = 625/' \
    -e 's/ntpr.*/ntpr = 100/' \
    -e 's/ntwr.*/ntwr = 10000/' template/long.mdin</code></pre>
	  <p>The long.mdin file will be used to perform umbrella
	  sampling. The scripts/simandana.slurm script assumes
	  the mdin template file is template/long.mdin and the
	  template disang file is template/img.disang.</p>
	</li>
	<li>
	  <pre class="cmd"><code>ndfes-path-prepguess.py -d template/img.disang -i template/short.mdin \
    --min=start/reactant.disang --min=start/product.disang -n 32 --dry-run</code></pre>
	  <p>Look at the image spacings that would be generated.
	  The surface-accelerated string method (sasm) shouldn&apos;t be too sensitive to the number
	  of images, but if you perform string calculations with other methods,
	  you might consider using more images if the spacing between them is too large.</p>
	</li>
	<li>
	  <code class="cmd">mkdir scan</code><br>
	  A work directory used to perform the scan which generates
	  initial structures.
	</li>
	<li>
	  <pre class="cmd"><code>ndfes-path-prepguess.py -d template/img.disang -i template/short.mdin \
    --min=start/reactant.disang --min=start/product.disang -n 32 --odir=scan</code></pre>
	  <p>Writes disang and mdin files to the scan directory.
	  It also writes scan/eq01fwd.sh which is a series of
	  commands that performs brief simulations in sequence
	  along the path,
	  restarting from the restart file of the previous
	  image.</p>
	</li>
	<li>
	  <pre class="cmd"><code>cat &lt;&lt;EOF > scan/sub.slurm
#!/bin/bash
#SBATCH --job-name="scan"
#SBATCH --output="sub.slurmout"
#SBATCH --error="sub.slurmerr"
#SBATCH --partition=main
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --requeue
#SBATCH --export=ALL
#SBATCH -t 2-00:00:00
#SBATCH --exclude=slepner[009-045,048,054-061,079-081]

set -e
set -u

module load york/amber

export LAUNCH="mpirun -n 4 sander.MPI"
export PARM="../template/qmmm.parm7"
bash eq01fwd.sh

EOF
</code></pre>
	  Write a slurm script to perform the scan.

	</li>
	<li>
	  <pre class="cmd"><code>rsync -avu ./ username@cluster:/path/to/scratch
ssh username@cluster
cd /path/to/scratch</code></pre>
	  Copy directory to remote cluster, login, and change directory on the remote machine.
	</li>
	<li>
	  <code class="cmd">cd ./scan && sbatch sub.slurm && cd ../</code><br>
	  Submit the job that scans the initial path
	</li>
	<li>
	  <pre class="cmd"><code>mkdir equil

for rst in scan/img*.rst7; do 
   base=$(basename ${rst})
   init=equil/init${base#img}
   cp ${rst} ${init}
done

for disang in scan/img*.disang; do
   cp ${disang} equil/
done

for mdin in scan/img*.mdin; do
   base=$(basename ${mdin})
   sed -e "s/nstlim.*$/nstlim=4000/" ${mdin} > equil/${base}
done

cat &lt;&lt;EOF > equil/equil.slurm
#!/bin/bash
#SBATCH --job-name="scan"
#SBATCH --output="sub.slurmout"
#SBATCH --error="sub.slurmerr"
#SBATCH --partition=main
#SBATCH --array=1-32
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --requeue
#SBATCH --export=ALL
#SBATCH -t 2-00:00:00
#SBATCH --exclude=slepner[009-045,048,054-061,079-081]

set -e
set -u

module load york/amber

export LAUNCH="mpirun -n 4 sander.MPI"
export PARM="../template/qmmm.parm7"

img=$(printf "img%03i" ${SLURM_ARRAY_TASK_ID})
init=$(printf "init%03i" ${SLURM_ARRAY_TASK_ID})

${LAUNCH} sander.MPI -O -p ${PARM} -i ${img}.mdin -o ${img}.mdout -c ${init}.rst7 -r ${img}.rst7 -x ${img}.nc -inf ${img}.mdinfo
EOF

cd equil
sbatch equil.slurm
cd ../
</code></pre>
	  <p>
	  This copies the results from scan/ to a directory equil/ and we then equilibrate
	  each state for 4 ps.  The scan directory only gave use reasonable starting configurations, but
	  we also need to equilibrate each structure for a bit before starting the string iterations.
	  The result of this equilibration is our "initial guess".
	  </p>
	</li>
	<li>
	  
	  <pre class="cmd"><code>mkdir init

for rst in equil/img*.rst7; do 
   base=$(basename ${rst})
   init=init/init${base#img}
   cp ${rst} ${init}
done

for disang in equil/img*.disang; do
   cp ${disang} init/
done

for mdin in equil/img*.mdin; do
   base=$(basename ${mdin})
   sed -e "s/nstlim.*$/nstlim=625/" ${mdin} > init/${base}
done</code></pre>
	  This copies the results from equil/ to init/. We are now ready to begin
	  the string method.
	</li>
	<li>
	  <code class="cmd">sbatch template/simandana.slurm</code><br>
	  <p>
	  This script contains a loop that will perform simulations,
	  analyze the results, and create the next directory of simulations.
	  I have provided 2 similar scripts called template/sim.slurm
	  and template/ana.slurm which breaks this up into separate
	  calculations. The template/sim.slurm script uses slurm arrays
	  to launch a set of simulations. The template/ana.slurm can then
	  be used to analyze the simulations once they have completed.
	  To use these scripts, replace the CIT variable with the
	  current iteration. For example,
	  </p>
	  <pre><code>
[user@cluster] sed -e "s/CIT/0/g" template/sim.slurm > sim.0.slurm
[user@cluster] sed -e "s/CIT/0/g" template/ana.slurm > ana.0.slurm
[user@cluster] sbatch --array=1-32 sim.0.slurm
sbatch: Submitted batch job 65541
[user@cluster] sbatch --dependency=afterok:65541 ana.0.slurm
sbatch: Submitted batch job 65542
	  </code></pre>
	  <p>
	  The scripts/submit_ftsm_pipeline.sh script will launch a sequence
	  of slurm scripts in a dependency chain in an analogous manner.
	  </p>
	</li>
	<li>
	  <p>
	  To make a movie of the 2D path optimization, you
	  can transfer the directory back to your local machine.
	  Make sure you have <a href="https://ffmpeg.org/">ffmpeg</a>
	  installed on your computer
	  (it&apos;s assumed to be the command called ffmpeg).
	  Modify the line in scripts/Make2dMovie.sh
	  reading "SCRIPT=PATH_TO_NDFES/examples/pmf2d/Example2d.py"
	  so that SCRIPT is the path to the Example2d.py script within
	  the ndfes distribution, and run 
	  <code class="cmd">bash scripts/Make2dMovie.sh</code>.
	  This will create a directory ./fullmovie that contains
	  a series of png images and a file named movie.sims.mp4.
	  When I ran the example, I obtained the following movie.
	  </p>
	  <video width="500" height="450" controls preload="none">
	    <source src="imgs/sasm.mp4" type="video/mp4" />
	  </video>
	  <p>
	    <strong>Download Video:</strong>
	     <a href="imgs/sasm.mp4">"MP4"</a>
	  </p>

	  <p>
	    In the movie, the first two frames are titled "init".
	    The first frame is the initial guess (the input path
	    before considering
	    the sampling), and the second frame is the output path
	    upon considering the sampling; that is, it is the
	    minimum free energy path from the current estimate
	    of the free energy surface.
	    If you look closely at the path, you can see 32 circles,
	    which are 32 points that uniformly discretize the path.
	    There are also 32 "x" marks that denote the next
	    set of proposed simulations (except for the first frame,
	    which mark the locations of the initial guess simulations).
	  </p>
	  <p>
	    Because the example uses the grid-based string method,
	    the simulations are not performed along the path.
	    Furthermore, you will see a pattern where the proposed
	    simulations are sometimes very close to the path,
	    but in the next iteration, they are further away,
	    and then even further away in the iteration after that,
	    and then the pattern repeats.
	  </p>
	  <p>
	    Also notice that the colors of the free energy surface
	    appear in the early iterations, but then some of them
	    disappear in later iterations.
	    The colored areas include those bins containing at
	    least 10 samples. This is controlled by setting
	    "--minsize=10" as an option to
	    <a href="../refdoc/ndfes_path.html">ndfes-path</a>
	    (it is also set in Make2dMovie.sh).
	    The uncolored areas of the free energy surface
	    formally have values of positive infinity; in practice,
	    the free energy in an unoccupied region is evaluated
	    by finding the center of the closest occupied bin
	    and adding a harmonic penalty:
	    F(<b>q</b>) = F(<b>q</b><sub>bin</sub>)
	    + 100*|<b>q</b>-<b>q</b><sub>bin</sub>|<sup>2</sup>.
	    In the early iterations, the size of the occupied region
	    increases because the free energy is evaluated from
	    the aggregate sampling; however, this example
	    sets the option "--neqit=4" when calling
	    <a href="../refdoc/ndfes_path.html">ndfes-path</a>
	    and
	    <a href="../refdoc/ndfes_path_analyzesims.html">ndfes-path-analyzesims.py</a>.
	    This option says to treat iterations 0, 1, 2, 3, and 4
	    as "equilibration" that will be phased-out and ultimately
	    excluded from later iterations.
	    The free energy surface of iteration 5 uses the sampling
	    from iterations 1,2,3,4,5. The free energy surface of iteration
	    6 uses the sampling from 2,3,4,5,6.
	    The free energy surface of iteration 9 uses the sampling from
	    5,6,7,8,9.
	    At that point and onwards, all sampling starting from iteration 5
	    are considered. For example, iteration 20 uses the sampling from
	    iterations 5,6,7,...,18,19,20.
	    The reasoning is: the initial guess is very poor (the string
	    evolves dramatically), so the short amount of sampling of
	    these early iterations likely do not well-represent an equilibrium
	    ensemble. If the early iterations are not well-equilibrated
	    then one likely will need to perform extra iterations to build
	    up enough equilibrated sampling to overwhelm the contributions
	    of the poor sampling.
	    The default behavior of
	    <a href="../refdoc/ndfes_path.html">ndfes-path</a>
	    and
	    <a href="../refdoc/ndfes_path_analyzesims.html">ndfes-path-analyzesims.py</a>
	    (i.e., if "--neqit" is not explicitly used) is to include all iterations.
	    This is equivalent to setting "--neqit=-1".
	  </p>
	  <p>
	    The options to the <a href="../refdoc/ndfes_path.html">ndfes-path</a>
	    program can be changed
	    to perform the
	    <a href="https://doi.org/10.1021/ja200173a">"modified string method"</a>
	    (Rosta et. al. J. Am Chem. Soc. 2011, 133, 8934).
	    In this case, the simulations are always performed
	    along the path, and the path is updated to
	    pass through the mean reaction coordinate positions
	    in the most recent set of simulations (that is,
	    it does not consider the aggregate sampling from
	    all previous iterations.)
	    To use the modified string method, replace the "--sasm"
	    option with "--msm --maxit=0".
	    You will likely need to use more images when performing
	    MSM, and you might prefer to represent the parametric
	    curve with piecewise-linear functions rather than
	    akima splines (use the "--linear" option).
	</p>
	</li>
	

      </ol>
    </p>

	
    <h3 id="InitialGuess">Make an initial guess</h3>
    <p>
      <b>You must have a coordinate file of the reactant (or product)
      state, and you must have decided which reaction coordinates are to
	be included in the free energy surface, and you must prepare
	a template disang file.</b>
      A disang file is considered a "template" if one-or-more of the
      harmonic centers are replaced with a placeholder.
      Normally, one would define the location of a harmonic potential
      with numerical r2 and r3 values.  These numerical values are
      replaced with a placeholder RCX, where X is an integer from 1 to 9.
      The placeholder RC3, for example, does not mean it is the
      third restraint in the disang file; it means it is the third
      template reaction coordinate in the disang file.
      The placeholders must appear in consecutive order
      (RC3 cannot appear before RC2, for example).
      The r2 and r3 values must both be replaced with a placeholder
      because only harmonic restraints are supported.
      In the <a href="#Dir">example</a>, I provide the coordinates
      of the reactant state (start/reactant.rst7) and a
      template disang file (<a href="#imgdisang">template/img.disang</a>).
    </p>
    <p>
      <b>One must prepare at least 2 disang files that define the
      ends of the initial path.</b>  These disang files should be
      the exact same as the template disang file, but with the
      RCX placeholders replaced by numerical values.
      In the <a href="#Dir">example</a>, I provide the disang
      files describing the reactant (start/reactant.disang)
      and product states (start/product.disang).
    </p>
    <p>
      The <a href="../refdoc/ndfes-path-prepguess.html">ndfes-path-prepguess.py</a> script is used to prepare mdin and disang files
      to generate an initial guess.
      Furthermore, ndfes-path-prepguess.py will produce a bash
      script that scans the path in sequence to avoid large
      perturbations encountered when trying to initiate each
      simulation from the same structure.
      The example uses two disang files to generate a linear
      path between the reactants and products; however, if
      we suspected that the mechanism was stepwise, we
      could generate a path from 3-or-more disang files.
      <a href="../refdoc/ndfes-path-prepguess.html#Examples">Click here for additional examples</a> using ndfes-ftsm-prepguess.py.
    </p>
    <p>
      You must run simulation to equilibrate each window.
      In the <a href="#Dir">example</a>,
      the simulations performed in the ./scan directory
      are run for only a brief amount of time to generate
      (unequilibrated) initial structures.
      For this reason, the example then reran the simulations
      in an equil/ directory to equilibrate each structure.
      The amount of equilibration that is necessary can vary
      depending on the nature of the reaction.
      If you fail try to skip the "scan"-phase (that is,
      you try to start each simulation from reactant.rst7),
      you will likely run in to problems because of the huge
      biasing potential felt by the initial structure.
    </p>

    

    <h3 id="Sample">Run the simulations</h3>
    <p>
      Each simulation can be run independently.
      
      The template/sim.slurm</a>
      script uses
      <a href="https://slurm.schedmd.com/job_array.html">slurm arrays</a>
      to launch each simulation from a single slurm file.
      The template script contains a placeholder variable
      called "CIT" (current iteration).
      If CIT=0, then it launches jobs in the ./init directory,
      otherwise it launches jobs in a directory called ./img%02i
      (where %02i is a zero-padded two-digit integer, e.g., 01).
      To create a slurm script for a specific directory, use
      sed; e.g., <code class="cmd">sed -e "s/CIT/0/" template/sim.slurm > sim.0.slurm</code>.
      To launch the simulations of path consisting
	    of 32 images, one would run:
	    <code class="cmd">sbatch --array=1-32 sim.X.slurm</code>.
	    If you have the resources, the <a href="#simslurm">template/simandana.slurm</a>
	    is preferred due to its simplicity; it does not require setting up
	    slurm dependency chains.
    </p>


    
    <h3 id="Analysis">Analyze the results and make a new guess</h3>
    <p>
      <p>The simulation results are analyzed to update the proposed
      pathway.
      The template/ana.slurm (or template/simandana.slurm)
      script does this in the following manner.</p>
      <ul>
	
	<li>
	  It uses <a href="../refdoc/ndfes-path-analyzesims.html">ndfes-path-analyzesims.py</a> to extract samples from the simulation dumpave files and writes a file ${curdir}/analysis/metafile.current, which lists the biasing potentials and samples extracted from the current path. It will also write ${curdir}/analysis/metafile.all, which includes all umbrella windows and sampling extracted from every iteration.
	</li>
	
	<li>
	  It uses the metafile.all as the input to <a href="../refdoc/ndfes.html">ndfes</a>, which generates a free energy surface calculated from the aggregate sampling from every iteration. The output is a file named ${curdir}/analysis/metafile.all.chk.
	</li>
	
	<li>
	  It uses metafile.all.chk as the input to <a href="../refdoc/ndfes-path.html">ndfes-path</a> to create a new directory of input, disang, and restart files for the next iteration.
	</li>
	
      </ul>
      
    </p>
    <p>
      The output of the analysis is stored in a file called ${curit}/analysis/analysis.slurmout, written to the current iteration directory. The output is the collection
      of outputs produced by ndfes-path-analyzesims.py, ndfes, and ndfes-path.
    </p>

  </body>
</html>
