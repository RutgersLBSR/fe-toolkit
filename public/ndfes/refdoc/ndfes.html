<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
  </head>
  <body>

    <h1>
      ndfes
    </h1>

    <p>
      <ul>
	<li>
	  NOTE: When installing ndfes from the gitlab distribution, it
	  should have compiled serial (ndfes) and OpenMP parallel
	  (ndfes_omp) versions of this code.
	  If you install the ndfes software included within AmberTools,
	  then it will only compile the serial version (ndfes)
	  unless you compiled Amber with the "-DOPENMP=TRUE" CMake
	  configure option. When you reconfigure Amber with this option,
	  it should compile ndfes.OMP (this is the Amber naming
	  convention). You could then create a symbolic link of ndfes.OMP
	  to ndfes_omp, or modify your scripts to use the appropriate
	  executable name.
	  The OpenMP version is highly recommended. You can set the
	  number of cores by typing "export OMP_NUM_THREADS=16", for
	  example.
	</li>
	
	<li>
	  <a href="#Overview">Overview</a>
	</li>
	<li>
	  <a href="#Usage">Usage</a>
	</li>
	<li>
	  <a href="#Options">Options</a>
	</li>
	<li>
	  <a href="#Metafile">Metafile format</a>
	</li>
	<li>
	  <a href="#Dumpave">Dumpave format</a>
	</li>
      </ul>
    </p>


    <h3 id="Overview">Overview</h3>
    <p>
      The ndfes program reads an input file that describes
      the biasing potential of each simulation and provides
      the filename of the raw samples.
      It then uses the vFEP or MBAR method to analyze the
      free energy surface and outputs the results to a checkpoint
      file that can be read and manipulated using the
      companion ndfes python library.
      The python scripts included with ndfes use the
      companion library to perform common tasks, like
      printing the free energy values to a text file.
    </p>

    <h3 id="Usage">Usage</h3>
    <p>
      To perform MBAR:
      <pre><code>ndfes --mbar -c CHK -w DX [-w DY [...]] [-p IDIM [-p IDIM [...]]] \
      [--nham NHAM] [--sdos DELTAU [--sepdos] [--hist HIST]] \
      [--temp TEMP] [--tol TOL] [--maxit MAXIT] \
      [--nboot NBOOT] [--btol BTOL] \
      [--start START] [--stop STOP] [--uniq] [--maxuniq MAXUNIQ] \
      metafile</code></pre>
      To perform vFEP:
      <pre><code>ndfes --vfep -c CHK -w DX [-w DY [...]] [-p IDIM [-p IDIM [...]]] \
      [--nquad NQUAD] [--order ORDER] \
      [--temp TEMP] [--tol TOL] [--maxit MAXIT] \
      [--nboot NBOOT] [--btol BTOL] \
      [--start START] [--stop STOP] [--uniq] [--maxuniq MAXUNIQ] \
      metafile</code></pre>
    </p>

    

    <h3 id="Options">Options</h3>
    <p>
      <ul>
	<li><code>--mbar</code>
	  <br>
	  Analyzes the sampling with the 
	  Multistate Bennett Acceptance Ratio method (MBAR)
	  <sup>[<a href="https://aip.scitation.org/doi/10.1063/1.2978177">Shirts (2008)</a>]</sup>
	  (or related methods, such as the weighted thermodynamic
	  perturbation (wTP)
	  <sup>[<a href="https://pubs.acs.org/doi/10.1021/acs.jctc.8b00571">Li (2018)</a>,
	    <a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c00794">Hu (2020)</a>]</sup>
	  and generalized weighted thermodynamic
	  perturbation (gwTP)
	  <sup>[<a href="https://pubs.acs.org/doi/10.1021/acs.jpca.2c06201">Giese (2022)</a>]</sup>).
	  This option is mutually exclusive with <code>--vfep</code>.
	  The free energy surface is the minus log of the unbiased
	  probability distribution approximated by a histogram
	  consisting of bins, whose widths are chosen as input.
	</li>
	<li><code>--vfep</code>
	  <br>
	  Analyzes the sampling with the variational free energy profile
	  (vFEP) method.
	  <sup>[<a href="https://pubs.acs.org/doi/10.1021/ct300703z">Lee (2013)</a>,
	    <a href="https://pubs.acs.org/doi/10.1021/ct400691f">Lee (2014)</a>,
	    <a href="https://pubs.acs.org/doi/10.1021/acs.jpca.1c00736">Giese (2021)</a>]</sup>
	  For low-dimensional free energy surfaces (N<sub>dim</sub> &le; 3),
	  the vFEP approach is faster or competitive with the MBAR method;
	  however, it cannot be used to estimate a target free energy surface
	  from sampling performed with a reference potential.
	  The free energy surface is parametrized by a multidimensional
	  Cardinal B-spline. The intersections of bins
	  are the locations of B-spline control parameters, rather than
	  a histogram approximation of the unbiased probability distribution.
	  This option is mutually exclusive with <code>--mbar</code>.
	</li>
	<li><code>--chkpt CHK</code> or <code>-c CHK</code>
	  <br>
	  Write the results of the analysis to a checkpoint file called CHK.
	  The checkpoint file is in XML format used to describe one-or-more
	  free energy surfaces, referred to as "models".
	  When running vFEP, the checkpoint file will consist of a single model:
	  the unbiased free energy surface of the potential which produced the sampling.
	  When running MBAR, the checkpoint file will contain multiple free
	  energy surfaces if the <code>--nham</code> option is used to perform
	  wTP or gwTP analysis.
	  The results can be imported into a python script as follows:
	  <pre><code>#!/usr/bin/env python3
import ndfes
all_models = ndfes.GetModelsFromFile(filename)
first_model = all_models[0]
help(first_model)</code></pre>
	</li>
	<li><code>--width DX</code> or <code>-w DX</code>
	  <br>
	  The width of the bins (DX is a floating point number).
	  For MBAR, the bins are used to create
	  a histogram of the unbiased probability distribution(s).
	  For vFEP, the bin "corners" are the locations of the Cardinal B-spline
	  centers.
	  The units are same as those used in the dumpave files;
	  if the reaction coordinates are listed in Angstroms, then
	  the width should also be supplied in Angstroms.
	  If only one width is supplied, then the width is applied to
	  all dimensions.
	  If one is analyzing a multidimensional free energy surface,
	  one can use -w multiple times, once for each dimension.
	  If -w is not used, then the bin width in all dimensions is 0.15.
	</li>
	<li><code>--periodic IDIM</code> or <code>-p IDIM</code>
	  <br>
	  Indicates that the specified dimension is periodic.
	  The index is a 1-based integer (a value of "2" denotes the
	  second reaction coordinate, for example).
	  If a dimension has been marked as periodic, then the
	  range of periodicity is assumed to be [0,360).
	  One can later use the checkpoint files to print the free
	  energy surface in other ranges; e.g., [-180,180).
	  This option can be used multiple times if more than one
	  dimension is periodic.
	</li>
	<li><code>--temp TEMP</code>
	  <br>
	  The temperature at which to analyze the free energy surface, in Kelvin.
	  In virtually all cases, this should be the same temperature listed
	  in the metafile. The analysis of free energy surfaces at a temperature
	  that wasn't simulated is an experimental feature that hasn't been
	  well-tested as of the time of this writing.
	  If the temperature is not specified, it defaults to 298.0.
	</li>
	<li><code>--tol TOL</code>
	  <br>
	  The convergence criteria of the nonlinear optimization algorithm, a float.
	  The default value of 1.e-9 should be fine in most circumstances.
	  In principle, smaller values should produce a more accurate answer,
	  but due to round-off errors, the optimization may fail to converge.
	</li>
	<li><code>--maxit MAXIT</code>
	  <br>
	  The maximum number of nonlinear optimizations to take before stopping,
	  an integer.
	  The default value is 99999. Most optimizations take between 50-200
	  steps to reach convergence.
	</li>
	<li><code>--nboot NBOOT</code>
	  <br>
	  The number of bootstrap optimizations used to estimate the
	  uncertainties in the calculated free energy values.
	  The default is 0 (does not estimate errors); however, one
	  would normally set this to 50.
	  Using larger values would technically produce a more accurate
	  estimate of the uncertainty, but the extra digits of accuracy
	  are rarely worth the added cost.
	  The total number of nonlinear optimizations scheduled to be
	  performed in is 1 + nboot.
	  The first optimization is to calculate the free energies from
	  the provided samples, and the remaining nboot optimizations
	  are calculated upon resampling the input data.
	  In brief, suppose a simulation produced N samples and the
	  autocorrelation in the biasing potential time series
	  produces a statistical inefficiency of g.
	  The cyclic block bootstrap resampling would generate
	  N/g integers in the range [0,N-1], denoted I<sub>k</sub>.
	  From these, a new set of N samples, &xi;(i), is constructed:
	  <p>
	    &xi;(I<sub>1</sub>), &xi;(I<sub>1</sub>+1), &middot;&middot;&middot; &xi;(I<sub>1</sub>+g-1),
	    &xi;(I<sub>2</sub>), &xi;(I<sub>2</sub>+1), &middot;&middot;&middot; &xi;(I<sub>2</sub>+g-1),
	    &middot;&middot;&middot;,
	    &xi;(I<sub>N/g</sub>), &xi;(I<sub>N/g</sub>+1), &middot;&middot;&middot; &xi;(I<sub>N/g</sub>+g-1)
	  </p>
	  where the index is wrapped to the range [0,N-1] if falls outside the range.
	  In other wordsd, the I<sub>k</sub> values are the first index of
	  a block of samples, where a block consists of g samples.
	  The new free energy surface is obtained with the new samples and stored.
	  The process is repeated to yield N<sub>boot</sub> free energy surfaces,
	  and the standard error of the free energy surfaces is the standard
	  deviation of the bootstrap distribution.
	  The bootstrap error estimates are a measure of uncertainty
	  due to the fluctuations in the observed sampling.
	  The boostrap procedure is a stochastic process based on random
	  numbers. Multiple runs of the same input will produce the
	  same free energy values, but slightly different error estimates.
	  The errors tend to be underestimates of the true uncertainty
	  because they do not consider the unobserved samples
	  one may encounter from longer simulations.
	  A better error estimate can be made by performing multiple
	  simulations with different thermostat random number seeds.
	  This is the so-called "ensemble average approach".
	  The bootstrap uncertainty of each trial can be evaluated,
	  and the ndfes-AvgFESs.py script can be used to make an
	  overall error estimate.
	</li>
	<li><code>--btol BTOL</code>
	  <br>
	  The nonlinear optimization convergence criteria for the bootstrap
	  optimizations.  The default value of 1.e-5 is a much looser
	  criteria than the initial optimization (--tol), because the
	  bootstrap optimizations are merely used to make an estimate
	  of the uncertainty.
	</li>
	<li><code>--start START</code>
	  <br>
	  START is a float [0.0,1.0) representing the
	  fraction of a simulation, before which the samples
	  are ignored.
	  When the dumpave files are read, only the samples from
	  --start to --stop are used to perform the analysis.
	  The default value of --start is 0.0.
	</li>
	<li><code>--stop STOP</code>
	  <br>
	  STOP is a float [0.0,1.0) representing a fraction
	  of a simulation, after which the samples are ignored.
	  When the dumpave files are read, only the samples from
	  --start to --stop are used to perform the analysis.
	  The default value of --stop is 1.0.
	</li>
	<li><code>--uniq</code>
	  <br>
	  If present, then the statistical inefficiency of
	  the input samples is calculated, and the petite
	  set of samples are analyzed by striding through
	  the data.  The stride is the statistical inefficiency.
	</li>
	<li><code>--maxuniq MAXUNIQ</code>
	  <br>
	  If --uniq has been used to select the statistically
	  independent samples for analysis, then one
	  can set the maximum number of samples to analyze
	  with --maxuniq (an integer).
	  For example --uniq --maxuniq=100 would analyze
	  only the first 100 statistically independent samples
	  from each simulation.
	  This can be useful for deciding how many samples
	  should be used to perform target potential evaluations
	  with the wTP or gwTP methods.
	  That is, one would need to use at least as many
	  samples as it would be required to estimate the
	  reference free energy surface from the reference
	  potential sampling.
	  The --maxuniq option allows you to quickly verify
	  that a limited selection of samples can at least
	  predict the reference free energy surface.
	</li>
	<li><code>--nham NHAM</code>
	  <br>
	  This option can only be used in conjunction with
	  --mbar. The --nham option specifies the number
	  of extra columns to read from the dumpave files.
	  The extra columns correspond to the unbiased
	  potential energies of target and reference
	  Hamiltonians.
	</li>
	<li><code>--sdos DELTAU</code>
	  <br>
	  This option can only be used in conjunction with
	  --mbar.  The --sdos option specifies that
	  density-of-states smoothing should be applied.
	  <sup>[<a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c00794">Hu (2020)</a>]</sup>
	  The DELTAU argument is a float in reduced energy units
	  (DELTAU * &beta;)
	  The default value is 0.0.
	  One typically only uses this option if the
	  predicted free energy surface contains
	  numerical noise that one wishes to suppress.
	  Given a limited amount of finite sampling, it
	  is possible that there are a few unlikely samples
	  that are over-represented in the distribution,
	  and the smoothing procedure reduces the impact
	  of those samples on the contribution to the
	  free energy calculation.
	  The value of DELTAU represents the bin width
	  of an energy histogram (within each spatial
	  histogram) used to model the observed density
	  of states.  This histogram is compared with
	  a normal distribution to scale the contribtions
	  of the samples.
	  A typical value to try is 0.2.
	</li>
	<li><code>--sepdos</code>
	  <br>
	  This is a developer option that would not
	  normally be used by a user.
	  If --sdos is used and one is performing gwTP
	  analysis, then this option controls whether the
	  smoothing is applied to the full set of
	  samples from all Hamiltonians or
	  the samples from the individual
	  Hamilonians.
	  It is arguably more correct to not use this option, but
          one may find it to yield more stable results in some
          circumstances.
	  The method described in the supporting information
	  of <a href="https://pubs.acs.org/doi/10.1021/acs.jpca.2c06201">Giese (2022)</a>
	  corresponds to <i>not</i> using this option.
	    
	</li>
	<li><code>--hist HIST</code>
	  <br>
	  This is a developer option that prints out
	  the energy histogram of each bin.
	  The output files will be named: HISThist_bin_*_target_*_ref_*.dat.
	  One would include a trailing slash on the prefix
	  to denote a directory; e.g., --hist=tmp/.
	</li>
	<li><code>--nquad NQUAD</code>
	  <br>
	  This option is only applicable in conjunction with --vfep.
	  The value of the option is an integer controlling the number
	  Gauss-Legendre quadrature points used to approximate
	  the configuration integral within the area of a bin.
	  The default value of 5 should be sufficient in most
	  circumstances.
	  The computational cost of performing vFEP highly
	  depends on this value, because it scaling is
	  proportional to N<sub>quad</sub><sup>N<sub>dim</sub></sup>.
	</li>
	<li><code>--order ORDER</code>
	  <br>
	  This is the order (an integer) of Cardinal B-splines.
	  The default value is 5.
	  It must be at least 3 to obtain a smooth free energy surface.
	  Larger orders require optimization of more parameters,
	  which can make the nonlinear optimization algorithm
	  require more steps to reach convergence.
	</li>
	
	
      </ul>
    </p>


    
    <h3 id="Metafile">Metafile file format</h3>
    <p>
      The metafile is the input read by ndfes.
      It consists for N<sub>sim</sub> lines, where
      N<sub>sim</sub> is the number of biased simulations.
      The biasing potential is assumed to be uncoupled harmonics:
      U<sub>bias</sub>(t) = &sum;<sub>i=1</sub><sup>N<sub>dim</sub></sup>
      k<sub>i</sub> (&xi;<sub>i</sub>(t) - &xi;<sub>0,i</sub>)<sup>2</sup>,
      where k is twice the force constant of the harmonic potential,
      &xi;(t) is a reaction coordinate value at time step t,
      and &xi;<sub>0</sub> is the umbrella potential center.
      The units of k should be listed such that U<sub>bias</sub>(t)
      has units of kcal/mol.
      If a dimension is periodic, it must be in degrees.
    </p>
    <p>
      The metafile must contain 3 + 2N<sub>dim</sub> columns and N<sub>sim</sub> rows.
      The columns are:
      <ul>
	<li>Column 1: 0-based integer
	  <br>
	  The "Hamiltonian index" of the potential which produced the samples.
	  The index is 0 unless wTP or gwTP analysis is performed.
	  If the dumpave contains extra columns of unbiased potential energies,
	  the Hamiltonian index listed here is the "index of the extra
	  column" that the sampled potential corresponds to; that is,
	  an index of 0 means "the first extra column of potential energies".
	</li>
	<li>
	  Column 2: float
	  <br>
	  The simulation temperature, in Kelvin.
	  You should run all simulations at the same temperature,
	  and the values listed in the second column should match
	  the --temp option on the ndfes command line.
	  The functionality of the program for analyzing multiple
	  temperatures has not been well-tested.
	</li>
	<li>
	  Column 3: string
	  <br>
	  The name of the disang file containing the samples from this simulation.
	  The filename can be an absolute or relative path.
	  When it is a relative path, it is relative to the current working
	  directory in which the ndfes program was launched.
	</li>
	<li>
	  Column 4: float
	  <br>
	  The umbrella potential center of reaction coordinate 1.
	  The units of the reaction coordinate must be the same as those listed in the dumpave file.
	</li>
	<li>
	  Column 5: float
	  <br>
	  Twice the umbrella potential force constant of reaction coordinate 1.
	  The units of the value must be listed such that the contribution to
	  the biasing potential has units of kcal/mol.
	</li>
	<li>
	  Columns 6, 8, 10, etc
	  <br>
	  These are the umbrella potential centers of the other reaction coordinates
	  for the calculation of a multidimensional free energy surface.
	</li>
	<li>
	  Columns 8, 9, 11, etc
	  <br>
	  These are twice the umbrella potential force constants of the other reaction coordinates
	  for the calculation of a multidimensional free energy surface.
	</li>
      </ul>
    </p>


    <h3 id="Dumpave">Dumpave format</h3>
    <p>
      The dumpave file consists of 1 + N<sub>dim</sub> or
      1 + N<sub>dim</sub> + N<sub>ham</sub> columns.
      Each row corresponds to a sample.
      The first column is a simulation time or simulation step.
      The next N<sub>dim</sub> columns are the values of
      the reaction coordinates at the current step.
      If wTP or gwTP analysis is desired, the dumpave file
      can contain N<sub>ham</sub> extra columns of
      unbiased potential energies.
      Each column corresponds to a potential energy function,
      and they must be consistently ordered across all
      dumpave files.
      When the extra columns are included, one must use
      the --nham=N<sub>ham</sub> option when running
      the ndfes program to tell it to read the extra
      columns.
      This will cause ndfes to store multiple free energy
      surfaces in the output checkpoint file (one for
      each unbiased potential).
    </p>

    
  </body>
</html>
