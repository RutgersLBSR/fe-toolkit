<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
    
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

  </head>
  <body>

    <h1>
      ndfes-AvgFESs.py
    </h1>

    <p>
      <ul>
	<li>
	  <a href="#Overview">Overview</a>
	</li>
	<li>
	  <a href="#Usage">Usage</a>
	</li>
	<li>
	  <a href="#Options">Options</a>
	</li>
	<li>
	  <a href="#Examples">Examples</a>
	</li>
      </ul>
    </p>


    <h3 id="Overview">Overview</h3>
    <p>
      This script reads models from ndfes checkpoint files
      and either averages or aligns the free energy surfaces
      to output a new checkpoint file with improved error
      estimates.
    </p>
    <p>
      Suppose you performed 
      N<sub>trial</sub> independent trials of the umbrella sampling.
      Analysis of each trial produces a free energy surface,
      whose values and bootstrap errors are
      \(F_i(\boldsymbol{\xi}_b)\) and \(\delta F_i(\boldsymbol{\xi}_b)\),
      respectively, where <i>i</i> denotes the trial and <i>b</i> is
      the spatial bin.
      The free energy values are defined only to within an arbitrary
      additive constant \(C_i\).
      We choose these constants to minimize the
      weighted squared error relative to a reference surface,
      \(F_{\text{ref}}(\boldsymbol{\xi}_b)\) with bootstrap uncertainties
      \(\delta F_{\text{ref}}(\boldsymbol{\xi}_b)\).
      The set of constants which minimize the squared errors
      shall be denoted \(C_i^{*}\).
      \begin{equation}
C_i^{*} = \underset{C_i}{\text{arg min}}\;\chi^2_i(C_i)
      \end{equation}
      \begin{equation}
\chi^2_i(C_i) = \sum_{b} \frac{w_{ib}}{\sum_{c} w_{ic}} \left[ F_i(\boldsymbol{\xi}_b) + C_i - F_{\text{ref}}(\boldsymbol{\xi}_b) \right]^2
      \end{equation}
      The (unnormalized) weights are chosen to be inversely proportional to
      the propagated uncertainty in the difference.
      \begin{equation}
w_{ib} = \frac{1}{ \left[\delta F_i(\boldsymbol{\xi}_b)\right]^2 +\left[\delta F_{\text{ref}}(\boldsymbol{\xi}_b)\right]^2 }
      \end{equation}
      After solving for the constants, the average free energy surface
      and combined error estimates are given below.
      \begin{equation}
\bar{F}(\boldsymbol{\xi}_{b}) = \frac{1}{N_{\text{trial}}} \sum_{i=1}^{N_{\text{trial}}} \left[ F_i(\boldsymbol{\xi}_b) + C_i^{*} \right]
      \end{equation}
      \begin{equation}
\delta\bar{F}(\boldsymbol{\xi}_{b})
      =
\sqrt{ \frac{1}{N_{\text{trial}}} \left[ \frac{\sum_{i=1}^{N_{\text{trial}}} \left\{F_i(\boldsymbol{\xi}_b) + C_i^{*} - F_{\text{ref}}(\boldsymbol{\xi}_b)\right\}^2}{N_{\text{trial}-1}} + \frac{\sum_{i=1}^{N_{\text{trial}}} \left\{\delta F_i(\boldsymbol{\xi}_b)\right\}^2}{N_{\text{trial}}} \right] }
      \end{equation}
      The first term in square brackets is the sample variance between trials,
      and the second term is the trial-averaged bootstrap variance.
      One can show that the above expression
      is the standard error of an average computed from samples which
      have their own error estimates.
      Instead of computing a bootstrap average of <i>points</i> (normal distributions with zero standard deviation), one computes the
      bootstrap average from normal distributions. Each bootstrap
      iteration extracts a random sample drawn from each normal distribution,
      and an average is computed. This resampling is repeated, and the standard
      deviation of the bootstrap averages from many resamplings is the standard error shown
      above.
    </p>


    <h3 id="Usage">Usage</h3>
    <p>
      To align several models to the first model (\(F_{\text{ref}}(\boldsymbol{\xi})\equiv F_1(\boldsymbol{\xi})\)), average the free energy surfaces, and write a new checkpoint file of the average (\(\bar{F}(\boldsymbol{\xi})\)) with a combined error estimate  (\(\delta\bar{F}(\boldsymbol{\xi})\)):
      <pre><code>ndfes-AvgFESs.py -o CHKPT \
    --fes CHKPT[=MIDX] --fes CHKPT[=MIDX] [--fes CHKPT[=MIDX] [...]]</code></pre>
      To align several models to a reference model (the --ref option manually specifies a \(F_{\text{ref}}(\boldsymbol{\xi})\) without including it in the average), average the free energy surfaces, and write a new checkpoint file of the average (\(\bar{F}(\boldsymbol{\xi})\)) with a combined error estimate (\(\delta\bar{F}(\boldsymbol{\xi})\)):
      <pre><code>ndfes-AvgFESs.py -o CHKPT --ref CHKPT[=MIDX] \
    --fes CHKPT[=MIDX] --fes CHKPT[=MIDX] [--fes CHKPT[=MIDX] [...]] </code></pre>
      <b>Recommended Usage</b>: To align several models to a reference model (the --ene option manually specifies a \(F_{\text{ref}}(\boldsymbol{\xi})\) without including it in the average) and write a new checkpoint file with a combined error estimate  (\(\delta\bar{F}(\boldsymbol{\xi})\)) of the reference model (\(F_{\text{ref}}(\boldsymbol{\xi})\)):
      <pre><code>ndfes-AvgFESs.py -o CHKPT --ene CHKPT[=MIDX] \
    --fes CHKPT[=MIDX] --fes CHKPT[=MIDX] [--fes CHKPT[=MIDX] [...]]</code></pre>
      In this way, one can calculate the free energy of the indendent trials
      and make a "best estimate" of the free energy from the aggregate sampling. One then uses ndfes-AvgFESs.py merely to update the uncertainty of the best estimate.
      
      
    </p>


    <h3 id="Options">Options</h3>
    <p>
      <ul>
	<li><code>--out CHKPT</code> or <code>-o CHKPT</code>
	  <br>
	  The output checkpoint file.
	  If --fes is used N times, the output will contain N+1 models.
	  The first model will either be the average free energy surface
	  or (if --ene is used) the reference free energy surface.
	  The remaining N models are the exact same as the input model,
	  except the free energy values are shifted by a constant to
	  reduce the weighted least squares difference with respect
	  to the reference. If no reference is supplied (meaning: neither
	  --ene nor --ref is used), then the first instance of --fes
	  is treated as the reference for the purpose of alignment.
	</li>
	<li><code>--fes CHKPT</code> or <code>--fes CHKPT=MIDX</code>
	  <br>
	  Extract the first model within the specified checkpoint file.
	  A checkpoint file may contain several models, and
	  a specific model can be extracted with the =MIDX suffix,
	  which MIDX is the (0-based) "model index".
	  If the suffix notation is not used, it is equivalent to
	  using a =0 suffix.
	</li>
	<li><code>--ref CHKPT</code> or <code>--ref CHKPT=MIDX</code>
	  <br>
	  The reference surface used to align the free energy surfaces.
	  The reference surface is not included in the average.
	  When this option is used, the first model in the
	  output will be the trial-averaged free energy surface.
	  This option is mutually exclusive with the --ene option.
	</li>
	<li><code>--ene CHKPT</code> or <code>--ene CHKPT=MIDX</code>
	  <br>
	  The reference surface used to align the free energy surfaces.
	  This is the exact same as the --ref option, except
	  the first model in the output checkpoint file will be
	  the exact same as the reference surface.
	  This option allows one to make an improved error estimate.
	  One would often compute free energy surfaces of individual
	  trials <i>and</i> the aggregate sampling from all trials.
	  The aggregate sampling is used as the reference with the
	  --ene option to make a better estimate of the error.
	</li>

      </ul>
    </p>


    
    <h3 id="Examples">Examples</h3>
    <p>
      <ul>
	<li>
	  Read the first model from 3 trials, align them to the first model in agg.chk, and write the result to out.chk to update the error estimate of agg.chk.
	  <br>
	  <code>ndfes-AvgFESs.py -o out.chk --ene agg.chk --fes t01.chk --fes t02.chk --fes t03.chk</code>
	</li>
	<li>
	  Read the second model from 3 trials, align them to the second model in agg.chk, and write the result to out.chk to update the error estimate of agg.chk.
	  <br>
	  <code>ndfes-AvgFESs.py -o out.chk --ene agg.chk=1 --fes t01.chk=1 --fes t02.chk=1 --fes t03.chk=1</code>
	</li>
	
      </ul>
    </p>


    
  </body>
</html>
