<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
  </head>
  <body>

    <h1>
      ndfes-PrepareAmberData.py
    </h1>

    <p>
      <ul>
	<li>
	  <a href="#Overview">Overview</a>
	</li>
	<li>
	  <a href="#Usage">Usage</a>
	</li>
	<li>
	  <a href="#Options">Options</a>
	</li>
	<li>
	  <a href="#Examples">Examples</a>
	</li>
      </ul>
    </p>


    <h3 id="Overview">Overview</h3>
    <p>
      This reads a disang and dumpave file,
      writes a new dumpave file containing only the
      desired reaction coordinates, and
      prints one line of the ndfes metafile to standard output.
      It handles the conversion of units based on the Amber
      restraint file conventions (the disang file lists
      angle restraint force constants in
      kcal mol<sup>-1</sup> radian<sup>2</sup>,
      but the reaction coordinate values are in degrees).
      Furthermore, this script can also read multiple
      mdout files containing the potential energies
      of several Hamiltonians to construct dumpave files
      suitable for wTP or gwTP analysis.
    </p>

    <h3 id="Usage">Usage</h3>
    <p>
      <pre><code>
ndfes-PrepareAmberData.py -d DISANG -o ODUMP \
      -i INP1 [INP2 [INP3 [...]]] \
      -r RC1 [-r RC2 [-r RC3 [...]]] \
      [-b RC1 [-b RC2 [-b RC3 [...]]]] \
      [--temp TEMP] [--sym SYM] [-w DX [-w DY [...]]]
      [--hamidx HAMIDX --ene HAM0MDOUT1 HAM0MDOUT2 [...] \
                            [--ene HAM1MDOUT1 [HAM1MDOUT1 [...]] \
                            [--ene HAM2MDOUT1 [HAM2MDOUT1 [...]]]]]
      [--prefix PREFIX]
      </code></pre>
    </p>


    <h3 id="Options">Options</h3>
    <p>
      <ul>
	<li><code>--disang DISANG</code> or <code>-d DISANG</code>
	  <br>
	  The Amber restraint file defining the umbrella potentials.
	</li>
	<li><code>--out ODUMP</code> or <code>-o ODUMP</code>
	  <br>
	  The output dumpave file suitable for processing with ndfes.
	  It will contain 1+N<sub>dim</sub>+N<sub>ham</sub> columns.
	  The first column is a sample index.
	  The next N<sub>dim</sub> columns are the reaction coordinates.
	  If --ene is used N<sub>ham</sub> times to read the potential energies from N<sub>ham</sub> Hamiltonians, then N<sub>ham</sub> additional columns of potential energies (kcal/mol) will also be written.
	  Alternatively, the -b option effectively treats N<sub>ham</sub>=2
	  to include 2 extra columns of potential energies.
	  
	</li>
	<li><code>--inp INP</code> or <code>--i INP</code>
	  <br>
	  This is either an input dumpave file or a NetCDF trajectory
	  file. Multiple files can be listed, in which case they are
	  treated as a series of restarts (the output dumpave is
	  a concatenation of sampling from each restart).
	</li>
	<li><code>--rxn RC</code> <code>-r RC</code>
	  <br>
	  A list of (1-based) restraint indexes defining the reaction
	  coordinates used to construct the free energy surface.
	  A value of 1 corresponds to the first reaction coordinate
	  defined in the restraint file.
	  This option can be used more than once to define a multidimensional
	  free energy surface.
	  The values listed here cannot also appear in the -b option.
	  Any restraint that does not appear in -r or -b is ignored;
	  the output dumpave will only contain the reaction coordinate
	  columns listed in the -r option.
	</li>
	<li><code>--bias RC</code> or <code>-b RC</code>
	  <br>
	  A list of (1-based) restraint indexes that are not included
	  as a part of the reduced dimensional space of reaction
	  coordinates. Instead, they are additional biases that one
	  wishes to remove. In other words, use of the -b option
	  means that you performed sampling with an additional bias,
	  corresponding to Hamiltonian index 0.
	  The free energy of a second Hamiltonian (index 1) can
	  be computed from wTP analysis which removes the effect
	  of the additional biasing potentials specified by -b.
	  The result is a dumpave file containing 2 extra columns
	  of potential energies. The first extra column is 0.0,
	  and the second extra column is the value of the extra
	  biasing potentials.
	  This option can be used more than once.
	  The restraint indexes listed here cannot appear in --rxn.
	</li>
	<li><code>--temp TEMP</code>
	  <br>
	  The temperature (float) of the simulation (Kelvin).
	  This should be the same for each simulation, and the
	  same temperature should be used when performing the
	  analysis with ndfes. Default: 298.0.
	</li>
	<li><code>--sym SYM</code> or <code>-s SYM</code>
	  <br>
	  The (1-based) restraint index of the reaction coordinate
	  to be symmetrized. By using this option, the values of the
	  specified reaction coordinate are negated when writing
	  the output dumpave file. Similarly, the location of the
	  umbrella window center along the specified axis is negated.
	  This is used to create "fake" samples to impose symmetry
	  on the free energy surface about the origin for simple
	  reactions. This option can be used only once (complicated
	  symmetries and the symmetrization of multiple coordinates
	  is not supported).
	  This option is rarely used, but when it is, one would
	  typically run ndfes-PrepareAmberData.py twice: once
	  with --sym and once without --sym to create "fake" and
	  "real" sampling, respectively.
	</li>
	<li><code>--width DX</code> or <code>-w DX</code> 
	  <br>
	  The width (float) of the bins one intends to use when
	  performing MBAR or vFEP analysis.
	  If used once, then the specified width is applied to
	  all coordinates listed in --rxn.
	  If used more than once, it must be used the same number
	  of times as --rxn, and each instance specifies the
	  bin width of corresponding reaction coordinate.
	  If this option is used, it will perturb the reaction
	  coordinate by +1.e-6 if it happens to lie directly
	  on the boundary of two bins.
	  This option is not normally used; it exists only in
	  certain cases where you wish to symmetrize the sampling
	  with the --sym option, in which case the "rounding
	  behavior" which assigns samples to bins must be
	  modified to "round away from zero" rather than "round
	  down". By shifting the samples that lie directly on
	  the boundary of two bins, one avoids the differences
	  in rounding behaviors between the "real" and "fake"
	  samples.
	</li>
	<li><code>--hamidx HAMIDX</code>
	  <br>
	  The (0-based) index of the Hamiltonian which produced the sampling.
	  If one is performing wTP or gwTP analysis, the output dumpave
	  will contain extra columns of potential energies corresponding
	  to each Hamiltonian. A value of --hamidx=0 means that the
	  sampling was performed with the Hamiltonian whose potential
	  energies are listed in the first extra column of potential energies.
	</li>
	<li><code>--ene MDOUT</code>
	  <br>
	  A sander mdout file produced by reanalyzing a trajectory file
	  using the imin=6, nstlim=0, ntpr=1 options.
	  The potential energies (EPtot) are
	  extracted from the file, and it is expected that the number
	  of potential energies listed in the file matches the number
	  of frames in the trajectory file.
	  A single instance of --ene can list multiple mdout files
	  that correspond to the analysis of multiple trajectory
	  files (assumed to be a series of restarts).
	  Each instance of the --ene option corresponds to a Hamiltonian.
	  The first instance of --ene corresponds to Hamiltonian index 0,
	  the second is index 1, etc.
	</li>
	<li><code>--prefix PREFIX</code>
	  <br>
	  The standard output of ndfes-PrepareAmberData.py will be
	  one line of the ndfes metafile input, which lists the
	  location of the dumpave file to read.
	  If the --prefix option is not used, the listed dumpave file
	  will be the same as what is provided in the --out option.
	  If --prefix is specified, the listed filename is adjusted
	  by removing the --prefix string from the beginning of the
	  filename. This is often used to prepare a separate
	  analysis subdirectory.
	</li>
	
      </ul>
    </p>


    
    <h3 id="Examples">Examples</h3>
    <p>
      <ul>
	<li>
	  A 1D FES umbrella window for vFEP or MBAR analysis
              There is only 1 collective variable (the first variable in the
              disang file).  All other restraints are ignored.
	  <br>
	  <code>ndfes-PrepareAmberData.py -d rc_0.00.disang -i rc_0.00.dumpave -o dumpaves/rc_0.00.dumpave -r 1</code>
	  <br>
	  The output file will contain 2 columns:
	  <ul>
            <li> The simulation step</li>
            <li> The value of the first collective variable</li>
	  </ul>
	  One would analyze the resulting files with:
              "<code>ndfes --mbar</code>" or "<code>ndfes --vfep</code>".

	</li>

	
	<li>
A 2D FES umbrella window for vFEP or MBAR analysis
              There are 2 collective variable (the first two variables in the
              disang file).  All other restraints are ignored.
	  <br>
	  <code>ndfes-PrepareAmberData.py -d rc_0.00_0.00.disang
                  -i rc_0.00_0.00.dumpave
            -o dumpaves/rc_0.00_0.00.dumpave -r 1 2</code>
	  <br>
	  The output file will contain 3 columns:
	  <ul>
                 <li> The simulation step</li>
                 <li> The value of the first collective variable</li>
                 <li> The value of the second collective variable</li>
	  </ul>
          One would analyze the resulting files with:
          "<code>ndfes --mbar</code>" or "<code>ndfes --vfep</code>".
	</li>

	
	<li>
              A 1D FES umbrella window for wTP analysis, in which the effect
              of a second biasing potential is removed.
	  <br>
	  <code>ndfes-PrepareAmberData.py -d rc_0.00.disang -i rc_0.00.dumpave
            -o dumpaves/rc_0.00.dumpave -r 1 -b 2</code>
	  <br>
	  The output file will contain 4 columns:
	  <ul>
            <li>The simulation step</li>
            <li>The value of the first collective variable</li>
            <li>The bias energy (kcal/mol) caused by the 2nd CV</li>
            <li>The potential energy without the bias (which is safely
              chosen to be 0.0</li>
	  </ul>
          One would analyze the resulting files with:
          <code>ndfes --mbar --nham=2</code>
          The biased FES would correspond to "model 0" and the
          unbiased FES would be "model 1".
	</li>

	
	<li>
              A 1D FES umbrella window for wTP analysis, in which a low
              level Hamiltonian is transformed to a high level Hamiltonian

              If the simulation was performed with the low level Hamiltonian,
              then --hamidx is set to 0 to indicate that the corresponding
              low level energies are stored in the first instance of --ene.
	  <br>
	  <code>ndfes-PrepareAmberData.py -d rc_0.00.disang 
                  --hamidx 0 -i LL_0.00.nc 
                  -o dumpaves/rc_0.00.dumpave -r 1 
            --ene LL_0.00.mdout --ene HL_0.00.mdout</code>
	  <br>
	  Whereas, if the high-level Hamiltonian produced the trajectory,
          then --hamidx=1 because the high level energies are stored in
          the second instance of --ene.
	  <br>
	  <code>ndfes-PrepareAmberData.py -d rc_0.00.disang 
                  --hamidx 1 -i HL_0.00.nc 
                  -o dumpaves/rc_0.00.dumpave -r 1 
                  --ene LL_0.00.mdout --ene HL_0.00.mdout
	  </code>
	  <br>
	  The output file will contain 4 columns:
	  <ul>
            <li>The simulation step</li>
            <li>The value of the first collective variable</li>
            <li>The low level potential energy</li>
            <li>The high level potential energy</li>
	  </ul>
          One would analyze the resulting files with:
          <code>ndfes --mbar --nham=2</code>
          The low level FES would correspond to "model 0" and the
          high level FES would be "model 1".
	</li>

	
	<li>
              This is the same as Example 4; however, the high-level
              simulations were restart several times to yield several
              trajectory and reanalysis files.

	  <br>
	  <code>
              ndfes-PrepareAmberData.py -d rc_0.00.disang 
                  --hamidx 1 -i HL_0.00.nc HL_0.00_rst1.nc HL_0.00_rst2.nc 
                  -o dumpaves/rc_0.00.dumpave -r 1 
                  --ene LL_0.00.mdout LL_0.00_rst1.mdout LL_0.00_rst2.mdout 
                  --ene HL_0.00.mdout HL_0.00_rst1.mdout HL_0.00_rst2.mdout
	  </code>
	  <br>
          The output file will contain 4 columns:
	  <ul>
                 <li>The simulation step</li>
                 <li>The value of the first collective variable</li>
                 <li>The low level potential energy</li>
                 <li>The high level potential energy</li>
	  </ul>
          One would analyze the resulting files with:
          <code>ndfes --mbar --nham=2</code>.
          The low level FES would correspond to "model 0" and the
          high level FES would be "model 1".

	</li>

	
	<li>
              This is the same as Example 4; however, an additional bias
              potential is removed from the 1D FES.

	  <br>
	  <code>
              ndfes-PrepareAmberData.py -d rc_0.00.disang 
                  --hamidx 1 -i HL_0.00.nc 
                  -o dumpaves/rc_0.00.dumpave -r 1 -b 2 
                  --ene LL_0.00.mdout --ene HL_0.00.mdout
	  </code>
	  <br>
          The output file will contain 6 columns:
	  <ul>
                 <li>The simulation step</li>
                 <li>The value of the first collective variable</li>
                 <li>The biased low level potential energy</li>
                 <li>The biased high level potential energy</li>
                 <li>The unbiased low level potential energy</li>
                 <li>The unbiased high level potential energy</li>
	  </ul>
          One would analyze the resulting files with:
          <code>ndfes --mbar --nham=4</code>.
          The biased low level FES would correspond to "model 0".
          The biased high level FES would be "model 1".
          The unbiased low level FES would be "model 2".
          The unbiased high level FES would be "model 3".
	</li>

	
	<li>
	  One normally uses ndfes-PrepareAmberData.html in a bash-loop
	  to collect the standard output in a file.
	  
	  <pre><code>#!/bin/bash
if [ ! -d analysis/dumpaves ]; then
    mkdir -p analysis/dumpaves
fi
truncate -s0 analysis/metafile
for disang in *.disang; do	      
    idump=${disang%.disang}.dumpave
    odump=analysis/dumpaves/${idump}
    for f in "${disang}" "${idump}"; do
        if [ ! -e "${f} ]; then
            echo "File not found: ${f}"
            exit 1
        fi
    done
    ndfes-PrepareAmberData.py -d ${disang} -i ${idump} -o ${odump} -r 1 --prefix=analysis/ >> analysis/metafile
done
cd analysis
ndfes --mbar -c chkpt metafile
	  </code></pre>
	</li>
	
      </ul>
    </p>


    
  </body>
</html>
