<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
  </head>
  <body>

    <h1>
      ndfes-path-prepguess.py
    </h1>

    <p>
      <ul>
	<li>
	  <a href="#Overview">Overview</a>
	</li>
	<li>
	  <a href="#Usage">Usage</a>
	</li>
	<li>
	  <a href="#Options">Options</a>
	</li>
	<li>
	  <a href="#Examples">Examples</a>
	</li>
      </ul>
    </p>


    <h3 id="Overview">Overview</h3>
    <p>
      Creates an initial guess for a string path
      given the proposed location of the reactant and product minima.
      If you have an initial guess for a string pathway,
      you do not need this script.
      Usage of this script is described in detail
      within the <a href="../ftsm/index.html#Guess">ndfes FTSM tutorial</a>.
    </p>
    <p>
      Specifically, this script reads a template disang and mdin files, and
      the proposed locations of the reactant and product state
      on the reduced dimensional free energy surface.
      Furthermore, given the number of windows you want to use,
      it linearly interpolates along the path and writes
      mdin and disang files for each window.
      The output is written to a directory specified on the command line,
      and bash scripts are provided which sample each window
      in sequence from the available structures.
      The location of the reactant and product states
      can be supplied by providing coordinate restart files
      and/or corresponding disang files.
    </p>
    <p>
      One could input more than 2 minima to
      this script, and it would linearly interpolate
      between each set of adjacent minima.
      That is, you may have some idea of where the
      transition state is located.
      You do not need to provide coordinate restart
      files for each control point, but at least
      one control point must have an input
      structure.
    </p>

    <h3 id="Usage">Usage</h3>
    <p>
      <pre><code>ndfes-path-prepguess.py  -d DISANG -i MDIN --min MIN [MIN [...]] -n NSIM [--dry-run | -o DIR]
</code></pre>
    </p>


    <h3 id="Options">Options</h3>
    <p>
      <ul>
	<li><code>--disang DISANG</code> or <code>-d DISANG</code>
	  <br>
	  Template disang file. To be a "template", the r2 and r3 values (the center
	  of the biasing harmonic) must appear as a string "RC1".
	  If more than 1 reaction coordinate is involved, then the r2 and r3 values
	  of the other restraints are "RCX", where X is an integer.
	  The order of the RCX strings must be consecutive, that is, RC1 must
	  appear before RC2.
	</li>
	<li><code>--mdin MDIN</code> or <code>-i MDIN</code>
	  <br>
	  The template disang file. The nstlim value would typically be
	  something very small like 250 or 500 because, at this point, one is
	  merely trying to create initial structures to begin dynamics.
	  Performing a full equilibration at this stage would be expensive
	  because the simulations are performed sequentially.
	</li>
	<li><code>--min MIN</code>
	  <br>
	  This is a file ending in .rst7 (a coordinate restart file)
	  or .disang (a restraint file).
	  This option defines a control point for the linearly-interpolated
	  parametric curve of reaction coordinates.
	  If a rst7 file is specified, then the template
	  disang file is used to measure the reaction coordinate
	  values from the structure, and the coordinates are later used
	  to initiate dynamics to prepare coordinates for the interpolated
	  window locations.
	  If a disang file is specified, then it must include the
	  exact same restraints as the template disang file, but
	  whose RCX placeholders are replaced by the appropriate numerical
	  values.  Furthermore, the script will replace the ".disang"
	  suffix with ".rst7" to check if there is a corresponding
	  structure for this control point.
	  If so, then it is later used to initiate dynamics to prepare
	  coordinates for the interpolated window locations.
	</li>
	<li><code>-n NSIM</code>
	  <br>
	  The number of windows used to describe the string.
	  This controls how many output disang and mdin files
	  are generated.
	  The output files will be named: DIR/img%02i.mdin and DIR/img%02i.disang,
	  where DIR is the string supplied to the <code>-o</code> option.
	</li>
	<li><code>--dry-run</code>
	  <br>
	  If present, then do not write the output files.
	  Instead, print the coordinates of the interpolated string
	  to standard output.
	</li>
	<li><code>-o DIR</code>
	  <br>
	  The directory where the output files should be written.
	  Default: "init".
	  The directory will be created if it does not exist.
	</li>

	
      </ul>
    </p>


    
    <h3 id="Examples">Examples</h3>
    <p>
      <ul>
	<li>
	  Suppose you have equilibrated a structure for the reactant
	  state. You need to make a guess for the location of the
	  product state. In this example, suppose there are two
	  reaction coordinates, whose template restraint file (template.disang)
	  looks like:
	  <pre><code>&rst iat= 291, 290, 291, 2193,
r1=-999, r2=RC1, r3=RC1, r4=999, rk2=50, rk3=50, rstwt=1.0,-1.0 /

&rst iat= 2200, 2189, 2200, 1984,
r1=-999, r2=RC2, r3=RC2, r4=999, rk2=50, rk3=50, rstwt=1.0,-1.0 /
</code></pre>
	  Perhaps the reactant structure corresponds to RC1=-1.5,RC2=1.5
	  and the guess for the product state is RC1=2.0,RC2=-1.2.
	  We could copy template.disang to reactant.disang and product.disang
	  and replace the RC1 and RC2 placeholders with the listed
	  values.
	  Furthermore, one could copy the equilibrated coordinate
	  restart file to reactant.rst7.
	  One could then create a string of 32 windows with the following command:
	  <pre><code>ndfes-path-prepguess.py -i template.mdin -d template.disang \
    --min reactant.disang --min product.disang --nsim 32 -o init
	  </code></pre>
	  This will create a new directory called "init" containing 32 mdin and 32 disang files,
	  a restart file named init01.rst7 (a copy of reactant.rst7), and an equilibration
	  script eq01fwd.sh.  The name of the equilibration script means "sequential equilibration
	  in the forward direction starting from the first supplied coordinate file".
	  The contents of the equilibration script looks like:
	  <pre><code>#!/bin/bash
set -e
set -u

#
# You can create a slurm script and run these
# commands in the following way:
#
# export LAUNCH="srun sander.MPI"
# export PARM="path/to/parm7"
# bash eq01fwd.sh
#

if [ "${LAUNCH}" == "" ]; then
    echo 'bash variable LAUNCH is undefined. Defaulting to: export LAUNCH="sander"'
    export LAUNCH="sander"
fi

if [ "${PARM}" == "" ]; then
    echo 'bash variable PARM is undefined. Please: export PARM="/path/to/parm7"'
    exit 1
else
   if [ ! -e "${PARM}" ]; then
       echo "File not found: ${PARM}"
       exit 1
   fi
fi

if [ ! -e "init01.rst7" ]; then
    echo "File not found: init01.rst7"
    exit 1
fi

${LAUNCH} -O -p ${PARM} -i img01.mdin -o img01.mdout -c init01.rst7 -r img01.rst7 -x img01.nc -inf img01.mdinfo
${LAUNCH} -O -p ${PARM} -i img02.mdin -o img02.mdout -c img01.rst7 -r img02.rst7 -x img02.nc -inf img02.mdinfo
${LAUNCH} -O -p ${PARM} -i img03.mdin -o img03.mdout -c img02.rst7 -r img03.rst7 -x img03.nc -inf img03.mdinfo
${LAUNCH} -O -p ${PARM} -i img04.mdin -o img04.mdout -c img03.rst7 -r img04.rst7 -x img04.nc -inf img04.mdinfo
${LAUNCH} -O -p ${PARM} -i img05.mdin -o img05.mdout -c img04.rst7 -r img05.rst7 -x img05.nc -inf img05.mdinfo
${LAUNCH} -O -p ${PARM} -i img06.mdin -o img06.mdout -c img05.rst7 -r img06.rst7 -x img06.nc -inf img06.mdinfo
[...]
	  </code></pre>
	  To use this script, one must export the LAUNCH and PARM environmental variables.
	  <pre><code>export LAUNCH="mpirun -n 4 sander.MPI"
export PARM=/path/to/parm7   
bash ./eq01fwd.sh
	  </code></pre>
	  You can submit the calculations to a cluster using slurm in a similar manner.
	  <pre><code>#!/bin/bash
#SBATCH --job-name="eq01fwd"
#SBATCH --output="eq01fwd.slurmout"
#SBATCH --error="eq01fwd.slurmerr"
#SBATCH --partition=main
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --requeue
#SBATCH --export=ALL
#SBATCH -t 2-00:00:00

set -e
set -u
module load amber	      
export LAUNCH="srun sander.MPI"
export PARM=/path/to/parm7
bash ./eq01fwd.sh	      
	  </code></pre>
	</li>
	
	<li>
	  Suppose you had equilibrated structures of the reacant (reactant.rst7) and transition state
	  (ts.rst7). In the example below, we directly supply the restart files as arguments
	  and let the script calculate the reaction coordinates from the structures.
	  <pre><code>ndfes-path-prepguess.py -i template.mdin -d template.disang \
    --min reactant.rst7 --min ts.rst7 --min product.disang --nsim 32 -o init
	  </code></pre>
	  In this case, we will see that "init" contains two restart files init01.rst7
	  and init02.rst7 and 3 equilibration scripts eq01fwd.sh, eq02rev.sh, and eq02fwd.sh.
	  The eq01fwd.sh file contains the commands that progress the reaction from the
	  reactant state to the midpoint between the reactant and transition states.
	  The eq02rev.sh script progresses from the transition state to the midpoint
	  between the reactant and transition states.
	  The eq02fwd.sh script progresses from the transition state to the product state.
	  All 3 of these scripts are independent of each other and can be simultaneously run.
	</li>
	
      </ul>
    </p>


    
  </body>
</html>
