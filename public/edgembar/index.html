<html>
  <head>
    <style>
      ul {
	  padding-left: 20px;
	  list-style-position: outside;
      }
      li {
	  padding-bottom: 0.5em;
      }
    </style>
  </head>
  <body>


    
    <h3>Networkwide Analysis of Alchemical Free Energy Simulation Data</h3>
    <p>
    The edgembar tool provided as a part of the Fe-ToolKit enables robust analysis and visualization of data from alchemical free energy (AFE) simulations and thermodynamic networks.  Each node of the network represents is a well-defined thermodynamic state, and the edges describe transformations between them.  Edge transformations occur in different environments (e.g., in the gas phase, aqueous solution, or  complexed with a protein molecule) such that the relative free energy is the difference between two environments.  The networkwide analysis imposes exact theoretical constraint conditions on the free energy estimates, as well as providing additional information about errors not necessarily evident from statistical analysis of the independent edges.
    </p>
    <p>
    Below we provide an Overview with links to more detailed information about setting up and performing edge free energy calculations, working examples of data analysis, interpretation of the HTML report, and extension to networkwide free energy analysis.  Following is concise description of the contents of the package, as well as a description of the general workflow of how it is used.  If using this package in a publication, please cite: Giese and York (<a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c01219">10.1021/acs.jcim.0c00613</a>).
    </p>
    <h3>Overview</h3>
    <p>
      <ul>
	<li>
	  <a href="introduction.html">Introduction</a>
	  <br>
	  Terminology, calculation hierarchy, and
	  a description of the free energy calculations of an edge.
	</li>

	<li>
	  <a href="edgecalc.html">Calculating the free energy of an edge</a>
	  <br>
	  Describes how to prepare the edgembar input file
	  and run the edgembar program
	</li>

	<li>
	  <a href="example.html">Analyzing the included example data</a>
	  <br>
	  The documentation includes a small set of data.
	  This section describes the shell commands used to analyze
	  the example.
	</li>
	
	<li>
	  <a href="edgeana.html">Understanding the edge free energy HTML report</a>
	  <br>
	  Describes how to generate a HTML report of the edge free energy analysis. The tables and figures within the report are explained. A description of the algorithm used to identify the production region is provided.
	</li>

	<li>
	  <a href="graphana.html">Networkwide free energy analysis</a>
	  <br>
	  Describes the generation of the free energy network report.
	</li>
	
      </ul>
      
    </p>

    <h3>Contents</h3>
    <p>
      The edgembar package contains a c++ program that performs the
      free energy analysis of an edge and stores the results in
      an output file.
      The package also supplies a python library and several
      utility python scripts to summarize the results in HTML
      format, perform networkwide free energy analysis,
      and summarize the network analysis in an HTML file.
    </p>

    <h3>General Workflow</h3>
    <p>
      The general steps for conducting AFE simulations and analyzing the results are as follows:
      <ul>
	<li>
	  Prepare your simulations by
	  <a href="edgecalc.html#DirOrg">organizing a directory structure</a>
	  into a hierarchy which separates the simulation results
	  between <a href="introduction.html#Edge">edges</a>,
	  the <a href="introduction.html#Env">environments</a>
	  in which the transformation is performed,
	  the <a href="introduction.html#Stage">stages</a>
	  defining the alchemical pathway, and
	  the independent <a href="introduction.html#Trial">trials</a>
	  used to improve statistics and estimate errors.
	  The contents of a trial's directory usually contain
	  the molecular dynamics (MD) program's input files to
	  sample each alchemical
	  <a href="introduction.html#State">state</a>.
	</li>
	<li>
	  Perform alchemical free energy simulations with your
	  favorite MD program to produce a MD output file for each state.
	</li>
	<li>
	  For each trial, parse the MD output files to write the potential
	  energy (and, optionally, &part;U/&part;&lambda;) time-series to
	  <a href="edgecalc.html#RawData">raw data files</a>
	  that can be read by edgembar.
	  The directory of each trial of each stage of each environment of
	  each edge will contain a separate set of extracted time-series. 
	</li>
	<li>
	  Prepare an <a href="edgecalc.html#XML">edgembar XML file</a>.
	  This is the input file read by edgembar.
	  The XML data structure communicates the hierarchy of edge,
	  environment, stage, trial, and state to the program so it
	  understands how to combine the data to obtain a relative
	  free energy.
	</li>
	<li>
	  Run the <a href="edgecalc.html#CXX">edgembar c++ program</a>.
	  It reads the XML input file, performs the analysis, and writes
	  the results to an output file.
	  The output file is a python script that populates a python
	  object with the results.
	  The supplied python library contains utility functions to import
	  the python script and extract or manipulate the results.
	</li>
	<li>
	  Write an <a href="edgeana.html">
	    HTML report of the edge free energy analysis</a>.
	  The HTML report is generated by executing the python output
	  file on the command line.
	</li>
	<li>
	  Repeat the edgembar analysis for multiple edges.
	  This results in a series of edgembar output (python) files.
	  A <a href="graphana.html">networkwide free energy analysis
	    and HTML report</a> can be
	  generated from the edgembar-WriteGraphHtml.py script,
	  given the edge output files to include within the graph.
	</li>
      </ul>
    </p>

    <h3>References</h3>

    <ul>
      <li>
	Alchemical Binding Free Energy Calculations in AMBER20:
	Advances and Best Practices for Drug Discovery
	<br>
	
	Tai-Sung Lee, Bryce K. Allen, Timothy J. Giese, Zhenyu Guo,
	Pengfei Li, Charles Lin, T. Dwight McGee, David A. Pearlman,
	Brian K. Radak, Yujun Tao, Hsu-Chun Tsai, Huafeng Xu,
	Woody Sherman, and Darrin M. York
	<br>
	J. Chem. Inf. Model. (2020) 60, 5595-5623
	(<a href="https://www.nature.com/articles/s41557-019-0391-x">10.1021/acs.jcim.0c00613</a>)
      </li>
      <li>
	Variational Method for Networkwide Analysis of Relative
	Ligand Binding Free Energies with Loop Closure and
	Experimental Constraints
	<br>
	Timothy J. Giese and Darrin M. York
	<br>
	J. Chem. Theory Comput. (2021) 17, 1326-1336
	(<a href="https://pubs.acs.org/doi/10.1021/acs.jctc.0c01219">10.1021/acs.jcim.0c00613</a>)
      </li>
      <li>
	AMBER Drug Discovery Boost Tools: Automated Workflow for
	Production Free-Energy Simulation Setup and Analysis
	(ProFESSA)
	<br>
	Abir Ganguly, Hsu-Chun Tsai, Mario Fernández-Pendás,
	Tai-Sung Lee, Timothy J. Giese, and Darrin M. York
	<br>
	J. Chem. Inf. Model. (2022) 62, 6069-6083
	(<a href="https://pubs.acs.org/doi/full/10.1021/acs.jcim.2c00879">10.1021/acs.jcim.2c00879</a>)
      </li>
    </ul>
    
  </body>
</html>

<!--
    <h3>Overview</h3>
    <p>
      <ul>
	<li>
	  <a href="introduction.html">Introduction</a>
	  <br>
	  Terminology, calculation hierarchy, and
	  a description of the free energy calculations of an edge.
	</li>

	<li>
	  <a href="edgecalc.html">Calculating the free energy of an edge</a>
	  <br>
	  Describes how to prepare the edgembar input file
	  and run the edgembar program
	</li>

	<li>
	  <a href="example.html">Analyzing the included example data</a>
	  <br>
	  The documentation includes a small set of data.
	  This section describes the shell commands used to analyze
	  the example.
	</li>
	
	<li>
	  <a href="edgeana.html">Understanding the edge free energy HTML report</a>
	  <br>
	  Describes how to generate a HTML report of the edge free energy analysis. The tables and figures within the report are explained. A description of the algorithm used to identify the production region is provided.
	</li>

	<li>
	  <a href="graphana.html">Networkwide free energy analysis</a>
	  <br>
	  Describes the generation of the free energy network report.
	</li>
	
      </ul>
      
    </p>

    <p>
      The edgembar package contains a c++ program that performs the
      free energy analysis of an edge and stores the results in
      an output file.
      The package also supplies a python library and several
      utility python scripts to summarize the results in HTML
      format, perform networkwide free energy analysis,
      and summarize the network analysis in an HTML file.
    </p>

    <p>
      The general workflow is:
      <ul>
	<li>
	  Prepare your simulations by
	  <a href="edgecalc.html#DirOrg">organizing a directory structure</a>
	  into a hierarchy which separates the simulation results
	  between <a href="introduction.html#Edge">edges</a>,
	  the <a href="introduction.html#Env">environments</a>
	  in which the transformation is performed,
	  the <a href="introduction.html#Stage">stages</a>
	  defining the alchemical pathway, and
	  the independent <a href="introduction.html#Trial">trials</a>
	  used to improve statistics and estimate errors.
	  The contents of a trial's directory usually contain
	  the molecular dynamics (MD) program's input files to
	  sample each alchemical
	  <a href="introduction.html#State">state</a>.
	</li>
	<li>
	  Perform alchemical free energy simulations with your
	  favorite MD program to produce a MD output file for each state.
	</li>
	<li>
	  For each trial, parse the MD output files to write the potential
	  energy (and, optionally, &part;U/&part;&lambda;) time-series to
	  <a href="edgecalc.html#RawData">raw data files</a>
	  that can be read from edgembar.
	  The directory of each trial of each stage of each environment of
	  each edge will contain a separate set of extracted time-series. 
	</li>
	<li>
	  Prepare an <a href="edgecalc.html#XML">edgembar XML file</a>.
	  This is the input file read by edgembar.
	  The XML data structure communicates the hierarchy of edge,
	  environment, stage, trial, and state to the program so it
	  understands how to combine the data to obtain a relative
	  free energy.
	</li>
	<li>
	  Run the <a href="edgecalc.html#CXX">edgembar c++ program</a>.
	  It reads the XML input file, performs the analysis, and writes
	  the results to an output file.
	  The output file is a python script that populates a python
	  object with the results.
	  The supplied python library contains utility functions to import
	  the python script and extract or manipulate the results.
	</li>
	<li>
	  Write an <a href="edgeana.html">
	    HTML report of the edge free energy analysis</a>.
	  The HTML report is generated by executing the python output
	  file on the command line.
	</li>
	<li>
	  Repeat the edgembar analysis for multiple edges.
	  This results in a series of edgembar output (python) files.
	  A <a href="graphana.html">networkwide free energy analysis
	    and HTML report</a> can be
	  generated from the edgembar-WriteGraphHtml.py script,
	  given the edge output files to include within the graph.
	</li>
      </ul>
    </p>
    
  </body>
</html>

//-->
