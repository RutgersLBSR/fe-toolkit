<html>
  <head>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    <style>
      ul {
	  padding-left: 20px;
	  list-style-position: outside;
      }
      li {
	  padding-bottom: 0.5em;
      }
    </style>
  </head>

  <body>

    <h3>Introduction</h3>
    This page describes the calculation of an edge relative free energy.
    To proceed, we need to introduce
    some terminology.
    <p>
      <ul>
	<li>
	  <a href="#Edge">The relative free energy of an edge</a> is the
	  difference between free energies evaluated in
	  two environments.
	</li>
	<li>
	  <a href="#Env">The transformation free energy in an environment</a>
	  is a sum of stage free energies; it is 
	  a transformation between two physical states in a single environment.
	</li>
	<li>
	  <a href="#Stage">A stage</a> free energy
	  is a trial-averaged free
	  energy between two states that may or may not represent
	  physical states.
	</li>
	<li>
	  <a href="#Trial">A trial</a> is a collection of simulations
	  performed along an alchemical progress coordinate, &lambda;,
	  from which the free energy of a stage may be evaluated.
	</li>
	<li>
	  <a href="#State">A state</a> refers to an ensemble characterized
	    by thermodynamic quantities and a potential energy
	  function. In other words, it is one of the &lambda;
	  simulations in a trial.
	</li>
	
      </ul>
    </p>

    <p>
      The states within a trial are analyzed to obtain
      state free energies and associated uncertainty estimates.
      The <a href="#Edge">relative free energy</a>
      of an edge and its <a href="#Error">error</a> are
      calculated from these quantities.
    </p>

    <p>
      The state free energies are evaluated with the
      <a href="#MBAR">multistate Bennett acceptance ratio</a> (MBAR)
      method.
      If &part;U/&part;&lambda; data is available,
      then edgembar will also report
      <a href="#TI">thermodynamic integration</a>
      free energies to make comparison.
      Finally, edgembar will also use MBAR to
      <a href="#Constrained">precompute data</a>
      necessary for networkwide free energy analysis.
      <sup>
	[
	<a href="https://doi.org/10.1021/acs.jctc.0c01219">Giese (2021)</a>
	]
      </sup>
    </p>

    
    <h3 id="State">States and schedules</h3>
    <p>
      A thermodynamic state is characterized by the number of particles,
      temperature, pressure, volume, and the potential energy of the
      system. In the context of alchemical free energy simulations,
      we are particularly interested in the free energy of changing
      a potential energy function U(<b>r</b>;&lambda;) that distinguishes
      between two states &lambda;=0 and &lambda;=1.
      The potential energy depends on the 3N array of atomic positions,
      <b>r</b>, and &lambda; is the transformation progress variable.
      To improve the accuracy of the calculated free energy
      estimate, one typically samples a series of U(<b>r</b>;&lambda;)
      at intermediate &lambda;-values (the alchemical states).
      The collection of N<sub>&lambda;</sub> values
      {&lambda;<sub>1</sub>,
      &lambda;<sub>2</sub>,
      &#183;&#183;&#183;,
      &lambda;<sub>N<sub>&lambda;</sub>-1</sub>,
      &lambda;<sub>N<sub>&lambda;</sub></sub>}
      shall be referred to
      as the <i>&lambda;-schedule</i>,
      whereas the U(<b>r</b>;&lambda;) defined by a particular
      value of &lambda; will be generically referred to as a <i>state</i>.
    </p>

    
    <h3 id="Trial">Trials</h3>
    <p>
      A <i>trial</i> is the collection of ensembles obtained
      by sampling each state of a &lambda;-schedule.
      From this sampling, the free energy of each state can
      be evaluated using thermodynamic integration
      or the multistate Bennett acceptance ratio method.
      The free energy of the trial is the difference between
      the &lambda;=0 and &lambda;=1 state free energies.
      To obtain better statistics, one often performs
      multiple simulations (multiple trials). Each trial produces
      an independent estimate of the free energy,
      and the mean of these estimates is referred to as
      the trial-averaged free energy.
      We shall not assume that the trials use a consistent
      &lambda;-schedule; therefore, 
      one cannot necessarily average the intermediate-state
      free energies.
    </p>

    
    <h3 id="Stage">Stages</h3>
    <p>
      A <i>stage</i> free energy is a trial-averaged
      free energy that connects two states which may or may not
      correspond to physical states.
      The transformation between physical states may be estimated
      with increased accuracy by separating it into components (stages)
      and using different methodologies tailored to each component.
      Although the free energies
      of the individual stages do not necessarily correspond to physical
      states, the sum of stage free energy must necessarily correspond to
      a difference in physical states.
      A fairly common strategy is to use a 3-stage protocol consisting
      of "decharge", "vdW", and "recharge" stages.
      The decharge and recharge stages often use linear
      or nonlinear mixing,
      <sup>
	[
	<a href="https://doi.org/10.1002%2Fjcc.21909">Steinbrecher (2011)</a>
	]
      </sup>
      U(<b>r</b>;&lambda;)
      =
      &lambda;<sup>m</sup>U(<b>r</b>;1)
      +
      (1-&lambda;)<sup>m</sup>U(<b>r</b>;0),
      to adjust the atomic charges and bonded molecular mechanical
      force field energy contributions,
      whereas the
      vdW stage employs a Lennard-Jones softcore function to adjust
      the van der Waals interactions.
      <sup>
	[
	<a href="https://doi.org/10.1016/0009-2614(94)00397-1">Beutler (1994)</a>,
	<a href="https://doi.org/10.1063/1.466707">Zacharias (1994)</a>,
	<a href="https://doi.org/10.1002/(SICI)1096-987X(20000415)21:5<388::AID-JCC5>3.0.CO;2-M">Tappura (2000)</a>,
	<a href="https://doi.org/10.1016/j.jmgm.2003.12.007">Hornak (2004)</a>
	]
      </sup>
      Alternatively, more sophisticated softcore potentials,
      such as the smooth-step softcore function,
      <sup>
	[
	<a href="https://doi.org/10.1021/acs.jctc.0c00237">Lee (2020)</a>
	]
      </sup>
      have been developed to stably perform
      transformations with only 1 stage.
    </p>
    
    <h3 id="Env">Environments</h3>
    <p>
      The environment refers to the surrounding in which a transformation
      is performed.
      The transformation free energy in a particular environment is
      the sum of stage free energies.
      The transformation free energy between two molecules modeled
      by molecular mechanics includes the differences in their
      internal energies, which the force field was not necessarily
      parametrized to reproduce.
      To circumvent this, one typically calculates the
      transformation free energy in two environments to evaluate
      a relative free energy.
    </p>

    
    <h3 id="Edge">Edge free energy</h3>
    <p>
      The edge free energy is a relative free
      energy between two environments, &Delta;&Delta;G<sub>e</sub>,
      where <i>e</i> indexes the edge.
    </p>
    
    \[
    \Delta\Delta G_{e}
    =
    \Delta G_{e,\text{tgt}}
    -
    \Delta G_{e,\text{ref}}
    \]

    <p>
      &Delta;G<sub>e,tgt</sub> and &Delta;G<sub>e,ref</sub>
      are the transformation free energies in the two
      environments. The "tgt" and "ref" subscripts are nothing
      more than labels used to refer to the environments.
      Specifically,
      "ref" denotes the reference environment (the free energy
      to subtract),
      and "tgt" refers to the target environment (the free energy
      to add).
      <ul>
	<li>
	  If &Delta;&Delta;G<sub>e</sub> was a ligand binding free energy, then
	  "tgt" is the transformation of the ligand
	  complexed to the biomolecule, and "ref" is
	  the transformation performed in solution.
	</li>
	<li>
	  If &Delta;&Delta;G<sub>e</sub> was a solvation free energy, then
	  "tgt" is the solvated environment, and
	  "ref" is the gas phase environment.
	</li>
	<li>
	  If there was only one environment, then "tgt"
	  refers to the environment, and &Delta;G<sub>e,ref</sub> &equiv; 0.
	</li>
      </ul>
    </p>

    <p>
      The transformation energy of edge <i>e</i> in environment <i>v</i>, &Delta;G<sub>e,v</sub>, is the sum of trial-averaged stage free energies,
 
      \[
      \Delta G_{e,v}
      =
      \sum_{s=1}^{N_{\text{stage},e,v}}
      \langle \Delta G_{e,v,s} \rangle
      \]

      where N<sub>stage,e,v</sub> is the number of stages,
      and &langle;&Delta;G<sub>e,v,s</sub>&rangle;
      is the trial-averaged stage free energy.

      \[
      \langle \Delta G_{e,v,s} \rangle = \frac{1}{N_{\text{trial},e,v,s}}\sum_{t=1}^{N_{\text{trial},e,v,s}} \Delta G_{e,v,s,t}
      \]

      N<sub>trial,e,v,s</sub> is the number of trials performed for
      stage <i>s</i>, and &Delta;G<sub>e,v,s,t</sub> is the
      free energy of trial <i>t</i>.
      
      \[
      \Delta G_{e,v,s,t} = G_{e,v,s,t,\lambda=1} - G_{e,v,s,t,\lambda=0}
      \]

      G<sub>e,v,s,t,&lambda;</sub> is the free energy of state &lambda;
      determined from TI or MBAR analysis of the trial.
    </p>

    
    <b id="Error">Error analysis</b>
    <p>
      Edgembar will report the state free energies along with an estimate of
      its standard error; that is,
      G<sub>e,v,s,t,&lambda;</sub> &plusmn; &delta;G<sub>e,v,s,t,&lambda;</sub>.
      These errors are estimated from the propagation of &part;U/&part;&lambda; uncertainties
      (when using TI) or by a circular moving block bootstrap procedure
      (when using MBAR).
      Given the state free energies (and their errors), the
      standard error of the trial free energy is,
      \[
      \delta\Delta G_{e,v,s,t} = \sqrt{ \left(\delta G_{e,v,s,t,\lambda=1}\right)^2 + \left(\delta G_{e,v,s,t,\lambda=0}\right)^2 }
      \]
      and the uncertainty in 
      &langle;&Delta;G<sub>e,v,s</sub>&rangle;,
      &Delta;G<sub>e,v</sub>,
      and &Delta;&Delta;G<sub>e</sub>
      are evaluated from these errors.
      \[
      \delta\langle \Delta G_{e,v,s} \rangle
      =
      \sqrt{\frac{1}{N_{\text{trial},e,v,s}} \left( \sum_{t=1}^{N_{\text{trial},e,v,s}} \frac{(\Delta G_{e,v,s,t} - \langle\Delta G_{e,v,s}\rangle)^2}{N_{\text{trial},e,v,s}-1} + \sum_{t=1}^{N_{\text{trial},e,v,s}} \frac{\left(\delta\Delta G_{e,v,s,t}\right)^2}{N_{\text{trial},e,v,s}}\right)}
      \]
      \[
      \delta\Delta G_{e,v} = \sqrt{ \sum_{s=1}^{N_{\text{stage},e,v}} \left(\delta\langle\Delta G_{e,v,s}\rangle\right)^2 }
      \]
      \[
      \delta\Delta\Delta G_{e} = \sqrt{ \left( \delta\Delta G_{e,\text{tgt}} \right)^2 + \left( \delta\Delta G_{e,\text{ref}} \right)^2 }
      \]
      </p>

    <h3 id="TI">Thermodynamic integration (TI) calculation of the state free energies within a trial</h3>
    <p>
      The simulation of each state <i>k</i> produces a time-series
      of &part;U/&part;&lambda; values.
      The mean f<sub>e,v,s,t,k</sub> and standard error
      &delta;f<sub>e,v,s,t,k</sub> of the time-series
      are readily computed.
      \[
      f_{e,v,s,t,k} = \frac{1}{N_{\text{samples},e,v,s,t,k}} \sum_{j=1}^{N_{\text{samples},e,v,s,t,k}} \frac{\partial U_{e,v,s}(\mathbf{r}_{jk}^{(t)};\lambda_k)}{\partial\lambda}
      \]
      \[
      \delta f_{e,v,s,t,k} =
      \left(
      \frac{g_{e,v,s,t,k}}{N_{\text{samples},e,v,s,t,k}}
      \sum_{j=1}^{N_{\text{samples},e,v,s,t,k}}
      \frac{
      \left(
      \partial U_{e,v,s}(\mathbf{r}_{jk}^{(t)};\lambda_k)/\partial\lambda
      -f_{e,v,s,t,k}
      \right)^2
      }
      {
      N_{\text{samples},e,v,s,t,k}-1
      }
      \right)^{1/2}
      \]
      <b>r</b><sub>jk</sub><sup>(t)</sup> is the 3N array
      of coordinates of sample <i>j</i> in trial <i>t</i> of state <i>k</i>,
      g<sub>e,v,s,t,k</sub>=2&tau;<sub>e,v,s,t,k</sub>+1
      is the statistical inefficiency,
      and &tau; is the autocorrelation time.
      <sup>
	[
	<a href="https://doi.org/10.1021/ct0502864">Chodera (2007)</a>
	]
      </sup>
      If the samples were statistically independent, or
      a strided subset of samples were analyzed to
      remove the correlation, then g=1.
      From the discrete set of N<sub>states,e,v,s,t</sub> means,
      one can parametrize a continuous curve f<sub>e,v,s,t</sub>(&lambda;) that
      passes through each observed mean.
      \[
      f_{e,v,s,t}(\lambda_k) = f_{e,v,s,t,k}\;\forall\; k\in1,\cdots,N_{\text{states},e,v,s,t}
      \]
      The state free energies are then given by the integral,
      <sup>
	[
	<a href="https://doi.org/10.1063/1.1749657">Kirkwood (1935)</a>
	]
      </sup>
      \[
      G_{e,v,s,t,i} = \int_{0}^{\lambda_i} f_{e,v,s,t}(\lambda) d\lambda
      \]
      and the propagated uncertainties are given below.
      \[
      \delta G_{e,v,s,t,i} = \sqrt{ \sum_{k=1}^{N_{\text{states},e,v,s,t}} \left( \frac{\partial G_{e,v,s,t,i}}{\partial f_{e,v,s,t,k}} \delta f_{e,v,s,t,k} \right)^2 }
      \]
      If the &part;U/&part;&lambda; time-series are available,
      then edgembar will report the TI free energies
      using 3 models for f(&lambda;):
      linear interpolation (integration of this curve is the trapezoidal rule),
      cubic natural spline interpolation (zero 2nd derivative end-point conditions),
      and clamped cubic spline interpolation (zero 1st derivative end-point conditions).
      In most cases,the cubic natural spline will produce the most reliable results.
      There are some softcore functions, such as the SSC(2) smooth-step function,
      <sup>
	[
	<a href="https://doi.org/10.1021/acs.jctc.0c00237">Lee (2020)</a>
	]
      </sup>
      that formally possess zero derivatives at the end-point; in
      these situations, the clamped cubic spline is
      expected to be the most appropriate interpolating function.
      In principle, other integration strategies are possible,
      such as various Gaussian quadratures, that require sampling
      at particular values of &lambda;
      corresponding to the roots of an orthogonal polynomial.
      The integral is then given by the weighted sum of observed
      means.
      One can show that this is equivalent to implicitly
      fitting a set of orthogonal polynomials to the data and
      analytically integrating the polynomials.
      Edgembar does not support TI evaluation with Gaussian quadratures
      because it does not assume the user performed the simulations
      at the appropriate quadrature roots with the expected precision.
    </p>

    
    <h3 id="MBAR">Multistate Bennett Acceptance Ratio (MBAR) calculation of the state free energies within a trial</h3>
    <p>
      Simulation of the N<sub>states,e,v,s,t</sub> states
      from trial <i>t</i>, stage <i>s</i>, environment <i>v</i>,
      of edge <i>e</i>
      produces trajectories containing N<sub>samples,e,v,s,t,k</sub>
      samples, where <i>k</i> indexes the state.
      The aggregate number of samples
      from all states within a trial is
      N<sub>samples,e,v,s,t</sub> = &sum;<sub>k</sub> N<sub>samples,e,v,s,t,k</sub>.
      The potential energy of the trial's
      N<sub>states,e,v,s,t</sub> states
      is evaluated at each configuration
      resulting in a N<sub>samples,e,v,s,t</sub>&times;N<sub>states,e,v,s,t</sub>
      array of potential energies,
      U<sub>e,v,s</sub>(<b>r</b><sub>jk</sub><sup>(t)</sup>;&lambda;<sub>l</sub>),
      where <b>r</b><sub>jk</sub><sup>(t)</sup> is the 3N array
      of coordinates of sample <i>j</i> produced by state <i>k</i>
      in trial <i>t</i>.
      These potential energies can be used to solve the MBAR/UWHAM equations
      <sup>
	[
	<a href="https://doi.org/10.1063/1.2978177">Shirts (2008)</a>,
	<a href="https://doi.org/10.1063/1.3701175">Tan (2012)</a>,
	<a href="https://doi.org/10.1021/acs.jpclett.5b01771">Zhang (2015)</a>
	]
      </sup>
      for the N<sub>states,e,v,s,t</sub> state free energies,
      \[
      \{\mathbf{G}\}_{e,v,s,t} = \{G_{e,v,s,t,1},G_{e,v,s,t,2},\cdots,G_{e,v,s,t,N_{\text{states},e,v,s,t}}\}
      \]
      by minimizing a convex objective function,
      <sup>
	[
	<a href="https://doi.org/10.1063/1.3701175">Tan (2012)</a>,
	<a href="https://doi.org/10.1021/acs.jctc.8b01010">Ding (2019)</a>
	]
      </sup>
      \[
      \{\mathbf{G}\}^{*}_{e,v,s,t} = \underset{\mathbf{G}}{\text{arg min}}\;F_{e,v,s,t}(\mathbf{G})
      \]
      defined below,
      \[
      \begin{split}
      F_{e,v,s,t}(\mathbf{G})
      =&
      \frac{1}{N_{\text{samples},e,v,s,t}}
      \sum_{k=1}^{N_{\text{states},e,v,s,t}}
      \sum_{j=1}^{N_{\text{samples},e,v,s,t,k}}
      \text{ln}
      \left(
      \sum_{l=1}^{N_{\text{states},e,v,s,t}}
      \text{exp}[-(\beta U_{e,v,s}(\mathbf{r}_{jk}^{(t)};\lambda_l)+b_l(G_{e,v,s,t,l}))]
      \right)
      \\
      &
      +\sum_{k=1}^{N_{\text{states},e,v,s,t}} \frac{N_{\text{samples},e,v,s,t,k}}{N_{\text{samples},e,v,s,t}} b_k(G_{e,v,s,t,k})
      \end{split}
      \]
      where the asterisk denotes the set of state free energies that minimize
      the objective,
      &beta;=1/(k<sub>B</sub>T), and
      \[
      b_k(G_{e,v,s,t,k}) = -\text{ln}\frac{N_{\text{samples},e,v,s,t,k}}{N_{\text{samples},e,v,s,t}} - \beta G_{e,v,s,t,k}.
      \]
      

    </p>


    
    <h3 id="Constrained">Constrained optimization of an edge's relative free energy</h3>
    <p>
      The total number of states in an edge includes all states
      from each trial, stage, and environment.
      \[
      N_{\text{states},e} = \sum_{v=1}^{N_{\text{env},e}}
      \sum_{s=1}^{N_{\text{stage},e,v}}
      \sum_{t=1}^{N_{\text{trial},e,v,s}}
      N_{\text{states},e,v,s,t}
      \]
      The state free energies are an array {<b>G</b>}<sub>e</sub>
      of length N<sub>states,e</sub>, and their values
      can be obtained from solution of the MBAR/UWHAM
      equations.
      \[
      \{\mathbf{G}\}^{*}_{e} = \underset{\mathbf{G}}{\text{arg min}}\; F_e(\mathbf{G})
      \]
      
      \[
      F_{e}(\mathbf{G})
      =
      \sum_{v=1}^{N_{\text{env},e}}
      \sum_{s=1}^{N_{\text{stages},e,v}}
      \frac{1}{N_{\text{trials},e,v,s}}
      \sum_{t=1}^{N_{\text{trials},e,v,s}}
      F_{e,v,s,t}(\mathbf{G})
      \]

      The edge objective function is the sum of trial objective functions
      because the trial objective functions are completely independent
      of each other. In other words, the minimization of F<sub>e</sub>
      is exactly equivalent to having optimized each F<sub>e,v,s,t</sub>
      in isolation.
    </p>
    <p>
      The relative free energy of the edge
      can be written as a linear combination of the state free energies,
      
      \[
      \Delta\Delta G_e(\mathbf{G}) = \{\mathbf{c}\}_e^T\cdot\{\mathbf{G}\}_e
      \]
      
      where the coefficients are:
   
      \[
      c_{e,v,s,t,\lambda}
      =
      \begin{cases}
      +N_{\text{trials},e,v,s}^{-1} & \text{if $v$=tgt, $\lambda$=1} \\
      -N_{\text{trials},e,v,s}^{-1} & \text{if $v$=tgt, $\lambda$=0} \\
      -N_{\text{trials},e,v,s}^{-1} & \text{if $v$=ref, $\lambda$=1} \\
      +N_{\text{trials},e,v,s}^{-1} & \text{if $v$=ref, $\lambda$=0} \\
      0 & \text{otherwise}
      \end{cases}
      \]

      Minimization of F<sub>e</sub>, as described, produces an unconstrained solution for the state free energies, and the resulting edge free energy will be given a special name and symbol: the <i>unconstrained edge free energy</i>,  &Delta;&Delta;G<sub>e</sub><sup>*</sup> = &Delta;&Delta;G<sub>e</sub>(<b>G</b><sup>*</sup>).
      One can perform the minimization several times while
      constraining the value of
      &Delta;&Delta;G<sub>e</sub> to different values, <i>x</i>.
      \[
      \begin{split}
      \{\mathbf{G}\}^{*}_{e}(x)
      =&
      \underset{\mathbf{G}}{\text{arg min}}\; F_e(\mathbf{G})
      \\
      &
      \text{subject to}\;\;
      \Delta\Delta G_e(\mathbf{G}) - x = 0
      \end{split}
      \]

      <span id="EdgeObj"></span>
      The value of the edge objective function
      can then be modeled in the vicinity of
      &Delta;&Delta;G<sub>e</sub><sup>*</sup>.
      \[
      F_e(x) \equiv F_e( \{\mathbf{G}\}_e^{*}(x) )
      \]
      From practical experience, F<sub>e</sub>(x) is usually well-fit
      by a quadratic centered about x=&Delta;&Delta;G<sub>e</sub><sup>*</sup>.
      The edge objective function, F<sub>e</sub>(x), is used
      to perform networkwide free energy analysis, which provides
      a means for strictly enforcing cycle closure conditions.
      <sup>
	[
	<a href="https://doi.org/10.1021/acs.jctc.0c01219">Giese (2021)</a>
	]
      </sup>
    </p>
    
  </body>
</html>
