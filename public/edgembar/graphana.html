<html>
  <head>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script>
window.MathJax = {
  tex: {
    tags: 'ams'
  }
};
    </script>
    
    <style>
      ul {
          padding-left: 20px;
          list-style-position: outside;
      }
      li {
          padding-bottom: 0.5em;
      }
      td {
          border: none;
          border-bottom: dashed 3px black;
          padding: 0.5em;
      }
      td.imgcol {
          width: 570px;
          padding-left: 0px;
      }

    </style>
  </head>

  <body>

    <h3>Networkwide free energy analysis</h3>
    <p>
      <ul>
	<li>
	  <a href="#Cycle">Cycles and cycle closures</a>
	</li>
	<li>
	  <a href="#CFE">Constrained network solution for the ligand energies</a>
	</li>
	<li>
	  <a href="#Prop">The Lagrange multiplier index and other properties</a>
	</li>
	<li>
	  <a href="#Gen">Using edgembar-WriteGraphHtml.py to perform the analysis</a>
	</li>
	<li>
	  <a href="#Report">The HTML graph analysis report</a>
	</li>
      </ul>
    </p>

    
    <h3 id="Cycle">Cycles and cycle closures</h3>
    <p>
      A <a href="https://en.wikipedia.org/wiki/Graph_theory">graph</a>
      consists of nodes and edges.
      In the present context, the nodes represent the free energy
      of the ligands, and the
      edges are the free energies that transform between them.
      A sequence of adjacent edges is a path that connects two nodes,
      such that the relative free energy between two ligands
      is the sum of edge free energies along the path.
      A <a href="https://en.wikipedia.org/wiki/Cycle_(graph_theory)">cycle</a>
      is a closed path whose starting and ending nodes are the same.
      Because the free energy is a
      <a href="https://en.wikipedia.org/wiki/State_function">state function</a>,
      the sum of free energies along a path defining a cycle should be zero
      (the free energy difference between a ligand and itself is zero).
    </p>
    <p>
      When edgembar analyzes the edge free energy, it does so in isolation
      without knowledge of the other edges in the graph.
      These results shall be called "unconstrained free energies".
      When the graph contains cycles, the free energy difference between
      two ligands may be computed from multiple different pathways,
      and the sum of unconstrained free energies along these paths
      may produce inconsistent results.
      This phenomenon also exhibits itself by the presence of
      "cycle closure errors". A cycle closure error is the sum
      of unconstrained free energies along a closed path.
      Part of the network free energy analysis is to obtain a set
      of ligand free energies (node free energies) that produce path-independent
      free energy differences and which do not have any cycle closure errors.
      The set of ligand free energies that satisfy these properties
      shall be called the "constrained free energies".
      They are "constrained" in the sense that 
      the states corresponding to the node from every
      adjacent edge share a common free energy value.
    </p>
    
    
    <h3 id="CFE">Constrained network solution for the ligand energies</h3>
    <p>
      To ensure that the ligand free energy differences are
      path-independent, we seek a set of N<sub>node</sub> ligand free energies,
      &Delta;G<sub>1</sub>,
      &Delta;G<sub>2</sub>, &centerdot;&centerdot;&centerdot;,
      &Delta;G<sub>N<sub>node</sub></sub>,
      such that any difference
      &Delta;&Delta;G<sub>ab</sub>=&Delta;G<sub>b</sub>
      -&Delta;G<sub>a</sub>
      can be computed from the ligand values rather
      than the path-specific edge free energies.
      In this context, &Delta;G<sub>a</sub> is the difference
      in free energy of ligand <i>a</i> in two environments.
      One can arbitrarily select one node to be the zero of
      energy (e.g., &Delta;G<sub>1</sub> &equiv; 0),
      and we then seek to find the remaining N<sub>node</sub>-1
      ligand free energies.
      The ligand free energies are the set of values
      which minimize the graph objective function, F.
      The graph objective function is a sum of <a href="introduction.html#EdgeObj">edge objective functions</a>.
      \[
      F(\mathbf{x}) = \sum_{(ab)}^{N_{\text{edge}}} F_{(ab)}( x_b-x_a )
      \]
      The summation includes all edges in a graph, indexed by the
      pair of nodes (a,b) defining the transformation.
      x<sub>b</sub>-x<sub>a</sub> is the edge free energy, as computed
      from the ligand free energies.
      Values of F<sub>(ab)</sub>(&Delta;x) can be precomputed
      and fit to a polynomial.
      The number of precomputed values is controlled by the
      <a href="edgecalc.html#CXX">--ncon option
	of the edgembar command-line interface</a>,
      and the polynomial fits are shown in the
      <a href="edgeana.html#ObjFcn">edge HTML report</a>.
      If F<sub>ab</sub>(&Delta;x) is modeled by a cubic,
      \[
      F_{(ab)}(\Delta x) = c_{ab}^{(0)} + c_{ab}^{(1)}\Delta x + c_{ab}^{(2)}\Delta x^2 + c_{ab}^{(3)}\Delta x^3
      \]
      then minimization of the graph objective must be performed
      with a nonlinear optimization algorithm.
      The edge objective functions are often well-fit to
      a quadratic, however.
      \begin{equation}\label{E:quadfit}
      \begin{split}
      F_{(ab)}(\Delta x)
      =& c_{ab}^{(0)} + c_{ab}^{(1)}\Delta x + c_{ab}^{(2)}\Delta x^2 \\
      =& \frac{k_{ab}}{2}(x_b-x_a-g_{(ab)})^2
      \end{split}
      \end{equation}
      In which case, the optimal set of ligand free energies
      can be found from a linear algebraic solution.
      The second line of eq. \ref{E:quadfit} expresses the
      quadratic about the minimum $$g_{(ab)}=\Delta\Delta G_{(ab)}$$
      which is the relative free energy of the edge calculated
      in isolation.  The k<sub>ab</sub> value is a "force constant".
    </p>


    <p>
      Given a quadratic fit of the edge objective functions, the
      value of the graph objective can be expressed in matrix notation
      as follows,
      \[
      \begin{split}
      F(\mathbf{x})
      =&
      \sum_{(ab)}^{N_{\text{edge}}} \frac{k_{ab}}{2}(x_b-x_a-g_{(ab)})^2
      \\
      =&
      \frac{1}{2} \sum_{(ab)}^{N_{\text{edge}}} \left[ (x_b-x_a) k_{ab} (x_b-x_a) - 2 (x_b-x_a)k_{ab}g_{(ab)} + g_{(ab)}k_{ab}g_{(ab)} \right]
      \\
      =&
      \frac{1}{2}
      \sum_{(ab)}^{N_{\text{edge}}}
      \sum_{(cd)}^{N_{\text{edge}}}
      (x_b-x_a)
      (\delta_{ac}\delta_{bd} k_{ab})
      (x_c-x_d)
      \\ &
      -
      \sum_{(ab)}^{N_{\text{edge}}}
      \sum_{(cd)}^{N_{\text{edge}}}
      (x_b-x_a)
      (\delta_{ac}\delta_{bd} k_{ab})
      g_{(ab)}
      \\ &
      +
      \frac{1}{2}
      \sum_{(ab)}^{N_{\text{edge}}}
      \sum_{(cd)}^{N_{\text{edge}}}
      g_{(ab)}
      (\delta_{ac}\delta_{bd} k_{ab})
      g_{(ab)}
      \\
      =&
      \frac{1}{2}\;\mathbf{x}^T\cdot\mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{U}\cdot\mathbf{x}
      -
      \mathbf{x}^T\cdot\mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{g}
      +
      \frac{1}{2}\;\mathbf{g}^T\cdot\mathbf{K}\cdot\mathbf{g}
      \end{split}
      \]
    </p>
    where <b>U</b> is a  N<sub>edge</sub>&times;N<sub>node</sub> matrix that expresses edge free energies in terms of ligand free energies,
    \[
    U_{(ab),c} = \delta_{bc}-\delta_{ac}
    \]
    <b>g</b> is a N<sub>edge</sub>&times;1 array of isolated-edge free energies,
    <b>K</b> is a N<sub>edge</sub>&times;N<sub>edge</sub> array of
    force constants,
    \[
    K_{(ab),(cd)} = \delta_{ac}\delta_{bd} k_{ab}
    \]
    and <b>x</b> is the N<sub>node</sub>&times;1 array of
    ligand free energies to be determined.
    \[
    x_a = \Delta G_a
    \]
    
    <p>
      The ligand free energy values, <b>x</b> are given eq. \ref{E:x}.
      \begin{equation}\label{E:x}
         \mathbf{x} = \mathbf{M}^{-1}\cdot\mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{g}
      \end{equation}
      \begin{equation}
         \mathbf{M} = \mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{U}
      \end{equation}
    
    The edge free energies are differences between the relative ligand free energies
      obtained from eq.  \ref{E:x}.
      \begin{equation}\label{E:ddG}
      \Delta\Delta G_{ab} = x_b-x_a
      \end{equation}
      We refer to the edge free energies computed from eq. \ref{E:ddG} as "constrained edge free energies", whereas the isolated-edge free energies (the quadratic minimum) are "unconstrained edge free energies".  In this context, the constraint on the graph is that all edges adjacent to a node/ligand is calculated from the same ligand energy. In contrast, the isolated-edge free energies effectively produce a different ligand free energy from every adject edge. It is the lack of consistency in the isolated-edge ligand free energies that produces cycle closure errors, and it is the enforced consistency in eq. \ref{E:x} that causes all thermodynamic cycles to close.
    </p>
    <p>
      One can also use Lagrange's method of undetermined multipliers
      to enforce linear constraints on the ligand free energies.
      Consider constraints of the form:
      \[
      \mathbf{D}\cdot\mathbf{x} = \mathbf{v}
      \]
      where <b>v</b> is a N<sub>ref</sub>&times;1 array of reference edge free energy values
      that one wants to exactly reproduce,
      and <b>D</b> is a N<sub>ref</sub>&times;N<sub>node</sub> matrix that expresses
      the relevant edges in terms of the ligand free energies.
      \[
      D_{(ab),c} = \delta_{bc}-\delta_{ac}
      \]
      The ligand free energies are obtained by minimizing eq. \ref{E:cobj},
      where <b>l</b> is a N<sub>ref</sub>&times;1 array of Lagrange multipliers
      \begin{equation}\label{E:cobj}
      \frac{\partial}{\partial x_c}
      \left\{
      F(\mathbf{x})
      -
      \left(
      \mathbf{x}^T\cdot\mathbf{D}^T-\mathbf{v}^T
      \right)
      \cdot\mathbf{l}
      \right\} = 0
      \end{equation}
      The ligand free energies become:
      \begin{equation}
      \mathbf{x} = \mathbf{M}^{-1}\cdot\left(\mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{g}-\mathbf{D}^T\cdot\mathbf{l}\right)
      \end{equation}
      \begin{equation}
      \mathbf{l}=\left(\mathbf{D}\cdot\mathbf{M}^{-1}\cdot\mathbf{D}^T\right)^{-1}\cdot\left(\mathbf{D}\cdot\mathbf{M}^{-1}\cdot\mathbf{U}^T\cdot\mathbf{K}\cdot\mathbf{g}-\mathbf{v}\right)
      \end{equation}
    </p>

    <p>
      A summary of properties:
      <ul>
	<li>
	  <b>UFE</b>: The <a href="introduction.html#Edge">unconstrained free energy of an edge</a>. When referring to a cycle, the UFE is the sum of unconstrained/isolated edge free energies along the closed path.
	</li>
	<li>
	  <b>dUFE</b>: The <a href="introduction.html#Error">standard error estimate</a> of the UFE. When referring to a cycle, the dUFE is error propagated from the edges.
	</li>
	<li>
	  <b>CFE</b>: The <a href="#CFE">constrained free energy of a
	    ligand</a>. The constrained free energy of an edge is a
	  difference in the ligand free energies. The word "constrained"
	  refers to the idea that a node has the same ligand free energy
	  in every adjacent edge, which is not strictly true when the
	  edges are calculated in isolation.
	</li>
	<li>
	  <b>dCFE</b>: The standard error estimate of the CFE.
	  The estimate is the standard deviation of results
	  obtained from bootstrap optimizations of the graph objective.
	  The bootstrap trials differ by perturbing the position
	  of the polynomials modelling the edge objectives by
	  random amounts following a normal distribution whose
	  standard deviation is the standard error of the
	  unconstrained free energy. In other words, the "minimum"
	  of the edge objectives are shifted to mimic the uncertainty
	  of the minimum&apos;s position.
	</li>
	<li>
	  <b>|Shift|</b>: The absolute value of the difference between
	  the UFE and CFE values.
	</li>
	<li>
	  <b>CC</b>: The cycle closure error. This is the absolute value of the sum of unconstrained edge free energies along the closed path.
	</li>
	<li>
	  <b>AvgCC</b>: The average CC of all cycles traversing a particular node or edge.
	</li>
	<li>
	  <b>MaxCC</b>: The maximum CC of any cycle traversing a particular node or edge.
	</li>
	<li>
	  <b>LMI</b>: The Lagrange multiplier index is the absolute value of the Lagrange multiplier used in the network analysis to constrain an edge to its unconstrained value.
	</li>
	<li>
	  <b>AvgLMI</b>: The average LMI of all edges directly connected to a node.
	</li>
	<li>
	  <b>OFC2</b>: The second-order coefficient c<sup>(2)</sup> of the polynomial fit to the edge objective function. (Objective Force Constant times 2).
	</li>
	<li>
	  <b>ErrMsgs</b>: The number of error messages reported by the analysis of an edge.
	</li>
	<li>
	  <b>Outliers</b>: If more than 2 trials of a stage were performed, then the mean and median of the trials is evaluated, and if a trial free energy differs from both the mean and median by more than 2 kcal/mol, it is marked as an outlier.
	</li>
      </ul>
    </p>
    
    
    <h3 id="Gen">Using edgembar-WriteGraphHtml.py to perform the analysis</h3>
    <p>
      The edgembar-WriteGraphHtml.py performs network analysis and
      writes an HTML file.
      One must have have first run edgembar on each edge to produce
      a python script of edge results, including the evaluation
      of the edge objective function. The default behavior of
      edgembar is to evaluate the edge objective at 5 points:
      -2, -1, 0, +1, and +2 kcal/mol from the unconstrained solution. 
    </p>
    <p>
      In the simplest case, one can run <code>edgembar-WriteGraphHtml.py -o output.html a~b.py a~c.py b~c.py [...etc...]</code>, where the list of python scripts are the list of outputs generated by edgembar for each edge.
      Note that only single graphs are supported.
      If you have an edge, d~e, that is not connected in any way to
      the other ligands, then you effectively have two, unconnected
      graphs -- this is not allowed. You will need to run the graph
      analysis on each graph separately.
    </p>
    <p>
      The graph script supports many options, use the "--help" option
      for a complete list.  Note that one can make comparison
      to experimental/reference free energies by
      <a href="example.html#Expt">using the -x option</a>, which
      reads a reference free energy of each ligand from a file.
    </p>
    
    <h3 id="Report">The HTML graph analysis report</h3>
    <table>
      <tr>
	<td class="imgcol"><img src="imgs/graph.png"></td>
	<td>
	  <p>
	    The top of the analysis displays the graph structure.
	    You can manipulate the positioning by clicking on a node and moving
	    it with the mouse.  Double-clicking an edge with open the edge
	    analysis report in a new tab or window.
	    The radio buttons on the right allow you to color the nodes and
	    edges based on the value of various node and edge properties.
	    The width/size of each edge can also be adjusted based on property.
	  </p>
	  <p>
	    Immediately below the graph is a selection area that allows you
	    to display a table of node, edge, or cycle properties.
	    All energies are in kcal/mol units.
	  </p>
	</td>
      </tr>

      <tr>
	<td class="imgcol"><img src="imgs/nodetable.png"></td>
	<td>
	  The table of node properties contains the constrained free energy
	  of each node (the ligand free energy), the standard error of
	  the free energy, the Lagrange multiplier index, and the
	  average cycle closure error of all cycles passing through the edge.
	  If experimental/reference free energy values were read from a file,
	  then they will appear as an additional column (Expt.), and
	  CFE-Expt. is the difference between the ligand free energies
	  and the reference values.
	  Clicking the header of any column will sort the rows based on
	  the value of the column (except for rows that have missing
	  data).
	  Clicking on any row will highlight the node in the graph display.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol"><img src="imgs/edgetable.png"></td>
	<td>
	  The table of edge properties includes all edges
	  within the graph. The UFE values are the unconstrained edge
	  free energies (the free energies obtained from MBAR analysis
	  of the trials), and the dUFE values are the error estimates.
	  If &part;U/&part;&lambda; data was available for all trials,
	  then the (unconstrained) thermodynamic integration
	  free energies are displayed. TI, TI3n, and TI3c are
	  the results from linear interpolation (trapezoidal rule),
	  cubic natural spline integration, and cubic clamped spline
	  integration, respectively.
	  Clicking the header of any column will sort the rows based on
	  the value of the column (except for rows that have missing
	  data).
	  Clicking on any row will highlight the edge in the graph display.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol"><img src="imgs/cyctable.png"></td>
	<td>
	  This table displays the properties of every cycle in
	  the graph. The "length" of a cycle is the number
	  of edges along the closed path.
	  If &part;U/&part;&lambda; data was available for all trials,
	  then CC(TI), CC(TI3n), and CC(TI3c) are the cycle closure
	  errors based on the thermodynamic integration free energy
	  results.
	  The summary statistics at the bottom of each table
	  include the number of data values, the mean of the values,
	  the mean of the absolute values, the standard deviation,
	  and the minimum and maximum values in the column.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol"><img src="imgs/regression.png"></td>
	<td>
	  The bottom of the page contains node and edge
	  regression statistics.
	  On the left, you can select individual node
	  properties to be plotted on the x- and y-axis.
	  The right side similarly allows comparison between
	  2 edge properties.
	  The solid lines are the linear regressions to
	  the data.
	  A summary of the statistics is shown below the plots.
	  R is the Pearson correlation,
	  MAD is the mean absolute deviation,
	  MSD is the mean signed deviation,
	  RMSD is the root mean squared deviation,
	  and m and b are the slope and intercept
	  of the regression line.
	  If you click on a data point, the
	  graph display will highlight the appropriate
	  node or edge.
	  Similarly, the row in the data table will
	  be highlighted yellow.
	</td>
      </tr>

      
    </table>
    
  </body>
</html>
