<html>
  <head>
    <style>
      ul {
	  padding-left: 20px;
	  list-style-position: outside;
      }
      li {
	  padding-bottom: 0.5em;
      }
    </style>
  </head>
  <body>

    <h3>Calculating the free energy of an edge</h3>

    <p>
      <ul>
	
	<li>
	  <a href="#DirOrg">Directory organization</a>
	  <br>
	  The organization of your simulations into a hierarchy
	  of <a href="introduction.html#State">states</a>,
	  <a href="introduction.html#Trial">trials</a>,
	  <a href="introduction.html#Stage">stages</a>,
	  <a href="introduction.html#Env">environments</a>,
	  and <a href="introduction.html#Edge">edge</a>
	  is described.
	</li>

	
	<li>
	  <a href="#RawData">The raw data files of a single trial</a>
	  <br>
	  A description of the raw data files required to perform
	  edge free energy analysis. Edgembar is program agnostic; that is,
	  it is not an Amber-specific analysis tool.
	</li>

	
	<li>
	  <a href="#amber2dats">Generating the raw data files
	    from Amber mdout files</a>
	  <br>
	  A description of the edgembar-amber2dats.py script which
	  generates the raw data from Amber mdout files.
	  This is provided as a convenience; its usage is not mandatory.
	</li>

    
	<li>
	  <a href="#XML">The XML input file</a>
	  <br>
	  The edgembar c++ program reads a XML input file
	  that describes the simulation data as a hierarchy of 
	  <a href="introduction.html#State">states</a>,
	  <a href="introduction.html#Trial">trials</a>,
	  <a href="introduction.html#Stage">stages</a>,
	  <a href="introduction.html#Env">environments</a>,
	  and <a href="introduction.html#Edge">edge</a>.
	</li>
	
	<li>
	  <a href="#Discover">Generating XML input files with supplied python utilities</a>
	  <br>
	  An edgembar python library is provided, including functionality
	  to ease the process of creating XML input files.
	  This functionality is described.
	</li>
	
    
	<li>
	  <a href="#CXX">Running the edgembar c++ program</a>
	  <br>
	  Overview of the edgembar c++ command line options.
	</li>
      </ul>
    </p>

    <h3 id="DirOrg">Directory organization</h3>
    <p>
      Edgembar does not make any assumptions about how
      you organize your directories, but however you choose
      to organize them, the structure must generally be capable
      of distinguishing between:
      <a href="introduction.html#State">states</a>,
      <a href="introduction.html#Trial">trials</a>,
      <a href="introduction.html#Stage">stages</a>,
      <a href="introduction.html#Env">environments</a>,
      and <a href="introduction.html#Edge">edge</a>.
    </p>
    <p>
      Here is an example of a 3-stage protocol.
    </p>
      <pre><code>
./a~b/com/decharge/t01/
./a~b/com/decharge/t02/
./a~b/com/vdw/t01/
./a~b/com/vdw/t02/
./a~b/com/recharge/t01/
./a~b/com/recharge/t02/
./a~b/aq/decharge/t01/
./a~b/aq/decharge/t02/
./a~b/aq/vdw/t01/
./a~b/aq/vdw/t02/
./a~c/aq/recharge/t01/
./a~c/com/decharge/t01/
./a~c/com/vdw/t01/
./a~c/com/recharge/t01/
./a~c/aq/decharge/t01/
./a~c/aq/vdw/t01/
./a~c/aq/recharge/t01/
      </code></pre>
    <p>
      The example consists of 2 edges: the transformation
      of ligand <i>a</i> to <i>b</i>, and the transformation
      of ligand <i>a</i> to <i>c</i>.
      The ligands do not have to be labeled "a", "b", and "c";
      these are just generic names for the purposes of example.
      A ligand name can be any string that contains only
      alpha-numeric characters (underscores are also allowed);
      however, most special characters are not allowed,
      like tilde (which is a special character used to separate
      two ligands that define an edge), spaces, nor forward and
      backslashes, which are easily confused with directory names.
      The edgembar python library uses the format
      <i>ligand1</i>~<i>ligand2</i> to define an edge that
      transforms ligand 1 to ligand 2.
      The directory structure does not need to use this convention,
      but I recommend it.
    </p>
    <p>
      The example directory structure is setup to evaluate
      the transformations in two environments, called "com"
      and "aq".
      The environments do not need to be called "com" and "aq";
      these are just generic names for the purpose of providing
      and example.
      The only point is: there are two environments in this
      example, and the directory structure is setup to distinguish
      between them.
      Considering that the HTML report will decompose the
      free energy and refer to components as "com" and "aq",
      I recommend using these names in your directory structure
      even though it's not a requirement.
    </p>
    <p>
      The example directory structure is setup to evaluate
      each transformation in 3 stages: "decharge", "vdw", and "recharge".
      Any string would suffice to describe the stages;
      they could be called "deq", "vdw", req", for example.
      You may not even have 3 stages.
      When the HTML report is generated, only the first 3 characters
      of the stage name are printed.
    </p>
    <p>
      The example directory structure is setup to perform
      2 trials of each a~b stage and only 1 trial for each
      a~c stage. Trial names can be any string; they do not need
      to be integers.
    </p>
    <p>
      Here is an example of a 1-stage protocol for a single edge.
    </p>
      <pre><code>
./a~b/com/t01/
./a~b/com/t02/
./a~b/aq/t01/
./a~b/aq/t02/
      </code></pre>
    <p>
      There is only 1 edge (a transforms to b),
      two environments (com and eq), and two
      independent sets of simulations are performed (t01 and t02).
      In this case, it is not necessary to distinguish
      between stages because there's only 1 stage in each
      environment.
      Similarly, if there was only 1 environment, then
      we would not need to distinguish between environments.
      If there was only 1 trial, we would not need to distinguish
      between trials.
    </p>
    <p>
      The hierarchical ordering does not need to be
      <i>edge/env/stage/trial</i>.
      One could setup their directories in any order; e.g.,
      <i>edge/trial/env/stage</i>.
      However, considering that a stage is a trial-averaged
      quantity, I recommend using a <i>edge/env/stage/trial</i>
      ordering.
    </p>
    
      
	
    <h3 id="RawData">The raw data files of a single trial</h3>
    <p>
      The directory (or subdirectory) of a trial
      must contain a series of "raw data" files.
      These files must be named "efep_<i>tlam</i>_<i>elam</i>.dat",
      where <i>tlam</i> is a string used to denote the
      state which produced the ensemble, and <i>elam</i> is
      a string used to denote the state which produced the
      potential energy within the file.
      (Note: "tlam" is the <i>trajectory</i> lambda,
      and "elam" is the <i>energy</i> lambda).
      The contents of each file is plain text containing
      two columns of numbers. The first column lists
      the simulation time (the units of time do not matter),
      and the second column of numbers is the potential energy
      (in kcal/mol)
      of state <i>elam</i>.
      Each row of the file corresponds to a sample
      generated from simulation of state <i>tlam</i>.
      To perform MBAR analysis, the trial directory
      must contain N<sub>&lambda;</sub>&times;N<sub>&lambda;</sub>
      efep files corresponding to each combination of
      <i>tlam</i> and <i>elam</i> values.
      If one wants to perform BAR analysis (rather than MBAR),
      then not all of the efep files are necessary.
      Instead, only the 3N<sub>&lambda;</sub>-2
      efep files corresponding to the tridiagonal
      of nearest neighbor combinations need be present.
    </p>

    <p>
      If you want to compare the results to TI analysis,
      then additional files of &part;U/&part;&lambda; time-series
      must be present.
      These files must be named "dvdl_<i>tlam</i>.dat",
      and they must contain two columns of numbers
      corresponding to the simulation time and the
      value of &part;U/&part;&lambda; (in kcal/mol) from the
      simulation performed with state <i>tlam</i>.
      If the dvdl files are present, then
      <i>tlam</i> and <i>elam</i> must be numerical
      values ranging from 0 to 1, because the TI
      free energy is evaluated from an integral
      from 0 to 1, and it must know the &lambda;
      values of each average.
    </p>

    <p>
      The following example lists the efep and dvdl
      files for trial "t1" of the "decharge" stage
      used to transform ligand
      1oiu to ligand 26 while complexed to the CDK2 protein.
      This trial involved 5 states (0 to 1 in increments of 0.25).
      My preference is to store the raw data files in a
      "dats/" subdirectory to isolate them from the
      MD program's input and output files,
      although this is not a requirement.
    </p>
    
    <pre><code style="font-size: 0.8em;">
[user@computer data]$ ls cdk2/1oiu~26/complex/decharge/t1/dats/*.dat
cdk2/1oiu~26/complex/decharge/t1/dats/dvdl_0.00000000.dat             cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.50000000_0.00000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/dvdl_0.25000000.dat             cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.50000000_0.25000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/dvdl_0.50000000.dat             cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.50000000_0.50000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/dvdl_0.75000000.dat             cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.50000000_0.75000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/dvdl_1.00000000.dat             cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.50000000_1.00000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.00000000_0.00000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.75000000_0.00000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.00000000_0.25000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.75000000_0.25000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.00000000_0.50000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.75000000_0.50000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.00000000_0.75000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.75000000_0.75000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.00000000_1.00000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.75000000_1.00000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.25000000_0.00000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_1.00000000_0.00000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.25000000_0.25000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_1.00000000_0.25000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.25000000_0.50000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_1.00000000_0.50000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.25000000_0.75000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_1.00000000_0.75000000.dat
cdk2/1oiu~26/complex/decharge/t1/dats/efep_0.25000000_1.00000000.dat  cdk2/1oiu~26/complex/decharge/t1/dats/efep_1.00000000_1.00000000.dat
    </pre></code>

	

    <h3 id="amber2dats">Generating the raw data files
      from Amber mdout files</h3>
    <p>
      If you perform alchemical free energy simulations with Amber's PMEMD
      program, then you can use the edgembar-amber2dats.py python script
      included with edgembar to construct the efep and dvdl files from
      the mdout files.
      Suppose the simulation of &lambda;=0.0 produced an output file
      named ./prod_0.00000000.mdout.
      You can write the
      ./dats/efep_0.00000000_*.dat and
      ./dats/dvdl_0.00000000.dat files by running:
      <pre><code>edgembar-amber2dats.py -o ./dats ./prod_0.00000000.mdout</code></pre>
      The python script will read the &amp;cntrl; section of the mdout
      file to determine the &lambda;-schedule.
      The python script can take multiple mdout files as command
      line arguments, extracting the raw data from each file in a loop;
      therefore, one can extract all dvdl and efep files from all
      simulations with
      <pre><code>edgembar-amber2dats.py -o ./dats $(ls ./prod_*.mdout)</code></pre>
      Note that the default behavior of edgembar-amber2dats.py is to
      only extract up to 10000 samples from each mdout file. If the
      mdout file contains more than 10000 samples, then a subset of
      samples is selected by choosing an appropriate stride.
      You can adjust this limitation with the <code>--nmax</code> option; e.g.,
      <code>--nmax=999999</code>.
      Furthermore, there is an <code>--exclude</code> option that will
      throw away samples deemed to be suspicious. A description of how
      it identifies suspicious samples is provided in the <code>--help</code>
      output.
    </p>


    <h3 id="XML">The XML input file</h3>
    <p>
      An XML input file describes 1 edge.
      It has the template format shown below.
      <pre><code>&lt;?xml version="1.0" ?&gt;
&lt;edge name="LigName1~LigName2"&gt;
    &lt;env name="EnvName"&gt;
        &lt;stage name="StageName"&gt;
            &lt;trial name="TrialName"&gt;
	        &lt;shift&gt;DeltaG&lt;/shift&gt;
                &lt;dir&gt;path/to/rawdata&lt;/dir&gt;
                &lt;ene&gt;Lam0StateName&lt;/ene&gt;
                &lt;ene&gt;OtherStateName&lt;/ene&gt;
                &lt;ene&gt;Lam1StateName&lt;/ene&gt;
            &lt;/trial&gt;
        &lt;/stage&gt;
    &lt;/env&gt;
&lt;/edge&gt;
      </code></pre>
      <ul>
	<li>
	  <i>LigName1</i> and <i>LigName2</i> should be the names of the
	  ligands.
	  Ligand names make be any sequence of alphanumeric characters
	  (underscores are also allowed).
	</li>
	<li>
	  <i>EnvName</i> must be either "target" or "reference"
	  regardless of how you organized your directory structure.
	  If there are two environments, then the &lt;edge&gt;
	  node will contain two child &lt;env&gt; nodes.
	</li>
	<li>
	  <i>TrialName</i> can be any string.
	</li>
	<li>
	  The &lt;trial&gt; node must contain a &lt;dir&gt; child.
	  The text of the &lt;dir&gt; node is the path (either
	  absolute or relative path) to the location containing
	  the files of raw data.
	</li>
	<li>
	  The &lt;trial&gt; node must contain a &lt;ene&gt; child node
	  for each state.  The text of the &lt;ene&gt; is a string;
	  it is the name of the state.
	  Given the directory location and state names, edgembar
	  will attempt to read the raw data files named
	  <i>dir</i>/efep_<i>TrajStateName</i>_<i>EneStateName</i>.dat.
	  If TI analysis is desired, then edgembar will also
	  read the files named <i>dir</i>/dvdl_<i>StateName</i>.dat,
	  and the state names must be floating point numbers between
	  0 and 1.
	</li>
	<li>
	  The &lt;shift&gt; node is optional. The <i>DeltaG</i>
	  values is a floating point number representing a
	  correction (in kcal/mol) to be added onto the calculated
	  free energy, such that the net free energy of the trial
	  is: G(&lambda;=1)-G(&lambda;=0) + DeltaG.
	  The default value of DeltaE is 0.0 kcal/mol.
	  One would normally only use this option if one is
	  applying an analytic free energy correction from
	  the use of Boresch restraints, for example.
	</li>
	<li>
	  The ordering of the &lt;ene&gt; nodes is significant.
	  When a &Delta;G is calculated for the trial, it
	  is computed as the difference in free energies between
	  the last &lt;ene&gt; node and the first &lt;ene&gt; node.
	  Therefore, it is possible to reverse the sign of the
	  trial&apos;s &Delta;G by reversing the ordering of the
	  &lt;ene&gt; children.
	  Similarly, if TI analysis is desired and the &lambda;
	  of the last state is less than the first state, then
	  the sign of the &part;U/&part;&lambda; values are reversed
	  before computing the trial&apos;s free energy.
	</li>
      </ul>
    </p>
    <p>
      Below is an example of the transformation from ligand 1oiu to ligand 26
      complexed to the CDK2 protein. The transformation consists of a single
      trial of a single stage. The trial contains 5 states.
      <pre><code>&lt;?xml version="1.0" ?&gt;
&lt;edge name="1oiu~26"&gt;
    &lt;env name="target"&gt;
        &lt;stage name="decharge"&gt;
            &lt;trial name="t1"&gt;
                &lt;dir&gt;cdk2/1oiu~26/complex/decharge/t1/dats&lt;/dir&gt;
                &lt;ene&gt;0.00000000&lt;/ene&gt;
                &lt;ene&gt;0.25000000&lt;/ene&gt;
                &lt;ene&gt;0.50000000&lt;/ene&gt;
                &lt;ene&gt;0.75000000&lt;/ene&gt;
                &lt;ene&gt;1.00000000&lt;/ene&gt;
            &lt;/trial&gt;
        &lt;/stage&gt;
    &lt;/env&gt;
&lt;/edge&gt;
</code></pre>

    </p>

    
    <h3 id="Discover">Generating XML input files with supplied python utilities</h3>
    <p>
      The edgembar python library contains a utility function called
      "DiscoverEdges" that allow you to easily construct the XML input
      files. One provides the function a python f-string (format string)
      acting as a template of your directory structure.
      From that template, the function traverses the file system to
      find the raw data files. Based on this search, it returns
      a python object (an instance of the "Edge" class), which
      is written as an XML file.
    </p>
    <p>
      As an example, consider the following directory structure,
      where the dats/ folders contain the raw data files of each
      trial.
      <pre><code>./cdk2/1oiu~1h1q/complex/decharge/t1/dats   ./cdk2/1oiu~26/solvated/decharge/t1/dats
./cdk2/1oiu~1h1q/complex/decharge/t2/dats   ./cdk2/1oiu~26/solvated/decharge/t2/dats
./cdk2/1oiu~1h1q/complex/recharge/t1/dats   ./cdk2/1oiu~26/solvated/recharge/t1/dats
./cdk2/1oiu~1h1q/complex/recharge/t2/dats   ./cdk2/1oiu~26/solvated/recharge/t2/dats
./cdk2/1oiu~1h1q/complex/vdw/t1/dats        ./cdk2/1oiu~26/solvated/vdw/t1/dats
./cdk2/1oiu~1h1q/complex/vdw/t2/dats        ./cdk2/1oiu~26/solvated/vdw/t2/dats
./cdk2/1oiu~1h1q/solvated/decharge/t1/dats  ./cdk2/26~1h1q/complex/decharge/t1/dats
./cdk2/1oiu~1h1q/solvated/decharge/t2/dats  ./cdk2/26~1h1q/complex/decharge/t2/dats
./cdk2/1oiu~1h1q/solvated/recharge/t1/dats  ./cdk2/26~1h1q/complex/recharge/t1/dats
./cdk2/1oiu~1h1q/solvated/recharge/t2/dats  ./cdk2/26~1h1q/complex/recharge/t2/dats
./cdk2/1oiu~1h1q/solvated/vdw/t1/dats       ./cdk2/26~1h1q/complex/vdw/t1/dats
./cdk2/1oiu~1h1q/solvated/vdw/t2/dats       ./cdk2/26~1h1q/complex/vdw/t2/dats
./cdk2/1oiu~26/complex/decharge/t1/dats     ./cdk2/26~1h1q/solvated/decharge/t1/dats
./cdk2/1oiu~26/complex/decharge/t2/dats     ./cdk2/26~1h1q/solvated/decharge/t2/dats
./cdk2/1oiu~26/complex/recharge/t1/dats     ./cdk2/26~1h1q/solvated/recharge/t1/dats
./cdk2/1oiu~26/complex/recharge/t2/dats     ./cdk2/26~1h1q/solvated/recharge/t2/dats
./cdk2/1oiu~26/complex/vdw/t1/dats          ./cdk2/26~1h1q/solvated/vdw/t1/dats
./cdk2/1oiu~26/complex/vdw/t2/dats          ./cdk2/26~1h1q/solvated/vdw/t2/dats</pre></code>
      
      One produces the following XML files
      <pre><code>[user@computer] ls xml/*.xml
1oiu~1h1q.xml  1oiu~26.xml  26~1h1q.xml</pre></code>
      by running the following script.
      
    <pre><code>#!/usr/bin/env python3
import edgembar,os
from pathlib import Path

odir = Path("xml")
if not odir.is_dir():
    os.makedirs(odir)
    
s = r"cdk2/{edge}/{env}/{stage}/{trial}/dats/efep_{traj}_{ene}.dat"
edges = edgembar.DiscoverEdges(s,target="complex",reference="solvated")

for edge in edges:
    fname = odir / (edge.name + ".xml")
    edge.WriteXml( fname )
    </pre></code>
    </p>
    <p>
      The key to this example is the definition of the template string, s.
      The template string contains several placeholders: {edge}, {env}, {stage}, {trial}, {traj}, and {ene}.
      Only the {edge}, {traj}, and {ene} placeholders are absolutely required.
    </p>
    <p>
      Recall that the XML format only accepts environments named
      "target" or "reference" (even though you can name your directories
      whatever you want).
      The DiscoverEdges function will correctly identify the environment
      if the placeholder matches any of the following "target",
      "tgt", "reference", or "ref". If the directory
      names did not use any of these variations, then you can
      invoke the DiscoverEdges function with two optional arguments
      (target=<i>TgtDirString</i>,reference=<i>RefDirString</i>)
      to specify the directory names of each environment.
    </p>
    <p>
      If the template string does not contain an {env} placeholder, then
      it assumes the transformation is within a single "target" environment.
    </p>
    <p>
      If the template string does not contain a {stage} placeholder, then
      it assumes the transformation is computed from a single stage
      named "STAGE".
    </p>
    <p>
      If the template string does not contain a {trial} placeholder, then
      it assumes the stage(s) are evaluated from a single trial named "1".
    </p>
    <p>
      The DiscoverEdges function accepts an optional list of trials
      to exclude. For example, calling:
      <pre><code>edges = edgembar.DiscoverEdges(s,exclude_trials=["t2"],target="complex",reference="solvated")</code></pre>
      will produce XML files that exclude trial "t2" from each stage.
    </p>
    <p>
      In the event that you need to reverse the order of the schedule
      used to evaluate a particular stage&apos, you can
      add the following snippet to the script after the call to DiscoverEdges
      and replace "STAGE" with the appropriate stage name.
      <pre><code>for edge in edges:
    for trial in edge.GetAllTrials():
        if trial.stage.name == "STAGE":
            trial.reverse()</code></pre>

    <h3 id="CXX">Running the edgembar c++ program</h3>
    <p>
      The c++ program can be compiled for serial (edgembar) or
      OpenMP parallel (edgembar_omp) execution.
      To control the number of threads, set the OMP_NUM_THREADS
      environmental variable. For example, to run with 8 threads,
      <code>OMP_NUM_THREADS=8 edgembar_omp [options] xml</code>,
      where "xml" is an xml file name.
      Run with "-h" or "--help" to see a full list of options.
      The default values of the optional parameters should suffice in
      most situations; we will only discuss a few options that
      control the analysis to be performed.
    </p>
    <p>
      <ul>
	<li>
	  edgembar reads an xml input file, performs analysis, and
	  writes the output to a python file.
	  By default, if the input xml file is <i>prefix</i>.xml,
	  then the output will be <i>prefix</i>.py.
	  You can use the --out option to manually choose the name
	  of the output file.
	</li>
	<li>
	  The default temperature is 298 K. Use the --temp option
	  appropriately.
	</li>
	<li>
	  By default, edgembar does not analyze the full set of
	  data listed in the efep files.
	  Instead, it applies an algorithm to automatically determine
	  the "production region" of each trajectory; that is,
	  it determines the portion of each trajectory to be
	  excluded as equilibration.
	  If you do not want to apply this procedure to the
	  data, then use the "--no-auto" option.
	</li>
	<li>
	  By default, edgembar will read every line from the raw
	  data files.  You can use the "--fstart", "--fstop", and
	  "--stride" options to read a subset of each data file.
	  fstart and fstop are fractions; all lines from the files
	  are read when --fstart=0.0 and --fstop=1.0.
	  The --stride option is an integer; --stride=1 means
	  to read every consecutive line, whereas --stride=2
	  would read every-other-line.
	</li>
	<li>
	  The current version of edgembar is not optimized to
	  reduce global memory consumption.
	  (This is something I can look at changing in the future
	  if it is actually a problem.)
	  Edgembar internally schedules a series of calculations
	  to be performed, makes copies of all data for each calculation,
	  and then loops over the calculations either in serial or
	  parallel. Therefore, if you saved an excessive number of
	  samples, then you run the risk of running out of memory,
	  especially if your options create large schedules of
	  calculations.
	  To reduce the memory requirements, you should use
	  the --fstart and --stride options
	  so that each simulation contributes 1000 to 5000
	  samples. It is not a good idea to supply it with 60,000
	  samples per trajectory, for example.
	  It is often better to run multiple, short
	  simulations than a few, long simulations.
	</li>
	<li>
	  By default, edgembar will inspect the collection
	  of raw data files. If all of the files necessary
	  to perform MBAR are available, then MBAR is performed.
	  If some files are missing, but it contains the
	  information necessary to perform BAR, then BAR is
	  performed.
	  There are other combinations of MBAR, BAR, and exponential
	  averaging that can be used, depending on the
	  availability of data.
	  If you want to manually enforce a particular
	  calculation mode, use the "--mode" option.
	</li>
	<li>Options which control the scheduling of calculations
	  <ul>
	    <li>
	      --ncon controls the number of constrained
	      objective function solutions to be found.
	      These calculations precompute the results
	      of the edge for later use when performing
	      networkwide free energy analysis.
	      If you do not intend to perform networkwide
	      free energy analysis, then set --ncon=0.
	      The default value is 2, which schedules
	      5 calculations (the unconstrained solution
	      and 2 calculations perturbed on
	      either side of the unconstrained result.)
	    </li>
	    <li>
	      The value of --ntimes does not, by itself,
	      schedule calculations, but if time-series
	      analysis is requested (with --halves and/or --fwdrev),
	      then the value of --ntimes controls the number
	      of additional calculations to be scheduled.
	      Specifically, the value of --ntimes creates a
	      uniform time-series whose values are the fraction
	      of a simulation (from 0 to 1).
	    </li>
	    <li>
	      If --halves is present, then 
	      --ntimes * 2 additional calculations are
	      scheduled for inclusion.
	      The value of ntimes defines a uniform
	      time-series
	      with values t<sub>i</sub> = i/ntimes, where i=[0,ntimes-1].
	      The samples up to time t<sub>i</sub> are
	      excluded as "proposed equilibration"
	      and the remaining samples are split into two
	      halves: a first half and a last half.
	      The free energy of each half is calculated
	      to answer the question: "Does the first
	      half of the simulation produce the same results
	      as the last half of the simulation if X%
	      of the trajectory is discarded as equilibration?"
	    </li>
	    <li>
	      If --fwdrev is present, then
	      --ntimes * 2 additional calculations are
	      scheduled for inclusion.
	      The value of ntimes creates a uniform
	      time-series
	      with values t<sub>i</sub> = (i+1)/ntimes, where i=[0,ntimes-1].
	      The "forward analysis" computes the free energy
	      using all samples from the beginning to t<sub>i</sub>.
	      The "reverse analysis" computes the free energy
	      using all samples from (1-t<sub>i</sub>) to the
	      end of the simulation.
	      Together, these results are compared to answer the
	      question: "Do the first X% of the simulations
	      reproduce the same results as the last X% of
	      the simulations?"
	    </li>
	    <li>
	      --nboot controls the number of bootstrap
	      calculations to obtain error estimates.
	      This applies to all calculations other
	      than the constrained optimizations.
	      For example, suppose one used the options:
	      <code>--ncon=2 --nboot=20 --ntimes=4 --halves --fwdrev</code>,
	      how many optimizations does edgembar perform?
	      <p>
		Answer:
		<br>
		(nboot+1)+(2*ncon+1)+(nboot+1)*(ntimes*2)+(nboot+1)*(ntimes*2) = 362
	      </p>
	      <p>
		It first performs an optimization of the production region
		and nboot=20 bootstrap optimizations to estimate errors.
		It then performs (2*ncon+1)=5 optimizations to generate a
		spline of the constrained edge objective function.
		The first/last-half time-series analysis is performed and each
		result is bootstrapped, requiring a total of
		(nboot+1)*(ntimes*2)=168 optimizations.
		The forward/reverse time-series analysis requires 
		similar effort.
		From this you can see that the time-series analysis
		is quite expensive, which is why it is not performed
		by default (you manually have to include the --halves
		and --fwdrev flags to perform it).
	      </p>
	    </li>
	  </ul>
	</li>
      </ul>
    </p>
	

  </body>
</html>
