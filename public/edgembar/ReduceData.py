#!/usr/bin/env python3
import numpy as np
import sys

for f in sys.argv[1:]:
    data = np.loadtxt(f)[::20,:]
    fh=open(f,"w")
    for row in data:
        fh.write("%.3f %.6f\n"%(row[0],row[1]))
    fh.close()

    
