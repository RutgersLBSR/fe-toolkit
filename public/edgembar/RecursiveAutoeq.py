#!/usr/bin/env python3
import math
import numpy as np

recursionPercent = 0.129449436703875


def GetTrialEquilPeriods(percent):
    eqs = []
    eq_length = 0
    prod_length = 1
    for i in range(100):
        if eq_length < 0.75:
            eqs.append(eq_length)
        # the production region is this fraction
        prod_length = (1-eq_length)
        # increase the equilibration length
        eq_length += prod_length * percent
        if eq_length > 0.75:
            eqs.append(eq_length)
            break
    return eqs

eqs = np.array(GetTrialEquilPeriods(recursionPercent))
for i,e in enumerate( eqs ):
    print("%.14f %i"%(e,0))


   
#x=(1-(1-(1-x)*p)*p)*p
    
#for i in range(1,12):
#    print(i,math.pow(0.75,1./i))
