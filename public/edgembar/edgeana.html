<html>
  <head>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    <style>
      ul {
          padding-left: 20px;
          list-style-position: outside;
      }
      li {
          padding-bottom: 0.5em;
      }
      td {
	  border: none;
	  border-bottom: dashed 3px black;
	  padding: 0.5em;
      }
      td.imgcol {
	  width: 431px;
	  padding-left: 0px;
      }

    </style>
  </head>
  <body>
    <h3>Understanding the edge free energy HTML report</h3>
    <p>
      The output of edgembar is a python script.
      Running the output through the python interpreter
      produces an HTML file summarizing the results
      of the edge.
      The name of the HTML file is the same as the output
      python script upon replacing the ".py"
      extension with ".html".
      The following is a discussion of the various
      sections of the HTML report.
    </p>
    <table>
      <tr>
	<td class="imgcol">
	  <img src="imgs/errors.png" />
	</td>
	<td>
	  <p>
	  The top of the file lists the name of the edge,
	  the version of edgembar that was used to generate the results,
	  the date and time it was run,
	  and the command-line arguments that were used.
	  </p>
	  <p>
	    This is followed by a list of issues that were
	    identified by the analysis that require your
	    attention. Because a simulation may have multiple
	    problems, the issues are prioritized, and only
	    the highest priority issue is included in the summary.
	  </p>
	</td>
      </tr>
      
      <tr id="ObjFcn">
	<td class="imgcol">
	  <img src="imgs/objfcn.png" />
	</td>
	<td>
	  These are values of the <a href="introduction.html#Constrained">
          MBAR objective function</a>. The minimum
	  is the value of the unconstrained solution, and the other
	  values are solutions which constrain the &Delta;&Delta;G
	  by some other amount. This objective function is used
	  to perform networkwide free energy analysis.
	  The function should look similar to a quadratic; it is
	  displayed only to verify that one-or-more observed values did
	  not produce something unexpected.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol">
	  <img src="imgs/edge.png" />
	</td>
	<td>
	  This is the relative free energy of the edge.
	  The decomposition of the energy into environmental components
	  is shown.
	  If time-series analysis was requested, then figures of
	  the forward/reverse and/or first/last-half analysis are provided.
	  The row labelled "Prod." is the analysis of the simulation
	  production regions.
	  Additional rows, produced by time-series analysis, are
	  the results upon discarding some percentage of each simulation
	  as equilibration.
	  If &part;dU/&part;&lambda; data was available for all trials,
	  then a table comparing the "Prod." values to the TI results
	  is provided.  It is assumed that the dvdl files
	  sample the same length of time as the efep files; however,
	  they may be sampled at different rates.
	  The TI results are analyzed upon excluding the same fraction
	  of the simulation as was used to analyze the "Prod." values.
	  <ul>
	    <li>
	      <b>&lt;f<sub>eq</sub>&gt;</b> is the average fraction of each
	      simulation discarded as equilibration
	    </li>
	    <li>
	      <b>&lt;g<sub>prod</sub>&gt;</b> is the average statistical
	      inefficiency of each simulation&apos;s analyzed
	      production region
	    </li>
	    <li>
	      <b>&Delta;G-MBAR</b> is the difference between the TI and MBAR
	      results
	    </li>
	    <li>
	      <b>T</b> is the
	      <a href="https://en.wikipedia.org/wiki/Welch%27s_t-test">
		Welch&apos;s t-statistic</a>, the absolute difference
	      divided by the standard error; it describes a difference in
	      units of standard error
	    </li>

	  </ul>
	</td>
      </tr>
      
      <tr>
	<td class="imgcol">
	  <img src="imgs/env.png" />
	</td>
	<td>
	  The summary of the environmental free energies and its decomposition
	  into stages is similar to the description provided above.
	  The time-series plots contain 3 lines. The green line is the
	  value of "Prod."; it is independent of the x-axis. It is
	  included to show the production result in comparison with the
	  other time-series values.  The shaded regions span a 95% confidence
	  interval (1.96&sigma;) above and below the mean.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol">
	  <img src="imgs/stage.png" />
	</td>
	<td>
	  This summarizes a stage free energy and its decomposition
	  into individual trials.
	  As an aside, note that the algorithm for detecting equilibration
	  and production
	  regions is not based on the time-series of free energies,
	  because the free energies are a collective property of
	  all states in a trial.
	  Instead, the algorithm is based on the potential energy
	  differences of a single trial, as discussed in more detail
	  below.
	  The forward potential energy difference is:
	  &Delta;U<sub>fwd</sub> = U({<b>r</b>}<sub>&lambda;</sub>;&lambda;<sub>i+1</sub>)-U({<b>r</b>}<sub>&lambda;</sub>;&lambda;<sub>i</sub>)
	  and the reverse energy difference is:
	  &Delta;U<sub>rev</sub> = U({<b>r</b>}<sub>&lambda;</sub>;&lambda;<sub>i</sub>)-U({<b>r</b>}<sub>&lambda;</sub>;&lambda;<sub>i-1</sub>),
	  where {<b>r</b>}<sub>&lambda;</sub> denotes the samples
	  obtained from simulation of state &lambda;.
	</td>
      </tr>
      
      <tr>
	<td class="imgcol">
	  <img src="imgs/trial.png" />
	</td>
	<td>
	  This is the free energy of a trial. It includes the free
	  energy of each state.
	  If TI analysis is available, a figure of the &part;U/&part;&lambda;
	  profile is shown. The dots are the means and the two curves are
	  the natural and clamped cubic splines.
	  The properties under "Prod. Region" are the result of the algorithm
	  for detecting equilibration and production regions.
	  The properties under "All data" are the analysis of the raw data.
	  The properties subscripted with "ana" correspond to the data
	  analyzed to produce the &Delta;G values.
	  If --no-auto was used, then the analysis uses "All Data",
	  otherwise the production region identified by the detection
	  algorithm is used.
	  <ul>
	    <li>
	      <b>f<sub>eq</sub></b>: The sampling fraction to be discarded as simulation, as determined by the detection algorithm. 
	    </li>
	    <li>
	      <b>N<sub>e</sub></b>: The number of samples to be discarded
	    </li>
	    <li>
	      <b>g<sub>prod</sub></b>: The statistical inefficiency of the production region upon discarding the unequilibrated samples
	    </li>
	    <li>
	      <b>g<sub>ana</sub></b>: The statistical inefficiency of the samples used in the calculation of &Delta;G
	    </li>
	    <li>
	      <b>N<sub>ana</sub></b>: The number of samples used in the calculation of &Delta;G
	    </li>
	    <li>
	      <b>g</b>: The statistical inefficiency of the available data
	    </li>
	    <li>
	      <b>N</b>: The number of available samples
	    </li>
	    <li>
	      <b>S</b>: The "<a href="#PhaseSpaceOverlap">phase space overlap</a>" between &lambda;<sub>i</sub> and   &lambda;<sub>i+1</sub>
	    </li>
	    <li>
	      <b>RE</b>: The "<a href="#ReweightingEntropy">reweighting entropy</a>" between adjacent windows. Fwd=&lambda;<sub>i</sub> and &lambda;<sub>i+1</sub>. Rev: &lambda;<sub>i</sub> and &lambda;<sub>i-1</sub>
	    </li>
	    <li>
	      <b>Conv</b>: Marked with "No" if the detection algorithm could not find a converged production region
	    </li>
	  </ul>
	</td>
      </tr>
      
      <tr>
	<td class="imgcol">
	  <img src="imgs/autoequil.png" />
	</td>
	<td>
	  If the detection algorithm finds that most of the simulation
	  should be discarded, then it will print
	  the following analysis, which is an explanation of why it
	  believes the simulation is not equilibrated.
	  To start, focus on the table.
	  There are 2 sections: &Delta;Ufwd and &Delta;Urev.
	  These are applications of the detection algorithm to 2 sets
	  of timeseries: the potential energy difference between neighbor
	  states in the forward and reverse direction.
	  <br>
	  &Delta;Ufwd = U(&lambda;<sub>i+1</sub>) - U(&lambda;<sub>i</sub>)
	  <br>
	  &Delta;Urev = U(&lambda;<sub>i-1</sub>) - U(&lambda;<sub>i</sub>)
	  <br>
	  The algorithm is applied to each timeseries to locate the
	  region that should be discarded as equilibration.
	  The amount to be discarded is the case which discards the most;
	  that is, the worse of the two cases.
	  
	  <ul>
	    <li>To begin, suppose we discard 0% of the samples as equilibration.
	      We will perform 3 tests.
	    </li>
	    <li>If any 2 of the 3 tests pass, then
	      we have identified the equilibration and production regions.
	    </li>
	    <li>
	      If 2 or more of the tests fail, then discard an additional 5%
	      of the samples and retry the tests.
	    </li>
	    <li>
	      Test 1: If the proposed production region is split in half,
	      what is the difference between the 2 means? Is the difference
	      less than 0.10 kcal/mol?  The &Delta; Column is the difference
	      between the 2 means. The cell is shaded green if it is less
	      than 0.10.
	    </li>
	    <li>
	      Test 2: If the proposed production region is split in half,
	      perform Welch&apos;s t-test on the means.
	      The null hypothesis is that the means are the same.
	      The test produces a p-value; if the p-value is less than
	      0.05, then we reject the null hypothesis.
	      If the p-value is greater than 0.05, then
	      the means might be the same (there is not enough evidence to
	      suggest otherwise).
	      The test passes if the p-value is larger than 0.05,
	      in which case the cell is shaded green.
	    </li>
	    <li>
	      Test 3: Perform a linear regression on the timeseries to
	      detect a drift (a nonzero slope).
	      A drift is detected via a two-tailed Wald Chi-Squared test.
	      This test produces a p-value; the null hypothesis is that
	      the slopes are the same.
	      If the p-value is smaller than 0.05, we reject the null
	      hypothesis.
	      If it is larger that 0.05, then there&apos;s not enough
	      evidence to conclude that the means are different
	      (the test passes and the cell is shaded green).
	    </li>
	    <li>
	      The N<sub>pass</sub> column counts the number of tests
	      that passed (the number of green cells).
	      N<sub>pass</sub> must be at least 2 in both the forward and
	      reverse directions for the algorithm to terminate
	      normally.
	    </li>
	  </ul>
	  The figures above the table
	  illustrate the &Delta;U values as a function of time,
	  and it also shows the first- and last-half means
	  as a fraction of the simulation is excluded as equilibration.
	</td>
      </tr>
      
    </table>

    <h3>
      Additional comments
    </h3>
    <ul>
      <li>
	When looking at plots of forward/reverse analysis,
	you would ideally observe the reverse time-series
	(the red line) plateu in the region f=0.0 to f=0.5.
	This would mean that the last fraction of samples
	yield consistent, stable values.
	If the reverse analysis is stable around f=0,
	but then trails off to another limiting value at
	f=1, then there may have been some transition
	and the beginning of the simulation likely
	isn&apos;t equilibrated.
      </li>
      <li>
	If the forward analysis displays a trend, or
	there is a big gap between the forward and
	reverse analysis near f=0,
	then the beginning of the simulation is likely
	not converged.
      </li>
      <li>
	The first- and last-half analysis would ideally
	overlap with one another. If there is a significant
	gap between the first and last halves, then
	perhaps you need more sampling, because it is
	unclear which half is necessarily better.
      </li>
      <li>
	<span id="PhaseSpaceOverlap"></span>
	The "phase space overlap" metric, S, shown in the
	tables is a number between 0 and 1.
	This index highly correlates with observed
	Hamiltonian replica exchange acceptance ratios.
	If the value is close to 0, then it is unlikely
	that exchanges will readily occur, and you
	should consider using a larger &lambda;-schedule.
	The metric is defined as follows.
	\[
	S_{ij} = \frac{O_{ij}}{\text{max}(O_{ii},O_{jj})}
	\]
	\[
	O_{ij}
	=
	\int
	\frac{
	e^{-\frac{1}{2} \left(\frac{x-\mu_i}{\sigma_i}\right)^2}
	}{\sigma_i\sqrt{2\pi}}
	\frac{
	e^{-\frac{1}{2} \left(\frac{x-\mu_j}{\sigma_j}\right)^2}
	}{\sigma_j\sqrt{2\pi}}
	dx
	\]
	&mu;<sub>k</sub> and &sigma;<sub>k</sub>; are the mean and standard deviation
	of the distribution of &Delta;U({<b>r</b>}<sub>&lambda;<sub>k</sub></sub>) =  U({<b>r</b>}<sub>&lambda;<sub>k</sub></sub>;&lambda;<sub>j</sub>) - U({<b>r</b>}<sub>&lambda;<sub>k</sub></sub>;&lambda;<sub>i</sub>), where {<b>r</b>}<sub>&lambda;<sub>k</sub></sub> are the samples drawn from simulation of state <i>k</i>.
      </li>
      <li>
	<span id="ReweightingEntropy"></span>
	The "reweighting entropy" (RE) is another measure
	of the phase space overlap between states.
	Its value varies from 0 (poor overlap) to 1 (excellent overlap).
	The "forward" entropy indicates if the ensemble of &lambda;<sub>i+1</sub>
	can be well-modelled by reweighting the samples of state &lambda;<sub>i</sub>.
	Similarly, the "reverse" entropy indicates if the samples from
	&lambda;<sub>i</sub> can model the ensemble of &lambda;<sub>i-1</sub>.
	The measure is an "entropy" in the sense that it is a property
	of the distribution of weights. If the weights were uniform, then
	the entropy is maximmized (RE=1), whereas if only a few samples
	contribute non-negligble weights, the RE approaches 0.

	\[
	\text{RE}_{ik} = -
	\frac{\sum_{j=1}^{N_i} w_{ik,j}\;\text{ln}\; w_{ik,j}}
	{\text{ln}\;N_i}
	\]
	\[
	w_{ik,j} = \frac{e^{-\beta(U(\mathbf{r}_{ij};\lambda_k) - U(\mathbf{r}_{ij};\lambda_i) )}}
	{\sum_{l=1}^{N_i} e^{-\beta(U(\mathbf{r}_{il};\lambda_k) - U(\mathbf{r}_{il};\lambda_i) )}}
	\]
	<b>r</b><sub>ij</sub> is sample <i>j</i> obtained from state &lambda;<sub>i</sub>,
	and N<sub>i</sub> is the number of samples from state &lambda;<sub>i</sub>.
      </li>
    </ul>
    
  </body>
</html>
