<html>
  <head>
    <style>
      ul {
          padding-left: 20px;
          list-style-position: outside;
      }
      li {
          padding-bottom: 0.5em;
      }
    </style>
  </head>
  <body>

    <h3>Analyzing the included example data</h3>

    <p>
      An example is provided within a tarball <a href="data.tar.gz">data.tar.gz</a>.
      To view the example, unpack the tarball by typing tar -xzf data.tar.gz.
      This will create a data/ subdirectory.
      In this directory, you will find
      <ul>
	<li>
	  a cdk2/ <a href="edgecalc.html#DirOrg">subdirectory,</a>
	</li>
	<li>
	  <a href="edgecalc.html#Discover">DiscoverEdges.py,</a>
	</li>
	<li>
	  a file of <a href="#Expt">experimental relative binding free energies</a>, ExptVals.txt,
	</li>
	<li>
	  and run_example.sh.
	</li>
      </ul>
      The cdk2 subdirectory contains the
      <a href="edgecalc.html#RawData">raw data files</a>
      for 3 edges of a ligand transformation network.
      To run the example, change to the data directory and
      type "bash run_example.sh".
      When it is finished, it will create a directory called "analysis".
      The analysis directory will contain
      <ul>
	<li>
	  the <a href="edgecalc.html#XML">XML input files</a>,
	</li>
	<li>
	  the <a href="edgecalc.html#CXX">python output files</a>,
	</li>
	<li>
	  the <a href="edgeana.html">HTML report of each edge</a>,
	</li>
	<li>
	  Graph.html, which summarizes the
	  <a href="graphana.html">networkwide free energy analysis</a>,
	</li>
	<li>
	  and GraphWithExpt.html, which is the exact same analysis,
	  but it includes comparisons with the <a href="#Expt">supplied reference values</a>.
	</li>
      </ul>
    </p>
    
    <p>
      This example should not be construed as a recommended simulation
      protocol.
      It was chosen as an example because it contains multiple trials
      of multiple stages in multiple environments, which produces an
      elaborate HTML report to be explained.
      Furthermore, there are many problems with these simulations.
      The HTML report will try to detect the problems and bring them
      to your attention.
      Finally, I did not want to include many megabytes of example
      data for you to download, so the sampling and number of trials
      have has been severely limited.
      The free energy results are "not good" -- and that's the main
      point, because we need to explain what is reported when the
      simulations are "not good".
    </p>

    <p>
      Let us discuss the run_example.sh script line-by-line.
      <ul>
	<li>If the script was run as an executable, then the
	  following lines tell the shell to interpret the
	  file as a series of bash commands.
	  "set -e" and "set -u" tell the bash interpreter to
	  exit if an error occurs or an undefined symbol is
	  used.
	  <pre><code>#!/bin/bash
set -e
set -u</code></pre>
	</li>
	<li>
	  This section runs the DiscoverEdges.py script, which
	  reads the directory structure and compares it to a template
	  string (defined within the DiscoverEdges.py script)
	  to generate an XML input file for each edge.
	  The XML input files are written to a ./analysis directory.
      <pre><code>echo ""
echo "Running: python3 DiscoverEdges.py"
python3 ./DiscoverEdges.py
echo "Finished DiscoverEdges.py"</pre></code>
	</li>
	<li>
	  The following snippet reads:
	  for each XML file within the analysis directory,
	  run edgembar to produce a python output file.
	  <pre><code>for xml in analysis/*.xml; do
    if [ -e "${xml}" ]; then
	echo ""
	echo "Running: time OMP_NUM_THREADS=4 edgembar_omp --halves --fwdrev ${xml}"
	time OMP_NUM_THREADS=4 edgembar_omp --halves --fwdrev ${xml}
	echo "Finished creating ${xml%.xml}.py"
    fi
done</pre></code>
	  The astrisks in "analysis/*.xml" is a wildcard that
	  matches any string. The value of the ${xml} variable
	  will by one of the file names that match the pattern.
	  If no files matched the pattern, then the value
	  of ${xml} will be the string "analysis/*.xml", where
	  the astrisks is literal rather than a wildcard.
	  Therefore, the if-statement <code>if [ -e "${xml}" ]; then</code>
	  asks the question: Does a file with this name exist?
	  (If there were no XML files, then the literal string
	  "analysis/*.xml" does not exist).
	  If the XML file exists, then edgembar is executed.
	  The output is stored in a python script with the same
	  name as the XML file, but with the ".xml" suffix
	  replaced by ".py".
	  <code>${xml%.xml}.py</code> is a string-substitution
	  that reads: if the last 4 characters of the ${xml}
	  variable are ".xml", then remove it and append
	  the variable with the 3 characters ".py".
	  <p>
	  In this example, the calculations are performed
	  with OpenMP parallelization using 4 threads,
	  and the options tell edgembar to perform
	  forward/reverse and first/last-half time-series
	  analysis.
	  The edgembar_omp executable is created if the
	  edgembar package was compiled from the standalone
	  FE-ToolKit package.
	  If you are using the version of FE-ToolKit provided
	  within the AmberTools software package, then you
	  should have invoked CMake with the -DOPENMP=TRUE
	  option to enable OpenMP compilation.
	  When compiled within AmberTools, the name of
	  the executable is edgembar.OMP rather than edgembar_omp,
	  to conform with the naming convention used within AmberTools.
	  </p>
	</li>
	<li>
	  The following lines of code execute each of the
	  output python scripts. This will create an
	  HTML file for each edge. The name of the
	  HTML file is the same as the python script;
	  however, the .py extension is replaced with .html.
	  <pre><code>for xml in analysis/*.xml; do
    py=${xml%.xml}.py
    if [ -e "${py}" ]; then
	echo ""
	echo "Running: python3 ${py}"
	python3 ${py}
	echo "Finished creating ${xml%.xml}.html"
    fi
done</pre></code>
	</li>
	<li>
	  The final commands run the edgembar-WriteGraphHtml.py
	  script to performed networkwide free energy
	  analysis.
	  The analysis reads the outputs of each edge
	  and writes the network analysis output as an
	  HTML file called analysis/Graph.html.
	  <pre><code>echo ""
echo "Running: edgembar-WriteGraphHtml.py -o analysis/Graph.html \$(ls analysis/*~*.py)"
edgembar-WriteGraphHtml.py -o analysis/Graph.html $(ls analysis/*~*.py)
echo "Finished creating analysis/Graph.html"</pre></code>
	</li>
	<li><span id="Expt"></span>
	  You may sometimes have reference/experimental
	  relative free energies that you would like to
	  compare against. If so, edgembar-WriteGraphHtml.py
	  has an optional argument "-x" that reads the
	  reference relative free energies from a file.
	  It will then include the reference free energies
	  within the generated HTML report.
	  The format of the file consists of two columns.
	  The first column is the name of the ligand
	  and the second column is the relative free energy
	  (in kcal/mol).
	  <pre><code>echo ""
echo "Running: edgembar-WriteGraphHtml.py -o analysis/GraphWithExpt.html -x ExptVals.txt \$(ls analysis/*~*.py)"
edgembar-WriteGraphHtml.py -o analysis/GraphWithExpt.html -x ExptVals.txt $(ls analysis/*~*.py)
echo "Finished creating analysis/GraphWithExpt.html"</code></pre>
	</li>
      </ul>
    </p>

    
  </body>
</html>
