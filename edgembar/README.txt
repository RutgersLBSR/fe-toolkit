INSTALLATION
------------

The configuration is performed with cmake. You will need cmake
(version 3.12 or later installed). The installation of the python
components is performed with pip.

To install cmake and update your version of pip, you can run:
   USERBASE=$(python3 -m site --user-base)
   USERSITE=$(python3 -m site --user-site)
   export PATH="${USERBASE}/bin:${PATH}"
   export PYTHONPATH="${USERSITE}:${PYTHONPATH}"
   python3 -m pip install pip --upgrade --user
   python3 -m pip install cmake --upgrade --user

You should consider adding the above export commands to your
${HOME}/.bashrc and then "source ~/.bashrc".

If, for whatever reason, pip is unavailable on your system, you
can install it using the directions here:
   https://pip.pypa.io/en/stable/installation/

To install edgembar,
   cd build
   bash ./run_cmake.sh
   make install VERBOSE=1 -j4
   cd ../local
   export PATH="${PWD}/bin:${PATH}"
   export PYTHONPATH="${PWD}/lib/python3.XX/site-packages:${PYTHONPATH}"
where python3.XX should be replaced by the appropriate python version.

Edgembar has dependencies on blas/lapack and nlopt.
The cmake configuration will check if these libraries are available.
If they are not available, it will automatically download them from
github and install them.

The blas/lapack libraries are installed from the openblas package.
   https://github.com/OpenMathLib/OpenBLAS
The nlopt software is used to perform nonlinear optimizations.
   https://github.com/stevengj/nlopt

These libraries could also be installed globally using your system's
package manager. For example, on Fedora, you could install them with:
   sudo dnf install NLopt-devel openblas

See the contents of build/run_cmake.sh to see how you can adjust
the compilers, compiler flags, python interpreter, and installation
directory.



