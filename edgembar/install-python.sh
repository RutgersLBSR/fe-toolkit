#!/bin/bash
set -e
set -u

PREFIX=${PWD}/local

PYTHONUSERBASE=${PREFIX} python3 -m pip install --prefix=${PREFIX} src/python

