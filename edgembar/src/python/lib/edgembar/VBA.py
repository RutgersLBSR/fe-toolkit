#!/usr/bin/env python3
import numpy as np
import math
from pathlib import Path
from . Trial import Trial
import yaml

def GetBoreschRstData( self: Trial) -> float:

    fname = Path(self.datadir) / "boresch_vba_rst.yaml"
    data = None

    dAr = None
    kb = 0.0019872 # kcal/mol/K
    T = 298 # K
    pi = math.pi
    V0 = 1660 # Angstroms^3 - Standard State volume for 1M

    k_rst=[]
    eq0_rst=[]

    if fname.is_file():
        with open(fname,"r") as fh:
            data = yaml.safe_load(fh)

        keys = ['Angle','Bond','Dihedral']

        for key in keys:
            if key == 'Angle':
                eq = data[key]['r2']
                for val in eq:
                    eq0_rst.append(np.sin(val*pi/180.0))
                k = data[key]['rk2']
                for val in k:
                    k_rst.append(val*(pi/180)**2)
            elif key == 'Dihedral':
                k = data[key]['rk2']
                for val in k:
                    k_rst.append(val*(pi/180)**2)
            elif key == 'Bond':
                eq = data[key]['r2']
                for val in eq:
                    eq0_rst.append(val**2)
                k = data[key]['rk2']
                for val in k:
                    k_rst.append(val)

    kk = np.prod(k_rst)
    rr = np.prod(eq0_rst)

    dAr = -kb*T*np.log(
    ((8 * pi**2 * V0)/rr) *
    ((kk**0.5)/((pi*kb*T)**3))
    )

    return dAr

