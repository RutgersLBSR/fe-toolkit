#!/usr/bin/env python3
import sys,os
import re
import numpy as np
import yaml

import argparse
from pathlib import Path


class SanderOpts(object):
    
    def __init__(self):
        from collections import defaultdict as ddict
        self.option = ddict(int)

        self.keys_int = ["nstlim","irest","icfe","ntpr",
                         "bar_intervall","ijarzynski"]
        
        self.keys_float = ["dt","clambda","bar_l_min",
                           "bar_l_max","bar_l_incr"]

        
    def process_line(self,line):
        cmdstr,sepstr,comstr = line.partition("!")
        cmdstr = cmdstr.strip()
        for key in self.keys_int:
            self.find_int(key,cmdstr)
        for key in self.keys_float:
            self.find_float(key,cmdstr)
            
            
    def find_int(self,key,line):
        if key in line:
            cols = line.replace("=","").replace(",","").strip().split()
            for icol in range(len(cols)-1):
                if cols[icol] == key:
                    self.option[key] = int( cols[icol+1] )
                    return True

        return False

    def find_float(self,key,line):
        if key in line:
            cols = line.replace("=","").replace(",","").strip().split()
            for icol in range(len(cols)-1):
                if cols[icol] == key:
                    #print(key,icol,cols)
                    try:
                        self.option[key] = float( cols[icol+1] )
                        return True
                    except:
                        pass

        return False



class SimData(object):
    def __init__(self,fname):
        self.fname=fname
        self.clambda,self.times,self.dvdls,self.workvals = extract_data(fname)

    def issame(self,other):
        res = False
        if self.clambda == other.clambda:
            if self.dvdls is not None and other.dvdls is not None:
                res=True
            elif self.workvals is not None and other.workvals is not None:
                res=True
        return res
        
    def append(self,other):
        t = self.times[-1]
        if self.dvdls is not None and other.dvdls is not None:
            self.dvdls.extend( other.dvdls )
            ts = [ t+x for x in other.times ]
            self.times.extend(ts)
        elif self.workvals is not None and other.workvals is not None:
            self.workvals.extend( other.workvals )
            ts = [ t+x for x in other.times ]
            self.times.extend(ts)
        else:
            raise Exception("Cannot combine equilibrium and nonequilibrium simulation data: {self.fname, other.fname}")
        

def extract_data( fname ):
    import os
    from collections import defaultdict as ddict

    fh = open(fname,"r")
    if not fh:
        raise Exception("Could not open %s\n"%(fname))

    sander = SanderOpts()
    for line in fh:
        if "4.  RESULTS" in line:
            break
        else:
            sander.process_line(line)


    clambda = 0
    times = []
    dvdls = None
    workvals = None
        
    fh = open(fname,"r")
    
    if sander.option["ijarzynski"] > 0:
        workvals = []
        dt = sander.option["dt"] * sander.option["nstlim"]
        if dt <= 0:
            dt = 1.
        curtime = 0
        for line in fh:
            if "Final work value:" in line:
                e = float(line.strip().split()[-1])
                curtime += dt
                times.append(curtime)
                workvals.append(e)
    else:
        clambda = sander.option["clambda"]
        dt = sander.option["dt"] * sander.option["ntpr"]
        if dt <= 0:
            dt = 1.
        curtime = 0
        dvdls = []
        reading_summary = False
        reading_first_step = False
        for line in fh:
                
            if "A V E R A G E S" in line \
               or "AVERAGES OVER" in line \
               or "R M S  F L U C T U A T I O N S" in line:
                reading_summary = True  
            elif "---------" in line:
                reading_summary = False
            
            if "NSTEP =        0" in line:
                reading_first_step = True
            elif "NSTEP =" in line:
                reading_first_step = False

            should_read_dvdl = (not reading_summary) and \
                (not reading_first_step)

            if should_read_dvdl:
                if "DV/DL  =" in line:
                    cols = line.strip().split()
                    dvdl = float( cols[-1] )
                    curtime += dt
                    times.append(curtime)
                    dvdls.append(dvdl)

    return clambda,times,dvdls,workvals




if __name__ == "__main__":

    import glob

    parser = argparse.ArgumentParser \
        ( formatter_class=argparse.RawTextHelpFormatter,
          description="""
          Extracts DVDL and MBAR data from 1-or-more mdout files and writes
          the data into timeseries files
          """)

    parser.add_argument \
        ("-o","--odir",
         help="Output directory. If not specified, then it is ./",
         type=str,
         required=True,
         default="." )
    

    parser.add_argument \
        ('mdout',
         metavar='mdout',
         type=str,
         nargs='*',
         help='Amber mdout file')
    
    
    try:
        import pkg_resources
        version = pkg_resources.require("edgembar")[0].version
    except:
        version = "unknown"
    
    parser.add_argument('--version', action='version',
                        version='%(prog)s {version}'.format\
                        (version=version))


    args = parser.parse_args()

    mdouts = []
    for arg in args.mdout:
        fs = glob.glob(arg)
        if len(fs) == 0:
            sys.stderr.write(f"Input filename pattern did not match any files: {arg}\n")
            continue
        mdouts.extend( fs )
    
    if len(mdouts) == 0:
        raise Exception("Failed to read list of mdout files")
        
    sims = []
    
    for arg in mdouts:
        if os.path.isfile( arg ):
            if ".mdout" in arg or ".out" in arg:
                data = SimData(arg)
                for isim in range(len(sims)):
                    if sims[isim].issame(data):
                        sims[isim].append(data)
                        data = None
                        break
                if data is not None:
                    sims.append(data)
            else:
                sys.stderr.write("File ignored: {arg}")
        else:
            sys.stderr.write("File not found: {arg}")
            

    sims.sort(key=lambda x: x.clambda)
    lams = [x.clambda for x in sims]

    if len(lams) == 0:
        raise Exception("Failed to read simulation data")
    
    if sims[0].workvals is not None:
        if len(sims) > 1:
            raise Exception("The data appears to mix equilibrium and nonequilibrium simulations")
        
        fh = open( Path(args.odir) / ("efep_%.8f_%.8f.dat"%(0,0)), "w" )
        for t in sims[0].times:
            fh.write("%10.4f %22.13e\n"%(t,0))
        fh = open( Path(args.odir) / ("efep_%.8f_%.8f.dat"%(0,1)), "w" )
        for t,e in zip(sims[0].times,sims[0].workvals):
            fh.write("%10.4f %22.13e\n"%(t,e))
    else:
        elams = [ x for x in lams ]
        if "%.8f"%(elams[0]) != "%.8f"%(0):
            elams.insert(0,0)
        if "%.8f"%(elams[-1]) != "%.8f"%(1):
            elams.append(1)
            
        for ilam,tlam in enumerate(lams):
            sim = sims[ilam]

            fh = open( Path(args.odir) / ("dvdl_%.8f.dat"%(tlam)), "w" )
            for t,e in zip(sim.times,sim.dvdls):
                fh.write("%10.4f %22.13e\n"%(t,e))
                
            for elam in elams:
                fh = open( Path(args.odir) / ("efep_%.8f_%.8f.dat"%(tlam,elam)), "w" )
                for t,e in zip(sim.times,sim.dvdls):
                    fh.write("%10.4f %22.13e\n"%(t,elam*e))
                    
        
            
            
